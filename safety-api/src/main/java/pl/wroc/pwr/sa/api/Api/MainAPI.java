package pl.wroc.pwr.sa.api.Api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.wroc.pwr.sa.et.Consequence;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.et.ScenarioProbabilityCounter;
import pl.wroc.pwr.sa.fr.FuzzyLogicFacade;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.ft.Fault;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.ft.FaultTreeOperations;
import pl.wroc.pwr.sa.ft.TopProbability;
import pl.wroc.pwr.sa.xml.XMLService;

/**
 * Created by PiroACC on 2015-06-05.
 */
public class MainAPI {

    /**
     * Reads Event Tree from XML file.
     *
     * @param path Path to XML file to be loaded.
     * @return EventTree object if tree is loaded correctly, null otherwise.
     */
    public EventTree readEventTreeFromFile(String path) {
        EventTree eventTreeToReturn = null;
        try {
            eventTreeToReturn = EventTree.loadFromXML(path);
        } catch (Exception ex) {
            Logger.getLogger(MainAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (eventTreeToReturn != null) {
            return eventTreeToReturn;
        } else {
            return null;
        }
    }

    /**
     * Counts probability for scenarios in Events Tree.
     *
     * @param options     Comma separated scenario indexes.
     * @param eventsTree EventTree object to analyse.
     * @return MembershipFunction object for EventTree scenario counter, null otherwise.
     */
    public MembershipFunction eventTreeScenarioCounter(String options, EventTree eventsTree) {
        int indexes[] = convertsStringToIntegers(options);
        MembershipFunction mf = null;
        if (eventsTree != null) {
            if (indexes.length == 1)
                try {
                    mf = ScenarioProbabilityCounter.countProbalityOfScenario(eventsTree.getLeafList().get(indexes[0]));
                } catch (Exception ex) {
                    Logger.getLogger(MainAPI.class.getName()).log(Level.SEVERE, null, ex);
                }
            else {
                try {
                    List<MembershipFunction> probab = new ArrayList<>();
                    for (int scenario : indexes) {
                        mf = ScenarioProbabilityCounter.countProbalityOfScenario(eventsTree.getLeafList().get(indexes[scenario]));
                        probab.add(mf);
                    }
                    MembershipFunction mf2 = sumMembershipFunctionsOfScenarios(probab);
                    mf = mf2;
                } catch (Exception ex) {
                    Logger.getLogger(MainAPI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return mf;
        } else {
            return null;
        }
    }

    private int[] convertsStringToIntegers(String scenarios){
        String splitStr[] = scenarios.split(",");
        int indexes[] = new int[splitStr.length];
        int i = 0;
        for (String a : splitStr) {
            indexes[i++] = Integer.parseInt(a);
        }
        return indexes;
    }

    private MembershipFunction sumMembershipFunctionsOfScenarios(List<MembershipFunction> scenariosProbabilities) throws Exception {
        FuzzyLogicFacade fuzzyLogicFacade = new FuzzyLogicFacade();
        MembershipFunction memebershipFunctionAcumulator = scenariosProbabilities.get(0);
        for (int i = 1; i < scenariosProbabilities.size(); i++) {
            memebershipFunctionAcumulator = fuzzyLogicFacade.add(memebershipFunctionAcumulator, scenariosProbabilities.get(i));
        }
        return memebershipFunctionAcumulator;
    }

    /**
     * Reads Fault Tree from XML file.
     *
     * @param path Path to XML file to be loaded.
     * @return FaultTree object if tree is loaded correctly, null otherwise.
     */
    public FaultTree readFaultTreeFromFile(String path) {
        FaultTree faultTree = null;
        try {
            faultTree = XMLService.readFileToObject(FaultTree.class, path);
        } catch (Exception ex) {
            Logger.getLogger(MainAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (faultTree != null) {
            return faultTree;
        } else {
            return null;
        }
    }

    /**
     * Checks The Cohesion Of The Event Tree.
     *
     * @param eventsTree EventTree object to analyse.
     * @return true if cohesion is fullfilled, false otherwise.
     */
    public boolean checkTheCohesionOfTheTree(EventTree eventsTree) {
        if (eventsTree != null) {
            try {
                eventsTree.checkTheCohesionOfTheTree();
            } catch (Exception ex) {
                Logger.getLogger(MainAPI.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets Fuzzy Importance Index of Fault Tree.
     *
     * @param faultTree object to analyse.
     * @return map of fuzzy importance index, where Fault is key and MembershipFunction is a value, null otherwise.
     */
    public Map<Fault, MembershipFunction> getFuzzyImportanceIndex(FaultTree faultTree) {
        if (faultTree != null) {
            Map<Fault, MembershipFunction> map = null;
            FaultTreeOperations operations = new FaultTreeOperations(faultTree);
            try {
                map = operations.getFuzzyImportanceIndex();
            } catch (Exception ex) {
                Logger.getLogger(MainAPI.class.getName()).log(Level.SEVERE, null, ex);
            }
            return map;
        } else {
            return null;
        }
    }

    /**
     * Analyze Fault Tree and show results.
     *
     * @param faultTree object to analyse.
     */
    public Map<String, TopProbability> analyseFaultTree(FaultTree faultTree) {
        if (faultTree != null) {
            System.out.print("\nResult of the analysis Fault Tree: ");
            Map<String, TopProbability> map = null;
            FaultTreeOperations operations = new FaultTreeOperations(faultTree);
            try {
                map = operations.analysisTree();
            } catch (Exception ex) {
                Logger.getLogger(MainAPI.class.getName()).log(Level.SEVERE, null, ex);
            }
            return map;
        } else {
            return null;
        }
    }
}