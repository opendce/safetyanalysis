<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>pl.wroc.pwr</groupId>
    <artifactId>SafetyAnalysis</artifactId>
    <version>0.3-SNAPSHOT</version>
    <packaging>pom</packaging>

    <url>https://opendce.atlassian.net/wiki/display/SA</url>
    <name>SafetyAnalysis</name>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>https://www.apache.org/licenses/LICENSE-2.0</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>Marian Jureczko</name>
        </developer>
        <developer>
            <name>Patryk Pluskota</name>
        </developer>
        <developer>
            <name>Lukasz Fafara</name>
        </developer>
        <developer>
            <name>Cezary Kobiela</name>
        </developer>
        <developer>
            <name>Marcin Całkowski</name>
        </developer>
        <developer>
            <name>Alexander Stolar</name>
        </developer>
        <developer>
            <name>Tomasz Czart</name>
        </developer>
        <developer>
            <name>Andrzej Likonski</name>
        </developer>
        <developer>
            <name>Dawid Bigaj</name>
        </developer>
        <developer>
            <name>Bogna Manowska</name>
        </developer>
        <developer>
            <name>Jakub Sztukiecki</name>
        </developer>
        <developer>
            <name>Paweł Idziak</name>
        </developer>
    </developers>

    <modules>
        <module>safety-gui</module>
        <module>safety-eventstree</module>
        <module>safety-faulttree</module>
        <module>safety-fuzzyreasoning</module>
        <module>safety-model</module>
        <module>safety-report</module>
        <module>safety-web</module>
        <module>safety-sonar</module>
        <module>safety-api</module>
    </modules>

    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.16.12</version>
        </dependency>
        <dependency>
            <groupId>org.easymock</groupId>
            <artifactId>easymock</artifactId>
            <version>3.4</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-java</artifactId>
            <version>${cucumber.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-junit</artifactId>
            <version>${cucumber.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>4.12</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.pitest</groupId>
                <artifactId>pitest-maven</artifactId>
                <configuration>
                    <targetClasses>
                        <param>pl.wroc.pwr.sa.*</param>
                    </targetClasses>
                    <targetTests>
                        <param>pl.wroc.pwr.sa.*</param>
                    </targetTests>
                    <outputFormats>
                        <outputFormat>HTML</outputFormat>
                        <outputFormat>XML</outputFormat>
                    </outputFormats>
                    <excludedClasses>
                        **/*AT.java
                        **/*IT.java
                    </excludedClasses>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <executions>
                    <execution>
                        <id>cucumber tests</id>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                        <phase>verify</phase>
                        <configuration>
                            <skip>false</skip>
                            <excludes>
                                <exclude>**/*IT*</exclude>
                            </excludes>
                            <includes>
                                <include>**/*AT*</include>
                            </includes>
                        </configuration>
                    </execution>
                    <execution>
                        <id>component tests</id>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                        <phase>verify</phase>
                        <configuration>
                            <skip>false</skip>
                            <excludes>
                                <exclude>**/*AT*</exclude>
                            </excludes>
                            <includes>
                                <include>**/*IT*</include>
                            </includes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.6.0</version>
                    <configuration>
                        <source>1.8</source>
                        <target>1.8</target>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.pitest</groupId>
                    <artifactId>pitest-maven</artifactId>
                    <version>1.2.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <version>2.20</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <sonar.host.url>http://snow.iiar.pwr.wroc.pl:8081</sonar.host.url>
        <cucumber.version>1.1.8</cucumber.version>
    </properties>
</project>
