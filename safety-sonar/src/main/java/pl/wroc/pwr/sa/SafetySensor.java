/*
 * SonarQube Issue Assign Plugin
 * Copyright (C) 2014 SonarSource
 * dev@sonar.codehaus.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */

package pl.wroc.pwr.sa;


import org.sonar.api.batch.Sensor;
import org.sonar.api.batch.SensorContext;
import org.sonar.api.measures.Measure;
import org.sonar.api.issue.Issue;
import org.sonar.api.resources.Project;
import org.sonar.api.issue.ProjectIssues;
import org.sonar.api.config.Settings;
import ft.*;
import sa.fr.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Insajdo on 2015-05-20.
 */
public class SafetySensor implements Sensor {
    //private static final Logger LOG = LoggerFactory.getLogger(SafetySensor.class);
    private Settings settings;
    private final ProjectIssues projectIssues;
    /**
     * Use of IoC to get Settings
     */
    public SafetySensor(Settings settings, ProjectIssues projectIssues) {
        this.settings = settings;
        this.projectIssues = projectIssues;
    }

    public boolean shouldExecuteOnProject(Project project) {
        return true;
    }

    public void analyse(Project project, SensorContext sensorContext){
        int blockerIssues = 0;
        int allIssues=0;
        String prob = null;
        for (Issue issue : projectIssues.issues()) {
            allIssues++;
            if(issue.severity().equals("BLOCKER")) {
                blockerIssues++;
            }
        }

        try {
            prob = faultTreeResultComparison(blockerIssues/allIssues);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Measure measure = new Measure(SafetyMetrics.BLOCKER_ISSUES);
            String text23 = "To jest tekst z funkcji analyse z sensora all: " + allIssues + " blocker: " +blockerIssues + " = " + blockerIssues/allIssues + " "+ prob;
            measure.setData(text23);
            sensorContext.saveMeasure(measure);
    }

    public String faultTreeResultComparison (double issues)throws Exception{
        FaultTree faultTree = new FaultTree();

        FuzzyLogicFacade flf = new FuzzyLogicFacade();
        flf.createTriangle(new ParamTriangle(0.0,0.5,0.9, Scale.LINEAR ), "wzor");
        flf.createTriangle(new ParamTriangle(issues,issues+0.5,issues+0.9, Scale.LINEAR ), "is");

        faultTree.setScale(flf);
        List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n1 =  scale.get(0);
        TriangleFunction n2 = scale.get(1);

        Fault out = faultTree.addFault("out");
        Fault f1 = faultTree.addFault("fault1");
        Fault f2 = faultTree.addFault("fault2");

        f1.setProbabilty(n1);
        f2.setProbabilty(n2);

        LogicGate g0 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);

        faultTree.addConnection(f1, g0);
        faultTree.addConnection(f2, g0);
        faultTree.addConnection(g0, out);

        FaultTreeOperations faultTreeOperations = new FaultTreeOperations(faultTree);
        MembershipFunction prob1, prob2;

        out.delete();
        prob1 = (faultTreeOperations.getProbability(out));
        faultTree.addConnection(g0, out);

        //f1.delete();
        prob2= ( faultTreeOperations.getProbability(f2));
        //faultTree.addConnection(g0, f1);


        return prob1.toString() + " " + prob2.toString();
    }

    @Override
    public String toString() {

        return getClass().getSimpleName();
    }
}
