package ft;

import sa.fr.MembershipFunction;
import sa.fr.TrapezoidFunction;
import sa.fr.TriangleFunction;

import javax.xml.bind.annotation.*;

/**
 *
 * @author n0npax
 */
@XmlRootElement(namespace = "pl.wroc.pw.sa.ft.Fault")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class Fault {
	private String name;
	private String description;
        private LogicGate parent = null;
        @XmlTransient
	private LogicGate child = null;

        @XmlElements({
            @XmlElement(name="trapezoid",type=TrapezoidFunction.class),
            @XmlElement(name="triangle",type=TriangleFunction.class)
        })
        private MembershipFunction probability;


        @XmlTransient
	private FaultTree faultTree = null;

    /**
     *
     * @param name
     */
    public Fault(String name) {
		this.name = name;
	}

    public Fault() {
	}

    /**
     *
     * @param name
     */
    public void setName(String name) {
		this.name = name;
	}

    /**
     *
     * @return
     */
    public String getName() {
		return name;
	}

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
		this.description = description;
	}

    /**
     *
     * @return
     */
    public String getDescription() {
		return this.description;
	}

    /**
     *
     * @param parent
     */
    public void setParent(LogicGate parent) {
		this.parent = parent;
	}



    /**
     *
     * @param child
     */
    public void setChild(LogicGate child) {
		this.child = child;
	}

    /**
     *
     * @return
     */
    @XmlTransient
    public LogicGate getChild() {
		return child;
	}

    /**
     *
     * @return
     */
    @XmlTransient
    public LogicGate getParent() {
		return parent;
	}

    /**
     *
     * @param probability
     */
    public MembershipFunction getProbability() {
        return this.probability;
    }

    public void setProbabilty(MembershipFunction probability) {
        this.probability = probability;
    }
    /**
     *
     * @param faultTree
     */
    public void setFaultTree(FaultTree faultTree) {
		this.faultTree = faultTree;
	}

    /**
     *
     * @return
     */
    public FaultTree getFaultTree() {
		return this.faultTree;
	}

    /**
     *
     */
    public void delete() {
		if (this.parent != null)
			this.parent.getChildren().remove(this);

		if (this.child != null)
			this.child.setParent(null);

		this.parent = null;
		this.child = null;
        if(this.faultTree != null) {
            this.faultTree.getFaultMap().remove(this.name);
        }
	}


}
