package ft;

import sa.fr.FuzzyLogicFacade;
import sa.fr.MembershipFunction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author n0npax
 */

public class FaultTreeOperations {

    private FaultTree tree;
    private Fault invisibleFauoltFII=null;

    /**
     *
     * @param f
     */
    public FaultTreeOperations(FaultTree f) {
        tree = f;
    }



    /**
     *
     * @param rewrite
     * @return
     * @throws Exception
     */
        public TopProbability getTopProbability(boolean rewrite) throws Exception {
    	    //parametr rewrite mowi o tym czy mamy napisac w naszym drzewie pole topProbability
            //uzywany jest podczas analysisTree
    	TopProbability top = new TopProbability();
    	top.topMembershipFunction = getProbability(tree.getRoot());
    	top.setTopSimilarityValues(tree.getScale());
    	if(rewrite)
    		tree.setTopProbability(top);
    	return top;
    }
    /**
     * Oblicza prawdopodobieństwo dla zadanego elementu.
     * Jeśli argumentem jest root, zwrac prawdopodobieństwo dla zdarzenia szczytowego.
     * @param f
     * @return MembershipFunction dla zadanego fulta
     * @throws Exception
     */
    public MembershipFunction getProbability(Fault f) throws Exception {
        LogicGate logicGate = f.getChild();
        MembershipFunction probability = f.getProbability();

        if (logicGate != null) {
            List<Fault> children = logicGate.getChildren();
            int size = children.size();

            if(size==1 && children.get(0)==invisibleFauoltFII)  //tylko ukryty fault
            {
                return probability; //gdy jest tylko jeden potomek i jest ukryty
            }

            if (size > 0) {
                MembershipFunction ret = getProbabilityHelper(children, logicGate.GetType(), size);
                f.setProbabilty(ret);
                return ret;
            }
        }
        return probability;

    }

    private MembershipFunction getProbabilityHelper(List<Fault> children, LogicGate.LogicGateType type, int size) throws Exception {

        FuzzyLogicFacade flf = new FuzzyLogicFacade();
        MembershipFunction ret = getProbability(children.get(0));
            for (int i = 0; i < size; i++)
            {
                ret = getProbability(children.get(0));
                if (children.get(0)!=invisibleFauoltFII)    //pierwsze dziecko to nie moze byc ukryty lisc.
                {
                    break;  //ukryty lisc nie jest pierwszy
                }
                else
                {
                    Fault f = children.get(0);  //zamiana kolejnosci dzieci
                    children.add(f);
                    children.remove(0);
                }
            }
            for (int i = 1; i < size; i++) {
                Fault f = children.get(i);
                if(f==invisibleFauoltFII) //czy dany Fault jest brany pod uwagę przy liczeniu topProbability
                {
                    continue;
                }
                if (type == LogicGate.LogicGateType.GATE_AND) {
                    ret = flf.multiply(ret,
                            getProbability(f));
                } else if (type == LogicGate.LogicGateType.GATE_OR) {
                    ret = flf.add(ret,
                            getProbability(f));
                }
            }
            return ret;
    }

    /**
     *
     * @return
     * @throws Exception
     */
    public Map<String, TopProbability> analysisTree() throws Exception {

            tree.updateLeafsList();

            List<Fault> leafs = tree.getLeafsList();

		if(tree.getTopProbability() == null)
			this.getTopProbability(true);

		Map<String, TopProbability> ret = new HashMap<String, TopProbability>();

		int size = leafs.size();
		for(int i=0; i<size; i++){
			Fault f = leafs.get(i);
			LogicGate parent = f.getParent();
			f.delete();
			ret.put(f.getName(), getTopProbability(false));
            if(parent != null  && f != null) {
                this.tree.addConnection(parent, f);
            }
		}
		return ret;
	}

    /**
     * FuzyImportanceIndex to lista różnic (p-p_{i})
     * p_{i} to górne prawdopodobieństwo zdarzenia bez uwzględnienia itego elementu
     * p to górne prawdopodobieństwo zdarzenia
     * @return HashMap z prawdopodobieństwem różnic p-p_{i} i odpowiednim Fault'em
     * @throws Exception
     */
    public Map<Fault, MembershipFunction> getFuzzyImportanceIndex() throws Exception
    {
        FuzzyLogicFacade flf =new FuzzyLogicFacade();
            tree.updateLeafsList();
            MembershipFunction top = getProbability(tree.getRoot());  // topPtobability p
            Map<Fault, MembershipFunction> fuzzyImportanceIndexLst = new HashMap<Fault, MembershipFunction>(); //mapa topProbability p_{i}, i
            for(Fault f : tree.getLeafsList())
            {
               invisibleFauoltFII=f;     //ucinam kolejno liść i obliczam topProbablity dla drzewa bez tego liscia.
                MembershipFunction mf = getProbability(tree.getRoot());
                fuzzyImportanceIndexLst.put(f, flf.subtract(top,mf));  //element p-p_{i} DOCELOWA WERSJA
                //fuzzyImportanceIndexLst.put(f, mf);  //element p_{i} zamiast p-p_{i} Póki jest problem z odejmowaniem
            }
            invisibleFauoltFII=null;    //odnowienie znacznika domyślnego obliczania całego drzewa.
            return fuzzyImportanceIndexLst;
    }
}