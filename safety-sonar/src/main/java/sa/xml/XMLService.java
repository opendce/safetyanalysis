/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sa.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.*;

/**
 *
 * @author Michal
 */
public class XMLService {
    /**
     * XmlService accepts path to XML file as input and returns a string with file contet
     * @param path ścieżka do pliku
     * @return
     * Readed file contents.
     * @throws Exception
     */
    protected static String readFileToString(String path) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(path));

        StringBuilder stringBuilder = new StringBuilder();
        String line;

        while((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line).append("\n");
        }
        bufferedReader.close();
        return stringBuilder.toString();
    }

    /**
     * XmlService accepts path to XML file as input and returns a domain object
     * (e.g. MembershipFunction) representing the file contet
     * @param oClass klasa, której objekt jest wczytywany
     * @param path ścieżka do pliku
     * @return
     * Readed object.
     * @throws Exception
     */
    public static <T> T readFileToObject(Class<T> oClass, String path) throws Exception {
        String xmlString = XMLService.readFileToString(path);
        return (T)JAXBContext.newInstance(oClass)
                                .createUnmarshaller()
                                .unmarshal(new StringReader(xmlString));
    }
    /**
     * XmlService accepts text XML representation as input and returns a domain object
     * (e.g. MembershipFunction) representing the text contet
     * @param oClass klasa, której objekt jest wczytywany
     * @param xml reprezentacja xml-owa obiektu
     * @return
     * Readed object.
     * @throws Exception
     */
    public static <T> T readXMLStringToObject(Class<T> oClass, String xml) throws Exception {
        return (T)JAXBContext.newInstance(oClass)
                            .createUnmarshaller()
                            .unmarshal(new StringReader(xml));
    }
    
    /**
     * XmlService accepts a domain object as input
     * and returns a string with its XML representation
     * @param o Object to parse to xml
     * @return
     * Readed file contents.
     * @throws Exception
     */
    public static String saveObjectToXMLString(Object o) throws Exception {
        final Marshaller marshaller = JAXBContext.newInstance(o.getClass())
                                        .createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        final StringWriter stringWriter = new StringWriter();
        marshaller.marshal(o, stringWriter);
        return stringWriter.toString();
    }
    
    /**
     * XmlService accepts a domain object and path to file as input
     * and saves domain object XML representation in the specified path
     * @param o Object to parse to xml
     * @param path ścieżka do pliku
     * @throws Exception
     */
    public static void saveObjectToFile(Object o, String path) throws Exception {
        File xmlFile = new File(path);

        JAXBContext context = JAXBContext.newInstance(o.getClass());
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(o, new FileOutputStream(xmlFile));
    }
}
