package sa.fr;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author N3
 */
@XmlRootElement(name = "triangleLingSymmetric")
public class TriangleLingSymmetric implements LingSymmetric {

	private String name;
	private int partition;
	private List<TriangleFunction> functions;
        
        public TriangleLingSymmetric() {
        }

	public TriangleLingSymmetric(String name, Scale scale, List<String> names) throws Exception {
		int i;
		double r;
		String n;
		double param1, param2, param3;
		this.name = name;
		this.partition = names.size() - 1;
		this.functions = new ArrayList<TriangleFunction>();
		if (scale == Scale.LINEAR) {
			if (partition > 1) {
				r = 1.0 / partition;
				n = names.get(0);
				param1 = 0;
				param2 = 0;
				param3 = r;
				ParamTriangle param = new ParamTriangle(param1, param2, param3, scale);
				this.functions.add(new TriangleFunction(param, n));
				for (i = 1; i < partition; i++) {
					n = names.get(i);
					param2 = param1 + r;
					param3 = param2 + r;
					param = new ParamTriangle(param1, param2, param3, scale);
					this.functions.add(new TriangleFunction(param, n));
					param1 = param2;
				}
				n = names.get(i);
				param2 = param3;
				param = new ParamTriangle(param1, param2, param3, scale);
				this.functions.add(new TriangleFunction(param, n));
			} else {
				throw new Exception("wrong number of names");
			}
		} else {
			if (partition > 1) {
				r = 12.0 / partition;
				n = names.get(0);
				param1 = -12;
				param2 = -12;
				param3 = -12 + r;
				ParamTriangle param = new ParamTriangle(param1, param2, param3, scale);
				this.functions.add(new TriangleFunction(param, n));
				for (i = 1; i < partition; i++) {
					n = names.get(i);
					param2 = param1 + r;
					param3 = param2 + r;
					param = new ParamTriangle(param1, param2, param3, scale);
					this.functions.add(new TriangleFunction(param, n));
					param1 = param2;
				}
				n = names.get(i);
				param2 = param3;
				param = new ParamTriangle(param1, param2, param3, scale);
				this.functions.add(new TriangleFunction(param, n));
			} else {
				throw new Exception("wrong number of names");
			}
		}
	}

	public int getPartition() {
		return partition;
	}

	public void setPartition(int partition) {
		this.partition = partition;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<String> getFunctionsNames() {
		List<String> names = new ArrayList<String>();
		for (TriangleFunction function : functions) {
			names.add(function.getName());
		}
		return names;
	}
	
	public List<TriangleFunction> getFunctions() {
		return functions;
	}
	
	public void setFunctions(List<TriangleFunction> functions) {
		this.functions = functions;
	}

	public TriangleFunction getFunction(String name) {
		for (TriangleFunction function : functions) {
			if (function.getName().equals(name)) {
				return function;
			}
		}
		return null;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		final TriangleLingSymmetric l = (TriangleLingSymmetric) obj;
		if (!l.getName().equals(name) || partition != l.getPartition() || !functions.equals(l.functions))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 41 * hash + (this.name != null ? this.name.hashCode() : 0);
		hash = 41 * hash + this.partition;
		return hash;
	}
}