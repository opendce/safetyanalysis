package sa.fr;

/**
 *
 * @author Maciej
 */
public class FuzzyOperations {
	
	public MembershipFunction add (MembershipFunction a, MembershipFunction b) throws Exception {
		return a.add(b);
	}
	
	public MembershipFunction subtract (MembershipFunction a, MembershipFunction b) throws Exception {
		return a.subtract(b);
	}
	
	public MembershipFunction multiply (MembershipFunction a, MembershipFunction b) throws Exception {
		return a.multiply(b);
	}
	
	public MembershipFunction divide (MembershipFunction a, MembershipFunction b) throws Exception {
		return a.divide(b);
	}
	
	public double getValue (MembershipFunction function, double argument) throws Exception {
		return function.getValue(argument);
	}
	
	public double getJacardSimilarity (MembershipFunction a, MembershipFunction b) throws Exception{
		return a.getJacardSimilarity(b);
	}
	
	public double getInclusionDegreeMeasure (MembershipFunction a, MembershipFunction b) throws Exception{
		return a.getInclusionDegreeMeasure(b);
	}
}
