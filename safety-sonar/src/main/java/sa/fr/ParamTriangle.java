package sa.fr;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author N3
 */
@XmlRootElement(name = "paramTriangle")
public class ParamTriangle extends Param {

	private double a, b, c, length, prob;

        public ParamTriangle() {
            super();
        }

	public ParamTriangle(double a, double b, double c, Scale scale, Type type, double length, double prob) throws Exception {
                super(scale, type);
		if (a > b || b > c) {
			throw new Exception("values in bad order");
		}
		if (scale == Scale.LINEAR) {
			if ((a < 0) || (b < 0) || (c < 0)
					|| (a > 1) || (b > 1) || (c > 1)) {
				throw new Exception("value exceedes the range");
			}
		} else {
			if ((a < -12) || (b < -12) || (c < -12)
					|| (a > 0) || (b > 0) || (c > 0)) {
				throw new Exception("value exceedes the range");
			}
		}
                if (type == Type.FIRST && length != 0){
                    throw new Exception("Fuzzy set of first type cannot have length value != 0 ");
                }
                if (type == Type.SECOND && length >= Math.min((c-b),(b-a))){
                    throw new Exception("Value length exceedes the range");
                }
                if (type == Type.FIRST && prob != 0){
                    throw new Exception("Fuzzy set of first type cannot have prob value != 0 ");
                }
                if (type == Type.SECOND && prob >= 1){
                    throw new Exception("Value prob exceedes the range");
                }
                    
		this.a = a;
		this.b = b;
		this.c = c;
                this.length = length;
                this.prob = prob;
	}
        
        public ParamTriangle(double a, double b, double c, Scale scale) throws Exception {
            this(a,b,c,scale,Type.FIRST,0,0);
        }

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}
        
        public double getLength(){
            return length;
        }
        
        public void setLength(double length){
            this.length = length;
        }
        
        public double getProb(){
            return prob;
        }
        
        public void setProb(double prob){
            this.prob = prob;
        }

	ParamTriangle add(Param y) throws Exception {
		if (y.getClass() != getClass())
			throw new Exception ("types are different");
		ParamTriangle x = (ParamTriangle) y;
                if (type != x.getType())
			throw new Exception ("fuzzy sets types are different");
		if (scale != x.getScale())
			throw new Exception ("scales are different");
		double a1, b1, c1, a2, b2, c2, a3, b3, c3;
		if (scale == Scale.LOGARITHMIC) {
			a1 = Math.pow(10, a);
			b1 = Math.pow(10, b);
			c1 = Math.pow(10, c);
			a2 = Math.pow(10, x.getA());
			b2 = Math.pow(10, x.getB());
			c2 = Math.pow(10, x.getC());
		} else {
			a1 = a;
			b1 = b;
			c1 = c;
			a2 = x.getA();
			b2 = x.getB();
			c2 = x.getC();
		}
		a3 = Math.min(1.0, a1 + a2);
		b3 = Math.min(1.0, b1 + b2);
		c3 = Math.min(1.0, c1 + c2);
		if (scale == Scale.LOGARITHMIC) {
			a3 = Math.log(a3) / Math.log(10);
			b3 = Math.log(b3) / Math.log(10);
			c3 = Math.log(c3) / Math.log(10);
		}
		return new ParamTriangle(a3, b3, c3, scale, type, length, prob);
	}

	ParamTriangle subtract(Param y) throws Exception {
		if (y.getClass() != getClass())
			throw new Exception ("types are different");
		ParamTriangle x = (ParamTriangle) y;
		if (type != x.getType())
			throw new Exception ("fuzzy sets types are different");
                if (scale != x.getScale())
			throw new Exception ("scales are different");
		double a1, b1, c1, a2, b2, c2, a3, b3, c3;
		if (scale == Scale.LOGARITHMIC) {
			a1 = Math.pow(10, a);
			b1 = Math.pow(10, b);
			c1 = Math.pow(10, c);
			a2 = Math.pow(10, x.getA());
			b2 = Math.pow(10, x.getB());
			c2 = Math.pow(10, x.getC());
		} else {
			a1 = a;
			b1 = b;
			c1 = c;
			a2 = x.getA();
			b2 = x.getB();
			c2 = x.getC();
		}

		a3 = Math.max(0.0, a1 - c2);
		b3 = Math.max(0.0, b1 - b2);
		c3 = Math.max(0.0, c1 - a2);
		if (scale == Scale.LOGARITHMIC) {
			a3 = Math.max(-12.0, Math.log(a3) / Math.log(10));
			b3 = Math.max(-12.0, Math.log(b3) / Math.log(10));
			c3 = Math.max(-12.0, Math.log(c3) / Math.log(10));
		}
		return new ParamTriangle(a3, b3, c3, scale, type, length, prob);
	}

	ParamTriangle multiply(Param y) throws Exception {
		if (y.getClass() != getClass())
			throw new Exception ("types are different");
		ParamTriangle x = (ParamTriangle) y;
		if (type != x.getType())
			throw new Exception ("fuzzy sets types are different");
                if (scale != x.getScale())
			throw new Exception ("scales are different");
		double a1, b1, c1, a2, b2, c2, a3, b3, c3;
		a1 = a;
		b1 = b;
		c1 = c;
		a2 = x.getA();
		b2 = x.getB();
		c2 = x.getC();
		if (scale == Scale.LOGARITHMIC) {
			a3 = Math.max(-12.0, a1 + a2);
			b3 = Math.max(-12.0, b1 + b2);
			c3 = Math.max(-12.0, c1 + c2);
		} else {
			a3 = a1 * a2;
			b3 = b1 * b2;
			c3 = c1 * c2;
		}
		return new ParamTriangle(a3, b3, c3, scale, type, length, prob);
	}

	ParamTriangle divide(Param y) throws Exception {
		if (y.getClass() != getClass())
			throw new Exception ("types are different");
		ParamTriangle x = (ParamTriangle) y;
                if (type != x.getType())
			throw new Exception ("fuzzy sets types are different");
		if (scale != x.getScale())
			throw new Exception ("scales are different");
		double a1, b1, c1, a2, b2, c2, a3, b3, c3;
		a1 = a;
		b1 = b;
		c1 = c;
		a2 = x.getA();
		b2 = x.getB();
		c2 = x.getC();

		if (scale == Scale.LOGARITHMIC) {
			a3 = Math.max(-12.0, a1 - c2);
			b3 = Math.max(-12.0, b1 - b2);
			c3 = Math.max(-12.0, c1 - a2);
		} else {
			a3 = Math.min(1.0, a1 / c2);
			b3 = Math.min(1.0, b1 / b2);
			c3 = Math.min(1.0, c1 / a2);
		}
		return new ParamTriangle(a3, b3, c3, scale, type, length, prob);
	}
        // tutaj przydaloby sie rozrozniac czy to zbior pierwszego czy drugiego rodzaju
        // bo dla drugiego rodzaju mamy przedzial wartosci, a nie wartosc
        // przynajmniej na razie w teorii, nie wiem jak bedziemy musieli to zaimplementowac...
	double getValue(double argument) throws Exception {
		if (type == Type.SECOND)
			throw new Exception ("Wrong fuzzy set type");
                double result = 0;
		if (a != b && b != c) {
			if (argument >= a && argument <= b) {
				result = (argument - a) / (b - a);
			} else if (argument > b && argument <= c) {
				result = (c - argument) / (c - b);
			}
		} else if (a == b && b != c && argument >= b && argument <= c) {
			result = (c - argument) / (c - b);
		} else if (a != b && b == c && argument >= a && argument <= c) {
			result = (argument - a) / (c - a);
		} else if (a == b && b == c && argument == a) {
			result = 1;
		}
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		final ParamTriangle p = (ParamTriangle) obj;
		if(Double.compare(p.getA(), a) != 0 ){
			return false;
		}
		if(Double.compare(p.getB(), b) != 0){
			return false;
		}
		if(Double.compare(p.getC(),c) != 0){
			return false;
		}
		if(p.getScale() != scale || p.getType() != type){
			return false;
		}
		if(Double.compare(p.getLength(), length) != 0){
			return false;
		}
		if (Double.compare(p.getProb(), prob) != 0){
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 97 * hash + (int) (Double.doubleToLongBits(this.a) ^ (Double.doubleToLongBits(this.a) >>> 32));
		hash = 97 * hash + (int) (Double.doubleToLongBits(this.b) ^ (Double.doubleToLongBits(this.b) >>> 32));
		hash = 97 * hash + (int) (Double.doubleToLongBits(this.c) ^ (Double.doubleToLongBits(this.c) >>> 32));
                hash = 97 * hash + (int) (Double.doubleToLongBits(this.length) ^ (Double.doubleToLongBits(this.length) >>> 32));
                hash = 97 * hash + (int) (Double.doubleToLongBits(this.prob) ^ (Double.doubleToLongBits(this.prob) >>> 32));
		hash = 97 * hash + (this.scale != null ? this.scale.hashCode() : 0);
                hash = 97 * hash + (this.type != null ? this.type.hashCode() : 0);
		return hash;
	}
}
