package pl.wroc.pwr.sa.ft.FIT.Framework;

import fit.ActionFixture;
import pl.wroc.pwr.sa.ft.Fault;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.ft.LogicGate;

/**
 *
 * @author n0npax
 */
public class CreateConnectionTestTree extends ActionFixture {
	static FaultTree f;
	static Fault f0, f1, f2, f3, f4, f5, f6, f7;
	static LogicGate l0, l1, l3;

    /**
     *
     */
    public void createTestTree() {
		f = new FaultTree();
		f0 = f.addFault("f0");
		f.setRoot(f0);
		f1 = f.addFault("f1");
		f2 = f.addFault("f2");
		f3 = f.addFault("f3");
		f4 = f.addFault("f4");
		f5 = f.addFault("f5");
		f6 = f.addFault("f6");
		f7 = f.addFault("f7");

		l0 = f.addLogicGate(LogicGate.LogicGateType.GATE_AND);
		l1 = f.addLogicGate(LogicGate.LogicGateType.GATE_AND);
		l3 = f.addLogicGate(LogicGate.LogicGateType.GATE_AND);

		f.addConnection(f0, l0);
		f.addConnection(l0, f1);
		f.addConnection(l0, f2);
		f.addConnection(l0, f3);
		f.addConnection(f1, l1);
		f.addConnection(l1, f4);
		f.addConnection(l1, f5);
		f.addConnection(f3, l3);
		f.addConnection(l3, f6);
		f.addConnection(l3, f7);

	}

    /**
     *
     * @return
     */
    public boolean isTreeCreated() {
		return f.getFaultMap().size() > 0;
	}
}
