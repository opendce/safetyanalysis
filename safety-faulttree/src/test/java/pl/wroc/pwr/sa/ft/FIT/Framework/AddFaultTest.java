package pl.wroc.pwr.sa.ft.FIT.Framework;

import fit.ActionFixture;
import pl.wroc.pwr.sa.ft.Fault;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.ft.LogicGate;

/**
 *
 * @author n0npax
 */
public class AddFaultTest extends ActionFixture {
	private FaultTree f;
	private Fault f0, f1, f2, f3, f4, f5, f11;
	private LogicGate l0, l1, l2;

    /**
     *
     */
    public void createTree() {
		f = new FaultTree();
	}

    /**
     *
     * @return
     */
    public boolean hasTreeRoot() {
		return f.getRoot() != null;

	}

    /**
     *
     * @param name
     */
    public void createFault(String name) {
		if (name.equals("f1"))
			f1 = f.addFault(name);
		else
			f11 = f.addFault(name);
	}

    /**
     *
     */
    public void setFaultAsARoot() {
		f.setRoot(f1);
	}

    /**
     *
     * @return
     */
    public boolean isFaultRoot() {
		return f.getRoot().equals(f1);
	}

    /**
     *
     */
    public void createTestTree() {
		f = new FaultTree();
		f0 = f.addFault("f0");
		f.setRoot(f0);
		f1 = f.addFault("f1");
		f2 = f.addFault("f2");
		f3 = f.addFault("f3");
		f4 = f.addFault("f4");
		f5 = f.addFault("f5");

		l0 = f.addLogicGate(LogicGate.LogicGateType.GATE_AND);
		l1 = f.addLogicGate(LogicGate.LogicGateType.GATE_AND);

		f.addConnection(f0, l0);
		f.addConnection(l0, f1);
		f.addConnection(l0, f2);
		f.addConnection(l0, f3);
		f.addConnection(f2, l1);
		f.addConnection(l1, f4);
		f.addConnection(l1, f5);

	}

    /**
     *
     * @param name
     */
    public void addCreatedFaultToTree(String name) {
		f.addConnection(l1, f11);
	}

    /**
     *
     * @return
     */
    public boolean isTreeConsistent() {
		if (!f.getRoot().equals(f0))
			return false;
		if (!f0.getChild().equals(l0))
			return false;

		if (!l0.getChildren().contains(f1))
			return false;
		if (!l0.getChildren().contains(f2))
			return false;
		if (!l0.getChildren().contains(f3))
			return false;

		if (!f1.getParent().equals(l0))
			return false;
		if (!f2.getParent().equals(l0))
			return false;
		if (!f3.getParent().equals(l0))
			return false;

		if (!f2.getChild().equals(l1))
			return false;
		if (!l1.getParent().equals(f2))
			return false;

		if (!l1.getChildren().contains(f4))
			return false;
		if (!l1.getChildren().contains(f5))
			return false;

		if (!f4.getParent().equals(l1))
			return false;
		if (!f5.getParent().equals(l1))
			return false;

		if (!l1.getChildren().contains(f11))
			return false;
		if (!f11.getParent().equals(l1))
			return false;

		return true;
	}
}
