/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.sa.ft;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.wroc.pwr.sa.fr.FuzzyLogicFacade;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;
import pl.wroc.pwr.sa.fr.ParamTriangle;
import pl.wroc.pwr.sa.fr.Scale;
import pl.wroc.pwr.sa.fr.TrapezoidFunction;
import pl.wroc.pwr.sa.fr.TriangleFunction;
import pl.wroc.pwr.sa.xml.XMLService;


/**
 *
 * @author n0npax
 */

public class XMLServiceTest {
	
	@Test
	public void testFaultToString() throws Exception {
            Fault f = new Fault("name");
            double functionValues[] = {0.05,0.5,0.55,0.6};
            MembershipFunction mF = createFunction(functionValues,Scale.LINEAR);
            f.setProbabilty(mF);
            f.setDescription("super description");
            String s = XMLService.saveObjectToXMLString(f);
            Fault f2 = XMLService.readXMLStringToObject(Fault.class, s);
            Assert.assertEquals(f2.getDescription(), f.getDescription());
            Assert.assertEquals(f2.getName(), f.getName());
            Assert.assertEquals(f2.getProbability(), f.getProbability());
	}
        
        @Test
	public void testEmptyFaultTreeToString() throws Exception {
            FaultTree faultTree = new FaultTree(); 
            String s = XMLService.saveObjectToXMLString(faultTree);
            faultTree = XMLService.readXMLStringToObject(FaultTree.class, s);
            FaultTree ft = XMLService.readXMLStringToObject(FaultTree.class, s);
            Assert.assertEquals(ft.getLeafsList(), faultTree.getLeafsList());

	}
        
        @Test
	public void testGateToString() throws Exception {
            LogicGate g = new LogicGate(LogicGate.LogicGateType.GATE_AND);
            Fault fp = new Fault("papa");
            g.setParent(fp);
            String s = XMLService.saveObjectToXMLString(g);
            //System.out.println(s);
            g = XMLService.readXMLStringToObject(LogicGate.class, s);
            LogicGate g2 = XMLService.readXMLStringToObject(LogicGate.class, s);
            Assert.assertEquals(g2.GetType(), g.GetType());
            Assert.assertEquals("papa",g2.getParent().getName());
            
	}
        
        @Test
	public void testFaultTreeToString() throws Exception {
            FaultTree faultTree = new FaultTree(); 
            faultTreeAnalysisBOTH(faultTree);
            String s = XMLService.saveObjectToXMLString(faultTree);
            FaultTree ft2 = XMLService.readXMLStringToObject(FaultTree.class, s);
            ft2.updateLeafsList();
            
            Assert.assertEquals(faultTree.getRoot().getName(),ft2.getRoot().getName());
            Assert.assertEquals(faultTree.getRoot().getProbability(),ft2.getRoot().getProbability());
           
            LinkedList<Fault> list1= new LinkedList<Fault>();
            LinkedList<Fault> list2= new LinkedList<Fault>();

            for(Fault f : ft2.getFaultMap().values())
            {
                list1.add(f);
            }
            
            for(Fault f : faultTree.getFaultMap().values())
            {
                list2.add(f);
            }
            if(list1.size()!=list2.size())
            {
                System.out.println("diffrent number of fault in tree");
                Assert.assertEquals(0, 1);
            }
            int sumator=0;
            System.out.println();
            for(int i=0; i < list1.size(); i++)
            {
                for(int j=0 ; j < list2.size(); j++)
                {
                    if(list2.get(j).getName().equals(list1.get(i).getName()))
                    {
                           sumator++;
                           Assert.assertEquals(list2.get(j).getName(), list1.get(i).getName());
                           
                           if(list2.get(j).getProbability().getClass().equals(new TriangleFunction().getClass()))
                           {
                               TriangleFunction f1 = (TriangleFunction) list1.get(i).getProbability();
                               TriangleFunction f2 = (TriangleFunction) list2.get(j).getProbability();
                               Assert.assertEquals(f1.getParameters(), f2.getParameters());
                           }
                           else if(list2.get(j).getProbability().getClass().equals(new TrapezoidFunction().getClass()))
                           {
                               TrapezoidFunction f1 = (TrapezoidFunction) list1.get(i).getProbability();
                               TrapezoidFunction f2 = (TrapezoidFunction) list2.get(j).getProbability();
                               Assert.assertEquals(f1.getParameters(), f2.getParameters());
                           }      
                    }
                }
            }
            Assert.assertEquals(sumator, list1.size()); //czy wszystkie fault zostały sprawdzone

        }
        
        @Test
	public void testFaultTreeToFile() throws Exception {
            FaultTree faultTree = new FaultTree(); 
            faultTreeAnalysisBOTH(faultTree);
            XMLService.saveObjectToFile(faultTree, "exampleTree.xml");
            FaultTree ft2 = XMLService.readFileToObject(FaultTree.class, "exampleTree.xml");
            Assert.assertEquals(ft2.getRoot().getName(), faultTree.getRoot().getName());

            ft2.updateLeafsList();
            
            Assert.assertEquals(faultTree.getRoot().getName(),ft2.getRoot().getName());
            Assert.assertEquals(faultTree.getRoot().getProbability(),ft2.getRoot().getProbability());
           
            LinkedList<Fault> list1= new LinkedList<Fault>();
            LinkedList<Fault> list2= new LinkedList<Fault>();

            for(Fault f : ft2.getFaultMap().values())
            {
                list1.add(f);
            }
            
            for(Fault f : faultTree.getFaultMap().values())
            {
                list2.add(f);
            }
            if(list1.size()!=list2.size())
            {
                System.out.println("diffrent number of fault in tree");
                Assert.assertEquals(0, 1);
            }
            int sumator=0;
            System.out.println();
            for(int i=0; i < list1.size(); i++)
            {
                for(int j=0 ; j < list2.size(); j++)
                {
                    if(list2.get(j).getName().equals(list1.get(i).getName()))
                    {
                           sumator++;
                           Assert.assertEquals(list2.get(j).getName(), list1.get(i).getName());
                           
                           if(list2.get(j).getProbability().getClass().equals(new TriangleFunction().getClass()))
                           {
                               TriangleFunction f1 = (TriangleFunction) list1.get(i).getProbability();
                               TriangleFunction f2 = (TriangleFunction) list2.get(j).getProbability();
                               Assert.assertEquals(f1.getParameters(), f2.getParameters());
                           }
                           else if(list2.get(j).getProbability().getClass().equals(new TrapezoidFunction().getClass()))
                           {
                               TrapezoidFunction f1 = (TrapezoidFunction) list1.get(i).getProbability();
                               TrapezoidFunction f2 = (TrapezoidFunction) list2.get(j).getProbability();
                               Assert.assertEquals(f1.getParameters(), f2.getParameters());
                           }      
                    }
                }
            }
            Assert.assertEquals(sumator, list1.size()); //czy wszystkie fault zostały sprawdzone
        
        }
        
        @Test
	public void testGateToFile() throws Exception {
            LogicGate g = new LogicGate(LogicGate.LogicGateType.GATE_AND);
            XMLService.saveObjectToFile(g, "exampleGate.xml");
            LogicGate g2 = XMLService.readFileToObject(LogicGate.class, "exampleGate.xml");
            Assert.assertEquals(g2.GetType(), g.GetType());

	}
        
        @Test
	public void testFaultToFile() throws Exception {
            Fault f = new Fault("name");
            double functionValues[] = {0.05,0.5,0.55,0.6};
            MembershipFunction mF = createFunction(functionValues,Scale.LINEAR);
            f.setProbabilty(mF);
            f.setDescription("super description");
            XMLService.saveObjectToFile(f, "exampleFault.xml");
            Fault f2 = XMLService.readFileToObject(Fault.class, "exampleFault.xml");
            Assert.assertEquals(f2.getDescription(), f.getDescription());
            Assert.assertEquals(f2.getName(), f.getName());
            Assert.assertEquals(f2.getProbability(), f.getProbability());
	}
        
        
    private MembershipFunction createFunction(double[] values, Scale scale) {
        ParamTrapezoid params;
        FuzzyLogicFacade flo = new FuzzyLogicFacade();
        MembershipFunction function = null;
        try {
            params = new ParamTrapezoid(values[0],values[2],values[2], values[3], scale);
            
            function = flo.createTrapezoid(params, "nXn");
        } catch (Exception ex) {

        }
        return function;
    }
	
        
     /**
     *
     * @param faultTree
     * @return
     * @throws Exception
     */
    public Map<String, TopProbability> faultTreeAnalysisBOTH(FaultTree faultTree) throws Exception
     {
       FuzzyLogicFacade flf = new FuzzyLogicFacade();
        flf.createTriangle(new ParamTriangle(0.01, 0.2, 0.4, Scale.LINEAR), "mało1");
        flf.createTriangle(new ParamTriangle(0.31, 0.5, 0.7, Scale.LINEAR), "średnio1");
        flf.createTriangle(new ParamTriangle(0.62, 0.8, 1, Scale.LINEAR), "dużo1");
        flf.createTriangle(new ParamTriangle(0.32, 0.5, 0.7, Scale.LINEAR), "średnio2");
        flf.createTriangle(new ParamTriangle(0.63, 0.8, 1, Scale.LINEAR), "dużo2");
        flf.createTriangle(new ParamTriangle(0.34, 0.5, 0.7, Scale.LINEAR), "średnio3");
        flf.createTriangle(new ParamTriangle(0.65, 0.8, 1, Scale.LINEAR), "dużo3");
        faultTree.setScale(flf);
        List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n111 =  scale.get(0);
        TriangleFunction n112 = scale.get(1);
        TriangleFunction n121 = scale.get(2);
        TriangleFunction n122 = scale.get(3);
        TriangleFunction n131 = scale.get(4);
        TriangleFunction n132 = scale.get(5);
        TriangleFunction n2 = scale.get(6);
        
        Fault root = faultTree.addFault("root");
        Fault f1 = faultTree.addFault("fault1");
        Fault f2 = faultTree.addFault("fault2");
        f2.setProbabilty(n2);
        Fault f11 = faultTree.addFault("fault11");
        Fault f12 = faultTree.addFault("fault12");
        Fault f13 = faultTree.addFault("fault13");
        Fault f111 = faultTree.addFault("fault111");
        f111.setProbabilty(n111);
        Fault f112 = faultTree.addFault("fault112");
        f112.setProbabilty(n112);
        Fault f121 = faultTree.addFault("fault121");
        f121.setProbabilty(n121);
        Fault f122 = faultTree.addFault("fault122");
        f122.setProbabilty(n122);
        Fault f131 = faultTree.addFault("fault131");
        f131.setProbabilty(n131);
        Fault f132 = faultTree.addFault("fault132");
        f132.setProbabilty(n132);

        LogicGate g0 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g1 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g2 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g11 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g12 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g13 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        faultTree.addConnection(root, g0);
        faultTree.addConnection(g0, f1);
        faultTree.addConnection(g0, f2);
        faultTree.addConnection(f1, g1);
        faultTree.addConnection(f2, g2);
        faultTree.addConnection(g1, f11);
        faultTree.addConnection(g1, f12);
        faultTree.addConnection(g1, f13);
        faultTree.addConnection(f11, g11);
        faultTree.addConnection(f12, g12);
        faultTree.addConnection(f13, g13);
        faultTree.addConnection(g11, f111);
        faultTree.addConnection(g11, f112);
        faultTree.addConnection(g12, f121);
        faultTree.addConnection(g12, f122);
        faultTree.addConnection(g13, f131);
        faultTree.addConnection(g13, f132);
     
        FaultTreeOperations faultTreeOperations = new FaultTreeOperations(faultTree);
         Map<String, TopProbability> expectedResult = new HashMap<String,TopProbability>();
         f111.delete();
         expectedResult.put(f111.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g11, f111);
            f112.delete();
         expectedResult.put(f112.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g11, f112);
            f121.delete();
         expectedResult.put(f121.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g12, f121);
            f122.delete();
         expectedResult.put(f122.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g12, f122);
            f131.delete();
         expectedResult.put(f131.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g13, f131);
            f132.delete();
         expectedResult.put(f132.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g13, f132);
            f2.delete();
         expectedResult.put(f2.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g1, f2);
         
         return expectedResult;
     }

}
