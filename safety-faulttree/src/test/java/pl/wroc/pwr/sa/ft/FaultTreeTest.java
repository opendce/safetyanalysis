/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.sa.ft;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import pl.wroc.pwr.sa.ft.Fault;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.ft.LogicGate;

/**
 * 
 * @author Anna
 */
public class FaultTreeTest {

    /**
     *
     */
    public FaultTreeTest() {
	}

	/**
	 * Test of SetRoot method, of class FaultTree.
	 */
	@Test
	public void testSetRoot() {
	}

	/**
	 * Test of GetRoot method, of class FaultTree.
	 */
	@Test
	public void testGetRoot() {
		FaultTree faultTree = new FaultTree();
		assertNull(faultTree.getRoot());

		Fault fault = new Fault("fault");
		faultTree.setRoot(fault);
		assertEquals(fault, faultTree.getRoot());

		faultTree.setRoot(null);
		assertNull(faultTree.getRoot());
	}

	/**
	 * Test of AddFault method, of class FaultTree.
	 */
	@Test
	public void testAddFault() {
		FaultTree faultTree = new FaultTree();

		Fault f2 = faultTree.addFault("fault2");
		assertEquals(faultTree, f2.getFaultTree());
		assertEquals("fault2", f2.getName());
		assertTrue(faultTree.getFaultMap().containsKey("fault2"));

		Fault f3 = faultTree.addFault("fault3");
		assertEquals(faultTree, f3.getFaultTree());
		assertEquals("fault3", f3.getName());
		assertTrue(faultTree.getFaultMap().containsKey("fault3"));

		Fault f4 = faultTree.addFault("fault2");
		assertNull(f4);
		assertTrue(faultTree.getFaultMap().containsKey("fault2"));
	}

	/**
	 * Test of GetFaultMap method, of class FaultTree.
	 */
	@Test
	public void testGetFaultMap() {
	}

	/**
	 * Test of AddLogicGate method, of class FaultTree.
	 */
	@Test
	public void testAddLogicGate() {
		FaultTree faultTree = new FaultTree();
		LogicGate lg1 = faultTree
				.addLogicGate(LogicGate.LogicGateType.GATE_AND);
		assertEquals(LogicGate.LogicGateType.GATE_AND, lg1.GetType());
		assertEquals(faultTree, lg1.getFaultTree());
	}

	/**
	 * Test of AddConnection method, of class FaultTree.
	 */
	@Test
	public void testAddConnection_Fault_LogicGate() {
		FaultTree faultTree = new FaultTree();
		Fault f1 = faultTree.addFault("parent");
		LogicGate g1 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
		faultTree.addConnection(f1, g1);

		assertEquals(g1, f1.getChild());
		assertEquals(f1, g1.getParent());

		LogicGate g2 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
		faultTree.addConnection(f1, g2);

		assertEquals(g2, f1.getChild());
		assertEquals(f1, g2.getParent());
		assertNull(g1.getParent());

		Fault f2 = faultTree.addFault("parent2");
		faultTree.addConnection(f2, g2);
		assertEquals(g2, f2.getChild());
		assertEquals(f2, g2.getParent());
		assertNull(f1.getChild());
	}

	/**
	 * Test of AddConnection method, of class FaultTree.
	 */
	@Test
	public void testAddConnection_LogicGate_Fault() {
		FaultTree faultTree = new FaultTree();
		Fault fault = faultTree.addFault("child");
		LogicGate logicGate = faultTree
				.addLogicGate(LogicGate.LogicGateType.GATE_AND);
		faultTree.addConnection(logicGate, fault);
		assertEquals(logicGate, fault.getParent());
		assertTrue(logicGate.getChildren().contains(fault));

		Fault fault2 = faultTree.addFault("child2");
		faultTree.addConnection(logicGate, fault2);
		assertEquals(logicGate, fault.getParent());
		assertEquals(logicGate, fault2.getParent());
		assertTrue(logicGate.getChildren().contains(fault));
		assertTrue(logicGate.getChildren().contains(fault2));

		LogicGate logicGate2 = faultTree
				.addLogicGate(LogicGate.LogicGateType.GATE_AND);
		faultTree.addConnection(logicGate2, fault);
		assertEquals(logicGate2, fault.getParent());
		assertEquals(logicGate, fault2.getParent());
		assertTrue(logicGate2.getChildren().contains(fault));
		assertTrue(logicGate.getChildren().contains(fault2));
		assertFalse(logicGate.getChildren().contains(fault));
	}

	/**
	 * Test of UpdateConnection method, of class FaultTree.
	 */
	@Test
	public void testUpdateConnection_Fault_LogicGate() {
		FaultTree faultTree = new FaultTree();
		Fault fault = faultTree.addFault("f1");
		LogicGate logicGate = faultTree
				.addLogicGate(LogicGate.LogicGateType.GATE_AND);
		faultTree.addConnection(fault, logicGate);
		Fault fault2 = faultTree.addFault("f2");
		LogicGate logicGate2 = faultTree
				.addLogicGate(LogicGate.LogicGateType.GATE_OR);
		faultTree.addConnection(fault2, logicGate2);
		faultTree.updateConnection(fault2, logicGate);

		assertEquals(fault2, logicGate.getParent());
		assertEquals(logicGate, fault2.getChild());
		assertNull(fault.getChild());
		assertNull(logicGate2.getParent());
	}

	/**
	 * Test of UpdateConnection method, of class FaultTree.
	 */
	@Test
	public void testUpdateConnection_LogicGate_Fault() {
		FaultTree faultTree = new FaultTree();
		Fault fault = faultTree.addFault("f1");
		LogicGate logicGate = faultTree
				.addLogicGate(LogicGate.LogicGateType.GATE_AND);
		faultTree.addConnection(logicGate, fault);
		Fault fault2 = faultTree.addFault("f2");
		LogicGate logicGate2 = faultTree
				.addLogicGate(LogicGate.LogicGateType.GATE_OR);
		faultTree.addConnection(logicGate2, fault2);
		faultTree.updateConnection(logicGate, fault2);

		assertEquals(logicGate, fault2.getParent());
		assertEquals(logicGate, fault.getParent());
		assertTrue(logicGate.getChildren().contains(fault2));
		assertTrue(logicGate.getChildren().contains(fault));
		assertFalse(logicGate2.getChildren().contains(fault2));
	}

	/**
	 * Test of DeleteConnection method, of class FaultTree.
	 */
	@Test
	public void testDeleteConnection() {
		FaultTree faultTree = new FaultTree();
		Fault fault = faultTree.addFault("f1");
		LogicGate logicGate = faultTree
				.addLogicGate(LogicGate.LogicGateType.GATE_AND);

		faultTree.addConnection(logicGate, fault);
		faultTree.deleteConnection(fault, logicGate);

		assertNull(fault.getParent());
		assertFalse(logicGate.getChildren().contains(fault));

		Fault fault2 = faultTree.addFault("f2");
		LogicGate logicGate2 = faultTree
				.addLogicGate(LogicGate.LogicGateType.GATE_OR);
		faultTree.addConnection(fault2, logicGate2);
		faultTree.deleteConnection(fault2, logicGate2);

		assertNull(fault.getChild());
		assertNull(logicGate.getParent());
	}
}