
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.sa.ft;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.wroc.pwr.sa.fr.*;

/**
 *
 * @author asus
 */
public class FaultTreeOperationsTest {

    /**
     *
     */
    public FaultTreeOperationsTest() {
    }

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    
               //Test pokazujący na czym wysypuje się nasz kod. Patrz do FuzzyOperationsTest
    	@Test
	public void testSubtract() throws Exception {
            try
            {
		        MembershipFunction a = new TriangleFunction (new ParamTriangle (0.3, 0.5, 0.9, Scale.LINEAR), "");
		        MembershipFunction b = new TriangleFunction (new ParamTriangle (0.4, 0.5, 0.6, Scale.LINEAR), "");
                FuzzyOperations operations = new FuzzyOperations();
		        MembershipFunction result = operations.subtract(b, a);
                MembershipFunction expResult = new TriangleFunction (new ParamTriangle (0.0, 0.0, 0.3, Scale.LINEAR), "");
                assertEquals(((TriangleFunction) expResult).getParameters().getA(), ((TriangleFunction) result).getParameters().getA(), 0.01);
                assertEquals(((TriangleFunction) expResult).getParameters().getB(), ((TriangleFunction) result).getParameters().getB(), 0.01);
                assertEquals(((TriangleFunction) expResult).getParameters().getC(), ((TriangleFunction) result).getParameters().getC(), 0.01);
            }
            catch(Exception e)
            {
                System.out.println(" -> nie przeszło");
            }
	}
      
    @Test
    public void testFuzzyImportanceIndexSmall() throws Exception 
    {
        FaultTree faultTree = new FaultTree(); 
        createSmallFaultTree(faultTree); //Tworzone jest drzewo do testów.
        FaultTreeOperations faultTreeOperation = new FaultTreeOperations(faultTree);
        
        Map<Fault, MembershipFunction> map = faultTreeOperation.getFuzzyImportanceIndex(); //obliczam FII    
        assertEquals(map.size(), faultTree.getLeafsList().size());  //rozmiar wektora FII i liści 
        
        ParamTriangle cp[] = { new ParamTriangle (0.0,0.4,0.7, Scale.LINEAR) , new ParamTriangle (0.0,0.3,0.6, Scale.LINEAR) , new ParamTriangle (0.0,0.2,0.5, Scale.LINEAR) };
        ArrayList<MembershipFunction> list = new ArrayList<MembershipFunction>(map.values());
        ParamTriangle rp[] = { (ParamTriangle) list.get(0).getParameters(), (ParamTriangle) list.get(1).getParameters(), (ParamTriangle) list.get(2).getParameters() };
        

        int assertsNum=0; //ilość porównań jakie mamy wykonać.
        for(ParamTriangle p : cp)
        {
            for(ParamTriangle r : rp)
            {
                if( Math.abs(p.getA()-r.getA())<0.01) //sprawdzam z którym elementem porównywać
                {
                     assertsNum++;  //zwiększam ilość porównań
                     assertEquals(p.getA(), r.getA(), 0.01);
                     assertEquals(p.getA(), r.getA(), 0.01);
                     assertEquals(p.getA(), r.getA(), 0.01);
                }
            }
        }
        assertEquals(assertsNum/3, rp.length); //czy wykonano wystarczająca dużo porównań
       }
        

    
    
    @Test
    public void testAnalysisTree() throws Exception {
         FaultTree faultTree1 = new FaultTree();
         FaultTreeOperations faultTreeOperation = new FaultTreeOperations(
                faultTree1);
         Map<String, TopProbability> expectedResult = new HashMap<String,TopProbability>();
         Map<String, TopProbability> result = new HashMap<String,TopProbability>();
    
         expectedResult = faultTreeAnalysisAND(faultTree1);
         result = faultTreeOperation.analysisTree();
 
        for (Entry<String, TopProbability> entry : expectedResult.entrySet()) {
            String key = entry.getKey();
            TopProbability value = entry.getValue();

            assertEquals(value.topMembershipFunction, result.get(key).topMembershipFunction);    
            assertEquals(value.topSimilarityValues, result.get(key).topSimilarityValues);
        }
         
         FaultTree faultTree2 = new FaultTree();
         FaultTreeOperations faultTreeOperation2 = new FaultTreeOperations(
                faultTree2);
         expectedResult = faultTreeAnalysisOR(faultTree2);
         result = faultTreeOperation2.analysisTree();
         for (Entry<String, TopProbability> entry : expectedResult.entrySet()) {
            String key = entry.getKey();
            TopProbability value = entry.getValue();

            assertEquals(value.topMembershipFunction, result.get(key).topMembershipFunction);    
            assertEquals(value.topSimilarityValues, result.get(key).topSimilarityValues);
        }
     }
    
    /**
     * Test of getTopProbability method, of class FaultTreeOperations.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetTopProbability() throws Exception {

        FaultTree faultTree1 = new FaultTree();
        TopProbability expectedResult = createFaultTreeAND(faultTree1);
        FaultTreeOperations faultTreeOperation = new FaultTreeOperations(faultTree1);
        TopProbability result = faultTreeOperation.getTopProbability(true);
        assertEquals(expectedResult.topMembershipFunction, result.topMembershipFunction);    
        assertEquals(expectedResult.topSimilarityValues, result.topSimilarityValues);          
        
        FaultTree faultTree2 = new FaultTree();
        expectedResult = createFaultTreeOR(faultTree2);
        faultTreeOperation = new FaultTreeOperations(faultTree2);
        result = faultTreeOperation.getTopProbability(true);
        assertEquals(expectedResult.topMembershipFunction, result.topMembershipFunction);    
        assertEquals(expectedResult.topSimilarityValues, result.topSimilarityValues);
        
        FaultTree faultTree3 = new FaultTree();
        expectedResult = createFaultTreeBoth(faultTree3);
        faultTreeOperation = new FaultTreeOperations(faultTree3);
        result = faultTreeOperation.getTopProbability(true);
        assertEquals(expectedResult.topMembershipFunction, result.topMembershipFunction);    
        assertEquals(expectedResult.topSimilarityValues, result.topSimilarityValues);
    }

    /**
     *
     * @param faultTree
     * @return
     * @throws Exception
     */
    public TopProbability createFaultTreeAND(FaultTree faultTree) throws Exception {
        FuzzyLogicFacade flf = new FuzzyLogicFacade();
        flf.createTriangle(new ParamTriangle(0.01, 0.2, 0.4, Scale.LINEAR), "mało1");
        flf.createTriangle(new ParamTriangle(0.31, 0.5, 0.7, Scale.LINEAR), "średnio1");
        flf.createTriangle(new ParamTriangle(0.62, 0.68, 1, Scale.LINEAR), "dużo1");
        flf.createTriangle(new ParamTriangle(0.32, 0.5, 0.7, Scale.LINEAR), "średnio2");
        flf.createTriangle(new ParamTriangle(0.63, 0.68, 1, Scale.LINEAR), "dużo2");
        flf.createTriangle(new ParamTriangle(0.34, 0.5, 0.7, Scale.LINEAR), "średnio3");
        flf.createTriangle(new ParamTriangle(0.65, 0.8, 1, Scale.LINEAR), "dużo3");
        faultTree.setScale(flf);
        List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n111 =  scale.get(0);
        TriangleFunction n112 = scale.get(2);
        TriangleFunction n121 = scale.get(0);
        TriangleFunction n122 = scale.get(1);
        TriangleFunction n131 = scale.get(1);
        TriangleFunction n132 = scale.get(2);
        TriangleFunction n2 = scale.get(1);
        MembershipFunction r31, r32, r33;
        r31 = flf.multiply(n111, n112);
        r32 = flf.multiply(n121, n122);
        r33 = flf.multiply(n131, n132);
        MembershipFunction expectedResult = flf.multiply(r31, r32);
        expectedResult = flf.multiply(expectedResult, r33);
        expectedResult = flf.multiply(expectedResult, n2);

        Fault root = faultTree.addFault("root");
        Fault f1 = faultTree.addFault("fault1");
        Fault f2 = faultTree.addFault("fault2");
        f2.setProbabilty(n2);
        Fault f11 = faultTree.addFault("fault11");
        Fault f12 = faultTree.addFault("fault12");
        Fault f13 = faultTree.addFault("fault13");
        Fault f111 = faultTree.addFault("fault111");
        f111.setProbabilty(n111);
        Fault f112 = faultTree.addFault("fault112");
        f112.setProbabilty(n112);
        Fault f121 = faultTree.addFault("fault121");
        f121.setProbabilty(n121);
        Fault f122 = faultTree.addFault("fault122");
        f122.setProbabilty(n122);
        Fault f131 = faultTree.addFault("fault131");
        f131.setProbabilty(n131);
        Fault f132 = faultTree.addFault("fault132");
        f132.setProbabilty(n132);

        LogicGate g0 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g1 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g2 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g11 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g12 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g13 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        faultTree.addConnection(root, g0);
        faultTree.addConnection(g0, f1);
        faultTree.addConnection(g0, f2);
        faultTree.addConnection(f1, g1);
        faultTree.addConnection(f2, g2);
        faultTree.addConnection(g1, f11);
        faultTree.addConnection(g1, f12);
        faultTree.addConnection(g1, f13);
        faultTree.addConnection(f11, g11);
        faultTree.addConnection(f12, g12);
        faultTree.addConnection(f13, g13);
        faultTree.addConnection(g11, f111);
        faultTree.addConnection(g11, f112);
        faultTree.addConnection(g12, f121);
        faultTree.addConnection(g12, f122);
        faultTree.addConnection(g13, f131);
        faultTree.addConnection(g13, f132);
        TopProbability topProbability = new TopProbability();
        topProbability.topMembershipFunction = expectedResult;
        topProbability.setTopSimilarityValues(flf);
        return topProbability;
    }

    /**
     *
     * @param faultTree
     * @return
     * @throws Exception
     */
    public TopProbability createFaultTreeOR(FaultTree faultTree) throws Exception {
         FuzzyLogicFacade flf = new FuzzyLogicFacade();
        flf.createTriangle(new ParamTriangle(0.1, 0.2, 0.4, Scale.LINEAR), "mało1");
        flf.createTriangle(new ParamTriangle(0.1, 0.25, 0.7, Scale.LINEAR), "średnio1");
        flf.createTriangle(new ParamTriangle(0.1, 0.48, 0.5, Scale.LINEAR), "mało0");
        flf.createTriangle(new ParamTriangle(0.12, 0.45, 0.6, Scale.LINEAR), "średnio2");
        flf.createTriangle(new ParamTriangle(0.1, 0.8, 1, Scale.LINEAR), "dużo2");
        flf.createTriangle(new ParamTriangle(0.14, 0.5, 0.6, Scale.LINEAR), "średnio3");
        flf.createTriangle(new ParamTriangle(0.15, 0.68, 1, Scale.LINEAR), "dużo3");
        faultTree.setScale(flf);
        List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n111 =  scale.get(0);
        TriangleFunction n112 = scale.get(1);
        TriangleFunction n121 = scale.get(2);
        TriangleFunction n122 = scale.get(3);
        TriangleFunction n131 = scale.get(4);
        TriangleFunction n132 = scale.get(5);
        TriangleFunction n2 = scale.get(6);
        MembershipFunction r31, r32, r33;
        r31 = flf.add(n111, n112);
        r32 = flf.add(n121, n122);
        r33 = flf.add(n131, n132);
        MembershipFunction expectedResult = flf.add(r31, r32);
        expectedResult = flf.add(expectedResult, r33);
        expectedResult = flf.add(expectedResult, n2);

        Fault root = faultTree.addFault("root");
        Fault f1 = faultTree.addFault("fault1");
        Fault f2 = faultTree.addFault("fault2");
        f2.setProbabilty(n2);
        Fault f11 = faultTree.addFault("fault11");
        Fault f12 = faultTree.addFault("fault12");
        Fault f13 = faultTree.addFault("fault13");
        Fault f111 = faultTree.addFault("fault111");
        f111.setProbabilty(n111);
        Fault f112 = faultTree.addFault("fault112");
        f112.setProbabilty(n112);
        Fault f121 = faultTree.addFault("fault121");
        f121.setProbabilty(n121);
        Fault f122 = faultTree.addFault("fault122");
        f122.setProbabilty(n122);
        Fault f131 = faultTree.addFault("fault131");
        f131.setProbabilty(n131);
        Fault f132 = faultTree.addFault("fault132");
        f132.setProbabilty(n132);

        LogicGate g0 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g1 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        //LogicGate g2 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g11 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g12 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g13 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        faultTree.addConnection(root, g0);
        faultTree.addConnection(g0, f1);
        faultTree.addConnection(g0, f2);
        faultTree.addConnection(f1, g1);
        //faultTree.addConnection(f2, g2);
        faultTree.addConnection(g1, f11);
        faultTree.addConnection(g1, f12);
        faultTree.addConnection(g1, f13);
        faultTree.addConnection(f11, g11);
        faultTree.addConnection(f12, g12);
        faultTree.addConnection(f13, g13);
        faultTree.addConnection(g11, f111);
        faultTree.addConnection(g11, f112);
        faultTree.addConnection(g12, f121);
        faultTree.addConnection(g12, f122);
        faultTree.addConnection(g13, f131);
        faultTree.addConnection(g13, f132);
        TopProbability topProbability = new TopProbability();
        topProbability.topMembershipFunction = expectedResult;
        topProbability.setTopSimilarityValues(flf);
        return topProbability;
    }

    /**
     *
     * @param faultTree
     * @return
     * @throws Exception
     */
    public TopProbability createFaultTreeBoth(FaultTree faultTree) throws Exception {
        FuzzyLogicFacade flf = new FuzzyLogicFacade();
        flf.createTriangle(new ParamTriangle(0.01, 0.2, 0.4, Scale.LINEAR), "mało1");
        flf.createTriangle(new ParamTriangle(0.31, 0.5, 0.7, Scale.LINEAR), "średnio1");
        flf.createTriangle(new ParamTriangle(0.62, 0.8, 1, Scale.LINEAR), "dużo1");
        flf.createTriangle(new ParamTriangle(0.32, 0.5, 0.7, Scale.LINEAR), "średnio2");
        flf.createTriangle(new ParamTriangle(0.63, 0.8, 1, Scale.LINEAR), "dużo2");
        flf.createTriangle(new ParamTriangle(0.34, 0.5, 0.7, Scale.LINEAR), "średnio3");
        flf.createTriangle(new ParamTriangle(0.65, 0.8, 1, Scale.LINEAR), "dużo3");
        faultTree.setScale(flf);
        List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n111 =  scale.get(0);
        TriangleFunction n112 = scale.get(1);
        TriangleFunction n121 = scale.get(2);
        TriangleFunction n122 = scale.get(3);
        TriangleFunction n131 = scale.get(4);
        TriangleFunction n132 = scale.get(5);
        TriangleFunction n2 = scale.get(6);
        MembershipFunction r31, r32, r33;
        r31 = flf.multiply(n111, n112);
        r32 = flf.add(n121, n122);
        r33 = flf.multiply(n131, n132);
        MembershipFunction expectedResult = flf.multiply(r31, r32);
        expectedResult = flf.multiply(expectedResult, r33);
        expectedResult = flf.add(expectedResult, n2);

        Fault root = faultTree.addFault("root");
        Fault f1 = faultTree.addFault("fault1");
        Fault f2 = faultTree.addFault("fault2");
        f2.setProbabilty(n2);
        Fault f11 = faultTree.addFault("fault11");
        Fault f12 = faultTree.addFault("fault12");
        Fault f13 = faultTree.addFault("fault13");
        Fault f111 = faultTree.addFault("fault111");
        f111.setProbabilty(n111);
        Fault f112 = faultTree.addFault("fault112");
        f112.setProbabilty(n112);
        Fault f121 = faultTree.addFault("fault121");
        f121.setProbabilty(n121);
        Fault f122 = faultTree.addFault("fault122");
        f122.setProbabilty(n122);
        Fault f131 = faultTree.addFault("fault131");
        f131.setProbabilty(n131);
        Fault f132 = faultTree.addFault("fault132");
        f132.setProbabilty(n132);

        LogicGate g0 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g1 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g2 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g11 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g12 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g13 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        faultTree.addConnection(root, g0);
        faultTree.addConnection(g0, f1);
        faultTree.addConnection(g0, f2);
        faultTree.addConnection(f1, g1);
        //faultTree.addConnection(f2, g2);
        faultTree.addConnection(g1, f11);
        faultTree.addConnection(g1, f12);
        faultTree.addConnection(g1, f13);
        faultTree.addConnection(f11, g11);
        faultTree.addConnection(f12, g12);
        faultTree.addConnection(f13, g13);
        faultTree.addConnection(g11, f111);
        faultTree.addConnection(g11, f112);
        faultTree.addConnection(g12, f121);
        faultTree.addConnection(g12, f122);
        faultTree.addConnection(g13, f131);
        faultTree.addConnection(g13, f132);
        TopProbability topProbability = new TopProbability();
        topProbability.topMembershipFunction = expectedResult;
        topProbability.setTopSimilarityValues(flf);
        return topProbability;
    }

    /**
     *
     * @param faultTree
     * @return
     * @throws Exception
     */
    public Map<String, TopProbability> faultTreeAnalysisAND(FaultTree faultTree) throws Exception
     {
        FuzzyLogicFacade flf = new FuzzyLogicFacade();
        flf.createTriangle(new ParamTriangle(0.0, 0.2, 0.4, Scale.LINEAR), "mało");
        flf.createTriangle(new ParamTriangle(0.3, 0.5, 0.7, Scale.LINEAR), "średnio");
        flf.createTriangle(new ParamTriangle(0.6, 0.8, 1, Scale.LINEAR), "dużo");
        faultTree.setScale(flf);
        List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n111 =  scale.get(0);
        TriangleFunction n112 = scale.get(2);
        TriangleFunction n121 = scale.get(0);
        TriangleFunction n122 = scale.get(1);
        TriangleFunction n131 = scale.get(1);
        TriangleFunction n132 = scale.get(2);
        TriangleFunction n2 = scale.get(1);
        
        Fault root = faultTree.addFault("root");
        Fault f1 = faultTree.addFault("fault1");
        Fault f2 = faultTree.addFault("fault2");
        f2.setProbabilty(n2);
        Fault f11 = faultTree.addFault("fault11");
        Fault f12 = faultTree.addFault("fault12");
        Fault f13 = faultTree.addFault("fault13");
        Fault f111 = faultTree.addFault("fault111");
        f111.setProbabilty(n111);
        Fault f112 = faultTree.addFault("fault112");
        f112.setProbabilty(n112);
        Fault f121 = faultTree.addFault("fault121");
        f121.setProbabilty(n121);
        Fault f122 = faultTree.addFault("fault122");
        f122.setProbabilty(n122);
        Fault f131 = faultTree.addFault("fault131");
        f131.setProbabilty(n131);
        Fault f132 = faultTree.addFault("fault132");
        f132.setProbabilty(n132);

        LogicGate g0 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g1 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g2 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g11 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g12 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g13 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        faultTree.addConnection(root, g0);
        faultTree.addConnection(g0, f1);
        faultTree.addConnection(g0, f2);
        faultTree.addConnection(f1, g1);
        faultTree.addConnection(f2, g2);
        faultTree.addConnection(g1, f11);
        faultTree.addConnection(g1, f12);
        faultTree.addConnection(g1, f13);
        faultTree.addConnection(f11, g11);
        faultTree.addConnection(f12, g12);
        faultTree.addConnection(f13, g13);
        faultTree.addConnection(g11, f111);
        faultTree.addConnection(g11, f112);
        faultTree.addConnection(g12, f121);
        faultTree.addConnection(g12, f122);
        faultTree.addConnection(g13, f131);
        faultTree.addConnection(g13, f132);
     
        FaultTreeOperations faultTreeOperations = new FaultTreeOperations(faultTree);
         Map<String, TopProbability> expectedResult = new HashMap<String,TopProbability>();
         f111.delete();
         expectedResult.put(f111.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g11, f111);
            f112.delete();
         expectedResult.put(f112.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g11, f112);
            f121.delete();
         expectedResult.put(f121.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g12, f121);
            f122.delete();
         expectedResult.put(f122.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g12, f122);
            f131.delete();
         expectedResult.put(f131.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g13, f131);
            f132.delete();
         expectedResult.put(f132.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g13, f132);
            f2.delete();
         expectedResult.put(f2.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g1, f2);
         
         return expectedResult;
     }

    /**
     *
     * @param faultTree
     * @return
     * @throws Exception
     */
    public Map<String, TopProbability> faultTreeAnalysisOR(FaultTree faultTree) throws Exception
     {
        FuzzyLogicFacade flf = new FuzzyLogicFacade();
        flf.createTriangle(new ParamTriangle(0.01, 0.2, 0.4, Scale.LINEAR), "mało1");
        flf.createTriangle(new ParamTriangle(0.31, 0.5, 0.7, Scale.LINEAR), "średnio1");
        flf.createTriangle(new ParamTriangle(0.62, 0.8, 1, Scale.LINEAR), "dużo1");
        flf.createTriangle(new ParamTriangle(0.32, 0.5, 0.7, Scale.LINEAR), "średnio2");
        flf.createTriangle(new ParamTriangle(0.63, 0.8, 1, Scale.LINEAR), "dużo2");
        flf.createTriangle(new ParamTriangle(0.34, 0.5, 0.7, Scale.LINEAR), "średnio3");
        flf.createTriangle(new ParamTriangle(0.65, 0.8, 1, Scale.LINEAR), "dużo3");
        faultTree.setScale(flf);
        List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n111 =  scale.get(6);
        TriangleFunction n112 = scale.get(5);
        TriangleFunction n121 = scale.get(4);
        TriangleFunction n122 = scale.get(3);
        TriangleFunction n131 = scale.get(2);
        TriangleFunction n132 = scale.get(0);
        TriangleFunction n2 = scale.get(1);
        
        Fault root = faultTree.addFault("root");
        Fault f1 = faultTree.addFault("fault1");
        Fault f2 = faultTree.addFault("fault2");
        f2.setProbabilty(n2);
        Fault f11 = faultTree.addFault("fault11");
        Fault f12 = faultTree.addFault("fault12");
        Fault f13 = faultTree.addFault("fault13");
        Fault f111 = faultTree.addFault("fault111");
        f111.setProbabilty(n111);
        Fault f112 = faultTree.addFault("fault112");
        f112.setProbabilty(n112);
        Fault f121 = faultTree.addFault("fault121");
        f121.setProbabilty(n121);
        Fault f122 = faultTree.addFault("fault122");
        f122.setProbabilty(n122);
        Fault f131 = faultTree.addFault("fault131");
        f131.setProbabilty(n131);
        Fault f132 = faultTree.addFault("fault132");
        f132.setProbabilty(n132);

        LogicGate g0 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g1 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g2 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g11 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g12 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g13 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        faultTree.addConnection(root, g0);
        faultTree.addConnection(g0, f1);
        faultTree.addConnection(g0, f2);
        faultTree.addConnection(f1, g1);
        faultTree.addConnection(f2, g2);
        faultTree.addConnection(g1, f11);
        faultTree.addConnection(g1, f12);
        faultTree.addConnection(g1, f13);
        faultTree.addConnection(f11, g11);
        faultTree.addConnection(f12, g12);
        faultTree.addConnection(f13, g13);
        faultTree.addConnection(g11, f111);
        faultTree.addConnection(g11, f112);
        faultTree.addConnection(g12, f121);
        faultTree.addConnection(g12, f122);
        faultTree.addConnection(g13, f131);
        faultTree.addConnection(g13, f132);
     
        FaultTreeOperations faultTreeOperations = new FaultTreeOperations(faultTree);
         Map<String, TopProbability> expectedResult = new HashMap<String,TopProbability>();
         f111.delete();
         expectedResult.put(f111.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g11, f111);
            f112.delete();
         expectedResult.put(f112.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g11, f112);
            f121.delete();
         expectedResult.put(f121.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g12, f121);
            f122.delete();
         expectedResult.put(f122.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g12, f122);
            f131.delete();
         expectedResult.put(f131.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g13, f131);
            f132.delete();
         expectedResult.put(f132.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g13, f132);
            f2.delete();
         expectedResult.put(f2.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g1, f2);
         
         return expectedResult;
     }

    /**
     *
     * @param faultTree
     * @return
     * @throws Exception
     */
    public Map<String, TopProbability> faultTreeAnalysisBOTH(FaultTree faultTree) throws Exception
     {
       FuzzyLogicFacade flf = new FuzzyLogicFacade();
        flf.createTriangle(new ParamTriangle(0.01, 0.2, 0.4, Scale.LINEAR), "mało1");
        flf.createTriangle(new ParamTriangle(0.31, 0.5, 0.7, Scale.LINEAR), "średnio1");
        flf.createTriangle(new ParamTriangle(0.62, 0.8, 1, Scale.LINEAR), "dużo1");
        flf.createTriangle(new ParamTriangle(0.32, 0.5, 0.7, Scale.LINEAR), "średnio2");
        flf.createTriangle(new ParamTriangle(0.63, 0.8, 1, Scale.LINEAR), "dużo2");
        flf.createTriangle(new ParamTriangle(0.34, 0.5, 0.7, Scale.LINEAR), "średnio3");
        flf.createTriangle(new ParamTriangle(0.65, 0.8, 1, Scale.LINEAR), "dużo3");
        faultTree.setScale(flf);
        List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n111 =  scale.get(0);
        TriangleFunction n112 = scale.get(1);
        TriangleFunction n121 = scale.get(2);
        TriangleFunction n122 = scale.get(3);
        TriangleFunction n131 = scale.get(4);
        TriangleFunction n132 = scale.get(5);
        TriangleFunction n2 = scale.get(6);
        
        Fault root = faultTree.addFault("root");
        Fault f1 = faultTree.addFault("fault1");
        Fault f2 = faultTree.addFault("fault2");
        f2.setProbabilty(n2);
        Fault f11 = faultTree.addFault("fault11");
        Fault f12 = faultTree.addFault("fault12");
        Fault f13 = faultTree.addFault("fault13");
        Fault f111 = faultTree.addFault("fault111");
        f111.setProbabilty(n111);
        Fault f112 = faultTree.addFault("fault112");
        f112.setProbabilty(n112);
        Fault f121 = faultTree.addFault("fault121");
        f121.setProbabilty(n121);
        Fault f122 = faultTree.addFault("fault122");
        f122.setProbabilty(n122);
        Fault f131 = faultTree.addFault("fault131");
        f131.setProbabilty(n131);
        Fault f132 = faultTree.addFault("fault132");
        f132.setProbabilty(n132);

        LogicGate g0 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g1 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g2 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g11 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        LogicGate g12 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);
        LogicGate g13 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_AND);
        faultTree.addConnection(root, g0);
        faultTree.addConnection(g0, f1);
        faultTree.addConnection(g0, f2);
        faultTree.addConnection(f1, g1);
        faultTree.addConnection(f2, g2);
        faultTree.addConnection(g1, f11);
        faultTree.addConnection(g1, f12);
        faultTree.addConnection(g1, f13);
        faultTree.addConnection(f11, g11);
        faultTree.addConnection(f12, g12);
        faultTree.addConnection(f13, g13);
        faultTree.addConnection(g11, f111);
        faultTree.addConnection(g11, f112);
        faultTree.addConnection(g12, f121);
        faultTree.addConnection(g12, f122);
        faultTree.addConnection(g13, f131);
        faultTree.addConnection(g13, f132);
     
        FaultTreeOperations faultTreeOperations = new FaultTreeOperations(faultTree);
         Map<String, TopProbability> expectedResult = new HashMap<String,TopProbability>();
         f111.delete();
         expectedResult.put(f111.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g11, f111);
            f112.delete();
         expectedResult.put(f112.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g11, f112);
            f121.delete();
         expectedResult.put(f121.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g12, f121);
            f122.delete();
         expectedResult.put(f122.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g12, f122);
            f131.delete();
         expectedResult.put(f131.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g13, f131);
            f132.delete();
         expectedResult.put(f132.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g13, f132);
            f2.delete();
         expectedResult.put(f2.getName(), faultTreeOperations.getTopProbability(false));
         faultTree.addConnection(g1, f2);
         
         return expectedResult;
     }

    /**
     *
     * @param flf
     * @param leafName
     * @return
     * @throws Exception
     */
    public TopProbability CalculateTopWhithoutLeafBoth(FuzzyLogicFacade flf, String leafName) throws Exception
     {
         List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n111 =  scale.get(0);
        TriangleFunction n112 = scale.get(2);
        TriangleFunction n121 = scale.get(0);
        TriangleFunction n122 = scale.get(1);
        TriangleFunction n131 = scale.get(1);
        TriangleFunction n132 = scale.get(2);
        TriangleFunction n2 = scale.get(1);
        
        MembershipFunction r31, r32, r33;
        if("n111".equals(leafName))
            r31 = n112;
        else if("n112".equals(leafName))
            r31 = n111;
        else
            r31 = flf.multiply(n111, n112);
        
        if("n121".equals(leafName))
            r32 = n122;
        else if("n122".equals(leafName))
            r32 = n121;
        else
            r32 = flf.add(n121, n122);
        
        if("n131".equals(leafName))
            r33 = n132;
        else if("n132".equals(leafName))
            r33 = n131;
        else
        r33 = flf.multiply(n131, n132);
        
        MembershipFunction expectedResult = flf.multiply(r31, r32);
        expectedResult = flf.multiply(expectedResult, r33);
        if(!"n2".equals(leafName))
            expectedResult = flf.add(expectedResult, n2);
        TopProbability topProbability = new TopProbability();
        topProbability.topMembershipFunction = expectedResult;
        topProbability.setTopSimilarityValues(flf);
        return topProbability;
     }

    /**
     *
     * @param flf
     * @param leafName
     * @return
     * @throws Exception
     */
    public TopProbability CalculateTopWhithoutLeafOR(FuzzyLogicFacade flf, String leafName) throws Exception
     {
         List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n111 =  scale.get(0);
        TriangleFunction n112 = scale.get(2);
        TriangleFunction n121 = scale.get(0);
        TriangleFunction n122 = scale.get(1);
        TriangleFunction n131 = scale.get(1);
        TriangleFunction n132 = scale.get(2);
        TriangleFunction n2 = scale.get(1);
        
        MembershipFunction r31, r32, r33;
        if("n111".equals(leafName))
            r31 = n112;
        else if("n112".equals(leafName))
            r31 = n111;
        else
            r31 = flf.add(n111, n112);
        
        if("n121".equals(leafName))
            r32 = n122;
        else if("n122".equals(leafName))
            r32 = n121;
        else
            r32 = flf.add(n121, n122);
        
        if("n131".equals(leafName))
            r33 = n132;
        else if("n132".equals(leafName))
            r33 = n131;
        else
        r33 = flf.add(n131, n132);
        
        MembershipFunction expectedResult = flf.add(r31, r32);
        expectedResult = flf.add(expectedResult, r33);
        if(!"n2".equals(leafName))
            expectedResult = flf.add(expectedResult, n2);
        TopProbability topProbability = new TopProbability();
        topProbability.topMembershipFunction = expectedResult;
        topProbability.setTopSimilarityValues(flf);
        return topProbability;
     }

    /**
     *
     * @param flf
     * @param leafName
     * @return
     * @throws Exception
     */
    public TopProbability CalculateTopWhithoutLeafAND(FuzzyLogicFacade flf, String leafName) throws Exception
     {
         List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n111 =  scale.get(0);
        TriangleFunction n112 = scale.get(2);
        TriangleFunction n121 = scale.get(0);
        TriangleFunction n122 = scale.get(1);
        TriangleFunction n131 = scale.get(1);
        TriangleFunction n132 = scale.get(2);
        TriangleFunction n2 = scale.get(1);
        
        MembershipFunction r31, r32, r33;
        if("n111".equals(leafName))
            r31 = n112;
        else if("n112".equals(leafName))
            r31 = n111;
        else
            r31 = flf.multiply(n111, n112);
        
        if("n121".equals(leafName))
            r32 = n122;
        else if("n122".equals(leafName))
            r32 = n121;
        else
            r32 = flf.multiply(n121, n122);
        
        if("n131".equals(leafName))
            r33 = n132;
        else if("n132".equals(leafName))
            r33 = n131;
        else
        r33 = flf.multiply(n131, n132);
        
        MembershipFunction expectedResult = flf.multiply(r31, r32);
        expectedResult = flf.multiply(expectedResult, r33);
        if(!"n2".equals(leafName))
            expectedResult = flf.multiply(expectedResult, n2);
        TopProbability topProbability = new TopProbability();
        topProbability.topMembershipFunction = expectedResult;
        topProbability.setTopSimilarityValues(flf);
        return topProbability;
     }
    
             
      public void createSmallFaultTree(FaultTree faultTree) throws Exception {
        FuzzyLogicFacade flf = new FuzzyLogicFacade();
        flf.createTriangle(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR), "");
        flf.createTriangle(new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR), "");
        flf.createTriangle(new ParamTriangle(0.3, 0.4, 0.5, Scale.LINEAR), "");

        faultTree.setScale(flf);
        List<TriangleFunction> scale = flf.getTriangleFunctions();
        TriangleFunction n1 =  scale.get(0);
        TriangleFunction n2 = scale.get(1);
        TriangleFunction n3 = scale.get(2);

        Fault root = faultTree.addFault("root");
        Fault f1 = faultTree.addFault("fault1");
        Fault f2 = faultTree.addFault("fault2");
        Fault f3 = faultTree.addFault("fault3");
        f3.setProbabilty(n1);
        f2.setProbabilty(n2);
        f1.setProbabilty(n3);

        LogicGate g0 = faultTree.addLogicGate(LogicGate.LogicGateType.GATE_OR);

        faultTree.addConnection(root, g0);
        faultTree.addConnection(g0, f1);
        faultTree.addConnection(g0, f2);
        faultTree.addConnection(g0, f3);

    }
      
}
