package pl.wroc.pwr.sa.ft.FIT.Framework;

import fit.ColumnFixture;
import pl.wroc.pwr.sa.ft.Fault;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.ft.LogicGate;

/**
 *
 * @author n0npax
 */
public class UpdateConnectionTest extends ColumnFixture {
	private FaultTree f;
	private Fault f0, f1, f2, f3, f4, f5, f6, f7;
	private LogicGate l0, l1, l3;

	private LogicGate logicGateFound;
	private Fault faultFound;

    
    public String logicGateName,

    /**
     *
     */
    faultName;

    /**
     *
     */
    private int numberOfCheckConsistent = 0;

    /**
     *
     */
    public UpdateConnectionTest() {
		f = CreateConnectionTestTree.f;
		f0 = CreateConnectionTestTree.f0;
		f1 = CreateConnectionTestTree.f1;
		f2 = CreateConnectionTestTree.f2;
		f3 = CreateConnectionTestTree.f3;
		f4 = CreateConnectionTestTree.f4;
		f5 = CreateConnectionTestTree.f5;
		f6 = CreateConnectionTestTree.f6;
		f7 = CreateConnectionTestTree.f7;
		l0 = CreateConnectionTestTree.l0;
		l1 = CreateConnectionTestTree.l1;
		l3 = CreateConnectionTestTree.l3;
	}

    /**
     *
     * @return
     */
    public boolean isFaultFound() {
		if (f0.getName().equals(faultName)) {
			faultFound = f0;
			return true;
		}
		if (f1.getName().equals(faultName)) {
			faultFound = f1;
			return true;
		}
		if (f2.getName().equals(faultName)) {
			faultFound = f2;
			return true;
		}
		if (f3.getName().equals(faultName)) {
			faultFound = f3;
			return true;
		}
		if (f4.getName().equals(faultName)) {
			faultFound = f4;
			return true;
		}
		if (f5.getName().equals(faultName)) {
			faultFound = f5;
			return true;
		}
		if (f6.getName().equals(faultName)) {
			faultFound = f5;
			return true;
		}
		if (f7.getName().equals(faultName)) {
			faultFound = f5;
			return true;
		}

		return false;
	}

    /**
     *
     * @return
     */
    public boolean isLogicGateFound() {
		if (logicGateName.equals("l0")) {
			logicGateFound = l0;
			return true;
		}
		if (logicGateName.equals("l1")) {
			logicGateFound = l1;
			return true;
		}
		if (logicGateName.equals("l3")) {
			logicGateFound = l3;
			return true;
		}
		return false;
	}

    /**
     *
     * @return
     */
    public boolean updateConnection() {
		if (logicGateFound != null && faultFound != null) {
			f.updateConnection(faultFound, logicGateFound);
			logicGateFound = null;
			faultFound = null;
			return true;
		}
		return false;
	}

    /**
     *
     * @return
     */
    public boolean isTreeConsistent() {
		if (!f.getRoot().equals(f0))
			return false;
		if (!f0.getChild().equals(l0))
			return false;

		if (!l0.getChildren().contains(f1))
			return false;
		if (!l0.getChildren().contains(f2))
			return false;
		if (!l0.getChildren().contains(f3))
			return false;

		if (!f1.getParent().equals(l0))
			return false;
		if (!f2.getParent().equals(l0))
			return false;
		if (!f3.getParent().equals(l0))
			return false;

		if (f1.getChild() != null)
			return false;

		if (!l1.getParent().equals(f2))
			return false;
		if (!f2.getChild().equals(l1))
			return false;

		if (!l1.getChildren().contains(f4))
			return false;
		if (!l1.getChildren().contains(f5))
			return false;

		if (!f4.getParent().equals(l1))
			return false;
		if (!f5.getParent().equals(l1))
			return false;

		if (!f3.getChild().equals(l3))
			return false;
		if (!l3.getParent().equals(f3))
			return false;

		if (!l3.getChildren().contains(f6))
			return false;
		if (!l3.getChildren().contains(f7))
			return false;

		if (!f6.getParent().equals(l3))
			return false;
		if (!f7.getParent().equals(l3))
			return false;

		return true;
	}
}
