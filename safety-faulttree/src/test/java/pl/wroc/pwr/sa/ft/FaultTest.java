/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.sa.ft;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import pl.wroc.pwr.sa.ft.Fault;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.ft.LogicGate;

/**
 * 
 * @author Anna
 */
public class FaultTest {

    /**
     *
     */
    public FaultTest() {

	}

	/**
	 * Test of SetName method, of class Fault.
	 */
	@Test
	public void testSetName() {
	}

	/**
	 * Test of GetName method, of class Fault.
	 */
	@Test
	public void testGetName() {
		Fault fault = new Fault("");
		assertEquals("", fault.getName());

		fault.setName("fault1");
		assertEquals("fault1", fault.getName());

		Fault fault1 = new Fault("fault1");
		fault1.setName("fault");
		assertEquals("fault", fault1.getName());
	}

	/**
	 * Test of SetDescription method, of class Fault.
	 */
	@Test
	public void testSetDescription() {
	}

	/**
	 * Test of GetDescription method, of class Fault.
	 */
	@Test
	public void testGetDescription() {
		Fault fault = new Fault("");
		assertEquals(null, fault.getDescription());

		fault.setDescription("To jest pierwszy fault");
		assertEquals("To jest pierwszy fault", fault.getDescription());
	}

	/**
	 * Test of SetParent method, of class Fault.
	 */
	@Test
	public void testSetParent() {
	}

	/**
	 * Test of SetChild method, of class Fault.
	 */
	@Test
	public void testSetChild() {
	}

	/**
	 * Test of GetChild method, of class Fault.
	 */
	@Test
	public void testGetChild() {
		Fault fault = new Fault("");
		assertNull(fault.getChild());

		LogicGate logicGate = new LogicGate(LogicGate.LogicGateType.GATE_AND);
		fault.setChild(logicGate);
		assertEquals(logicGate, fault.getChild());
	}

	/**
	 * Test of GetParent method, of class Fault.
	 */
	@Test
	public void testGetParent() {
		Fault fault = new Fault("");
		assertNull(fault.getParent());

		LogicGate logicGate = new LogicGate(LogicGate.LogicGateType.GATE_AND);
		fault.setParent(logicGate);
		assertEquals(logicGate, fault.getParent());
	}

	/**
	 * Test of SetProbability method, of class Fault.
	 */
	@Test
	public void testSetProbability() {
	}

	/**
	 * Test of GetProbability method, of class Fault.
	 */
	@Test
	public void testGetProbability() {

	}

	/**
	 * Test of SetFaultTree method, of class Fault.
	 */
	@Test
	public void testSetFaultTree() {
	}

	/**
	 * Test of GetFaultTree method, of class Fault.
	 */
	@Test
	public void testGetFaultTree() {
		Fault fault = new Fault("");
		assertNull(fault.getFaultTree());

		FaultTree faultTree = new FaultTree();
		fault.setFaultTree(faultTree);
		assertEquals(faultTree, fault.getFaultTree());
	}

	/**
	 * Test of Delete method, of class Fault.
	 */
	@Test
	public void testDelete() {
		FaultTree tree = new FaultTree();

		Fault fault = tree.addFault("fault");
		LogicGate logicGate = new LogicGate(LogicGate.LogicGateType.GATE_AND);
		LogicGate logicGate2 = new LogicGate(LogicGate.LogicGateType.GATE_AND);
		Fault fault2 = tree.addFault("fault2");

		fault.setParent(logicGate);
		fault2.setParent(logicGate);
		logicGate.getChildren().add(fault);
		logicGate.getChildren().add(fault2);

		fault.setChild(logicGate2);
		logicGate2.setParent(fault);

		fault.delete();

		assertNull(logicGate2.getParent());
		assertFalse(logicGate.getChildren().contains(fault));
		assertTrue(logicGate.getChildren().contains(fault2));

		assertNull(fault.getParent());
		assertNull(fault.getChild());
	}
}