/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.sa.ft;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import pl.wroc.pwr.sa.ft.Fault;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.ft.LogicGate;

/**
 * 
 * @author Anna
 */
public class LogicGateTest {

    /**
     *
     */
    public LogicGateTest() {
	}

	/**
	 * Test of SetType method, of class LogicGate.
	 */
	@Test
	public void testSetType() {
	}

	/**
	 * Test of GetType method, of class LogicGate.
	 */
	@Test
	public void testGetType() {
		LogicGate logicGate = new LogicGate(null);
		assertEquals(LogicGate.LogicGateType.GATE_AND, logicGate.GetType());

		logicGate.setType(LogicGate.LogicGateType.GATE_AND);
		assertEquals(LogicGate.LogicGateType.GATE_AND, logicGate.GetType());

		logicGate.setType(LogicGate.LogicGateType.GATE_OR);
		assertEquals(LogicGate.LogicGateType.GATE_OR, logicGate.GetType());
	}

	/**
	 * Test of GetChildren method, of class LogicGate.
	 */
	@Test
	public void testGetChildren() {
		LogicGate logicGate = new LogicGate(LogicGate.LogicGateType.GATE_AND);
		assertTrue(logicGate.getChildren().isEmpty());

		Fault fault = new Fault("fault");
		logicGate.getChildren().add(fault);
		assertTrue(logicGate.getChildren().contains(fault));

		Fault fault2 = new Fault("");
		logicGate.getChildren().add(fault2);
		assertTrue(logicGate.getChildren().contains(fault2));
	}

	/**
	 * Test of SetParent method, of class LogicGate.
	 */
	@Test
	public void testSetParent() {
	}

	/**
	 * Test of GetParent method, of class LogicGate.
	 */
	@Test
	public void testGetParent() {
		LogicGate logicGate = new LogicGate(null);
		assertNull(logicGate.getParent());

		Fault fault = new Fault("fault");
		logicGate.setParent(fault);
		assertEquals(fault, logicGate.getParent());

		logicGate.setParent(null);
		assertNull(logicGate.getParent());
	}

	/**
	 * Test of SetFaultTree method, of class LogicGate.
	 */
	@Test
	public void testSetFaultTree() {
	}

	/**
	 * Test of GetFaultTree method, of class LogicGate.
	 */
	@Test
	public void testGetFaultTree() {
		LogicGate logicGate = new LogicGate(LogicGate.LogicGateType.GATE_AND);
		assertNull(logicGate.getFaultTree());

		FaultTree faultTree = new FaultTree();
		logicGate.setFaultTree(faultTree);
		assertEquals(faultTree, logicGate.getFaultTree());
	}

	/**
	 * Test of Delete method, of class LogicGate.
	 */
	@Test
	public void testDelete() {
		LogicGate logicGate = new LogicGate(LogicGate.LogicGateType.GATE_AND);
		Fault fault = new Fault("fault");
		Fault fault2 = new Fault("fault2");
		Fault fault3 = new Fault("fault3");
		logicGate.setParent(fault);
		fault.setChild(logicGate);
		logicGate.getChildren().add(fault2);
		logicGate.getChildren().add(fault3);
		fault2.setParent(logicGate);
		fault3.setParent(logicGate);
		logicGate.delete();

		assertNull(fault.getChild());
		assertNull(logicGate.getParent());
		assertFalse(logicGate.getChildren().contains(fault2));
		assertFalse(logicGate.getChildren().contains(fault3));
		assertNull(fault2.getParent());
		assertNull(fault3.getParent());
	}
}