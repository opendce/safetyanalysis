package pl.wroc.pwr.sa.ft;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;

import pl.wroc.pwr.sa.fr.FuzzyLogicFacade;

/**
 *
 * @author n0npax
 */
@XmlRootElement(namespace = "pl.wroc.pw.sa.ft.FaultTree")

public class FaultTree {
	private Fault root = null;

	private Map<String, Fault> faultMap = new HashMap<String, Fault>();
	private TopProbability topProbability;
	private FuzzyLogicFacade scale;
	private List<Fault> leafs = new ArrayList<Fault>();

    /**
     *
     */
    public FaultTree() {
	}

    public static FaultTree fromXml(String xml) throws JAXBException {
	    JAXBContext jaxbContext = JAXBContext.newInstance(FaultTree.class);
	    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
	    StringReader reader = new StringReader(xml);
	    return  (FaultTree) unmarshaller.unmarshal(reader);
    }

    /**
     *
     * @param root
     */
    public void setRoot(Fault root) {
		this.root = root;
	}

    /**
     *
     * @return
     */
    public Fault getRoot() {
		return root;
	}

    /**
     *
     * @param name
     * @return
     */
    public Fault addFault(String name) {
		Fault fault = null;
		
		if(!this.faultMap.containsKey(name)) {
			fault = new Fault(name);
			fault.setFaultTree(this);
			this.faultMap.put(fault.getName(), fault);
			if(this.root == null)
				this.root = fault;
		}
		return fault;
	}

    /**
     *
     * @return
     */
    public Map<String, Fault> getFaultMap() {
		return faultMap;
	}
    
        public void setFaultMap(Map<String, Fault> x) {
		this.faultMap=x;
	}

    /**
     *
     * @param type
     * @return
     */
    public LogicGate addLogicGate(LogicGate.LogicGateType type) {
		LogicGate gate = new LogicGate(type);
		gate.setFaultTree(this);
		return gate;
	}

    /**
     *
     * @param parent
     * @param child
     */
    public void addConnection(Fault parent, LogicGate child) {
		if (parent.getChild() != null || child.getParent() != null) {
			updateConnection(parent, child);
		} else {
			parent.setChild(child);
			child.setParent(parent);
		}
	}

    /**
     *
     * @param parent
     * @param child
     */
    public void addConnection(LogicGate parent, Fault child) {
		if (child.getParent() != null) {
			updateConnection(parent, child);
		} else {
			if(faultMap.get(child.getName()) == null)
				faultMap.put(child.getName(), child);
			parent.getChildren().add(child);
			child.setParent(parent);
		}
	}

    /**
     *
     * @param parent
     * @param child
     */
    public void updateConnection(Fault parent, LogicGate child) {
		if (parent.getChild() != null)
			deleteConnection(parent, parent.getChild());
		if (child.getParent() != null)
			deleteConnection(child.getParent(), child);
		addConnection(parent, child);
	}

    /**
     *
     * @param parent
     * @param child
     */
    public void updateConnection(LogicGate parent, Fault child) {
		deleteConnection(child, child.getParent());
		addConnection(parent, child);
	}

    /**
     *
     * @param fault
     * @param logicGate
     */
    public void deleteConnection(Fault fault, LogicGate logicGate) {
		if (fault.getChild() != null) {
			if (fault.getChild().equals(logicGate)) {
				fault.setChild(null);
				logicGate.setParent(null);
			}
		} else if (logicGate.getChildren().contains(fault)) {
			logicGate.getChildren().remove(fault);
			fault.setParent(null);
		}
	}

    /**
     *
     */
    public void updateLeafsList(){
		leafs.clear();
		updateLeafs(this.root);
	}
	
	private void updateLeafs(Fault f){
    	LogicGate logicGate = f.getChild();
    	if(logicGate!=null){
    		List<Fault> children = logicGate.getChildren();
    		for (Fault ch : children) {
				updateLeafs(ch);
			}
    	}else
    		leafs.add(f);
    }

    /**
     *
     * @return
     */
    public List<Fault> getLeafsList(){
		return this.leafs;
	}

        public void setLeafsList(List<Fault> x){
		this.leafs=x;
	}
    /**
     *
     * @return
     */
    public TopProbability getTopProbability(){
		return this.topProbability;
	}

    /**
     *
     * @param t
     */
    public void setTopProbability(TopProbability t){
		this.topProbability = t;
	}

    /**
     *
     * @return
     */
    public FuzzyLogicFacade getScale() {
		return scale;
	}

    /**
     *
     * @param scale
     */
    public void setScale(FuzzyLogicFacade scale) {
		this.scale = scale;
	}
}
