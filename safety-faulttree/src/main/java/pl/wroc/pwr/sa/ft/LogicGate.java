package pl.wroc.pwr.sa.ft;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author n0npax
 */
@XmlRootElement(namespace = "pl.wroc.pw.sa.ft.LogicGate")

public class LogicGate {

    /**
     *
     */
    public static enum LogicGateType {

        GATE_OR,
        GATE_AND
	}

        private LogicGateType logicGateType = LogicGateType.GATE_AND;
        
        @XmlTransient
	private List<Fault> children = new ArrayList<Fault>();
        //@XmlTransient
	private Fault parent = null;
        @XmlTransient
	private FaultTree faultTree = null;

    /**
     *
     * @param logicGateType
     */
    public LogicGate(LogicGateType logicGateType) {
		if (logicGateType != null)
			this.logicGateType = logicGateType;
	}
    
     /**
     *
     * @param no param for xml
     */
    public LogicGate() {
        this.logicGateType = LogicGateType.GATE_OR;
    }

    /**
     *
     * @param logicGateType
     */
    public void setType(LogicGateType logicGateType) {
		if (logicGateType != null)
			this.logicGateType = logicGateType;
	}

    /**
     *
     * @return
     */
    public LogicGateType GetType() {
		return logicGateType;
	}

    public void setLogicGateType(LogicGateType lgt)
    {
        this.logicGateType=lgt;
    }
    
    public LogicGateType getLogicGateType()
    {
        return logicGateType;
    }
    /**
     *
     * @return
     */
    @XmlTransient
    public List<Fault> getChildren() {
		return children;
	}
    
    public void getChildren(List<Fault> x) {
            this.children=x;
    }

    /**
     *
     * @param parent
     */
    public void setParent(Fault parent) {
		this.parent = parent;
	}

    /**
     *
     * @return
     */
    public Fault getParent() {
		return parent;
	}

    /**
     *
     * @param faultTree
     */
    public void setFaultTree(FaultTree faultTree) {
		this.faultTree = faultTree;
	}
    /**
     *
     * @return
     */
    @XmlTransient
    public FaultTree getFaultTree() {
		return faultTree;
	}

    /**
     *
     */
    public void delete() {
		if (this.parent != null) {
			this.parent.setChild(null);
			this.parent = null;
		}
		if (this.children != null) {
			for (Fault ch : this.children) {
				ch.setParent(null);
			}
			this.children.clear();
		}
	}
}
