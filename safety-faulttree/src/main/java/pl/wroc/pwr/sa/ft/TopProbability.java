package pl.wroc.pwr.sa.ft;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

import pl.wroc.pwr.sa.fr.FuzzyLogicFacade;
import pl.wroc.pwr.sa.fr.MembershipFunction;

/**
 *
 * @author n0npax
 */
@XmlRootElement(namespace = "pl.wroc.pw.sa.ft.TopProbability")
public class TopProbability {

    /**
     *
     */
    public MembershipFunction topMembershipFunction;

    /**
     *
     */
    public Map<MembershipFunction, Double> topSimilarityValues = new HashMap<MembershipFunction,Double>();

    /**
     *
     * @param a
     * @throws Exception
     */
    public void setTopSimilarityValues(FuzzyLogicFacade a) throws Exception{
		if(topMembershipFunction==null || a==null)
			return;
		

		//Mozna dodac do FaultTree z gory, jakiego jest typu drzewem, 
		//wtedy nie bedzie trzeba sprawdzac tego tak
		List<?> l = null;
        //obiekt a nie spełnia tutaj żadnego warunku, dlatego funkcja wyrzuca null
		if(a.getTrapezoidFunctions().size() > 0)
			l = a.getTrapezoidFunctions();
		else if(a.getTriangleFunctions().size() > 0)
			l = a.getTriangleFunctions();
		
                MembershipFunction membershipFunction;
        try{
		    Iterator<MembershipFunction> iterator = (Iterator<MembershipFunction>) l.iterator();
            FuzzyLogicFacade fuzzyLogicFacade = new FuzzyLogicFacade();

		    while(iterator.hasNext()){
                    membershipFunction = iterator.next();
                    topSimilarityValues.put(membershipFunction, fuzzyLogicFacade.getJacardSimilarity(membershipFunction, topMembershipFunction));// getJacardSimilarity(topMembershipFunction, membershipFunction));
		    }
        }
        catch (Exception e){
            System.out.println("\nWystapil blad w TopProbability, w setTopSimilarityValues:" + e);
        }

		
	}
}
