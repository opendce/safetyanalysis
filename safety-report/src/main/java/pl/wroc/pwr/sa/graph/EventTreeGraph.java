/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.sa.graph;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import pl.wroc.pwr.sa.et.Edge;
import pl.wroc.pwr.sa.et.Event;
import pl.wroc.pwr.sa.et.EventTree;



/**
 *
 * @author Robert Juzak
 */
public class EventTreeGraph {
    private final int EVENT_COLUMN_WIDTH = 200;
    private final int EVENT_COLUMT_HEIGHT_BASE = 80;
    private final int EVENT_TEXT_HEIGHT = 50;
    private final int MARGIN = 10;
    
    /**
     * Returns a BufferedImage object with Event Tree graph that can be placed in a report.
     * 
     * @param eventTree the Event Tree of which graph will be drawn
     * @return          a BuffedImage with the graph
     * @see             BufferedImage
     */
    public BufferedImage createEventTreeGraphImage(EventTree eventTree) {
        BufferedImage image = null;
        Edge initialBranch = null;
        
        if (eventTree != null) {
            initialBranch = eventTree.getInitialBranch();
            
            int eventsCount = eventTree.getEvents().size();
            //calculating image size
            int width = EVENT_COLUMN_WIDTH * eventsCount + 2*MARGIN;
            int height = EVENT_COLUMT_HEIGHT_BASE * eventsCount + 2*MARGIN + EVENT_TEXT_HEIGHT;
            
            //eventHeight is the initial height calculated on the base of eventsCount. Edges that belong to next edges are half to height. That gives a nice regular efect.
            int eventHeight = EVENT_COLUMT_HEIGHT_BASE * eventsCount;

            //creating image to draw on it
            image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2d = image.createGraphics();
            
            //backgroud color
            g2d.setColor(Color.white);
            g2d.fillRect(0, 0, width, height);
            
            //draw everything in black
            g2d.setColor(Color.black);
            if (initialBranch != null) {           
                //
                //drawing events
                //
                //start drawing with a small margin
                int x = MARGIN;
                int y = MARGIN ;
                
                Event event = initialBranch.getEvent();
                while (event != null)
                {
                    //drawing rectangle that represent the event
                    g2d.drawRect(x, y, EVENT_COLUMN_WIDTH, EVENT_TEXT_HEIGHT);
                    
                    //drawing the event name inside the rect (no length check!)
                    g2d.drawString(event.getName(), x+MARGIN, y+MARGIN+EVENT_TEXT_HEIGHT/2);
                    
                    //drawing vertical line that represent the event border
                    //g2d.drawLine(x+EVENT_COLUMN_WIDTH, y+ EVENT_TEXT_HEIGHT, x+EVENT_COLUMN_WIDTH, y+ EVENT_TEXT_HEIGHT+eventHeight);
                   
                    //move to the next event
                    x+= EVENT_COLUMN_WIDTH;               
                    event = event.getChild();
                }
                
                //
                //drawing edges
                //
                //edges are drawed below the event rectangle
                x = MARGIN;
                y = eventHeight/2 + EVENT_TEXT_HEIGHT;
                
                //start from inirial branch and draw the edges recuscive
                Edge edge = initialBranch;
                drawEdge(x, y, edge, g2d,(y-MARGIN-EVENT_TEXT_HEIGHT)/2, eventsCount);
                
                g2d.dispose();
            }
        }
        return image;
    }
    
    private void drawEdge(int x, int y, Edge edge, Graphics2D g2d, int edgeHeight, int level) {
        
        if (edge.isLeaf()) {
            //if edge is a leaf, draw the edge to the end of the graph
            g2d.drawLine(x, y, x+ EVENT_COLUMN_WIDTH*level, y);
            
        } else {
            
            //draw the horizonlat line representing the edge
            g2d.drawLine(x, y, x+EVENT_COLUMN_WIDTH, y);
            
            //draw the vertical line for edge chindren
            g2d.drawLine(x+EVENT_COLUMN_WIDTH, y+edgeHeight, x+EVENT_COLUMN_WIDTH, y-edgeHeight);
            
            //draw add children edges (2 edges expected. when no edge on damaged edge, nothing happens, but the vertical line stays)
            for ( Edge e : edge.getChild() ) {
                if (e != null){
                    if (e.getType()) {
                        //draw true edge
                        //move to the end of the event column (EVENT_COLUMN_WIDTH), set the next edge height as the half of present height. it gives a nice regular effect
                        drawEdge(x+EVENT_COLUMN_WIDTH, y-edgeHeight, e, g2d,edgeHeight/2, level-1);
                    } else {
                        //draw false edge
                        drawEdge(x+EVENT_COLUMN_WIDTH, y+edgeHeight, e, g2d,edgeHeight/2, level-1);
                    }
                }
            }
        }
        
        //drawing edge type as string
        String type = edge.getType() ? "true" : "false" ;
        g2d.drawString(type, x+EVENT_COLUMN_WIDTH/2, y-5);
        
        //drawing edge propability as string
        if (edge.getProbability() != null) {
            g2d.drawString(edge.getProbability().toString(), x+EVENT_COLUMN_WIDTH/2, y+5);
        }
        
    }
}

