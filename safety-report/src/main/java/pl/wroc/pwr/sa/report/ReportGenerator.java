package pl.wroc.pwr.sa.report;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

import org.odftoolkit.simple.TextDocument;
import org.odftoolkit.simple.draw.FrameRectangle;
import org.odftoolkit.simple.draw.FrameStyleHandler;
import org.odftoolkit.simple.draw.Image;
import org.odftoolkit.simple.style.Font;
import org.odftoolkit.simple.style.StyleTypeDefinitions;
import org.odftoolkit.simple.style.StyleTypeDefinitions.AnchorType;
import org.odftoolkit.simple.style.StyleTypeDefinitions.FrameVerticalPosition;
import org.odftoolkit.simple.text.Paragraph;

/**
 *
 * @author Robert Juzak
 */
public class ReportGenerator {

    private TextDocument report;

    /**
     * Prepare a report with empty date, logo and title paragraph
     *
     * @see #setDate(java.util.Date) 
     * @see #setLogo(java.lang.String)
     * @see #setTitle(java.lang.String)
     */
    public ReportGenerator() {
        try {
            report = TextDocument.newTextDocument();

            report.addParagraph(" ");
            report.addParagraph(" ");
            report.addParagraph(" ");
        } catch (Exception ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Prepare a report with given date, logo and title paragraph
     *
     * @param creationDate the date that would by set in the right corner
     * @param imagePath the path to the logo image
     * @param title the title ot the report
     */
    public ReportGenerator(Date creationDate, String imagePath, String title) {

        try {
            //creating a new document
            report = TextDocument.newTextDocument();

            //date paragraph
            report.addParagraph(" ");
            setDate(creationDate);

            //creating an PWr image
            report.addParagraph(" ");
            setLogo(imagePath);

            //setting a centered title
            report.addParagraph(" ");
            setTitle(title);

        } catch (Exception ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Creates a odt file with the report.
     *
     * @param filePath the output file path and name
     */
    public void save(String filePath) {
        try {
            report.save(filePath);
        } catch (Exception ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Sets the report creation date. The date is placed in the top right
     * corner.
     *
     * @param date the reports generation date
     * @see Date
     */
    public void setDate(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        Paragraph dateParagraph = report.getParagraphByIndex(0, true);
        dateParagraph.setTextContent(dateFormat.format(date) + "\n");
        dateParagraph.setHorizontalAlignment(StyleTypeDefinitions.HorizontalAlignmentType.RIGHT);

        Font dateParagraphFont = dateParagraph.getFont();
        dateParagraphFont.setSize(12);
        dateParagraphFont.setFamilyName("Calibri");

        dateParagraph.setFont(dateParagraphFont);
    }

    /**
     * Sets the report logo. The image is placed centrally below the date and
     * uppon the title.
     *
     * @param imagePath the path to the image
     */
    public void setLogo(String imagePath) {
        try {
            Paragraph imageParagraph = report.getParagraphByIndex(1, true);
            Image reportImage = Image.newImage(imageParagraph, new URI(imagePath));
            reportImage.setRectangle(new FrameRectangle(0, 0, 15, 3, StyleTypeDefinitions.SupportedLinearMeasure.CM));

        } catch (URISyntaxException ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Sets the title of the report. The title in centered below the logo.
     *
     * @param title the reports title
     */
    public void setTitle(String title) {
        Paragraph titleParagraph = report.getParagraphByIndex(2, true);

        titleParagraph.setTextContent("\n" + title + "\n");
        titleParagraph.setHorizontalAlignment(StyleTypeDefinitions.HorizontalAlignmentType.CENTER);

        Font titleParagraphFont = titleParagraph.getFont();
        titleParagraphFont.setSize(22);
        titleParagraphFont.setFontStyle(StyleTypeDefinitions.FontStyle.BOLD);
        titleParagraphFont.setFamilyName("Calibri");

        titleParagraph.setFont(titleParagraphFont);
    }

    /**
     * Sets the result of the readEvetTreeFromFile method.
     *
     * @param result the result of event tree loading
     */
    public void setReadEventTreeFromFileResult(String result) {
        report.addParagraph("\nRead Event Tree From File Result" + result);

        Paragraph readEventTreeFromFileResultParagraph = report.getParagraphByReverseIndex(0, true);

        Font readEventTreeFromFileResultParagraphFont = readEventTreeFromFileResultParagraph.getFont();
        readEventTreeFromFileResultParagraphFont.setFamilyName("Calibri");

        readEventTreeFromFileResultParagraph.setFont(readEventTreeFromFileResultParagraphFont);
    }

    /**
     * Add an image to the last added paragraph
     *
     * @param image an BufferedImage to add to the last added paragraph
     * @see BufferedImage
     */
    public void addImageToLastAddedParagraph(BufferedImage image) {
        if (image != null) {
            Paragraph eventTreeImageParagraph = report.getParagraphByReverseIndex(0, true);

            //creating a tempolary file on disk
            File tmp = new File("tmp.png");
            try {
                ImageIO.write(image, "png", tmp);

                //setting the image into the document
                Image treeImage = Image.newImage(eventTreeImageParagraph, new URI("tmp.png"));
                treeImage.setVerticalPosition(FrameVerticalPosition.TOP);

                FrameStyleHandler handler = treeImage.getStyleHandler();
                handler.setAchorType(AnchorType.AS_CHARACTER);

                //deleting the teprorary file
                tmp.delete();
            } catch (IOException ex) {
                Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
            } catch (URISyntaxException ex) {
                Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Sets the result of cehesionCheck method.
     *
     * @param result the result of the event tree cehesion check
     */
    public void setCohesionCheckResult(String result) {
        report.addParagraph("\nCohesion Check Result" + result);

        Paragraph cohesionCheckResultParagraph = report.getParagraphByReverseIndex(0, true);

        Font cohesionCheckResultParagraphFont = cohesionCheckResultParagraph.getFont();
        cohesionCheckResultParagraphFont.setFamilyName("Calibri");

        cohesionCheckResultParagraph.setFont(cohesionCheckResultParagraphFont);
    }

    /**
     * Sets the result of eventTreeScenarioCounter method.
     *
     * @param result the result of evet tree scenario counter
     */
    public void setEventTreeScenarioCounterResult(String result) {
        report.addParagraph("\nEvent Tree Scenario Counter Result" + result);

        Paragraph eventTreeScenarioCounterResultParagraph = report.getParagraphByReverseIndex(0, true);

        Font eventTreeScenarioCounterResultParagraphFont = eventTreeScenarioCounterResultParagraph.getFont();
        eventTreeScenarioCounterResultParagraphFont.setFamilyName("Calibri");

        eventTreeScenarioCounterResultParagraph.setFont(eventTreeScenarioCounterResultParagraphFont);

    }

    /**
     * Sets the result of the readFaultTreeFromFile method.
     *
     * @param result the result of fault tree loading
     */
    public void setReadFaultTreeFromFileResult(String result) {
        report.addParagraph("\nRead Fault Tree From File Result" + result);

        Paragraph readFaultTreeFromFileResultParagraph = report.getParagraphByReverseIndex(0, true);

        Font readFaultTreeFromFileResultParagraphFont = readFaultTreeFromFileResultParagraph.getFont();
        readFaultTreeFromFileResultParagraphFont.setFamilyName("Calibri");

        readFaultTreeFromFileResultParagraph.setFont(readFaultTreeFromFileResultParagraphFont);
    }

    /**
     * Sets the result of analysisTree method.
     *
     * @param result the result of the fault tree analysis
     */
    public void setAnalysisResult(String result) {
        report.addParagraph("\nFault Tree Analysis Result" + result);

        Paragraph analysisResultParagraph = report.getParagraphByReverseIndex(0, true);

        Font analysisResultParagraphFont = analysisResultParagraph.getFont();
        analysisResultParagraphFont.setFamilyName("Calibri");

        analysisResultParagraph.setFont(analysisResultParagraphFont);

    }

    /**
     * Sets the result of getFuzzyImportanceIndex method.
     *
     * @param result the string with fuzzy importance result
     */
    public void setFuzzyImportanceIndexResult(String result) {
        report.addParagraph("\nFuzzy Importance Result" + result);

        Paragraph fuzzyImportanceResultParagraph = report.getParagraphByReverseIndex(0, true);

        Font fuzzyImportanceResultParagraphFont = fuzzyImportanceResultParagraph.getFont();
        fuzzyImportanceResultParagraphFont.setFamilyName("Calibri");

        fuzzyImportanceResultParagraph.setFont(fuzzyImportanceResultParagraphFont);
    }
}
