package pl.wroc.pwr.sa.graph;

import java.awt.image.BufferedImage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.ft.FaultTree;

/**
 *
 * @author robjuz
 */
public class GraphCreatorTest {

    EventTreeGraph eventTreeGraph;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        eventTreeGraph = new EventTreeGraph();
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of createEventTreeGraphImage method, of class GraphCreator.
     */
    @Test
    public void testCreateEventTreeGraphImage() throws Exception {
        //null pointer case
        EventTree eventTree = null;
        BufferedImage expResult = null;
        BufferedImage result = eventTreeGraph.createEventTreeGraphImage(eventTree);
        assertEquals(expResult, result);
        assertNull(result);
        
        //empty tree case
        eventTree = new EventTree();
        result = eventTreeGraph.createEventTreeGraphImage(eventTree);
        assertNotNull(result);
        
        //valid event tree case
        eventTree = EventTree.loadFromXML("testEventTree.xml");
        expResult = null;
        result = eventTreeGraph.createEventTreeGraphImage(eventTree);
        assertNotNull(result);
    }
}
