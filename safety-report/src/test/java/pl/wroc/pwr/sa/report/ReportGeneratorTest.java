/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.sa.report;

import java.awt.image.BufferedImage;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.odftoolkit.simple.TextDocument;
import org.odftoolkit.simple.text.Paragraph;

/**
 *
 * @author Mati
 */
public class ReportGeneratorTest {
    
    TextDocument report;
    String filePath;
    
    public ReportGeneratorTest() throws Exception 
    {
        report = TextDocument.newTextDocument();
        filePath = null;
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createFile method, of class ReportGenerator.
     */
    @Ignore
    public void testCreateFile() throws Exception {
        filePath = "test.odt";
        report = TextDocument.newTextDocument();

        ReportGenerator instance = new ReportGenerator();
        instance.save(filePath);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDate method, of class ReportGenerator.
     */
    @Test
    public void testSetDate() {
//        System.out.println("setDate");
        String date = "2015/03/12";
        report.addParagraph(date);
        
        Paragraph dateParagraph = report.getParagraphByIndex(0, true);

        assertEquals(is("2015/03/12").toString(),is(dateParagraph.getTextContent()).toString());
     
    }

    /**
     * Test of setTitle method, of class ReportGenerator.
     */
    @Test
    public void testSetTitle() {
      //  System.out.println("setTitle");
        String title = "Safety Analysis\nReport";
        report.addParagraph(title);
        
        Paragraph titleParagraph = report.getParagraphByIndex(0, true);
        
        assertEquals(is("Safety Analysis\\nReport").toString(), is(titleParagraph.getTextContent()).toString());
    }

    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////
    /**
     * Test of setReadEventTreeFromFileResult method, of class ReportGenerator.
     */
    @Test
    public void testSetReadEventTreeFromFileResult() {
      //  System.out.println("setReadEventTreeFromFileResult");
        String result = "\ntest tekst";
        report.addParagraph("\nRead Event Tree From File Result " + result);
        
        Paragraph readEventTreeFromFileResultParagraph = report.getParagraphByReverseIndex(0, true);
        
        assertEquals(is("\\nRead Event Tree From File Result \\ntest tekst").toString(), is(readEventTreeFromFileResultParagraph.getTextContent()).toString());
    }

    /**
     * Test of drawEventTree method, of class ReportGenerator.
     */
    @Ignore
    public void addImageToCurrentParagraph(BufferedImage image){
        System.out.println("add image to paragraph");
        ReportGenerator instance = new ReportGenerator();
        instance.addImageToLastAddedParagraph(image);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCohesionCheckResult method, of class ReportGenerator.
     */
    @Test
    public void testSetCohesionCheckResult() {
      //  System.out.println("setCohesionCheckResult");
        String result = "\ntest tekst";
        report.addParagraph("\nCohesion Check Result " + result);
        
        Paragraph cohesionCheckResultParagraph = report.getParagraphByReverseIndex(0, true);
        
        assertEquals(is("\\nCohesion Check Result \\ntest tekst").toString(), is(cohesionCheckResultParagraph.getTextContent()).toString());
    }

    /**
     * Test of setEventTreeScenarioCounterResult method, of class ReportGenerator.
     */
    @Test
    public void testSetEventTreeScenarioCounterResult() {
      //  System.out.println("setEventTreeScenarioCounterResult");
        String result = "\ntest tekst";
        report.addParagraph("\nEvent Tree Scenario Counter Result " + result);
        
        Paragraph eventTreeScenarioCounterResultParagraph = report.getParagraphByReverseIndex(0, true);
        
        assertEquals(is("\\nEvent Tree Scenario Counter Result \\ntest tekst").toString(), is(eventTreeScenarioCounterResultParagraph.getTextContent()).toString());
    }

    /**
     * Test of setReadFaultTreeFromFileResult method, of class ReportGenerator.
     */
    @Test
    public void testSetReadFaultTreeFromFileResult() {
      //  System.out.println("setReadFaultTreeFromFileResult");
        String result = "\ntest tekst";
        report.addParagraph("\nRead Fault Tree From File Result " + result);
        
        Paragraph readFaultTreeFromFileResultParagraph = report.getParagraphByReverseIndex(0, true);
        
        assertEquals(is("\\nRead Fault Tree From File Result \\ntest tekst").toString(), is(readFaultTreeFromFileResultParagraph.getTextContent()).toString());
    }

    /**
     * Test of setAnalysisResult method, of class ReportGenerator.
     */
    @Test
    public void testSetAnalysisResult() {
      //  System.out.println("setAnalysisResult");
        String result = "\ntest tekst";
        report.addParagraph("\nFault Tree Analysis Result " + result);
        
        Paragraph analysisResultParagraph = report.getParagraphByReverseIndex(0, true);
        
        assertEquals(is("\\nFault Tree Analysis Result \\ntest tekst").toString(), is(analysisResultParagraph.getTextContent()).toString());
    }

    /**
     * Test of setFuzzyImportanceIndexResult method, of class ReportGenerator.
     */
    @Test
    public void testSetFuzzyImportanceIndexResut() {
      //  System.out.println("setFuzzyImportanceIndexResult");
        String result = "\ntest tekst";
        report.addParagraph("\nFuzzy Importance Result " + result);
        
        Paragraph fuzzyImportanceResultParagraph = report.getParagraphByReverseIndex(0, true);
        
        assertEquals(is("\\nFuzzy Importance Result \\ntest tekst").toString(), is(fuzzyImportanceResultParagraph.getTextContent()).toString());
    }
    
}
