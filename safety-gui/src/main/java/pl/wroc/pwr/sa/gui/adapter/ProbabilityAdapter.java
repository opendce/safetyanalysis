package pl.wroc.pwr.sa.gui.adapter;

import java.io.Serializable;
import pl.wroc.pwr.sa.et.Probability;

/**
 *
 * @author babaj
 */
public class ProbabilityAdapter implements Serializable {
    private String name;
    private Probability probability;
    
    public Probability getProbability() {
        return probability;
    }

    public void setProbability(Probability probability) {
        this.probability = probability;
    }
        
    public void setName(String name) {
        this.name = name;
    }
        
    @Override
    public String toString() {
        return name;
    }
}