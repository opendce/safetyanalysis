package pl.wroc.pwr.sa.gui.panels;


import pl.wroc.pwr.sa.gui.adapter.interfaces.TreeAdapter;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.layout.mxOrganicLayout;
import com.mxgraph.swing.handler.mxKeyboardHandler;
import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.mxGraphOutline;
import com.mxgraph.util.mxUndoManager;
import com.mxgraph.view.mxGraph;
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import pl.wroc.pwr.sa.gui.PopUp;

/**
 *
 * @author babaj, marek
 */
public class DrawPanel extends JPanel{
    
    public static mxGraph graph = new mxGraph();
    Object parent = graph.getDefaultParent();
    
    final JScrollPane scrollPane;
    protected mxGraphComponent graphComponent;
    protected mxGraphOutline graphOutline;
    protected mxUndoManager undoManager;
    protected File currentFile;
    protected mxRubberband rubberband;
    protected mxKeyboardHandler keyboardHandler;
    private TreeAdapter treeAdapter;
    
    private Object vAB, vCD, vA, vB, vC, vD;
    
    public DrawPanel(TreeAdapter treeAdapter)  {
        this.treeAdapter = treeAdapter;
        graph.setAllowDanglingEdges(false); 
 mxIGraphLayout layout = new mxOrganicLayout(graph);
 layout.execute(graph.getDefaultParent());
        Map<String, Object> stil = new HashMap<String, Object>();
        setLayout(new BorderLayout());
        graphComponent = new mxGraphComponent(graph);
        scrollPane = new JScrollPane(graphComponent);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        add(graphComponent, BorderLayout.CENTER);
        installListeners();
        setVisible(true);        
    }       
    
    protected void showPopupMenu(MouseEvent e) {
        Object cell = graphComponent.getCellAt(e.getX(), e.getY());
        
        PopUp popUp;
        if (cell == null)
            popUp  = new PopUp(e.getX(), e.getY(), treeAdapter);
        else
            popUp = new PopUp(e.getX(), e.getY(), cell, treeAdapter);
        
        popUp.show(graphComponent, e.getX(), e.getY());
    }
    
    protected void installListeners() {
        
        graphComponent.getGraphControl().addMouseListener(new MouseAdapter() {

        @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3)
                        showPopupMenu(e);
            }
        });
    
    }
 
}