package pl.wroc.pwr.sa.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import pl.wroc.pwr.sa.et.EventTree;

/**
 *
 * @author babaj
 */
public class JMenuEventListener implements ActionListener{
    private final String items[] = {"Eksportuj", "Importuj", "Weryfikacja"};
    private EventTree tree;
        
    public JMenuEventListener(EventTree tree){
        this.tree = tree;
    }
    
    public void actionPerformed(ActionEvent e) {
       JMenuItem menuItem = (JMenuItem) e.getSource();
       if(menuItem.getText().equals(items[2])) {
           JOptionPane.showMessageDialog(null, "Drzewo zostało zweryfikowane.", "Postęp weryfikacji.", JOptionPane.INFORMATION_MESSAGE);
       }
    }
}