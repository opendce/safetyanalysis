package pl.wroc.pwr.sa.gui.panels;


import pl.wroc.pwr.sa.gui.adapter.EventsTreeAdapter;
import pl.wroc.pwr.sa.gui.adapter.interfaces.TreeAdapter;
import com.mxgraph.model.mxCell;
import pl.wroc.pwr.sa.gui.enums.ChartTypes;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import pl.wroc.pwr.sa.et.Edge;
import pl.wroc.pwr.sa.et.Event;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;
import pl.wroc.pwr.sa.fr.Scale;
import pl.wroc.pwr.sa.gui.ExportXML;
import pl.wroc.pwr.sa.gui.JMenuEventListener;

/**
 *
 * @author babaj
 */
public class Window extends javax.swing.JFrame {

    private final static String INITIAL_PANEL = "Initial Panel";
    private final static String DRAW_PANEL = "Draw Panel";

    private TreeAdapter treeAdapter;
    private JMenuBar menuBar;
    private JMenu menu;
    private JMenuItem menuItem;
    private EventTree tree = new EventTree();
    private DrawPanel drawPanel;
    private PalettePanel palettenPanel;
    ArrayList<Integer> toDelete = new ArrayList<Integer>();
    ArrayList<Integer> toDeleteGraph = new ArrayList<Integer>();

    
    
    public void setChartType(ChartTypes chartTypes) {
        initializeMenuBar();        
        switch(chartTypes) {
            case EVENT_TREE :
                treeAdapter = new EventsTreeAdapter.Creator(tree).create();
                drawPanel = new DrawPanel(treeAdapter);
                palettenPanel = new PalettePanel(chartTypes, treeAdapter, drawPanel);
                getContentPane().add(palettenPanel, BoxLayout.X_AXIS); 
                jPanel1.add(drawPanel, DRAW_PANEL);                
                ((CardLayout)jPanel1.getLayout()).show(jPanel1, DRAW_PANEL);
            break;
            default:
            break;
        }        
    }
    /**
     * Creates new form Window
     */
    public Window() {
        initComponents();
        setTitle("Graph Creator");
        jPanel1.add(new ChooseTreeTypePanel(this), INITIAL_PANEL);
        
    }

    private void initializeMenuBar() {
        menuBar = new JMenuBar();
        menu = new JMenu("Opcje");        
        menuItem = new JMenuItem("Eksportuj");
        menuItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                
                ExportXML.saveGraph(treeAdapter.getSourceTree());
            }
        });
        menu.add(menuItem);
        menuItem = new JMenuItem("Importuj");
        menuItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                treeAdapter.clear();
                TreeAdapter treeAdapterTemp = new EventsTreeAdapter.Creator(tree).create();
                tree = ExportXML.loadEventTreeGraph();
                System.out.println(tree.toString() + "\n");
                
                
                
                drawTree();
                
            }
        });

        
        menu.add(menuItem);
        menuItem = new JMenuItem("Weryfikacja");
        menuItem.addActionListener(new JMenuEventListener(tree));
        menu.add(menuItem);
        menuBar.add(menu);
        setJMenuBar(menuBar);
    }
    
    void setDrawPanel() {        
        initializeMenuBar(); 
        getContentPane().getComponent(0).setVisible(true);
        jPanel1.add(drawPanel, DRAW_PANEL);                
        ((CardLayout)jPanel1.getLayout()).show(jPanel1, DRAW_PANEL);
    }    


    
    public EventTree getEventTree()
    {
        return tree;
    }
    
    public void setEventTree(EventTree _tree)
    {
        tree = _tree;
    }
    
    void drawTree()
    {
        
            treeAdapter.clear();
            Edge.count = 0;
            Event.count = 0;
            
            int i = 0;
            for(Entry<Integer, Event> entry : tree.getEvents().entrySet()) {
                Event currentEvent = entry.getValue();
                if(i == 0){
                    treeAdapter.addFirstEvent();
                    palettenPanel.setFirst(false);
                }else
                {
                    treeAdapter.addEvent(currentEvent.getName(), currentEvent.getProbability());  
                }

                treeAdapter.draw(drawPanel);
                System.out.println("draw ");
                i++;
            }
        
            for(int id : toDelete){

                    mxCell cell = treeAdapter.getCell(id);
                    System.out.println("Do usuniecia:" + id + "\n");
                    DrawPanel.graph.getModel().beginUpdate();
                    DrawPanel.graph.getModel().remove(cell);
                    DrawPanel.graph.getModel().endUpdate();
            }


            for(int j : toDelete){
                Edge edge = treeAdapter.getSourceTree().getEdges().get(j);
                if(edge != null)
                {
                    treeAdapter.removeChildEdges(edge);
                    treeAdapter.getSourceTree().removeEdge(edge);
                }
            }

}
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(640, 480));
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        jPanel1.setLayout(new java.awt.CardLayout());
        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Window().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}