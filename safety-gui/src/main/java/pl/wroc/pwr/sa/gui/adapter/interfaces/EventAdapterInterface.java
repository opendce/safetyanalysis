package pl.wroc.pwr.sa.gui.adapter.interfaces;

import pl.wroc.pwr.sa.et.Probability;

/**
 *
 * @author babaj
 */
public interface EventAdapterInterface {
    
    public void setName(String name);
    public Probability getProbability();
    public void setProbabilty(Probability probability);
}
