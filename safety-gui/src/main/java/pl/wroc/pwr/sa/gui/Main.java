package pl.wroc.pwr.sa.gui;

import pl.wroc.pwr.sa.gui.panels.DrawPanel;
import pl.wroc.pwr.sa.gui.panels.Window;

/**
 * Hello world!
 *
 */
public class Main 
{
    public static void main( String[] args )
    {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Window().setVisible(true);
            }
        });
    }
}
