package pl.wroc.pwr.sa.gui.enums;


/**
 *
 * @author babaj
 */
public enum ChartTypes {
    EVENT_TREE(0), FAULT_TREE(1), FUZZY_REASONING(2);
    
    int type;
    private ChartTypes(int type) {
        this.type = type;
    }
    
}