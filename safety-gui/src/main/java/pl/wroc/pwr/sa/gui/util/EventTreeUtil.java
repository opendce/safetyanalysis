package pl.wroc.pwr.sa.gui.util;


import com.mxgraph.swing.mxGraphComponent;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JPanel;
import pl.wroc.pwr.sa.gui.panels.DrawPanel;
import pl.wroc.pwr.sa.et.Edge;

/**
 *
 * @author babaj
 */
public class EventTreeUtil extends TreeUtil {

    JPanel jPanel;
    
    public EventTreeUtil(DrawPanel drawPanel) {
        jPanel = drawPanel;
    }
    
    private Dimension getDimension() {
        Component[] components = jPanel.getComponents();
        mxGraphComponent mxGraphComponent1 = null;
        for (int i = 0; i < components.length; i++) {
            if(components[i] instanceof mxGraphComponent) {
                mxGraphComponent1 = (mxGraphComponent) components[i];
            }
        }
        return mxGraphComponent1.getSize();
    }
    
    public Dimension getEventCellDimension(int cellsCount) {
        if(cellsCount == 1) {
            return new Dimension((int)getDimension().width/2, getDimension().height);
        } else {
            return new Dimension((int)getDimension().width/(cellsCount+1), getDimension().height);
        }
    }
    
    public double getDeltaEdgesCurve(Edge currentEdge) {
          int pow = currentEdge.getEvent().getId();
          System.out.println("pow: "+pow);
          int result = (int) Math.pow(2, pow);
          return 0.5/result;
    }
    
    public double getDeltaEdgesCurve(int eventCount) {
          int result = (int) Math.pow(2, eventCount);
          return 0.5/result;
    }
    
}