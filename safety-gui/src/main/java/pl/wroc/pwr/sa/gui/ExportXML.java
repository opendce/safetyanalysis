package pl.wroc.pwr.sa.gui;

import pl.wroc.pwr.sa.gui.adapter.EventsTreeAdapter;
import pl.wroc.pwr.sa.gui.adapter.interfaces.TreeAdapter;
import com.mxgraph.io.mxCodec;
import com.mxgraph.io.mxCodecRegistry;
import com.mxgraph.io.mxModelCodec;
import com.mxgraph.io.mxObjectCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxXmlUtils;
import java.io.File;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import pl.wroc.pwr.sa.gui.panels.DrawPanel;
import pl.wroc.pwr.sa.et.Edge;
import pl.wroc.pwr.sa.et.Event;
import pl.wroc.pwr.sa.et.EventTree;

/**
 *
 * @author babaj
 */
public class ExportXML {

    private mxCell cell;
    private mxXmlUtils xmlUtils;
    private static mxCodec codec;
    private mxGraphComponent graphComponent;
    public static ExportXML export = new ExportXML();
    private TreeAdapter treeAdapter;
    private DrawPanel drawPanel;
    
    public ExportXML() {            
        graphComponent = new mxGraphComponent(DrawPanel.graph);
    }   
    
    static {                
        mxCodecRegistry.register(new mxModelCodec());
        mxCodecRegistry.addPackage("pl.wroc.pwr.gui");
        mxCodecRegistry.register(new mxObjectCodec(new pl.wroc.pwr.sa.gui.Parameters()));
        mxCodecRegistry.addPackage("adaptee.events.tree");
        mxCodecRegistry.register(new mxObjectCodec(new pl.wroc.pwr.sa.gui.adapter.ProbabilityAdapter()));
    }
    
    public void setTreeAdapter(TreeAdapter treeAdapter){
        this.treeAdapter = treeAdapter;
    }
    
    public TreeAdapter getTreeAdapter(){
        return treeAdapter;
    }
    
    public void setDrawPanel(DrawPanel drawPanel){
        this.drawPanel = drawPanel;
    }
    
    public static EventTree loadEventTreeGraph() {
        String path = ExportXML.chooseFileOpenPath();
        EventTree tmpTree = null;
        try {
            tmpTree = EventTree.loadFromXML(path);
        } catch (Exception ex) {
            Logger.getLogger(ExportXML.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tmpTree;
    }
    
    public static void saveGraph(EventTree tree) {
        String fileName = ExportXML.chooseFileSavePath();
        if(!fileName.endsWith(".xml")) {
            fileName = fileName.concat(".xml");
        }
        try {
            System.out.println("ZAPISYWANIE: \n");
                for( int temp : tree.getEdges().keySet())
                {
                    System.out.println(temp + "\n");
                }
            EventTree.saveTreeToXML(fileName, tree);
        } catch (Exception ex) {
            Logger.getLogger(ExportXML.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static String chooseFileOpenPath() {
        
        final JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);
        
        fileChooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
            public String getDescription() {
                return "Safety Analysis Trees (*.xml)";
            }
            
            public boolean accept(File file) {
                if (file.isDirectory()) {
                    return true;
                } else {
                    return file.getName().toLowerCase().endsWith(".xml");
                }
            }
        });
        
        int value = fileChooser.showOpenDialog(null);
        if(value == JFileChooser.APPROVE_OPTION) {
            
            File file = fileChooser.getSelectedFile();
            return file.getAbsolutePath();
        }
        else if(value == JFileChooser.CANCEL_OPTION) {
            
            return null;
        }
        return null;
    }
    
    public static String chooseFileSavePath() {
        
        final JFileChooser fileChooser = new JFileChooser();
        
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);
        
        fileChooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
            public String getDescription() {
                return "Safety Analysis Trees (*.xml)";
            }
            
            public boolean accept(File file) {
                if (file.isDirectory()) {
                    return true;
                } else {
                    return file.getName().toLowerCase().endsWith(".xml");
                }
            }
        });
        
        int value = fileChooser.showSaveDialog(null);
        
        if(value == JFileChooser.APPROVE_OPTION) {
            
            File file = fileChooser.getSelectedFile();
            return file.getAbsolutePath();
            
        }
        else if(value == JFileChooser.CANCEL_OPTION) {
            
            return null;
        }      
        return null;
    }
}