package pl.wroc.pwr.sa.gui.cli;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.et.TreesService;
import pl.wroc.pwr.sa.api.Api.MainAPI;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;
import pl.wroc.pwr.sa.ft.Fault;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.ft.TopProbability;
import pl.wroc.pwr.sa.graph.EventTreeGraph;
import pl.wroc.pwr.sa.graph.FaultTreeGraph;
import pl.wroc.pwr.sa.report.ReportGenerator;

/**
 * Main example demonstrating Apache Commons CLI.  Apache Commons CLI and more
 * details on it are available at http://commons.apache.org/cli/.
 *
 * @author Dustin
 */
public class MainCLI {
    private MainAPI api;
    private EventTree eventsTree;
    private FaultTree faultTree;
    private ReportGenerator reportGenerator;
    private EventTreeGraph eventTreeGraph;
    private FaultTreeGraph faultTreeGraph;
    private boolean generateReport;


    public MainCLI() {
        api = new MainAPI();
        eventsTree = null;
        faultTree = null;
        reportGenerator = null;
        generateReport = false;
        eventTreeGraph = null;
        faultTreeGraph = null;
    }

    /**
     * Reads Event Tree from XML file.
     * <p/>
     * Writes to system output; "Read Event Tree file from: path Succes" for success
     * and "Read Event Tree file from: path Fail" for fail.
     *
     * @param path Path to XML file to be loaded.
     */
    private void readEventTreeFromFile(String path) {
        eventsTree = api.readEventTreeFromFile(path);
        String outputText = "";
        if (eventsTree != null) {
            outputText = ("\nRead Event Tree file from: " + path + " Success");
        } else {
            outputText = ("\nRead Event Tree file from: " + path + " Fail");
        }
        System.out.print(outputText);
        if (generateReport) {
            reportGenerator.setReadEventTreeFromFileResult(outputText);
            reportGenerator.addImageToLastAddedParagraph(eventTreeGraph.createEventTreeGraphImage(eventsTree));
        }
    }

    /**
     * Counts probability for scenarios in Events Tree.
     * <p/>
     * Writes to system output; Membership Function for scenarios and probabilities.
     *
     * @param option Comma separated scenario indexes.
     */
    private void eventTreeScenarioCounter(String option) {
        String outputText = "";
        MembershipFunction mf = api.eventTreeScenarioCounter(option, eventsTree);
        if (mf != null) {
            ParamTrapezoid pt = (ParamTrapezoid) mf.getParameters();
            outputText += ("\nMembership Function for scenarioo: [" + option + "]");
            outputText += ("\nA: " + pt.getA());
            outputText += ("\nB: " + pt.getB());
            outputText += ("\nC: " + pt.getC());
            outputText += ("\nD: " + pt.getD());
        } else {
            outputText = "There is no tree";
        }
        System.out.print(outputText);
        if (generateReport) {
            reportGenerator.setEventTreeScenarioCounterResult(outputText);
        }
    }

    /**
     * Reads Fault Tree from XML file.
     * <p/>
     * Writes to system output; "Read Fault Tree file from: path Success" for success and "Read Fault Tree file from: path Fail" for fail.
     *
     * @param path Path to XML file to be loaded.
     */
    private void readFaultTreeFromFile(String path) {
        String outputText = "";
        faultTree = api.readFaultTreeFromFile(path);
        if (faultTree != null) {
            outputText = "\nRead Fault Tree file from: " + path + " Success";
        } else {
            outputText = "\nRead Fault Tree file from: " + path + " Fail";
        }
        System.out.print(outputText);
        if (generateReport) {
            reportGenerator.setReadFaultTreeFromFileResult(outputText);
            reportGenerator.addImageToLastAddedParagraph(faultTreeGraph.createFaultTreeGraphImage(faultTree));
        }
    }

    /**
     * Checks The Cohesion Of The Event Tree.
     * <p/>
     * Writes to system output; "Event true is ok" for true and "Event tree is not ok" with exception details for false.
     */
    private void checkTheCohesionOfTheTree() {
        String outputText = "";
        boolean ifEventsTreeIsOk = api.checkTheCohesionOfTheTree(eventsTree);
        if (ifEventsTreeIsOk) {
            outputText += "\nResult of the analysis Events Tree: ";

            outputText += "\nEvents tree is ok";
        } else {
            outputText = "There is no tree that I could analyze.";
        }
        System.out.print(outputText);
        if (generateReport) {
            reportGenerator.setCohesionCheckResult(outputText);
        }
    }

    /**
     * Gets Fuzzy Importance Index of Fault Tree.
     * <p/>
     * Writes to system output; Results of analysis as list of keys and values.
     */
    private void getFuzzyImportanceIndex() {
        String outputText = "";
        Map<Fault, MembershipFunction> map2 = api.getFuzzyImportanceIndex(faultTree);
        outputText += "\nResult of the analysis Fault Tree: ";
        if (map2 != null) {
            for (Map.Entry<Fault, MembershipFunction> entry : map2.entrySet()) {
                Fault key = entry.getKey();
                MembershipFunction value = entry.getValue();
                outputText += ("\nKey: " + key.getName() + " = Value: " + value.getName());
            }
        } else {
            outputText = "There is no tree that I could analyze.";
        }
        System.out.print(outputText);
        if (generateReport) {
            reportGenerator.setFuzzyImportanceIndexResult(outputText);
        }
    }

    /**
     * Analyze Fault Tree and show results.
     * <p/>
     * Writes to system output; Results of analysis as list of keys and values.
     */
    private void analysisTree() {
        String outputText = "";
        Map<String, TopProbability> map = api.analyseFaultTree(faultTree);
        if (map != null) {
            outputText += "\nResult of the analysis Fault Tree: ";
            for (Map.Entry<String, TopProbability> entry : map.entrySet()) {
                String key = entry.getKey();
                TopProbability value = entry.getValue();
                outputText += ("\nKey: " + key + " = Value: " + value);
            }
        } else {
            outputText = ("There is no tree that I could analyze.");
        }

        System.out.print(outputText);
        if (generateReport) {
            reportGenerator.setAnalysisResult(outputText);
        }
    }

    private void importanceAnalise(String name) {
        String outputText = "";
        String[] results = new String[4];
        if (eventsTree != null) {
            System.out.print("\nResult of the analysis Events Tree: ");
            TreesService service = new TreesService();
            boolean ok = true;
            try {
                results = service.doAnalise(eventsTree, name);
            } catch (Exception ex) {
                Logger.getLogger(MainCLI.class.getName()).log(Level.SEVERE, null, ex);
                outputText += "\nEvents tree is not ok" + ex.getMessage();
                ok = false;
            }
            if (ok) {
                outputText += "\n"+ results[0] + "\n" + "Drzewo przed:" +results[1] + "\nPrawdopodobieństwo przed: "+ results[2] +"\n\nDrzewo po:" +results[3] +"\nPrawdopodobieństwo po: "+ results[4];
            }
        } else {
            outputText = "There is no tree that I could analyze.";
        }

        System.out.print(outputText);
    }

    /**
     * Apply Apache Commons CLI GnuParser to command-line arguments.
     *
     * @param commandLineArguments Command-line arguments to be processed with
     *                             Gnu-style parser.
     */
    public void useGnuParser(final String[] commandLineArguments) {
        final CommandLineParser cmdLineGnuParser = new GnuParser();

        final Options gnuOptions = constructGnuOptions();
        CommandLine commandLine;
        try {
            commandLine = cmdLineGnuParser.parse(gnuOptions, commandLineArguments);
            if (commandLine.hasOption("r")) {
                generateReport = true;
                reportGenerator = new ReportGenerator(new Date(), "PWr_logo.png", "Safety Analysis - Report");
                eventTreeGraph = new EventTreeGraph();
                faultTreeGraph = new FaultTreeGraph();
            }
            if (commandLine.hasOption("e")) {
                readEventTreeFromFile(commandLine.getOptionValue("e"));
            }
            if (commandLine.hasOption("f")) {
                readFaultTreeFromFile(commandLine.getOptionValue("f"));
            }
            if (commandLine.hasOption('a')) {
                analysisTree();
            }
            if (commandLine.hasOption('c')) {
                checkTheCohesionOfTheTree();
            }
            if (commandLine.hasOption('i')) {
                getFuzzyImportanceIndex();
            }
            if (commandLine.hasOption('s')) {
                eventTreeScenarioCounter(commandLine.getOptionValue("s"));
            }
            if (commandLine.hasOption('p')) //analiza przy usuwaniu poddrzewa True
            {
                importanceAnalise(commandLine.getOptionValue("n"));   //po n nazwa zdarzenia
            }

            if (reportGenerator != null) {
                reportGenerator.save(commandLine.getOptionValue("r"));
                System.out.print("\nCreated file: " + commandLine.getOptionValue("r"));
            }

        } catch (ParseException parseException)  // checked exception
        {
            System.out.print(
                    "Encountered exception while parsing using GnuParser:\n"
                            + parseException.getMessage());
        }
    }

    /**
     * Construct and provide GNU-compatible Options.
     *
     * @return Options expected from command-line of GNU form.
     */
    public Options constructGnuOptions() {
        final Options gnuOptions = new Options();
        gnuOptions
                .addOption("c", "checkTheCohesionOfTheEventTree", false, "Check The Cohesion Of The Event Tree")
                .addOption("a", "analyseFaultTree", false, "Analyse fault tree and show results")
                .addOption("i", "getFuzzyImportanceIndex", false, "Get Fuzzy Importance Index of Faut Tree")
                .addOption("p", "importanceAnalise", false, "Check importance analisis")
                .addOption(OptionBuilder.withArgName("file")
                        .hasArg()
                        .withLongOpt("eventtree")
                        .withDescription("Read Event Tree from file")
                        .create("e"))
                .addOption(OptionBuilder.withArgName("file")
                        .hasArg()
                        .withLongOpt("faulttree")
                        .withDescription("Read Fault Tree from file")
                        .create("f"))
                .addOption(OptionBuilder.withArgName("scenario")
                        .hasArg()
                        .withLongOpt("evtsce")
                        .withDescription("Count probability for scenarios")
                        .create("s"))
                .addOption(OptionBuilder.withArgName("file")
                        .hasArg()
                        .withLongOpt("report")
                        .withDescription("Generate report")
                        .create("r"))
                .addOption(OptionBuilder.withArgName("eventname")
                        .hasArg()
                        .withLongOpt("evntnme")
                        .withDescription("Get event name")
                        .create("n"));
        return gnuOptions;
    }


    /**
     * Write "help" to the provided OutputStream.
     *
     * @param options
     * @param printedRowWidth
     * @param header
     * @param footer
     * @param spacesBeforeOption
     * @param spacesBeforeOptionDescription
     * @param displayUsage
     * @param out
     */
    public void printHelp(
            final Options options,
            final int printedRowWidth,
            final String header,
            final String footer,
            final int spacesBeforeOption,
            final int spacesBeforeOptionDescription,
            final boolean displayUsage,
            final OutputStream out) {
        final String commandLineSyntax = "SafetyAnalysis";
        final PrintWriter writer = new PrintWriter(out);
        final HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp(
                writer,
                printedRowWidth,
                commandLineSyntax,
                header,
                options,
                spacesBeforeOption,
                spacesBeforeOptionDescription,
                footer,
                displayUsage);
        writer.flush();
    }

    /**
     * Main executable method used to demonstrate Apache Commons CLI.
     *
     * @param commandLineArguments Commmand-line arguments.
     */
    public static void main(final String[] commandLineArguments) {
        MainCLI mainCLI = new MainCLI();
        System.out.print("Safety Analysis\n");
        if (commandLineArguments.length < 1) {
            mainCLI.printHelp(
                    mainCLI.constructGnuOptions(), 80, "GNU HELP", "End of GNU Help",
                    5, 3, true, System.out);
        }
        mainCLI.useGnuParser(commandLineArguments);
    }
}