package pl.wroc.pwr.sa.gui.enums;


/**
 *
 * @author babaj
 */
public enum EventsTreeButtons {
    CICLE("shape=ellipse", "src/main/resources/images/ellipse.png"), RECTANGLE("shape=rectangle", "src/main/resources/images/rectangle.png");
    
    private final String shape;
    private final String imagepath;

    private EventsTreeButtons(String shape, String imagepath) {
        this.shape = shape;
        this.imagepath = imagepath;
    }
    
    public String getShape() {
        return shape;
    }
    
    public String getImagePath() {
        return imagepath;
    }
    
}