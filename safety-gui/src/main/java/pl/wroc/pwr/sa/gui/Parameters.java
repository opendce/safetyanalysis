package pl.wroc.pwr.sa.gui;

import java.io.Serializable;

/**
 *
 * @author marek
 */

public class Parameters implements Serializable {
        
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Parameters(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Parameters() {
        this.x = 0;
        this.y = 0;
    }
    
    public String toString() {
        return "x:" + x + " y:" + y;
    }
}