package pl.wroc.pwr.sa.gui.adapter;

import pl.wroc.pwr.sa.gui.adapter.interfaces.EdgeAdapterInterface;
import pl.wroc.pwr.sa.et.Edge;

/**
 *
 * @author babaj
 */
public class EdgeAdapter implements EdgeAdapterInterface {
   
private Edge edge;

    private EdgeAdapter(Creator creator) {
        this.edge = creator.edge;
    }

     public static class Creator {
        
        Edge edge;
        public Creator(Edge edge) {
            this.edge = edge;
        }
        
        public EdgeAdapter create() {
            return new EdgeAdapter(this);
        }
        
    }
}