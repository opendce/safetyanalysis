package pl.wroc.pwr.sa.gui.util;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import pl.wroc.pwr.sa.gui.panels.DrawPanel;

/**
 *
 * @author babaj
 */
public class TreeUtil {
    
    public mxCell getCellByID(String id) {
        return (mxCell) ((mxGraphModel)DrawPanel.graph.getModel()).getCell(id);
    }
    public int getEdgeIDByCellID(String id) {
        return Integer.parseInt(id.replace("E", ""));
    }
}