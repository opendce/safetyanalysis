package pl.wroc.pwr.sa.gui.adapter;

import pl.wroc.pwr.sa.gui.adapter.interfaces.TreeAdapter;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxConnectionConstraint;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import pl.wroc.pwr.sa.gui.panels.DrawPanel;
import pl.wroc.pwr.sa.et.Edge;
import pl.wroc.pwr.sa.et.Event;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.fr.FuzzyLogicFacade;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;
import pl.wroc.pwr.sa.fr.ParamTriangle;
import pl.wroc.pwr.sa.fr.TrapezoidFunction;
import pl.wroc.pwr.sa.fr.TriangleFunction;
import pl.wroc.pwr.sa.gui.util.EventTreeUtil;

/**
 *
 * @author babaj
 */
@XmlRootElement(name = "eventTreeAdapter")
@XmlAccessorType(XmlAccessType.FIELD)
public class EventsTreeAdapter implements TreeAdapter {
    
    private final int STRANGE_CONSTANTS = 2;
    
    private EventTree eventsTree;
    private EventTreeUtil eventTreeUtil;
    FuzzyLogicFacade flo = new FuzzyLogicFacade();

    public void setEventsTree(EventTree eventsTree) {
        this.eventsTree = eventsTree;
    }

    private EventsTreeAdapter(Creator creator) {
//        ExportXML.export.setTreeAdapter(this);
        this.eventsTree = creator.eventsTree;
    }
    /*
     * Funkcja ma narysować Event
     */

    public EventTree getSourceTree() {
        return eventsTree;
    }
    
    public ParamTrapezoid getTrapezoid(String name){
        System.out.println("trapezoid: "+flo.getTrapezoidFunctions().size());
        for(TrapezoidFunction tmp : flo.getTrapezoidFunctions())
            if(tmp.getName().equals(name))
                return tmp.getParameters();
        return null;
    }
    
    public EventTreeUtil getEventTreeUtil(){
        return eventTreeUtil;
    }
    
    public ParamTriangle getTriangle(String name){
        for(TriangleFunction tmp : flo.getTriangleFunctions())
            if(tmp.getName().equals(name))
                return tmp.getParameters();
        return null;
    }
    
    public void addParamTrapezoid(ParamTrapezoid param, String name){
//        paramTrapezoid.put(name, param);
        try {
            addEvent(name, flo.createTrapezoid(param, name));
        } catch (Exception ex) {
            Logger.getLogger(EventsTreeAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addParamTriangle(ParamTriangle param, String name){
//        paramTriangle.put(name, param);
        try {
            addEvent(name, flo.createTriangle(param, name));
        } catch (Exception ex) {
            Logger.getLogger(EventsTreeAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public void editParamTrapezoid(ParamTrapezoid param, String name, int id, Object cell){
        eventsTree.getEvents().get(id).setName(name);
        try {
            eventsTree.getEvents().get(id).setProbabilty(flo.createTrapezoid(param, name));
        } catch (Exception ex) {
            Logger.getLogger(EventsTreeAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
        DrawPanel.graph.getModel().beginUpdate();
        DrawPanel.graph.cellLabelChanged(cell, name, false);
        DrawPanel.graph.getModel().endUpdate();
    }
    
    public void editParamTriangle(ParamTriangle param, String name, int id, Object cell){
        eventsTree.getEvents().get(id).setName(name);
        try {
            eventsTree.getEvents().get(id).setProbabilty(flo.createTriangle(param, name));
        } catch (Exception ex) {
            Logger.getLogger(EventsTreeAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
        DrawPanel.graph.getModel().beginUpdate();
        DrawPanel.graph.cellLabelChanged(cell, name, false);
        DrawPanel.graph.getModel().endUpdate();
    }
    
    public void addFirstEvent(){
        try {
                Event event = new Event();
                event.setName("");
                eventsTree.addInitEvent(event);
            } catch (Exception ex) {
                Logger.getLogger(EventsTreeAdapter.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    public void addEvent(String eventName, MembershipFunction probability){
        try {
            Event event = new Event();
            event.setName(eventName);
            event.setProbabilty(probability);
            eventsTree.addEvent(event);
        } catch (Exception ex) {
            Logger.getLogger(EventsTreeAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      
    public void addEvent(String eventName, MembershipFunction probability, int id){
        try {
            Event event = new Event();
            event.setName(eventName);
            event.setProbabilty(probability);
            event.setId(id);
            eventsTree.addEvent(event);
        } catch (Exception ex) {
            Logger.getLogger(EventsTreeAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void draw(DrawPanel drawPanel) {
        eventTreeUtil = new EventTreeUtil(drawPanel);
        
        Dimension dimension = eventTreeUtil.getEventCellDimension(eventsTree.getEvents().size());
        drawEvents(dimension);
        drawEdges();
    }
    
    private void drawEvents(Dimension dimension) {
        EventAdapter eventAdapter; 
        int x = 0, y = 0;
        mxCell cell;
        DrawPanel.graph.getModel().beginUpdate();
        for(Event event : eventsTree.getEvents().values()) 
        {
            eventAdapter = new EventAdapter.Creator(event).create();            
            if((cell = eventTreeUtil.getCellByID(""+(event.getId()+STRANGE_CONSTANTS))) == null) {
                DrawPanel.graph.insertVertex(DrawPanel.graph.getDefaultParent(), ""+(event.getId()+STRANGE_CONSTANTS), event.getName(), x, y, dimension.width, dimension.height);                
            } else {
                DrawPanel.graph.resizeCell(cell, new mxRectangle(x, y, dimension.width, dimension.height));
            }
            x += dimension.width;
        }
           if((cell = eventTreeUtil.getCellByID("final")) == null) {
                DrawPanel.graph.insertVertex(DrawPanel.graph.getDefaultParent(), "final", new ProbabilityAdapter(), x, y, dimension.width, dimension.height,"fillColor=red");
            } else {
                DrawPanel.graph.resizeCell(cell, new mxRectangle(x, y, dimension.width, dimension.height));
            } 
       DrawPanel.graph.getModel().endUpdate();
       setLabelBounds();
    }
    
    private void setLabelBounds() {
        Hashtable<Object, mxCellState> states = DrawPanel.graph.getView().getStates();
        Enumeration<mxCellState> cellstates = states.elements();
        while(cellstates.hasMoreElements()) {
            cellstates.nextElement().setLabelBounds(new mxRectangle(0, 0, 0, 0));
        }
    }
    
    private void drawEdges() {
        mxCell parentEdge ,cell, nextCell, finalCell = eventTreeUtil.getCellByID("final");
        Object vA; 
        mxCellState edgeState, targetState;
        mxConnectionConstraint connectionConstraint;
        Event currentEvent = null;
        double delta = 0;
        int eventCount = 0;
       
        for(Edge edge : eventsTree.getEdges().values()) {
            if(currentEvent == null || currentEvent != edge.getEvent()) {
                System.out.println("1");
                delta = eventTreeUtil.getDeltaEdgesCurve(eventCount);
                eventCount++;
                currentEvent = edge.getEvent();
            }
            if(edge.getParent() == null && edge.getChild().isEmpty()) {
                System.out.println("2");
                DrawPanel.graph.getModel().beginUpdate();
                cell = eventTreeUtil.getCellByID(""+(edge.getEvent().getId()+STRANGE_CONSTANTS));
                vA = DrawPanel.graph.insertEdge(DrawPanel.graph.getDefaultParent(), "E"+edge.getId(), String.valueOf(edge.getType()), cell, finalCell,"strokeWidth=4;strokeColor=#66FF00");
                DrawPanel.graph.setConnectionConstraint(vA, cell, true, new mxConnectionConstraint(new mxPoint(0,0.5)));
                DrawPanel.graph.setConnectionConstraint(vA, finalCell, false, new mxConnectionConstraint(new mxPoint(0,0.5)));
                DrawPanel.graph.getModel().endUpdate();
            } else if(edge.getParent() == null && !edge.getChild().isEmpty()) {
                System.out.println("3");
                DrawPanel.graph.getModel().beginUpdate();
                nextCell = eventTreeUtil.getCellByID(""+(edge.getChild().get(0).getEvent().getId()+STRANGE_CONSTANTS));
               // cell = eventTreeUtil.getCellByID(""+edge.getEvent().getId()+STRANGE_CONSTANTS);
                vA = eventTreeUtil.getCellByID("E"+edge.getId());
                DrawPanel.graph.getModel().setTerminal(vA, nextCell, false);
                DrawPanel.graph.getModel().endUpdate();
            } else if(edge.getParent() != null && edge.getChild().isEmpty()) {
               
                System.out.println("4");
                
                DrawPanel.graph.getModel().beginUpdate();
                parentEdge = eventTreeUtil.getCellByID("E"+edge.getParent().getId());
                cell = eventTreeUtil.getCellByID(""+(edge.getEvent().getId()+STRANGE_CONSTANTS));
                edgeState = DrawPanel.graph.getView().getState(parentEdge);
                targetState = DrawPanel.graph.getView().getState(cell);
                connectionConstraint = DrawPanel.graph.getConnectionConstraint(edgeState, targetState, false);
              
                //  if((vA = eventTreeUtil.getCellByID("E"+edge.getId())) == null) {
                
                
                double probability = 1;
                if(edge.getType() == true)
                {
                    vA = DrawPanel.graph.insertEdge(DrawPanel.graph.getDefaultParent(), "E"+edge.getId(), String.valueOf(edge.getType())+" "+probability, cell, finalCell,"strokeWidth=4;strokeColor=#66FF00");
                }
                else 
                {
                    vA = DrawPanel.graph.insertEdge(DrawPanel.graph.getDefaultParent(), "E"+edge.getId(), String.valueOf(edge.getType())+" "+(1-probability), cell, finalCell,"strokeWidth=4;strokeColor=#66FF00");

                }
                    DrawPanel.graph.setConnectionConstraint(vA, cell, true, connectionConstraint);
                    System.out.println("-delta: "+(connectionConstraint.getPoint().getY()+delta));
                    System.out.println("+delta: "+(connectionConstraint.getPoint().getY()-delta));
                    System.out.println("delta: "+delta);
                    if(edge.getType())
                    DrawPanel.graph.setConnectionConstraint(vA, finalCell, false, new mxConnectionConstraint(new mxPoint(0,connectionConstraint.getPoint().getY()+delta)));
                    else
                    {
                        DrawPanel.graph.setConnectionConstraint(vA, finalCell, false, new mxConnectionConstraint(new mxPoint(0,connectionConstraint.getPoint().getY()-delta)));
                    }
                DrawPanel.graph.getModel().endUpdate();
            } else {
                System.out.println("5");
               
               if(edge.getChild().get(0) != null)
               {
                    DrawPanel.graph.getModel().beginUpdate();
                    nextCell = eventTreeUtil.getCellByID(""+(edge.getChild().get(0).getEvent().getId()+STRANGE_CONSTANTS));
                    vA = eventTreeUtil.getCellByID("E"+edge.getId());
                    DrawPanel.graph.getModel().setTerminal(vA, nextCell, false);
                    DrawPanel.graph.getModel().endUpdate();
               }
            }
        }
    }

    public void clear() {
        eventsTree.clear();
        DrawPanel.graph.getModel().beginUpdate(); 
        DrawPanel.graph.removeCells(DrawPanel.graph.getChildVertices(DrawPanel.graph.getDefaultParent()));
        DrawPanel.graph.getModel().endUpdate();
    }

    
    public void removeEdge(Object cell) {
        int id = eventTreeUtil.getEdgeIDByCellID(((mxCell)cell).getId());
        System.out.println("Usunieto removeEdge TreeAdapter: " + id);
        Edge edge = eventsTree.getEdges().get(id);
        if(edge != null)
        {
            removeChildEdges(edge);
        }
        
            
        DrawPanel.graph.getModel().beginUpdate();
        DrawPanel.graph.getModel().remove(cell);
        DrawPanel.graph.getModel().endUpdate();
        
        eventsTree.removeEdge(eventsTree.getEdges().get(id));              
        printEdge();
        
    }

    
    public void printEdge(){
        for(Map.Entry<Integer, Edge> entry2 : eventsTree.getEdges().entrySet()) {
            System.out.println(entry2.getKey());
        }
    }

    
    
    public void removeChildEdges(Edge edge) {      
              
         mxCell edgeCell;
            ArrayList<Edge> lista = edge.getChild();
            if(lista != null)
            {
               for (Edge childEdge : lista)
               {                   
                   if(childEdge != null)
                   {
                       removeChildEdges(childEdge);
                       edgeCell = eventTreeUtil.getCellByID("E"+childEdge.getId());
                       System.out.println("RYSOWANIE: usun " + childEdge.getId() + "\n");
                       DrawPanel.graph.getModel().beginUpdate();
                       DrawPanel.graph.getModel().remove(edgeCell);
                       DrawPanel.graph.getModel().endUpdate();
                       //childEdge = null;

                   }else System.out.println("CHILD NULL\n");           
               }
            }
            else System.out.println("LISTA NULL\n");
    }
    
   
    public void removeVertex(Object cell) {
        mxCell edgeCell = null;
        int id = eventTreeUtil.getEdgeIDByCellID(((mxCell)cell).getId());
        System.out.println("Removing: "+ id);       
        ArrayList<Edge> toDelete = new ArrayList<Edge>();
        Event event = eventsTree.getEvents().get(id-STRANGE_CONSTANTS);
        
        for(Entry <Integer, Edge> entry : eventsTree.getEdges().entrySet())
        {
            Edge currentEdge = entry.getValue();
            if(currentEdge.getEvent().getName().equals(event.getName()))
            {
                toDelete.add(currentEdge);
            }
        }

        for(Edge tempEdge : toDelete)
        {
            System.out.println("Usunieto removeEdge TreeAdapter: " + id);
            if(tempEdge != null)
            {
                eventsTree.removeEdge(tempEdge);
            }
            
            edgeCell = eventTreeUtil.getCellByID("E"+tempEdge.getId());

            DrawPanel.graph.getModel().beginUpdate();
            DrawPanel.graph.getModel().remove(edgeCell);
            DrawPanel.graph.getModel().endUpdate();
            printEdge();
        }
        
        System.out.println("EVENT: " + event.toString());
        eventsTree.removeEvent(event);
        
        
        for(Entry<Integer, Event> entry : eventsTree.getEvents().entrySet()) {
        Event currentEvent = entry.getValue();
        {
            System.out.println("EVENTY W ADAPTERZE: " + currentEvent.getName() + currentEvent.getId() + "\n");  
        }
        
        }
        
        for(Entry <Integer, Edge> entry : eventsTree.getEdges().entrySet())
        {
            Edge currentEdge = entry.getValue();
            System.out.println("EDGES W ADAPTERZE - NAZWY EVENTOW: " + currentEdge.getEvent().getName() + currentEdge.getId() + "\n"); 
            
        }
        
        
        System.out.println("Size:"+eventsTree.getEvents().size());
        DrawPanel.graph.getModel().beginUpdate();
        DrawPanel.graph.getModel().remove(cell);            
        DrawPanel.graph.getModel().endUpdate();       
    }

    
    
    private void drawLastEdges(Event event) {
        mxCell parentEdge, cell;
        mxCellState edgeState, targetState;
        mxConnectionConstraint connectionConstraint;
        Object finalCell = eventTreeUtil.getCellByID("final");
        Event currentEvent = null;
        double delta = 0;
        for(Edge edge : eventsTree.getEdges().values()) {
            if(edge.getEvent() == event) 
            {
                delta = eventTreeUtil.getDeltaEdgesCurve(edge);
                currentEvent = edge.getEvent();
                DrawPanel.graph.getModel().beginUpdate();
                parentEdge = eventTreeUtil.getCellByID("E"+edge.getParent().getId());
                cell = eventTreeUtil.getCellByID(""+(edge.getEvent().getId()+STRANGE_CONSTANTS));
                edgeState = DrawPanel.graph.getView().getState(parentEdge);
                targetState = DrawPanel.graph.getView().getState(cell);
                connectionConstraint = DrawPanel.graph.getConnectionConstraint(edgeState, targetState, false);
                Object vA = DrawPanel.graph.insertEdge(DrawPanel.graph.getDefaultParent(), "E"+edge.getId(), String.valueOf(edge.getType()), cell, finalCell,"strokeWidth=4;strokeColor=#66FF00");
                    DrawPanel.graph.setConnectionConstraint(vA, cell, true, connectionConstraint);
                    if(edge.getType())
                    DrawPanel.graph.setConnectionConstraint(vA, finalCell, false, new mxConnectionConstraint(new mxPoint(0,connectionConstraint.getPoint().getY()+delta)));
                    else
                    DrawPanel.graph.setConnectionConstraint(vA, finalCell, false, new mxConnectionConstraint(new mxPoint(0,connectionConstraint.getPoint().getY()-delta)));
                DrawPanel.graph.getModel().endUpdate();
            }
        }
    }
    /*
     * Brawo dla Panów ktorzy pisza bledny kod!!!
     */
    private void recheckLastEdges()
    {
        
        int lastEvent = eventsTree.getEvents().size()-1;
        Event event = eventsTree.getEvents().get(lastEvent);
        if(event.getChild() == null)
        {
            for(Edge edge : eventsTree.getEdges().values())
            {
                if(edge.getEvent() == event && !edge.getChild().isEmpty())
                {
                    edge.setChild(null);
                }
            }
        }
        drawLastEdges(event);
        
    }
    
    private void removeChildVertex(Event childEvent) {
        
        mxCell childVertex = eventTreeUtil.getCellByID(""+(childEvent.getId()+STRANGE_CONSTANTS));
        System.out.println("Removing: "+childEvent.getId());
        DrawPanel.graph.getModel().beginUpdate();
        Object[] childEdges = DrawPanel.graph.getEdges(childVertex);
        for (Object childEdge : childEdges) {
           DrawPanel.graph.getModel().remove(childEdge);
        }
        DrawPanel.graph.getModel().remove(childVertex);        
        DrawPanel.graph.getModel().endUpdate();
        if(childEvent.getChild() != null) {
            removeChildVertex(childEvent.getChild());
        }
        
        
    }

    public mxCell getCell(int id) {
        return eventTreeUtil.getCellByID("E"+id);
    }
    
    
    public Event getEvent(Object cell)
    {
        int id = eventTreeUtil.getEdgeIDByCellID(((mxCell)cell).getId());
        System.out.println("Removing: "+ id);       
        ArrayList<Edge> toDelete = new ArrayList<Edge>();
        return eventsTree.getEvents().get(id-STRANGE_CONSTANTS);
    }
    
     public static class Creator {
        
        private EventTree eventsTree;
        public Creator(EventTree eventsTree) {
            this.eventsTree = eventsTree;
        }
        
        public EventsTreeAdapter create() {
            return new EventsTreeAdapter(this);
        }
    
}
}