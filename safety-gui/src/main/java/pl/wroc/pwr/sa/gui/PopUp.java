package pl.wroc.pwr.sa.gui;

import com.mxgraph.model.mxCell;
import pl.wroc.pwr.sa.gui.panels.ParameterWindow;
import pl.wroc.pwr.sa.gui.adapter.interfaces.TreeAdapter;
import pl.wroc.pwr.sa.gui.panels.DrawPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import pl.wroc.pwr.sa.et.Edge;
import pl.wroc.pwr.sa.et.Event;
import pl.wroc.pwr.sa.gui.panels.AddNewChild;
import pl.wroc.pwr.sa.et.EventTree;

/**
 *
 * @author marek
 */

public class PopUp extends JPopupMenu {
    ParameterWindow parameterWindow;
    
    public PopUp(final int x, final int y, TreeAdapter treeAdapter) {
        super();
        parameterWindow = new ParameterWindow(treeAdapter);
        JMenuItem addVertex = new JMenuItem("Wstaw wierzchołek");
        addVertex.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                DrawPanel.graph.getModel().beginUpdate();
                DrawPanel.graph.insertVertex(DrawPanel.graph.getDefaultParent(), null, new Parameters(), x, y, 80, 30);
                DrawPanel.graph.getModel().endUpdate();
            }
            
        });
        
        this.add(addVertex);
    }

    
    public PopUp(final int x, final int y, final Object cell, final TreeAdapter treeAdapter) {
        super();
        parameterWindow = new ParameterWindow(treeAdapter);
        JMenuItem editVertex = new JMenuItem("Edytuj parametry");
        editVertex.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
//               parameterWindow.setCell(cell);
//               parameterWindow.setVisible(true);
                new AddNewChild(treeAdapter, cell);
           }
            
        });
        

        JMenuItem removeCell = new JMenuItem("Usuń element");
        removeCell.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if(DrawPanel.graph.getModel().isEdge(cell)){
                    treeAdapter.removeEdge(cell);
                    System.out.println("edge");}
                else {
                    System.out.println("ver " + cell);
                    treeAdapter.removeVertex(cell);
                }
                
            }
            
        });
        
        this.add(editVertex);
        if(DrawPanel.graph.getModel().isEdge(cell))
        {
            this.add(removeCell);
        }
        else if(treeAdapter.getEvent(cell).getChild() == null)
        {
            this.add(removeCell);
        }
        
        
    }

}