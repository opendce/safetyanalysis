/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.sa.gui.enums;

/**
 *
 * @author TOSHIBBA
 */
public enum FuzzyTypes {
    TRAPEZOID(0), TRIANGLE(1);
    
    int type;
    private FuzzyTypes(int type){
        this.type = type;
    }
}
