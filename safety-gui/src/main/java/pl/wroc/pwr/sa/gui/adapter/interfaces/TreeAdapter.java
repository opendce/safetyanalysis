package pl.wroc.pwr.sa.gui.adapter.interfaces;

import pl.wroc.pwr.sa.gui.adapter.ProbabilityAdapter;
import com.mxgraph.model.mxCell;
import pl.wroc.pwr.sa.gui.panels.DrawPanel;
import pl.wroc.pwr.sa.et.Edge;
import pl.wroc.pwr.sa.et.Event;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;
import pl.wroc.pwr.sa.fr.ParamTriangle;
/**
 *
 * @author babaj
 */
public interface TreeAdapter {
    
    public void draw(DrawPanel drawPanel);
    public EventTree getSourceTree();
    public void clear();
    public void addEvent(String eventName, MembershipFunction probability);
    public void addEvent(String eventName, MembershipFunction probability, int id);
    public void addFirstEvent();
    public void addParamTriangle(ParamTriangle param, String name);
    public void addParamTrapezoid(ParamTrapezoid param, String name);
    public void editParamTriangle(ParamTriangle param, String name, int id, Object cell);
    public void editParamTrapezoid(ParamTrapezoid param, String name, int id, Object cell);
    public ParamTrapezoid getTrapezoid(String name);
    public ParamTriangle getTriangle(String name);
    public void removeEdge(Object cell);
    public void removeVertex(Object cell);
    public void setEventsTree(EventTree eventsTree);
    public mxCell getCell(int id);
    public void removeChildEdges(Edge edge);
    public Event getEvent(Object cell);
}