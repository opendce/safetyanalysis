package pl.wroc.pwr.sa.gui.adapter;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import pl.wroc.pwr.sa.gui.panels.DrawPanel;
import pl.wroc.pwr.sa.et.Event;
import pl.wroc.pwr.sa.et.Probability;
import pl.wroc.pwr.sa.gui.adapter.interfaces.EventAdapterInterface;

/**
 * new EventAdapter.Creator(new Event()).create();
 * Do Interfejsu bedzie dodawane tylko to co potrzebuje zmiany
 * @author babaj
 */
public class EventAdapter implements EventAdapterInterface{

    private Event event;
    private ProbabilityAdapter probabilityAdapter;
    private Object cell;
    
    private EventAdapter(Creator creator) {
        event = creator.event;
    }

    public Event getEvent() {
        return event;
    }
        
    public void setName(String name) {
        int id=event.getId()+2;
        event.setName(name);
        cell=(mxCell) ((mxGraphModel) (DrawPanel.graph.getModel())).getCell(String.valueOf(id));
        if(probabilityAdapter == null) {
            probabilityAdapter = new ProbabilityAdapter();
            probabilityAdapter.setName(name);
        } else {
            probabilityAdapter.setName(name);
        }
        System.out.println(name);
        DrawPanel.graph.getModel().setValue(cell, probabilityAdapter);
    }

    public Probability getProbability() {
        return probabilityAdapter.getProbability();
    }

    public void setProbabilty(Probability probability) {
        probabilityAdapter.setProbability(probability);
    }
    
    public void update(Event event) {
        this.event.update(event);
        this.setName(event.getName());
    }

    
    
    public static class Creator {
        
        Event event;
        public Creator(Event event) {
            this.event = event;
        }
        
        public EventAdapter create() {
            return new EventAdapter(this);
        }
        
    }
}