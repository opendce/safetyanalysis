package pl.wroc.pwr.sa.gui;

import com.mxgraph.io.mxCodec;
import com.mxgraph.io.mxCodecRegistry;
import com.mxgraph.io.mxModelCodec;
import com.mxgraph.io.mxObjectCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.view.mxGraph;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.w3c.dom.Document;

/**
 *
 * @author lukas, marek, babaj
 */
public class DummyTest {
    
    mxGraph graph = new mxGraph();
    mxGraph graph1 = new mxGraph();
    
    public DummyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
        
        Object parent = graph.getDefaultParent();
        mxCodecRegistry.register(new mxModelCodec());
        mxCodecRegistry.addPackage("pl.wroc.pwr.gui");
        mxCodecRegistry.register(new mxObjectCodec(new pl.wroc.pwr.sa.gui.Parameters()));
        mxCodec codec;
        
        graph.getModel().beginUpdate();
        Object v1 = graph.insertVertex(parent, "1", new Parameters(20, 100), 20, 20, 80, 30);
        Object v2 = graph.insertVertex(parent, "2", new Parameters(24, 32), 240, 150, 80, 30);
        graph.insertEdge(parent, null, "Edge", v1, v2);
        graph.getModel().endUpdate();
        
        codec = new mxCodec();
        String xml = mxXmlUtils.getXml(codec.encode(graph.getModel()));
        
        Document document = mxXmlUtils.parseXml(xml);
        codec = new mxCodec(document);
        codec.decode(document.getDocumentElement(), graph1.getModel());
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testModelObjectsLength() {
        Object[] cells = graph.getChildCells(graph.getDefaultParent());
        Object[] cells1 = graph1.getChildCells(graph1.getDefaultParent());
                
        assertEquals(cells.length, 3);
        assertEquals(cells.length, cells1.length);        
    }
    
    @Test
    public void testModelObjectsID() {
        Object[] cells = graph.getChildCells(graph.getDefaultParent());
        Object[] cells1 = graph1.getChildCells(graph1.getDefaultParent());
        
        mxCell c;
        mxCell c1;
        
        for (int i=0; i<3; i++) {
            c = new mxCell(cells[i]);
            c1 = new mxCell(cells1[i]);
            assertEquals(c.getId(), c1.getId());
        }
        
    }
    
    @Test 
    public void testModelObjectsParameters() {
        Object[] cells = graph.getChildCells(graph.getDefaultParent());
        Object[] cells1 = graph1.getChildCells(graph1.getDefaultParent());
        
        Parameters p;
        Parameters p1;
        for (int i=0; i<3; i++) {
            if (graph.getModel().getValue(cells[i]) instanceof Parameters && graph1.getModel().getValue(cells1[i]) instanceof Parameters) {
                p=((Parameters) graph.getModel().getValue(cells[i]));
                p1=((Parameters) graph1.getModel().getValue(cells1[i]));
                assertEquals(p.getX(), p1.getX());
                assertEquals(p.getY(), p1.getY());
            }
            else
                assertEquals(graph.getModel().getValue(cells[i]), graph1.getModel().getValue(cells1[i]));
        }
        
    }

}