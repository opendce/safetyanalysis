/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.sa.gui;


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import pl.wroc.pwr.sa.gui.cli.MainCLI;
/**
 *
 * @author Adrian
 */
public class MainCLITest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
     @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }
    
    @After
    public void tearDown() {
        System.setOut(null);
    }

    /**
     * Test of constructGnuOptions method, of class MainCLI.
    */
    @Test
    public void testconstructGnuOptions() {
        
      Options gnuOptions = new Options();
      gnuOptions
                .addOption("c", "checkTheCohesionOfTheEventTree", false, "Check The Cohesion Of The Event Tree")
                .addOption("a", "analyseFaultTree", false, "Analyse fault tree and show results")
                .addOption("i", "getFuzzyImportanceIndex", false, "Get Fuzzy Importance Index of Faut Tree")
              .addOption("p", "importanceAnalise", false, "Check importance analisis")
              .addOption(OptionBuilder.withArgName("file")
                      .hasArg()
                      .withLongOpt("eventtree")
                      .withDescription("Read Event Tree from file" )
                                .create( "e" ))
                .addOption(OptionBuilder.withArgName("file")
                        .hasArg()
                        .withLongOpt("faulttree")
                        .withDescription("Read Fault Tree from file")
                        .create("f"))
                .addOption( OptionBuilder.withArgName( "scenario" )
                                .hasArg()
                                .withLongOpt("evtsce")
                                .withDescription(  "Count probability for scenarios" )
                                .create( "s" ))
              .addOption(OptionBuilder.withArgName("file")
                      .hasArg()
                      .withLongOpt("report")
                      .withDescription("Generate report")
                      .create("r"))
              .addOption( OptionBuilder.withArgName("eventname")
                      .hasArg()
                      .withLongOpt("evntnme")
                      .withDescription("Get event name")
                      .create("n"));

      assertEquals(gnuOptions.toString(), new MainCLI().constructGnuOptions().toString());
    }
    
    /**
     * Test of useGnuParser method, of class MainCLI.
     */
    @Test
    public void testuseGnuParser() {
        
        String [] commandLineArguments = new String[1];
        MainCLI mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments[0] = "-l";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("Encountered exception while parsing using GnuParser:\n"
                + "Unrecognized option: -l").toString(), is(outContent.toString()).toString());
        
        outContent.reset();
        mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments[0] = "-a";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("There is no tree that I could analyze.").toString(), 
                is(outContent.toString()).toString());
        
        outContent.reset();
        mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments[0] = "-c";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("There is no tree that I could analyze.").toString(), 
                is(outContent.toString()).toString());
        
        outContent.reset();
        mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments[0] = "-i";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("There is no tree that I could analyze.").toString(), 
                is(outContent.toString()).toString());
        
        outContent.reset();
        mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments = new String[2];
        commandLineArguments[0] = "-e";
        commandLineArguments[1] = "wrong.xml";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("\\nRead Event Tree file from: wrong.xml Fail").toString(),
                is(outContent.toString()).toString());
        
        outContent.reset();
        mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments = new String[2];
        commandLineArguments[0] = "-f";
        commandLineArguments[1] = "wrong.xml";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("\\nRead Fault Tree file from: wrong.xml Fail").toString(),
                is(outContent.toString()).toString());
        
        outContent.reset();
        mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments = new String[2];
        commandLineArguments[0] = "-e";
        commandLineArguments[1] = "treeXmltest2.xml";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("\\nRead Event Tree file from: treeXmltest2.xml Success").toString(), 
                is(outContent.toString()).toString());
        
        outContent.reset();
        mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments = new String[3];
        commandLineArguments[0] = "-e";
        commandLineArguments[1] = "treeXmltest2.xml";
        commandLineArguments[2] = "-c";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("\\nRead Event Tree file from: treeXmltest2.xml Success\\n"
                + "Result of the analysis Events Tree: \\nEvents tree is ok").toString(), 
                is(outContent.toString()).toString());

        /*
        outContent.reset();
        commandLineArguments = new String[3];
        commandLineArguments[0] = "-e";
        commandLineArguments[1] = "treeXmltest2.xml";
        commandLineArguments[2] = "-s 1";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("").toString(),
                is(outContent.toString()).toString());
        */

        outContent.reset();
        mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments = new String[2];
        commandLineArguments[0] = "-r";
        commandLineArguments[1] = "Report.odt";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("\\nCreated file: Report.odt").toString(), 
                is(outContent.toString()).toString());


        outContent.reset();
        mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments = new String[2];
        commandLineArguments[0] = "-f";
        commandLineArguments[1] = "exampleFaultTree.xml";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("\\nRead Fault Tree file from: exampleFaultTree.xml Success").toString(),
                is(outContent.toString()).toString());

        /* -a seems to be bugged
        outContent.reset();
        commandLineArguments = new String[3];
        commandLineArguments[0] = "-f";
        commandLineArguments[1] = "exampleFaultTree.xml";
        commandLineArguments[2] = "-a";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("").toString(), 
                is(outContent.toString()).toString());
        */

        outContent.reset();
        mainCLI = new MainCLI();
        mainCLI.constructGnuOptions();
        commandLineArguments = new String[3];
        commandLineArguments[0] = "-f";
        commandLineArguments[1] = "exampleFaultTree.xml";
        commandLineArguments[2] = "-i";
        mainCLI.useGnuParser(commandLineArguments);
        assertEquals( is("\\nRead Fault Tree file from: exampleFaultTree.xml Success\\nResult of the analysis Fault Tree: \\nKey: root = Value: ").toString(),
                is(outContent.toString()).toString());

        
    }

    
    /**
     * Test of printHelp method, of class MainCLI.
     */
    @Ignore
    public void testPrintHelp() {
        MainCLI testMain = new MainCLI();
        testMain.printHelp(testMain.constructGnuOptions(), 80, "GNU HELP", "End of GNU Help",
               5, 3, true, System.out);
        
        assertEquals( is("usage: SafetyAnalysis [-c] [-a] [-s scenario] [-i] [-e null] "
                + "[-f file]\\r\\nGNU HELP\\r\\n     "
                + "-a,--analyseFaultTree                Analyse fault tree and show results\\r\\n     "
                + "-c,--checkTheCohesionOfTheTree   Check The Cohesion Of The Event Tree\\r\\n     "
                + "-e,--eventtree                   Read Event Tree from file\\r\\n     "
                + "-f,--faulttree <file>            Read Fault Tree from file\\r\\n     "
                + "-i,--getFuzzyImportanceIndex     Get Fuzzy Importance Index of Faut Tree\\r\\n     "
                + "-s,--evtsce <scenario>           Count probability for scenarios\\r\\n"
                + "-r,--report <file>               Generate report\\r\n"
                + "End of GNU Help\\r\\n").toString(), is(outContent.toString()).toString());
    }
    
}
