package pl.wroc.pwr.sa.gui.steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pl.wroc.pwr.sa.api.Api.MainAPI;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
/**
 * Created by dawid on 2017-06-11.
 */
public class EventTreeSingleScenarioCounterSteps {
    private MainAPI api;
    private EventTree tree;
    private String option;
    private MembershipFunction mf;
    private ParamTrapezoid pt;

    @Before
    public void beforeScenario() throws Exception {
        api = new MainAPI();
        tree = api.readEventTreeFromFile("src/test/resources/EventTreeForAcceptanceTests.xml");
    }

    @Given("^I have an event tree$")
    public void iHaveAnEventTree() throws Exception {
        assertNotNull(tree);
    }

    @And("^I have a string with the scenario index (-?\\d)$")
    public void iHaveAStringWithTheScenarioIndex(int index) throws Exception {
        assertNotNull(index);
        option = Integer.toString(index);
    }

    @When("^I calculate the probability of the scenario and assign it to the MemberShipFunction object$")
    public void iCalculateTheProbabilityOfTheScenarioAndAsignItToTheMemberShipFunctionObject() throws Exception {
        mf = api.eventTreeScenarioCounter(option, tree);
    }

    @And("^assign parameters from MemberShipFunction object to the ParamTrapezoid object$")
    public void assignParametersFromMemberShipFunctionObjectToTheParamTrapezoidObject() throws Exception {
        pt = (ParamTrapezoid) mf.getParameters();
    }

    @Then("^The first parameter should be equal (\\d+.\\d+)$")
    public void theFirstParameterShouldBeEqual(double i) throws Exception {

        assertEquals(i, Math.round(pt.getA() * 100.0) / 100.0, 0);
    }

    @And("^The second parameter should be equal (\\d+.\\d+)$")
    public void theSecondParameterShouldBeEqual(double i) throws Exception {
        assertEquals(i, Math.round(pt.getB() * 100.0) / 100.0, 0);
    }

    @And("^The third parameter should be equal (\\d+.\\d+)$")
    public void theThirdParameterShouldBeEqual(double i) throws Exception {
        assertEquals(i, Math.round(pt.getC() * 100.0) / 100.0, 0);
    }

    @And("^The fourth parameter should be equal (\\d+.\\d+)$")
    public void theFourthParameterShouldBeEqual(double i) throws Exception {
        assertEquals(i, Math.round(pt.getD() * 100.0) / 100.0, 0);
    }
}
