package pl.wroc.pwr.sa.gui.steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pl.wroc.pwr.sa.api.Api.MainAPI;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by dawid on 2017-06-11.
 */
public class EventTreeTwoScenariosCounterSteps {
    private MainAPI api;
    private EventTree tree;
    private String option;
    private MembershipFunction mf;
    private ParamTrapezoid pt;

    @Before
    public void beforeScenario() {
        api = new MainAPI();
        tree = api.readEventTreeFromFile("src/test/resources/EventTreeForAcceptanceTests.xml");
    }

    @Given("^I have an event tree EventTreeForAcceptanceTests.xml$")
    public void iHaveAnEventTreePozarXml() throws Exception {
        assertNotNull(tree);
    }

    @And("^I have a string with the scenarios indexes (\\d+) i (\\d+)$")
    public void iHaveAStringWithTheScenariosIndexesI(int index1, int index2) throws Exception {
        assertNotNull(index1);
        assertNotNull(index2);
        option = Integer.toString(index1) + "," + Integer.toString(index2);
    }

    @When("^I calculate the probability of the scenarios and assign it to the MemberShipFunction object$")
    public void iCalculateTheProbabilityOfTheScenariosAndAsignItToTheMemberShipFunctionObject() throws Exception {
        mf = api.eventTreeScenarioCounter(option, tree);
    }

    @And("^assign calculated parameters from MemberShipFunction object to the ParamTrapezoid object$")
    public void assignCalculatedParametersFromMemberShipFunctionObjectToTheParamTrapezoidObject() throws Exception {
        pt = (ParamTrapezoid) mf.getParameters();
    }

    @Then("^The calculated first parameter should be equal (\\d+.\\d+)$")
    public void theFirstCalculatedParameterShouldBeEqual(double i) throws Exception {

        assertEquals(i, Math.round(pt.getA() * 100.0) / 100.0, 0);
    }

    @And("^The calculated second parameter should be equal (\\d+.\\d+)$")
    public void theSecondCalculatedParameterShouldBeEqual(double i) throws Exception {
        assertEquals(i, Math.round(pt.getB() * 100.0) / 100.0, 0);
    }

    @And("^The calculated third parameter should be equal (\\d+.\\d+)$")
    public void theThirdCalculatedParameterShouldBeEqual(double i) throws Exception {
        assertEquals(i, Math.round(pt.getC() * 100.0) / 100.0, 0);
    }

    @And("^The calculated fourth parameter should be equal (\\d+.\\d+)$")
    public void theFourthCalculatedParameterShouldBeEqual(double i) throws Exception {
        assertEquals(i, Math.round(pt.getD() * 100.0) / 100.0, 0);
    }
}
