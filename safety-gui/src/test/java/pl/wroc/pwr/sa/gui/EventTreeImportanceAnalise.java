package pl.wroc.pwr.sa.gui;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by radoslaw on 2017-06-17.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        format = { "pretty", "html:target/cucumber" },
        glue = "pl.wroc.pwr.sa.gui.steps",
        features = "classpath:cucumber/ImportanceAnaliseTest.feature"
)
public class EventTreeImportanceAnalise {
}
