package pl.wroc.pwr.sa.gui;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by dawid on 2017-06-11.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        format = { "pretty", "html:target/cucumber" },
        glue = "pl.wroc.pwr.sa.gui.steps",
        features = "classpath:cucumber/EventTreeTwoScenariosCounter.feature"
)
public class EventTreeTwoScenariosCounter {
}
