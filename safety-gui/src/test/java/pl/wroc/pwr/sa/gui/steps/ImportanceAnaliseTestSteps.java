package pl.wroc.pwr.sa.gui.steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pl.wroc.pwr.sa.api.Api.MainAPI;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.et.TreesService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class ImportanceAnaliseTestSteps {
    private MainAPI api;
    private EventTree tree;
    private String[] results;
    private String cut;
    private TreesService ts;

    @Before
    public void beforeScenario() {
        api = new MainAPI();
        tree = api.readEventTreeFromFile("src/test/resources/EventTreeForImportanceAnaliseTest.xml");
    }

    @Given("^Event tree EventTreeForImportanceAnaliseTest.xml$")
    public void iHaveAnEventTreeForImportanceAnalise() throws Exception {
        assertNotNull(tree);
    }

    @And("^I have a string with the event to cut \"([^\"]*)\"$")
    public void iHaveAStringWithTheEventToCut(String arg0) throws Throwable {
        assertNotNull(arg0);
        cut = arg0;
    }

    @When("^I do doAnalise for that eventTree$")
    public void iDoDoAnaliseForThatEventTree() throws Throwable {
        ts = new TreesService();
        results = ts.doAnalise(tree,cut);
    }

    @Then("^EventTreeForImportanceAnaliseTest.xml should not be changed and should still be defined by string which is equal \"([^\"]*)\"$")
    public void oldEventTreeShouldBeDefinedWithString(String arg0) throws Throwable {
        /**
         *  Return event tree structure:
         * [EventTree.id,EventTree.events.size(),EventTree.edges.size()|
         * Events.id:Event edges] i.e (1,3,7|0:1|1:2,3|2:4,5,6,7)
         *
         * @return String information about tree structure
         * Komentarz z opisu metody toString  - Klasa EventTree metoda toString.
         */
        assertEquals(arg0, results[1]);
    }

    @And("^New event tree should be defined with string \"([^\"]*)\"$")
    public void newEventTreeShouldBeDefinedWithString(String arg0) throws Throwable {
        assertEquals(arg0,results[3]);
    }

    @And("^Probability before shoud be equal \"([^\"]*)\"$")
    public void probabilityBeforeShoudBeEqual(String arg0) throws Throwable {
        assertEquals(arg0, results[2]);
    }

    @And("^Probability after shoud be equal \"([^\"]*)\"$")
    public void probabilityAfterShoudBeEqual(String arg0) throws Throwable {
        assertEquals(arg0,results[4]);
    }
}
