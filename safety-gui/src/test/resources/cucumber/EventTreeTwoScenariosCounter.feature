Feature: Etsc
        As a user
        I want to calculate the probability of two scenarios
        Scenario: Read an Event Tree from file, count probability of chosen scenarios, display it
        Given I have an event tree EventTreeForAcceptanceTests.xml
        And I have a string with the scenarios indexes 0 i 1
        When I calculate the probability of the scenarios and assign it to the MemberShipFunction object
        And assign calculated parameters from MemberShipFunction object to the ParamTrapezoid object
        Then The calculated first parameter should be equal 0.07
        And The calculated second parameter should be equal 0.18
        And The calculated third parameter should be equal 0.33
        And The calculated fourth parameter should be equal 0.52