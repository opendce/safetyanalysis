Feature: ImportanceAnaliseTest
  As a user
  I want to calculate importanceAnalise for tree
  Scenario: Read an Event Tree from file, count importanceAnalise, display it
    Given Event tree EventTreeForImportanceAnaliseTest.xml
    And I have a string with the event to cut "e5"
    When I do doAnalise for that eventTree
    Then EventTreeForImportanceAnaliseTest.xml should not be changed and should still be defined by string which is equal "(1,3,7|3:7|4:8,9|5:10,11,12,13)"
    And  New event tree should be defined with string "(1,3,6|3:7|4:9|5:10,11,12,13)"
    And Probability before shoud be equal "0.0"
    And Probability after shoud be equal "0.0"