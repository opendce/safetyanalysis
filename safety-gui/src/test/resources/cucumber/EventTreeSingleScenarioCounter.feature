Feature: Etsc
        As a user
        I want to calculate the probability of one scenario
        Scenario: Read an Event Tree from file, count probability of chosen scenario, display it
        Given I have an event tree
        And I have a string with the scenario index 0
        When I calculate the probability of the scenario and assign it to the MemberShipFunction object
        And assign parameters from MemberShipFunction object to the ParamTrapezoid object
        Then The first parameter should be equal 0.02
        And The second parameter should be equal 0.06
        And The third parameter should be equal 0.12
        And The fourth parameter should be equal 0.2