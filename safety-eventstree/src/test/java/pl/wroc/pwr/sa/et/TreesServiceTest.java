package pl.wroc.pwr.sa.et;

import org.junit.Test;
import java.util.ArrayList;
import org.custommonkey.xmlunit.Diff;
import static org.custommonkey.xmlunit.XMLAssert.assertXMLEqual;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Test;
import org.junit.Before;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import pl.wroc.pwr.sa.fr.FuzzyLogicFacade;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;
import pl.wroc.pwr.sa.fr.Scale;
import static org.junit.Assert.*;

public class TreesServiceTest {

    @Test
    public void testGetTreeFromCollection() throws Exception {
        Map<Integer, EventTree> col = new HashMap<Integer, EventTree>();
        Event e1 = new Event();
        Event e2 = new Event();
        e2.setParent(e1);
        e1.setName("Init event");

        EventTree tree = new EventTree();

        tree.setId(5);
        tree.setDescription("Pamparapam");
        tree.addInitEvent(e1);
        tree.addEvent(e2);

        col.put(0, tree);

        TreesService ts = new TreesService(col);
        assertNull(ts.getTreeFromCollection("Brak"));
        assertEquals(ts.getTreeFromCollection("Pamparapam"), tree);
        //assertEquals(ts.getTreeFromCollection("Pamparapam").toString(), "(5,2,3|0:0|1:1,2)");
    }
    
    /*
    @Test
    public void testFindEventByName() throws Exception
    {
        Event a = new Event();
        Event b = new Event();
        
        a.setName("a");
        b.setName("b");
        b.setParent(a);
        
        EventTree tree = new EventTree();
        tree.addInitEvent(a);
        tree.addEvent(b);
        
        Map<Integer, EventTree> col = new HashMap<Integer, EventTree>();
        col.put(0, tree);

        TreesService ts = new TreesService(col);
        
        
    }
    */
    
    @Test
    public void testModificationEventTree() throws Exception {
        Map<Integer, EventTree> col = new HashMap<Integer, EventTree>();
        Event e1 = new Event();
        Event e2 = new Event();
        e2.setParent(e1);
        e1.setName("Init event");

        EventTree tree = new EventTree();

        tree.setId(5);
        tree.setDescription("Pamparapam");
        tree.addInitEvent(e1);
        tree.addEvent(e2);

        col.put(0, tree);
        TreesService ts = new TreesService(col);
        assertEquals(null, ts.modificationEventTree("Brak", "Init event"));
    }
}