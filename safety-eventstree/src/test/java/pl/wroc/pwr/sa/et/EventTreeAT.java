package pl.wroc.pwr.sa.et;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Patryk on 11.05.2017.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        format = { "pretty", "html:target/cucumber" },
        glue = "pl.wroc.pwr.sa.et.steps",
        features = "classpath:cucumber/EventTree.feature"
)
public class EventTreeAT {
}
