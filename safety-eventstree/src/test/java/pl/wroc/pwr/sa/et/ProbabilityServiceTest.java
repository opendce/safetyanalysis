/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.sa.et;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import pl.wroc.pwr.sa.fr.FuzzyLogicFacade;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTriangle;
import pl.wroc.pwr.sa.fr.Scale;

/**
 *
 * @author gracz
 */
public class ProbabilityServiceTest {

    @Before
    public void preTest() {
        Event.count = 0;
        Edge.count = 0;
    }

    /**
     * Test of computing probability for consequence
     */
    @Test
    public void testComputingProbabilityForConsequence() throws Exception {
        System.out.println("testComputingProbabilityForConsequence");

        // create variable
        EventTree tree = new EventTree();
        ProbabilityService service = new ProbabilityService();
        Event e1 = new Event();
        Event e2 = new Event();
        FuzzyLogicFacade flf = new FuzzyLogicFacade();
        MembershipFunction probabilityFirst = flf.createTriangle(new ParamTriangle(0.2, 0.3, 0.7, Scale.LINEAR), "tf1");
        MembershipFunction probabilitySecond = flf.createTriangle(new ParamTriangle(0.1, 0.4, 0.6, Scale.LINEAR), "tf2");
        tree.addNewConsequence("con1");

        // set variable        
        tree.setId(1);
        e1.setProbabilty(probabilityFirst);
        e2.setProbabilty(probabilitySecond);
        e1.setChild(e2);
        e2.setParent(e1);
        tree.addInitEvent(e1);
        tree.addEvent(e2);
        Consequence c1 = tree.addNewConsequence("con1");
        ArrayList<Consequence> l = new ArrayList<Consequence>();
        l.add(c1);
        for (Edge e : tree.getLeafList()) {
            tree.assignLeafToConsequence(e, c1);
        }
        
        MembershipFunction p = service.computeProbabilityForConsequence(tree, l);
        assertNotNull(p);
        assertNotSame(probabilityFirst, p);
        assertNotSame(probabilitySecond, p);
        
    }

    /**
     * Test of computing probability for leaf
     */
    @Test
    public void testComputingProbabilityForLeaf() throws Exception {
        System.out.println("testComputingProbabilityForLeaf");
        
        
        // create variable
        EventTree tree = new EventTree();
        ProbabilityService service = new ProbabilityService();
        Event e1 = new Event();
        Event e2 = new Event();
        FuzzyLogicFacade flf = new FuzzyLogicFacade();
        MembershipFunction probabilityFirst = flf.createTriangle(new ParamTriangle(0.2, 0.3, 0.7, Scale.LINEAR), "tf1");
        MembershipFunction probabilitySecond = flf.createTriangle(new ParamTriangle(0.1, 0.4, 0.6, Scale.LINEAR), "tf2");
        tree.addNewConsequence("con1");
        
        
        // set variable        
        tree.setId(1);
        e1.setProbabilty(probabilityFirst);
        e2.setProbabilty(probabilitySecond);
        e1.setChild(e2);
        e2.setParent(e1);
        tree.addInitEvent(e1);
        tree.addEvent(e2);
        List<Edge> leaf = tree.getLeafList();
        
        
        MembershipFunction p = service.computeProbabilityForLeaf(tree, leaf);
        assertNotNull(p);
        assertNotSame(probabilityFirst, p);
        assertNotSame(probabilitySecond, p);
    }
}