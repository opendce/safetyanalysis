/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.sa.et;

import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import org.custommonkey.xmlunit.Diff;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.custommonkey.xmlunit.XMLAssert.assertXMLEqual;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import pl.wroc.pwr.sa.fr.FuzzyLogicFacade;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;
import pl.wroc.pwr.sa.fr.Scale;

public class EventTreeTest {

    @Before
    public void preTest() {
        Event.count = 0;
        Edge.count = 0;
    }

    /**
     * Test of create event tree with specified id
     */
    @Test
    public void testCreateTree() {
        System.out.println("testCreateEventTree");

        // Tree behavior
        EventTree tree = new EventTree();
        tree.setId(1);
        assertEquals("(1,0,0|)", tree.toString());

        tree.setId(Integer.MAX_VALUE);
        assertEquals("(" + Integer.MAX_VALUE + ",0,0|)", tree.toString());

        tree.setId(Integer.MIN_VALUE);
        assertEquals("(" + Integer.MIN_VALUE + ",0,0|)", tree.toString());
    }

    /**
     * Test of adding init event to tree with auto edges
     */
    @Test
    public void testAddingInitEvent() throws Exception {
        System.out.println("testAddingInitEvent");

        // Tree behavior
        Event e1 = new Event();
        EventTree tree = new EventTree();
        tree.setId(5);
        tree.addInitEvent(e1);
        assertEquals("(5,1,1|0:0)", tree.toString());
    }

    /**
     * Test of adding init event to tree with existing events
     */
    @Test(expected = Exception.class)
    public void testAddingInitEventException() throws Exception {
        System.out.println("testAddingInitEventException");

        // Tree behavior
        Event e1 = new Event();
        Event e2 = new Event();
        EventTree tree = new EventTree();
        tree.setId(5);
        tree.addEvent(e2);
        tree.addInitEvent(e1);
    }

    /**
     * Test of adding second event to tree (first is init)
     */
    @Test
    public void testAddingSecondEvent() throws Exception {
        System.out.println("testAddingSecondEvent");

        // Tree behavior
        Event e1 = new Event();
        Event e2 = new Event();
        e2.setParent(e1);

        EventTree tree = new EventTree();
        tree.setId(5);
        tree.addInitEvent(e1);
        tree.addEvent(e2);
        assertEquals("(5,2,3|0:0|1:1,2)", tree.toString());

        // auto event parents and clear
        tree.clear();
        e1 = new Event();
        e2 = new Event();
        tree.setId(2);
        tree.addInitEvent(e1);
        tree.addEvent(e2);
        assertEquals("(2,2,3|0:0|1:1,2)", tree.toString());
    }

    /**
     * Test of adding third event to tree
     */
    @Test
    public void testAddingThirdEvent() throws Exception {
        System.out.println("testAddingThirdEvent");

        // Tree behavior
        Event e1 = new Event();
        Event e2 = new Event();
        Event e3 = new Event();

        EventTree tree = new EventTree();
        tree.setId(5);
        tree.addInitEvent(e1);
        tree.addEvent(e2);
        tree.addEvent(e3);
        assertEquals("(5,3,7|0:0|1:1,2|2:3,4,5,6)", tree.toString());
    }

    /**
     * Test of removing events from tree
     */
    @Test
    public void testRemovingEvents() throws Exception {
        System.out.println("testRemovingEvents");

        // Tree behavior
        Event e1 = new Event();
        Event e2 = new Event();
        Event e3 = new Event();

        EventTree tree = new EventTree();
        tree.setId(1);
        tree.addInitEvent(e1);
        tree.addEvent(e2);
        tree.addEvent(e3);
        tree.removeEvent(e2);
        assertEquals("(1,1,1|0:0)", tree.toString());

        tree.addEvent(e2);
        tree.addEvent(e3);
        tree.removeEvent(e3);
        assertEquals("(1,2,3|0:0|1:7,8)", tree.toString());

        // Methods behavior
        assertNull(tree.removeEvent(e3));
        assertEquals(e2, tree.removeEvent(e2));
    }

    /**
     * Test of removing edges from tree
     */
    @Test
    public void testRemovingEdges() throws Exception {
        System.out.println("testRemovingEvents");

        // Tree behavior
        Event e1 = new Event();
        Event e2 = new Event();
        Event e3 = new Event();

        EventTree tree = new EventTree();
        tree.setId(1);
        tree.addInitEvent(e1);
        tree.addEvent(e2);
        tree.addEvent(e3);
        tree.removeEdge(tree.getEdges().get(1)); //1 is edge of e1 event
        assertEquals("(1,3,4|0:0|1:2|2:5,6)", tree.toString());
    }

    /**
     * Test of updatings events from tree
     */
    @Test
    public void testUpdatingEvents() throws Exception {
        System.out.println("testUpdatingEvents");

        // Tree behavior
        Event e1 = new Event();
        Event e2 = new Event();
        Event e3 = new Event();
        e3.setId(1);
        EventTree tree = new EventTree();
        tree.setId(1);
        tree.addInitEvent(e1);
        tree.addEvent(e2);
        tree.updateEvent(e3);
        assertEquals("(1,2,3|0:0|1:1,2)", tree.toString());

        // Methods behavior
        assertEquals(e3, tree.updateEvent(e2));
    }

    /**
     * Test of adding new consequence
     */
    @Test
    public void testAddingNewConsequence() {
        System.out.println("testAddingNewConsequence");
        EventTree tree = new EventTree();
        tree.setId(1);
        Integer amountOfConsequences = 5;
        for (int i = 0; i < amountOfConsequences; i++) {
            String desc = "con" + i;
            tree.addNewConsequence(desc);
        }
        assertEquals((Integer) amountOfConsequences, (Integer) tree.getConsequences().size());
    }

    /**
     * Test of removing consequence from set
     */
    @Test
    public void testRemovingConesquence() {
        System.out.println("testRemovingConesquence");
        EventTree tree = new EventTree();
        tree.setId(1);
        Integer amountOfConsequences = 10;
        for (int i = 0; i < amountOfConsequences; i++) {
            String desc = "desc" + i;
            Consequence c = tree.addNewConsequence("desc1");
            assertTrue(tree.removeConesquence(c));
        }
    }

    /**
     * Test of assigning leaf to consequence
     */
    @Test
    public void testAssignLeafToConsequence() throws Exception {
        EventTree tree = new EventTree();
        tree.setId(1);
        Event e1 = new Event();
        Event e2 = new Event();
        e2.setParent(e1);
        Consequence c = tree.addNewConsequence("con1");
        for (Edge e : tree.getLeafList()) {
            tree.assignLeafToConsequence(e, c);
            assertTrue(c.getLeafsList().contains(e));
        }
    }

    /**
     * Test checking the cohesion of the tree
     */
    @Test
    public void testCheckingTheCohesionOfTheTree() throws Exception  {
        System.out.println("testCheckingTheCohesionOfTheTree");
        EventTree tree = new EventTree();
        tree.setId(1);
        Event e1 = new Event();
        Event e2 = new Event();
        Event e3 = new Event();
        e1.setChild(e2);
        e2.setParent(e1);
        e2.setChild(e3);
        e3.setParent(e2);
        e3.setChild(null);
        tree.addInitEvent(e1);
        tree.addEvent(e2);
        tree.addEvent(e3);
        try {
            tree.checkTheCohesionOfTheTree();
        } catch (Exception e) {
            assertTrue(false);
        }
        assertTrue(true);
    }
    
    @Test
    public void testReadingEventEmptyTreeFromXMLString() throws Exception
    {
        System.out.println("testReadingEventTreeFromXMLString");
        
        EventTree correctEventTree = new EventTree();
        correctEventTree.setId(1);
        
        System.out.println("Test Case 1 : Empty Tree");
        String xml = 
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "\n<eventTree>"
                + "\n    <edges/>"
                + "\n    <events/>"
                + "\n    <id>1</id>"
                + "\n</eventTree>"
                + "\n";
        
        EventTree testSubject = EventTree.loadFromXMLString(xml);
        assertEquals(testSubject.toString(),correctEventTree.toString());
       
    }
    @Test
    public void testReadingEventCompleteTreeFromXMLString() throws Exception
    {
       System.out.println("Test Case 3 : Direct Tree with MembershipFunctions");    

       StringBuilder sb = new StringBuilder();
       BufferedReader br = new BufferedReader(new FileReader("treeXmltest4.xml"));
       while(true) {
           String sCurrentLine = br.readLine();
           if(sCurrentLine!=null) sb.append(sCurrentLine);
           else break;   
       }
       br.close();
       
       EventTree testSubject = EventTree.loadFromXMLString(sb.toString());
       
       double functionValues[] = {0.5,0.5,0.5,0.5};
       MembershipFunction mF = createFunction(functionValues,Scale.LINEAR);       
       
       EventTree correctEventTree = new EventTree();
       correctEventTree.setId(1);
         
       Edge.count = 7;
       Event.count = 3;
       Event e4 = new Event();    
       e4.setName("e4");        
       e4.setProbabilty(mF);
       Event e5 = new Event();      
       e5.setName("e5");
       e5.setParent(e4); 
       e5.setProbabilty(mF);
       Event e6 = new Event();      
       e6.setName("e6");        
       e6.setParent(e5);
       e6.setProbabilty(mF);
        
       correctEventTree.addInitEvent(e4);
       correctEventTree.addEvent(e5);   
       correctEventTree.addEvent(e6); 
        
       assertEquals(testSubject.toString(),correctEventTree.toString());
       
    }
    @Test
    public void testReadingEventIncompleteTreeFromXMLString() throws Exception
    {
       System.out.println("Test Case 2 : Direct Tree Without MembershipFunctions");    

       StringBuilder sb = new StringBuilder();
       BufferedReader br = new BufferedReader(new FileReader("treeXmltest2.xml"));
       while(true) {
           String sCurrentLine = br.readLine();
           if(sCurrentLine!=null) sb.append(sCurrentLine);
           else break;   
       }
       br.close();
       EventTree testSubject = EventTree.loadFromXMLString(sb.toString());
        // czytanie obiektów Edges z wczytanego drzewa
       Map<Integer, Edge> testEdge = testSubject.getEdges();
        Iterator<Integer> itEdges = testEdge.keySet().iterator();
        while (itEdges.hasNext()) {
            System.out.println(testEdge.get(itEdges.next()).toString() + " OK");
        }
        // czytanie obiektów Event z wczytanego drzewa
        Map<Integer, Event> testEvent = testSubject.getEvents();
        Iterator<Integer> itEvents = testEvent.keySet().iterator();
        while (itEvents.hasNext()) {
            System.out.println(testEvent.get(itEvents.next()).toString() + " OK");
        }
       assertFalse(testSubject.getEdges().isEmpty());
       assertFalse(testSubject.getEvents().isEmpty());
       EventTree correctEventTree = new EventTree();
       correctEventTree.setId(1);
       
       Edge.count = 0;
       Event.count = 0;
       Event e1 = new Event();    
       e1.setName("e1");   
       Event e2 = new Event();      
       e2.setName("e2");
       e2.setParent(e1);       
       Event e3 = new Event();      
       e3.setName("e3");        
       e3.setParent(e2);
       correctEventTree.addInitEvent(e1);
       correctEventTree.addEvent(e2);   
       correctEventTree.addEvent(e3); 
       assertEquals(testSubject.toString(),correctEventTree.toString());
       
    }
    
    
    @Test
    public void testSavingEventEmptyTreeToXMLString() throws Exception
    {
        //Ignorowanie enterów, tabów i innych szkaradnych rzeczy
        XMLUnit.setIgnoreWhitespace(true);

        System.out.println("testSavingEventTreeToXMLString");
             
        //Puste drzewo
        System.out.println("Test Case 1 : Empty Tree");
        String correctXml = 
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<eventTree>"
                + "<id>1</id>"
                + "</eventTree>";
        
        EventTree tree1 = new EventTree();
        tree1.setId(1);
        
        String xml = EventTree.saveTreeToXMLString(tree1); 
        Diff myDiff = new Diff(correctXml, xml);
        assertTrue("XML similar " + myDiff.toString(),myDiff.similar());
    }
    
    @Test
    public void testSavingEventIncompleteTreeToXMLString() throws Exception
    {
        //Ignorowanie enterów, tabów i innych szkaradnych rzeczy
        XMLUnit.setIgnoreWhitespace(true);
        
        //Drzewo w formie e1 -> e2 -> e3 bez rozgałęzień
       System.out.println("Test Case 2 : Direct Tree Without MembershipFunctions");
       

       StringBuilder sb = new StringBuilder();
       BufferedReader br = new BufferedReader(new FileReader("treeXmltest2.xml"));
       while(true) {
           String sCurrentLine = br.readLine();
           if(sCurrentLine!=null) sb.append(sCurrentLine);
           else break;   
       }
       br.close();
       String correctXml = sb.toString();
       
       EventTree tree2 = new EventTree();
       tree2.setId(1);
       Edge.count = 0; 
       Event.count = 0;
       Event e1 = new Event();    
       e1.setName("e1");   
       Event e2 = new Event();      
       e2.setName("e2");
       e2.setParent(e1);       
       Event e3 = new Event();      
       e3.setName("e3");        
       e3.setParent(e2);
        
       tree2.addInitEvent(e1);
       tree2.addEvent(e2);   
       tree2.addEvent(e3); 
       String xml = EventTree.saveTreeToXMLString(tree2);
       
       Diff myDiff = new Diff(correctXml, xml);
       assertTrue("XML similar " + myDiff.toString(), myDiff.similar());
    }
      
    @Test
    public void testSetEventFalse() throws Exception
    {
        // Arrage
        Event a = new Event();
        Event b = new Event();
        Event c = new Event();
        Event d = new Event();
        EventTree tree = new EventTree();
        
        // Act
        a.setName("a");
        b.setName("b");
        b.setParent(a);
        c.setName("c");
        c.setParent(b);
        d.setName("d");
        d.setParent(c);
        
        tree.addInitEvent(a);
        tree.addEvent(b);
        tree.addEvent(c);
        tree.addEvent(d);

        tree.setEventFalse(b);

        // Assert "(0,4,15|0:0|1:1,2|2:3,4,5,6|3:7,8,9,10,11,12,13,14)" pierwotne drzewo
        assertEquals("(0,4,2|0:0|1:2|2|3)", tree.toString());
    }
    
    @Test
    public void testFindEdgesWithTrueStatement() throws Exception
    {
        // Arrage
        Event a = new Event();
        Event b = new Event();
        Event c = new Event();
        EventTree tree = new EventTree();
        ArrayList<Edge> edges;
        Edge e;
        
        // Act
        a.setName("a");
        b.setName("b");
        b.setParent(a);
        c.setName("c");
        c.setParent(b);
        tree.addInitEvent(a);
        tree.addEvent(b);
        tree.addEvent(c);
        edges = tree.findEdgesWithTrueStatement(b);
        e = edges.get(0);
        
        // Assert
        assertEquals(edges.size(), 1);
        assertEquals(e.toString(), "[1,1,0,3|4,True,True]");
    }
    
    @Test
    public void testSavingEventCompleteTreeToXMLString() throws Exception
    {
        //Ignorowanie enterów, tabów i innych szkaradnych rzeczy
       XMLUnit.setIgnoreWhitespace(true);
       System.out.println("Test Case 3 : Direct Tree with MembershipFunctions");
       
       StringBuilder sb = new StringBuilder();
       BufferedReader br = new BufferedReader(new FileReader("treeXmltest4.xml"));
       while(true) {
           String sCurrentLine = br.readLine();
           if(sCurrentLine!=null) sb.append(sCurrentLine);
           else break;   
       }
       br.close();
       
       double functionValues[] = {0.5,0.5,0.5,0.5};
       MembershipFunction mF = createFunction(functionValues,Scale.LINEAR);
       
       String correctXml = sb.toString();
       
       EventTree tree3 = new EventTree();
       tree3.setId(1);
       Edge.count = 7;
       Event.count = 3;
       Event e4 = new Event();    
       e4.setName("e4");        
       e4.setProbabilty(mF);
       Event e5 = new Event();      
       e5.setName("e5");
       e5.setParent(e4); 
       e5.setProbabilty(mF);
       Event e6 = new Event();      
       e6.setName("e6");        
       e6.setParent(e5);
       e6.setProbabilty(mF);
        
       tree3.addInitEvent(e4);
       tree3.addEvent(e5);   
       tree3.addEvent(e6); 
       String xml = EventTree.saveTreeToXMLString(tree3);
       Diff myDiff = new Diff(correctXml, xml);
       assertTrue(true);
    }
     private MembershipFunction createFunction(double[] values, Scale scale) {
        ParamTrapezoid params;
        FuzzyLogicFacade flo = new FuzzyLogicFacade();
        MembershipFunction function = null;
        try {
            params = new ParamTrapezoid(values[0],values[2],values[2], values[3], scale);
            
            function = flo.createTrapezoid(params, "nn");
        } catch (Exception ex) {
            fail(ex.getCause().toString());
            Logger.getLogger(ScenarioProbabilityCounterTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return function;
    }

    @Test
    public void testSavingEventTreeAfterRemoveEvent() throws Exception {

        double functionValues[] = {0.5,0.5,0.5,0.5};
        MembershipFunction mF = createFunction(functionValues,Scale.LINEAR);

        EventTree tree = new EventTree();
        tree.setId(1);
        Edge.count = 15;
        Event.count = 4;
        Event e4 = new Event();
        e4.setName("e4");
        e4.setProbabilty(mF);
        Event e5 = new Event();
        e5.setName("e5");
        e5.setParent(e4);
        e5.setProbabilty(mF);
        Event e6 = new Event();
        e6.setName("e6");
        e6.setParent(e5);
        e6.setProbabilty(mF);

        tree.addInitEvent(e4);
        tree.addEvent(e5);
        tree.addEvent(e6);

        String xml1 = EventTree.saveTreeToXMLString(tree);

        assertEquals("[5,4,6,e5]", tree.getEvents().get(5).toString());
        assertEquals("[6,5,null,e6]",tree.getEvents().get(6).toString());

        tree.removeEvent(e6);

        assertEquals("[5,4,null,e5]",tree.getEvents().get(5).toString());
        Assert.assertNull(tree.getEvents().get(6));
        Assert.assertNull(tree.getEvents().get(5).getChild());

        //wygląda na to że usunęło dobrze, ale dlaczego są nieprawidłowości w xml?
        String xml2 = EventTree.saveTreeToXMLString(tree);

        /*System.out.println(xml1.toString());
        System.out.println(xml2.toString());*/

    }
}