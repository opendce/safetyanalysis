package pl.wroc.pwr.sa.et.steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import pl.wroc.pwr.sa.et.Event;
import pl.wroc.pwr.sa.et.EventTree;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import static org.junit.Assert.*;

/**
 * Created by Patryk on 11.05.2017.
 */
public class EventTreeATSteps {

    private EventTree tree;
    private BufferedReader bufferedReader;
    private String filepath;
    private EventTree newTree;

    @Before
    public void beforeScenario() {
        tree = new EventTree();
    }

    @Given("^I have an event tree$")
    public void iHaveAnEventTree() throws Exception {
        assertNotNull(tree);
    }

    @When("^I set its ID to (-?\\d)$")
    public void iSetItsIDTo(int ID) throws Exception {
        tree.setId(ID);
    }

    @Then("^The event tree should be defined with string \"([^\"]*)\"$")
    public void theEventTreeShouldBeDefinedWithString(String eventTreeString) throws Exception {
        assertEquals(eventTreeString, tree.toString());
    }

    @And("^I add an init event$")
    public void iAddAnInitEvent() throws Exception {
        tree.addInitEvent(new Event());
    }

    @And("^I set its description to \"([^\"]*)\"$")
    public void iSetItsDescriptionTo(String description) throws Exception {
        tree.setDescription(description);
    }

    @And("^Its description should be \"([^\"]*)\"$")
    public void itsDescriptionShouldBe(String description) throws Exception {
        assertEquals(description, tree.getDescription());
    }

    @And("^I add a second event to tree$")
    public void iAddASecondEventToTree() throws Exception {
        tree.addEvent(new Event());
    }

    @And("^I add a third event to tree$")
    public void iAddAThirdEventToTree() throws Exception {
        tree.addEvent(new Event());
    }

    @When ("^I save my event tree to XML file called \"([^\"]*)\"$")
    public void iSaveMyEventTreeToXMLFileCalled(String filename) throws Exception {
        EventTree.saveTreeToXML(filename, tree);
        filepath = filename;
    }

    @Then("^A new event tree should be created$")
    public void aNewEventTreeShouldBeCreated() throws Exception {
        assertNotNull(newTree);
    }

    @Then("^XML file should be available$")
    public void xmlFileShouldBeAvailable() throws Exception {
        assertTrue(new File(filepath).exists());
    }

    @When("^I load stored xml file$")
    public void iLoadStoredXmlFile() throws Exception {
        bufferedReader = new BufferedReader(new FileReader(filepath));
    }

    @Then("^File should not be empty$")
    public void fileShouldNotBeEmpty() throws Exception {
        assertNotNull(bufferedReader.readLine());
        bufferedReader.close();
    }

    @And("^XML file should contain my event tree$")
    public void xmlFileShouldContainMyEventTree() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document doc = factory.newDocumentBuilder().parse(new File(filepath));

        XPathFactory xFactory = XPathFactory.newInstance();
        XPath xpath = xFactory.newXPath();
        XPathExpression expr = xpath.compile("//eventTree//description[contains(.,'Drzewo zdarzen')]");
        Object result = expr.evaluate(doc,XPathConstants.NODESET);
        NodeList nodes = (NodeList)result;
        assertTrue(nodes.getLength() > 0);
    }

    @When("^I load an event tree from stored xml file$")
    public void iLoadAnEventTreeFromStoredXmlFile() throws Exception {
        newTree = EventTree.loadFromXML(filepath);
    }

    @And("^New events tree description should be \"([^\"]*)\"$")
    public void newEventsTreeDescriptionShouldBe(String description) throws Exception {
        assertEquals(description, newTree.getDescription());
    }

    @And("^New event tree should be defined with string \"([^\"]*)\"$")
    public void newEventTreeShouldBeDefinedWithString(String eventTreeString) throws Exception {
        assertEquals(eventTreeString, newTree.toString());
    }

    @But("^New event tree should not be defined with string \"([^\"]*)\"$")
    public void newEventTreeShouldNotBeDefinedWithString(String eventTreeString) throws Exception {
        assertFalse(eventTreeString.equals(newTree.toString()));
    }
}
