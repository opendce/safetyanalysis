/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.sa.et;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;


public class EventTest {
    
    @Before 
    public void preTest() {
        Event.count = 0;
    }

    /**
     * Test of creating new event
     */
    @Test
    public void testcreateEvent() {
        System.out.println("testcreateEvent");
        
        Event e1 = new Event();
        Event e2 = new Event();
        e1.setName("Init event");
        assertEquals("[0,null,null,Init event]", e1.toString());
        
        e2.setId(5);
        e1.setChild(e2);
        assertEquals("[0,null,5,Init event]", e1.toString());
    }

}
