/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.sa.et;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import pl.wroc.pwr.sa.fr.FuzzyLogicFacade;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;
import pl.wroc.pwr.sa.fr.Scale;

/**
 *
 * @author Karol
 */
public class ScenarioProbabilityCounterTest {
    
    private static final String FUNCTION_NAME = "";
    private static final String FAIL = "Exception thrown , propably by ";
    private static final double functionValues[] = {0.5,0.5,0.5,0.5};
    
    private MembershipFunction function = createFunction(functionValues, Scale.LINEAR);
    
    private List<Edge> edges;
    private List<Event> events;
    
    
    @Before
    public void setUp() { 
        edges = new ArrayList<Edge>(Arrays.asList(new Edge(),new Edge(),new Edge(),new Edge()));
        events = new ArrayList<Event>(Arrays.asList(new Event(),new Event(),new Event(),new Event()));
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void shouldNotRewindButMultiplyFunctionLinear_countPropabilityOfScenario() throws Exception {
        //when the sun is shining
        double[] pV = {0.0625,0.0625,0.0625,0.0625}; // pV - properValues for function
        
        MembershipFunction expectedObject = createFunction(pV, Scale.LINEAR);
        
        MembershipFunction returnedObject = null;
        //back in time then
        try{
           prepareEdgesAndEvents(function, true);
           returnedObject = ScenarioProbabilityCounter.countProbalityOfScenario(edges.get(edges.size() -1));
        }catch(Exception e) {
            fail(FAIL + this.toString());
            throw new Exception(e);
        }
        
        //miserably failing to achieve those
        
        assertTrue(expectedObject.equals(returnedObject));
    }
    
    
    @Test
    public void shouldRewindAndMultiplyLinear_countPropabilityOfScenario() throws Exception {
        double[] expectedValues = {0.0625,0.0625,0.0625,0.0625};
        MembershipFunction expectedObject = createFunction(expectedValues, Scale.LINEAR);
        MembershipFunction returnedObject = null;
        
        try{
            prepareEdgesAndEvents(function, false);
           returnedObject = ScenarioProbabilityCounter.countProbalityOfScenario(edges.get(edges.size() -1));
        }catch(Exception e) {
            fail(FAIL + this.toString());
            throw new Exception(e);
        }
        
        assertTrue(returnedObject.equals(expectedObject));
    }
    
    @Test
    public void shouldCountPropabilityOfGivenConsequience() throws Exception {
        double[] vexpectedValues = {0.0625,0.0625,0.0625,0.0625};
        MembershipFunction expectedObject1 = createFunction(vexpectedValues, Scale.LINEAR);
        MembershipFunction expectedObject2 = createFunction(vexpectedValues, Scale.LINEAR);
        List<MembershipFunction> returnedObject = null;
        
        Consequence consequence = new Consequence();
        consequence.addLeaf(edges.get(edges.size() -1));
        
        try {     
           prepareEdgesAndEvents(function, true);
           returnedObject = ScenarioProbabilityCounter.countProbalityOfConsequence(consequence);
        } catch (Exception ex) {
            fail(FAIL + this.toString());
            throw new Exception(ex);
        }
        
        assertEquals(returnedObject.size(), 2);
        assertEquals(returnedObject.get(0), expectedObject1);
        assertEquals(returnedObject.get(1), expectedObject2);
    }
    
    @Test
    public void shouldCountPropabilitiesForGivenConsequences() throws Exception {
        double[] expectedValues = {0.0625, 0.0625, 0.0625, 0.0625};
        
        MembershipFunction expectedFunc1 = createFunction(expectedValues, Scale.LINEAR);
        
        List<MembershipFunction> returnedValues = null;
        
        List<Consequence> consequences = new ArrayList<Consequence>();
        Consequence consequence = new Consequence();
        consequence.addLeaf(edges.get(edges.size() -1));
        
        consequences.add(consequence);
        try {
            prepareEdgesAndEvents(function, true);
            returnedValues = ScenarioProbabilityCounter.countProbalityOfTrees(consequences);
        } catch (Exception ex) {
            fail(FAIL + this.toString());
            throw new Exception(ex);
        }
        
        assertEquals(returnedValues.size(), 3);
        for(final MembershipFunction function : returnedValues ) {
            assertTrue(function.equals(expectedFunc1));
        }
        
    }

    @Test
    public void testWholeTree1() {
        double[][] functionValuesLocal = {
                {0.1,0.2,0.3,0.4},
                {0.1,0.2,0.3,0.4},
                {0.1,0.2,0.3,0.4}
        };
        
/*        double[][] expectedFunctionValues = {
                {0.001,0.08,0.027,0.064},
                {0.001,0.08,0.027,0.064},
                {0.006,0.028,0.128,0.144},
                {0.006,0.028,0.128,0.144},
                {0.006,0.028,0.128,0.144},
                {0.006,0.028,0.128,0.144},
                {0.036,0.098,0.192,0.342},
                {0.036,0.098,0.192,0.342},
                {0.049,0.160,0.475,0.694} 
        };
*/        
        double[][] expectedFunctionValues = {
                {0.036,0.147,0.147,0.32400000000000007},
                {0.036,0.147,0.147,0.32400000000000007},
                {0.216,0.3429999999999999,0.3429999999999999,0.7290000000000001},
                {0.216,0.3429999999999999,0.3429999999999999,0.7290000000000001},
                {0.036,0.147,0.147,0.32400000000000007},
                {0.036,0.147,0.147,0.32400000000000007},
                {0.216,0.3429999999999999,0.3429999999999999,0.7290000000000001},
                {0.216,0.3429999999999999,0.3429999999999999,0.7290000000000001},
                {0.504,0.9799999999999998,0.9799999999999998,1.0} 
        };
        
        List<MembershipFunction> expectedFunctions = new ArrayList();
        
        expectedFunctions.add(createFunction(expectedFunctionValues[0], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[1], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[2], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[3], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[4], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[5], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[6], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[7], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[8], Scale.LINEAR));
        
        List<Consequence> consequences = buildTreeTest1(functionValuesLocal, Scale.LINEAR);
        
        List<MembershipFunction> returnedFunctions = null;
        try {
            returnedFunctions = ScenarioProbabilityCounter.countProbalityOfTrees(consequences);
        } catch (Exception ex) {
            fail();
            Logger.getLogger(ScenarioProbabilityCounterTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        assertNotNull(returnedFunctions);
        assertFunctionsEqual(returnedFunctions, expectedFunctions);
    }
    
    @Test
    public void testWholeTree2() {
        double[][] functionValuesLocal = {
                {0.1,0.2,0.3,0.4},
                {0.1,0.2,0.3,0.4},
                {0.1,0.2,0.3,0.4}
        };
        
        /*double[][] expectedFunctionValues = {
                {0.001,0.08,0.027,0.064},
                {0.036,0.098,0.192,0.342},
                {0.037,0.106,0.219,0.406},
                {0.006,0.028,0.128,0.144},
                {0.006,0.028,0.128,0.144},
                {0.044,0.134,0.347,0.550} 
        };*/
        double[][] expectedFunctionValues = {
                {0.036,0.147,0.147,0.32400000000000007},
                {0.216,0.3429999999999999,0.3429999999999999,0.7290000000000001},
                {0.252,0.4899999999999999,0.4899999999999999,1.0},
                {0.216,0.3429999999999999,0.3429999999999999,0.7290000000000001},
                {0.216,0.3429999999999999,0.3429999999999999,0.7290000000000001},
                {0.46799999999999997,0.8329999999999997,0.8329999999999997,1.0} 
        };
        
        List<MembershipFunction> expectedFunctions = new ArrayList();
        
        expectedFunctions.add(createFunction(expectedFunctionValues[0], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[1], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[2], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[3], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[4], Scale.LINEAR));
        expectedFunctions.add(createFunction(expectedFunctionValues[5], Scale.LINEAR));
        
        List<Consequence> consequences = buildTreeTest2(functionValuesLocal, Scale.LINEAR);
        
        List<MembershipFunction> returnedFunctions = null;
        try {
            returnedFunctions = ScenarioProbabilityCounter.countProbalityOfTrees(consequences);
        } catch (Exception ex) {
            fail();
            Logger.getLogger(ScenarioProbabilityCounterTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //assertNotNull(returnedFunctions);
        assertFunctionsEqual(returnedFunctions, expectedFunctions);
    }
    
    
    private void setParentsForEdge(MembershipFunction function, Boolean nonRewindable) throws Exception {   
        // iteracja nie iteratorami bo chcemy kolejnosc, a iteratory nie zapewniaja
        for( int i = 1 ; i < edges.size() ; i++) {
            Edge parent = edges.get(i-1);
            Edge child = edges.get(i);
            child.setParent(parent);
            child.setProbability(function);
            child.setType(nonRewindable);
            child.setEvent(events.get(i));
        }
        
        Edge firstOne = edges.get(0);
        firstOne.setType(nonRewindable);
        firstOne.setEvent(events.iterator().next());
        firstOne.setProbability(function);
    }    

    private void setEventsForEdges(MembershipFunction function) {

        for( final Event event : events ) {
            event.setProbabilty(function);
        }
    }

    private void prepareEdgesAndEvents(MembershipFunction function, boolean nonRewindable) {
        // to nie ja, to moi poprzednicy, konstruktory throwujace exceptiony (sic!)
        try{
            setEventsForEdges(function);
            setParentsForEdge(function, nonRewindable);
        }catch( Exception e) {
            fail("Failed due to inability to initialize edges for test:" + this.toString());
            e.printStackTrace();
        }
    }    
    
    private MembershipFunction createFunction(double[] values, Scale scale) {
        ParamTrapezoid params;
        FuzzyLogicFacade flo = new FuzzyLogicFacade();
        MembershipFunction function = null;
        try {
            params = new ParamTrapezoid(values[0],values[2],values[2], values[3], scale);
            
            //function = new TrapezoidFunction(params, FUNCTION_NAME);
            function = flo.createTrapezoid(params, FUNCTION_NAME);
        } catch (Exception ex) {
            fail(ex.getCause().toString());
            Logger.getLogger(ScenarioProbabilityCounterTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return function;
    }
    
    private List<Consequence> buildTreeTest1(double[][] functionValues, Scale scaleType) {
        
        //root Edge and Event
        Edge rootedge = new Edge();
        Event rootEvent = new Event();
        rootedge.setEvent(rootEvent);
        rootEvent.setProbabilty(createFunction(functionValues[0], scaleType));
        
        //2nd level of tree
        Edge levelTwoEdge1 = new Edge();
        Edge levelTwoEdge2 = new Edge();
        
        levelTwoEdge1.setParent(rootedge);
        levelTwoEdge1.setType(true);
        levelTwoEdge2.setParent(rootedge);
        levelTwoEdge1.setType(false);
        
        Event eventTwo = new Event();
        eventTwo.setProbabilty(createFunction(functionValues[1], scaleType));
        
        levelTwoEdge1.setEvent(eventTwo);
        levelTwoEdge2.setEvent(eventTwo);
        
        //3rd level
        Edge levelThreeEdge1 = new Edge();
        Edge levelThreeEdge2 = new Edge();
        Edge levelThreeEdge3 = new Edge();
        Edge levelThreeEdge4 = new Edge();
        
        levelThreeEdge1.setParent(levelTwoEdge1);
        levelThreeEdge1.setType(true);
        levelThreeEdge2.setParent(levelTwoEdge1);
        levelThreeEdge2.setType(false);
        levelThreeEdge3.setParent(levelTwoEdge2);
        levelThreeEdge3.setType(true);
        levelThreeEdge4.setParent(levelTwoEdge2);
        levelThreeEdge4.setType(false);
        
        Event eventThree = new Event();
        eventThree.setProbabilty(createFunction(functionValues[2], scaleType));
        
        levelThreeEdge1.setEvent(eventThree);
        levelThreeEdge2.setEvent(eventThree);
        levelThreeEdge3.setEvent(eventThree);
        levelThreeEdge4.setEvent(eventThree);
        
        Consequence consequence1 = new Consequence();
        consequence1.addLeaf(levelThreeEdge1);
        
        Consequence consequence2 = new Consequence();
        consequence2.addLeaf(levelThreeEdge2);
        
        Consequence consequence3 = new Consequence();
        consequence3.addLeaf(levelThreeEdge3);
        
        Consequence consequence4 = new Consequence();
        consequence4.addLeaf(levelThreeEdge4);
        
        List<Consequence> consequences = new ArrayList<Consequence>( Arrays.asList(consequence1, consequence2, consequence3, consequence4));
        
        return consequences; 
    }    
    
    private List<Consequence> buildTreeTest2(double functionValues[][], Scale scale) {
                //root Edge and Event
        Edge rootedge = new Edge();
        Event rootEvent = new Event();
        rootedge.setEvent(rootEvent);
        rootEvent.setProbabilty(createFunction(functionValues[0], scale));
        
        //2nd level of tree
        Edge levelTwoEdge1 = new Edge();
        Edge levelTwoEdge2 = new Edge();
        
        levelTwoEdge1.setParent(rootedge);
        levelTwoEdge1.setType(true);
        levelTwoEdge2.setParent(rootedge);
        levelTwoEdge1.setType(false);
        
        Event eventTwo = new Event();
        eventTwo.setProbabilty(createFunction(functionValues[1], scale));
        
        levelTwoEdge1.setEvent(eventTwo);
        levelTwoEdge2.setEvent(eventTwo);
        
        //3rd level
        Edge levelThreeEdge1 = new Edge();
        Edge levelThreeEdge2 = new Edge();
        Edge levelThreeEdge4 = new Edge();
        
        levelThreeEdge1.setParent(levelTwoEdge1);
        levelThreeEdge1.setType(true);
        levelThreeEdge2.setParent(levelTwoEdge1);
        levelThreeEdge2.setType(false);
        levelThreeEdge4.setParent(levelTwoEdge2);
        levelThreeEdge4.setType(false);
        
        Event eventThree = new Event();
        eventThree.setProbabilty(createFunction(functionValues[2], scale));
        
        levelThreeEdge1.setEvent(eventThree);
        levelThreeEdge2.setEvent(eventThree);
        levelThreeEdge4.setEvent(eventThree);
        
        Consequence consequence1 = new Consequence();
        consequence1.addLeaf(levelThreeEdge1);
        
        Consequence consequence2 = new Consequence();
        consequence2.addLeaf(levelThreeEdge2);
        
        consequence1.addLeaf(levelThreeEdge4);
        
        List<Consequence> consequences = new ArrayList<Consequence>( Arrays.asList(consequence1, consequence2));
        
        return consequences; 
    }

    private void assertFunctionsEqual(List<MembershipFunction> returnedFunctions, List<MembershipFunction> expectedFunctions) {
        assertNotNull(returnedFunctions);
        assertNotNull(expectedFunctions);
        
        assertEquals(returnedFunctions.size(), expectedFunctions.size());
        for( int i = 0 ; i < returnedFunctions.size() ; i++ ) {
            MembershipFunction returned = returnedFunctions.get(i);
            MembershipFunction expected = expectedFunctions.get(i);

            assertTrue(returned.equals(expected));
        }
    }

}
