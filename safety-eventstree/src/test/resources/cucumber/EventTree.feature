Feature: EventTree
  As a user
  I want to create an event tree with three events and save it to xml file
  So that I don't need to create it once again
  Scenario: Create an Event Tree, set its description, add three events and save it to XML file
    Given I have an event tree
    When I set its description to "Drzewo zdarzen"
    And I set its ID to 5
    And I add an init event
    And I add a second event to tree
    And I add a third event to tree
    Then The event tree should be defined with string "(5,3,7|0:0|1:1,2|2:3,4,5,6)"
    And Its description should be "Drzewo zdarzen"
    When I save my event tree to XML file called "eventTree.xml"
    Then XML file should be available
    When I load stored xml file
    Then File should not be empty
    And XML file should contain my event tree
    When I load an event tree from stored xml file
    Then A new event tree should be created
    And New events tree description should be "Drzewo zdarzen"
    And New event tree should be defined with string "(5,3,7|0:0|1:1,2|2:3,4,5,6)"
    But New event tree should not be defined with string "(5,2,3|0:0|1:1,2)"