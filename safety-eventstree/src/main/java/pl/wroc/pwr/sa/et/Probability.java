package pl.wroc.pwr.sa.et;
import javax.xml.bind.annotation.XmlRootElement;

//
// grupa evets tree  
//
//
public class Probability {
    private Integer value;
    
    public Probability(Integer v){
        value = v;
    }

    public Integer getValue() {
        return value;
    }
    
    public Probability multiply(Probability p){
        
        return new Probability(this.getValue()*p.getValue());
    }
    public Probability add(Probability p){
        return new Probability(this.getValue()+p.getValue());
    }
}