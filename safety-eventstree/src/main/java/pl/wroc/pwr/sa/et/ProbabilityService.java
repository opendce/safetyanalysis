/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.sa.et;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import pl.wroc.pwr.sa.fr.FuzzyOperations;
import pl.wroc.pwr.sa.fr.MembershipFunction;

/**
 *
 * @author gracz
 */
public class ProbabilityService implements EventsTreeService {

    private FuzzyOperations operation;
    

    public ProbabilityService() {
        operation = new FuzzyOperations();
    }

    public String analyse(EventTree treeToAnalyze) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Compute probability for consequence's list
     *
     * @param tree target tree
     * @param leaf leaf's list for each compute probability
     * @return Probability value
     */
    public MembershipFunction computeProbabilityForLeaf(EventTree tree, List<Edge> leaf) throws Exception {
        // returned probability    
        MembershipFunction tmpProbability;
        tree.checkTheCohesionOfTheTree();
        List<MembershipFunction> probabilityList = new ArrayList<MembershipFunction>();


        for (Edge l : leaf) {
            Edge edge = l;
            tmpProbability = edge.getEvent().getProbability();
            while (edge.getParent() != null) {
                edge = edge.getParent();
                tmpProbability = operation.multiply(tmpProbability, edge.getEvent().getProbability());
            }
            l.setProbability(tmpProbability);
            probabilityList.add(tmpProbability);
        }// end of computing probabilities for each path


        // start adding probabilities from paths
        ListIterator<MembershipFunction> iter = probabilityList.listIterator();
        tmpProbability = iter.next();
        while (iter.hasNext()) {
            tmpProbability = operation.add(tmpProbability, iter.next());
        }
        return tmpProbability;
    }

    /**
     * Compute probability for consequence's list
     *
     * @param tree target tree
     * @param consequence consequence's list for each compute probability
     * @return Probability value
     */
    public MembershipFunction computeProbabilityForConsequence(EventTree tree, List<Consequence> consequence) throws Exception {
        // returned probability        
        MembershipFunction tmpProbability;
        tree.checkTheCohesionOfTheTree();
        // list with probability from each path            
        List<MembershipFunction> probabilityList = new ArrayList<MembershipFunction>();
        
        
        for (Consequence c : consequence) {
            for (Edge leaf : c.getLeafsList()) {
                Edge edge = leaf;
                tmpProbability = edge.getEvent().getProbability();
                while (edge.getParent() != null) {
                    edge = edge.getParent();
                    tmpProbability = operation.multiply(tmpProbability, edge.getEvent().getProbability());
                }
                // save computed membership function in consequence variable                
                c.setProbability(tmpProbability);
                probabilityList.add(tmpProbability);
            }
        }// end of computing probabilities for each path
        // start adding probabilities from paths
        
        
        ListIterator<MembershipFunction> iter = probabilityList.listIterator();
        tmpProbability = iter.next();
        while (iter.hasNext()) {
            tmpProbability = operation.add(tmpProbability, iter.next());
        }
        return tmpProbability;
    }
}
