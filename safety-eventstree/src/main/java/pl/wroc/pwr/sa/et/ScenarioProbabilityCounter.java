/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.sa.et;

import static java.lang.System.in;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.wroc.pwr.sa.fr.FuzzyLogicFacade;
import pl.wroc.pwr.sa.fr.FuzzyOperations;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;
import pl.wroc.pwr.sa.fr.Scale;
import pl.wroc.pwr.sa.fr.TrapezoidFunction;

/**
 *
 * @author Maciej
 */
public class ScenarioProbabilityCounter {
    static public MembershipFunction countProbalityOfScenario(Edge edge) throws Exception
    {      
        FuzzyLogicFacade flo = new FuzzyLogicFacade();
        MembershipFunction mF = null;
        
        //Lista krawędzi repzentuje scenariusz:
        //Popatrzmy na to w ten sposób -> od każdego zdarzenia są przypisane dwie krawędzie
        //Wydarzyło się albo się nie wydarzyło
        //Nasze krawędzie prowadzą nas do - konsekwencji, najwidoczniej jest to obiekt przechowujący scenariusz
        //Oznacza to że konsekwencja przechowuje informacje o tym czy zdarzenia się wydarzyły czy nie, a to w konsekwecji daje nam konsekwecje :D
        //Co należy zatem zrobić aby zdobyć nasze prawdopodobieństwo? Po koleji pobieramy kolejne ekrawędzie poprzez zawarte w nich pole -> parent
        //Zawerające informacje o rodzicu naszej zaistaniałej sytuacji, robimy tak póki nie otrzymamy null wtedy kończmy nasze poszukiwania i
        //cieszmy się prawie napewno nie poprawnym rezulatem :P
        Edge tmpEdge = edge;
        if(edge.getType()) mF = tmpEdge.getEvent().getProbability();
        else  mF = reverseEvent(tmpEdge.getEvent().getProbability());    
        
        
        while(tmpEdge.getParent() != null)
        {
            if(tmpEdge.getParent().getEvent().getProbability() != null) {
                if(tmpEdge.getParent().getType())  mF = flo.multiply(tmpEdge.getParent().getEvent().getProbability(), mF);
                else
                {
                    MembershipFunction tmpP = reverseEvent(tmpEdge.getParent().getEvent().getProbability());
                    mF = flo.multiply(tmpP, mF);
                }
            }
            tmpEdge = tmpEdge.getParent();
        }
        return mF;             
    }
    static public List<MembershipFunction> countProbalityOfConsequence(Consequence con) throws Exception
    {
        FuzzyLogicFacade flo = new FuzzyLogicFacade();
        
          MembershipFunction mF = null;
            
          List<Edge> leafs = con.getLeafsList();
          
          List<MembershipFunction> probab = new ArrayList<MembershipFunction>();
          for(Edge e : leafs)
          {
              probab.add(countProbalityOfScenario(e));
          }
         /* Edge scenario = con.getNextScenario();
          while (scenario!=null)
          {
              probab.add(countProbalityOfScenario(scenario));
              scenario = con.getNextScenario();
          }*/
                 
          mF = probab.get(0);
          for(int i=1;i<probab.size();i++)
          {
             mF = flo.add(mF,probab.get(i));
          }
          
          probab.add(mF);
          
          return probab;
    }
    static public List<MembershipFunction> countProbalityOfTrees(List<Consequence> con) throws Exception
    {
        FuzzyLogicFacade flo = new FuzzyLogicFacade();
           
        List<MembershipFunction> probab = new ArrayList<MembershipFunction>();
        MembershipFunction mF = null;
        probab.addAll(countProbalityOfConsequence(con.get(0)));
        
        mF = probab.get(probab.size()-1);
        
        for(int i=1;i<con.size();i++)
          {
             probab.addAll(countProbalityOfConsequence(con.get(i)));
             mF = flo.add(mF,probab.get(probab.size()-1));
          }
        probab.add(mF);
        return probab;
    }
    
    
    static public MembershipFunction reverseEvent(MembershipFunction a)
    {
        //Musi byś to funkcja trapezowa bo inaczej wybuchnie pl.wroc.pwr.sa.fr.TrapezoidFunction
        //Wiem, wiem jest paskudna, ale działa...
        System.out.println(a.getClass());
        FuzzyLogicFacade flo = new FuzzyLogicFacade();
        TrapezoidFunction b = (TrapezoidFunction) a;
        double revA = 1 - b.getParameters().getD();
        double revB = 1 - b.getParameters().getC();
        double revC = 1 - b.getParameters().getB();
        double revD = 1 - b.getParameters().getA();
        try {
            MembershipFunction  mF = flo.createTrapezoid(new ParamTrapezoid(revA, revB, revC, revD, Scale.LINEAR), null);
            return mF;
        } catch (Exception ex) {
            Logger.getLogger(ScenarioProbabilityCounter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    
}
