package pl.wroc.pwr.sa.et;

import java.util.ArrayList;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import javax.xml.bind.annotation.*;

@XmlRootElement(namespace = "pl.wroc.pwr.sa.et.EventTree")
public class Edge {

    public static int count = 0;
    private int id;
    private Event event;
    @XmlTransient
    private Edge parent;   
    private ArrayList<Edge> child;
    private boolean type;
    private boolean branch = true;
    private MembershipFunction probability;

    public synchronized static void setCount(int count) {
        Edge.count = count;
    }

    public synchronized static int getCount() {
        return Edge.count;
    }

    public MembershipFunction getProbability() {
        return probability;
    }

    public void setProbability(MembershipFunction probability) {
        this.probability = probability;
    }

    public Edge() {
        this.id = getCount();
        setCount(getCount() + 1);
        this.child = new ArrayList<Edge>(2);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        if (id >= getCount()) {
            setCount(id + 1);
        }
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
    @XmlTransient
    public Edge getParent() {
        return parent;
    }

    public void setParent(Edge edge) {
        this.parent = edge;
    }
    
    public ArrayList<Edge> getChild() {
        return child;
    }

    public void setChild(ArrayList<Edge> edges) {
        
            this.child = edges;  
                
    }

    public void addChild(Edge edge) {
        this.child.add(edge);
    }

    public boolean getType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public boolean getBranch() {
        return branch;
    }

    public void setBranch(boolean branch) {
        this.branch = branch;
    }

   public boolean isLeaf() {
        if (child.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return edge structure:
     * [Edge.id,Edge.eventId,Edge.parent,Edge.child,Edge.branch,Edge.type i.e
     * [1,10,2,3|4,False,True]
     *
     * @return String information about edge structure
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("");
        if (this.child.size() > 0) {
            for (int i = 0; i < child.size(); i++) {
                builder.append(child.get(i).getId());
                builder.append("|");
            }
            builder.replace(builder.length() - 1, builder.length(), "");
        } else {
            builder.append("null");
        }
        return "["
                + this.id + ","
                + this.event.getId() + ","
                + (this.parent != null ? this.parent.getId() : "null") + ","
                + builder + ","
                + (this.branch ? "True" : "False") + ","
                + (this.type ? "True" : "False") + "]";
    }
}