package pl.wroc.pwr.sa.et;

//import sun.org.mozilla.javascript.internal.Evaluator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.fr.TrapezoidFunction;
import pl.wroc.pwr.sa.fr.TriangleFunction;

/**
 * Created by J.Ch on 2015-04-02.
 * klasa ma za zadanie pracować na kolekcji obiektów EventTree
 */
public class TreesService {

    //private ArrayList<EventTree> eventsTreeCollection = new ArrayList<EventTree>();
    private Map<Integer, EventTree> eventsTreeCollection = new HashMap<Integer, EventTree>();

    public TreesService (Map<Integer, EventTree> collection)
    {
        this.eventsTreeCollection = collection;
    }
    public TreesService(){}

    /**
     * J.Ch funkcja wyszukujaca drzewo z kolekcji drzew
     * @param description
     * @return
     */
    public EventTree getTreeFromCollection (String description) {
        if (this.eventsTreeCollection == null)
            return null;
        else {
            Iterator<Integer> itTreesService = this.eventsTreeCollection.keySet().iterator();
            while (itTreesService.hasNext()) {
                EventTree anEventsTreeCollection = this.eventsTreeCollection.get(itTreesService.next());
                if (anEventsTreeCollection.getDescription().equals(description))
                    return anEventsTreeCollection;
            }
            return null;
            //     System.out.println("EventTree not found ");
        }
    }

    /**
     * J.Ch funkcja zwracajaca zmodyfikowane drzewo do analizy waznosci
     * @param description
     * @param eventName
     * @return
     */
    public EventTree modificationEventTree (String description, String eventName){


            if (getTreeFromCollection(description) == null) {           // sprawdzenie czy wybrany opis pasuje do drzewa
                System.out.println("EventTree not found");
                return null;
            }

            if (getTreeFromCollection(description).findEvent(eventName) == null) {  // sprawdzenie czy zdarzenie znajduje sie w drzewie
                System.out.println("No Event in EventTree");
                return null;
            }
            if (getTreeFromCollection(description).findEdgeWithTrueStatement(getTreeFromCollection(description).findEvent(eventName)) == null) {
                System.out.println("This event has no edge with true statement");
                return null;
            }
        getTreeFromCollection(description).removeEdge( getTreeFromCollection(description).findEdgeWithTrueStatement(getTreeFromCollection(description).findEvent(eventName)) );
        return getTreeFromCollection(description);
    }

    /**
     * J.Ch funkcja obliczajaca wartosci pozostalych scenariuszy w drzewie i wyswietlajaca wynik
     * @param tree
     * @return
     * @throws Exception
     */
    public String importanceAnalisis (EventTree tree) throws Exception {
        ProbabilityService temp = new ProbabilityService() ;
        //System.out.println( temp.computeProbabilityForLeaf(tree, tree.getLeafList()) );
        
        MembershipFunction membershipFunction = temp.computeProbabilityForLeaf(tree, tree.getLeafList());
        
        if (membershipFunction instanceof TriangleFunction) {
            TriangleFunction triangleFunction = (TriangleFunction)membershipFunction;
            return String.valueOf(triangleFunction.getParameters().getProb());
        } else if (membershipFunction instanceof TrapezoidFunction) {
            TrapezoidFunction triangleFunction = (TrapezoidFunction)membershipFunction;
            return String.valueOf(triangleFunction.getParameters().getProb());
        }
        
        throw new Exception("Unknown type of MembershipFunction");
    }

    /**
     * Funkcja zwraca String z prawdopodobieństwem przed i po działaniu
     * operacji (usunięcia poddrzewa True)
     * @param tree
     * @param eventName
     */
    public String[] doAnalise(EventTree tree, String eventName) {
        String[] results = new String[5];
        try {
            EventTree testTree = tree;
            results[1] = tree.toString();
            results[2] = importanceAnalisis(testTree);
            Event initToRemove = testTree.findEvent(eventName);
            if(initToRemove != null) {
                ArrayList<Edge> edgeList = testTree.findEdgesWithTrueStatement(initToRemove);

                for (int i = 0; i < edgeList.size(); i++) {
                    boolean done = testTree.removeEdgeAndItsChildren(edgeList.get(i));
                }
                results[3] = testTree.toString();
                results[4] = importanceAnalisis(testTree);
                results[0] = "Funkcja zadziałała poprawnie";
                return results;
            }
            else{
                results[0] = "Nie znaleziono zadanego zdarzenia";
                return results;
            }
        } catch (Exception e) {
            e.printStackTrace();
            results[0] = "Wystąpił błąd działania funkcji.";
            return  results;
        }
    }
}
