/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.sa.et;

import java.util.ArrayList;
import java.util.List;
import pl.wroc.pwr.sa.fr.MembershipFunction;

public class Consequence {

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Edge> getLeafsList() {
        return leafs;
    }
    public Edge getScenario(int i)
    {
        if(i>leafs.size()-1) return leafs.get(0);
        return leafs.get(i);
    }
    public Edge getNextScenario()
    {
        if(iterator>leafs.size()-1)
        {
            iterator = 0;
            return null;
        }
        return leafs.get(iterator);
    }
    /**
     * Remove leaf from list
     *
     * @param leaf leaf to remove from list
     * @return Boolean value, describes success or not
     */
    public boolean removeLeaf(Edge leaf) {
        return leafs.remove(leaf);
    }

    public boolean containsLeaf(Edge leaf) {
        return leafs.contains(leaf);
    }

    /**
     * Add leaf to list
     *
     * @param leaf leaf add to list
     * @return Boolean value, describes success or not
     */
    public boolean addLeaf(Edge leaf) {
        return leafs.add(leaf);
    }

    public void setProbability(MembershipFunction probability) {
        this.probability = probability;
    }

    public MembershipFunction getProbability() {
        return probability;
    }
    
    private MembershipFunction probability;
    private String description;
    private List<Edge> leafs = new ArrayList<Edge>();
    private int iterator=0;//wiem, wiem jak to wyglada, ale musze to zrobic
}
