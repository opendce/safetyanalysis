package pl.wroc.pwr.sa.et;


import java.util.*;
import java.util.LinkedList;
import java.util.Map.Entry;
import javax.xml.bind.annotation.*;

import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.xml.XMLService;


@XmlRootElement(name = "eventTree")
@XmlAccessorType(XmlAccessType.FIELD)
public class EventTree {

    private int id;
    private String description;
    @XmlTransient
    private Map<Integer, Event> events = new HashMap<Integer, Event>();
    @XmlTransient
    private Map<Integer, Edge> edges = new HashMap<Integer, Edge>();
    private Set<Consequence> consequence = new HashSet<Consequence>();
    //Pole stworzone ze względu na xml
    private Edge InitialBranch = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
   
    public Edge getInitialBranch()
    {
        return InitialBranch;
    }
    
    public Map<Integer, Event> getEvents() {
        return events;
    }

    public void setEvents(Map<Integer, Event> events) {
        this.events = events;
    }

    public Map<Integer, Edge> getEdges() {
        return edges;
    }

    public void setEdges(Map<Integer, Edge> edges) {
        this.edges = edges;
    }

    /**
     * Add init event to tree and auto generate edge
     *
     * @param event new event with specified id
     * @return Event the previous value associated with key, otherwise null
     */
    public Event addInitEvent(Event event) throws Exception {
        // add init edge
        if (!this.events.isEmpty()) {
            throw new Exception("Can't add init event to tree. There are already events");
        }
        if (!this.edges.isEmpty()) {
            throw new Exception("Can't add init edge to tree. There are already edges");
        }

        Edge edge = new Edge();
        edge.setEvent(event);
        edge.setType(true);
        this.edges.put(edge.getId(), edge);

        InitialBranch = edge;
        // add event
        return events.put(event.getId(), event);
    }

    /**
     * Add new event to tree and auto generate edges
     *
     * @param event new event with specified id
     * @return Event the previous value associated with key, otherwise null
     */
    public Event addEvent(Event event) throws Exception {
        if (this.events.isEmpty()) {
            throw new Exception("Can't find init event");
        }
        Map<Integer, Edge> tmpEdges = new HashMap<Integer, Edge>(); // due to and concurrentmodification exception
        // set parent event
        if (event.getParent() == null) {
            event.setParent(this._getLastEvent());
        }

        Event parentEvent = event.getParent();
        Iterator<Integer> itEdge = this.edges.keySet().iterator();
        while (itEdge.hasNext()) {
            Edge edgeParent = this.edges.get(itEdge.next());

            // add new edges based on parent edges
            if (edgeParent.getEvent() == parentEvent) {
                // TRUE
                Edge edge = new Edge();
                edge.setEvent(event);
                edge.setParent(edgeParent);
                edge.setType(true);
                tmpEdges.put(edge.getId(), edge);
                edgeParent.addChild(edge); // add child to parent edge

                // FALSE
                edge = new Edge();
                edge.setEvent(event);
                edge.setParent(edgeParent);
                edge.setType(false);
                tmpEdges.put(edge.getId(), edge);
                edgeParent.addChild(edge); // add child to parent edge
            }
        }
        itEdge = tmpEdges.keySet().iterator();
        while (itEdge.hasNext()) {
            Edge edge = tmpEdges.get(itEdge.next());
            this.edges.put(edge.getId(), edge);
        }

        // set child to parent event
        parentEvent.setChild(event);

        // add event
        return events.put(event.getId(), event);
    }

    /**
     * Update event in tree
     *
     * //@param event event to update in tree
     * @return Event the previous value associated with key, otherwise null
     */
    public Event updateEvent(Event newEvent) {
        Event event = this.events.get(newEvent.getId());
        event.update(newEvent);
        return events.put(event.getId(), event);
    }

    /**
     * B_G Function removes from all true-type edges and their scenarios
     * @param event
     * @return
     */
    public boolean setEventFalse(Event event){

        if(this.events.containsKey(event.getId())){
            ArrayList<Edge> edges = findEdgesWithTrueStatement(event);
            if(edges != null){
                for(Edge e : edges){
                    if(!removeEdgeAndItsChildren(e)){
                        System.out.println("Something went wrong");
                        System.out.println("Failed to remove all the edges starting with " + e.getId());
                    }
                }
            } else {
                System.out.println("Given event has not 'true-type' edges");
                return false;
            }

        } else System.out.println("Given event doesn't exist");

        return false;
    }

    /**
     * B_G, P. K.
     * @param event
     * @return all 'true type' edges for a given event
     */
    public ArrayList<Edge> findEdgesWithTrueStatement(Event event){

        if(this.events.containsKey(event.getId())){
            ArrayList<Edge> result = new ArrayList<Edge>();

            Iterator<Integer> itEdge = this.edges.keySet().iterator();
            while (itEdge.hasNext()) {
                Edge edgeParent = this.edges.get(itEdge.next());
                if (edgeParent.getEvent().getName().equals(event.getName()) && edgeParent.getType()) {
                    result.add(edgeParent);
                }
            }

            return result;
        } else System.out.println("Given event doesn't exist");

        return null;
    }

    /**
     * B_G Function removes given edge and its 'children'
     * @param edge
     * @return true on success
     */
    public boolean removeEdgeAndItsChildren(Edge edge){
        if(this.edges.containsKey(edge.getId())) {
            Event event = edge.getEvent();
            event = event.getChild();
            this.edges.remove(edge.getId());

            while(event != null){
                this._removeEventEdges(event);
                event = event.getChild();
            }
            return true;
        } else System.out.println("Given edge doesn't exist");
            return false;
    }


    /**
     * Remove event from tree, child events and edges
     *
     * @param event event to remove from tree
     * @return Event the previous value associated with key, otherwise null
     */
    public Event removeEvent(Event event) {
        if (this.events.containsKey(event.getId())) {
            Event tmp = event;
            Event child;
            do {
                child = event.getChild();
                this._removeEventEdges(event);          // remove event edges
                this.events.remove(event.getId());      // remove event
                if (event.getParent() != null) {
                    event.getParent().setChild(null);   // set parent child to null
                }
                event = child;
            } while (event != null);
            
            return tmp;
        }
        else System.out.println("Nie ma takiego zdarzenia");  //
        return null;
    }

    /**
     * Remove edge from tree and his childs
     *
     * @param edge edge to remove from tree
     * @return Edge the previous value associated with key, otherwise null
     */
    public Edge removeEdge(Edge edge) {
       
      ArrayList<Edge> tempArr = null;
      ArrayList<Edge> tempArr2 = null; 
      
      int index;      
      System.out.println("Klucze przed usunieciem: \n"); 
      for( int temp : edges.keySet())
      {
          System.out.println(temp + "\n");
      }
      if(edge != null)
      { 
          if (this.edges.containsKey(edge.getId())) 
          {
              tempArr = edge.getChild();
              if(tempArr != null)
              {
                  for (Edge child : tempArr) 
                  {   
                      if(child != null)
                      {
                          this.removeEdge(child);
                          this.edges.remove(child.getId());
                          
                      }
                  }
              }
          }
          
          System.out.println("Usunieto removeEdge EventTree: " + edge.getId());
          this.edges.remove(edge.getId());
          tempArr2 = edge.getParent().getChild();
          Edge parentTemp = edge.getParent();          
          index = tempArr2.indexOf(edge);
          tempArr2.set(index, null);
          
          
          boolean emptyArray = true;
          for (int i = 0; i < tempArr2.size(); i++)
          {
              if (tempArr2.get(i) != null)
              {
                      emptyArray = false;
                      break;
              }
          }
          if (emptyArray == true)
          {
              //parentTemp.getChild().clear();
              parentTemp.setChild(new ArrayList<Edge>());
          }
      }
      
      System.out.println("Klucze po usunieciu: \n"); 
      for( int temp : edges.keySet())
      {
          System.out.println(temp + "\n");
      }
        
        return null;
    }

    /**
     * Removing tree data, events, edges from tree and setting static counters
     * to 0
     *
     * @return void
     */
    public void clear() {
        this.id = 0;
        this.description = null;
        this.events.clear();
        this.edges.clear();
        Event.setCount(0);
        Edge.setCount(0);
    }

    /**
     * Return event tree structure:
     * [EventTree.id,EventTree.events.size(),EventTree.edges.size()|
     * Events.id:Event edges] i.e (1,3,7|0:1|1:2,3|2:4,5,6,7)
     *
     * @return String information about tree structure
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("(");
        builder.append(this.id);
        builder.append(",");
        builder.append(this.events.size());
        builder.append(",");
        builder.append(this.edges.size());
        builder.append("|");

        if (this.events.size() > 0) {
            // events
            Iterator<Integer> itEvent = this.events.keySet().iterator();
            while (itEvent.hasNext()) {
                Event event = this.events.get(itEvent.next());
                builder.append(event.getId());
                builder.append(":");

                // edges
                Iterator<Integer> itEdge = this.edges.keySet().iterator();
                while (itEdge.hasNext()) {
                    Edge edge = this.edges.get(itEdge.next());
                    if (event.getId() == edge.getEvent().getId()) {
                        builder.append(edge.getId());
                        builder.append(",");
                    }
                }
                builder.replace(builder.length() - 1, builder.length(), "|");
            }

            builder.replace(builder.length() - 1, builder.length(), ")");
        } else {
            builder.append(")");
        }
        return builder.toString();
    }

    private void _removeEventEdges(Event event) {
        if (this.events.containsKey(event.getId())) {
            Iterator<Integer> it = this.edges.keySet().iterator();
            while (it.hasNext()) {
                Edge child = this.edges.get(it.next());
                if (child.getEvent() == event) {
                    it.remove();
                }
            }
        }
    }

    private Event _getLastEvent() {
        Iterator<Integer> it = this.events.keySet().iterator();
        while (it.hasNext()) {
            Event event = this.events.get(it.next());
            if (event.getChild() == null) {
                return event;
            }
        }
        return null;
    }

    public Set<Consequence> getConsequences() {
        return consequence;
    }

    /**
     * Create new consequence
     *
     * @param description verbal description of the consequence
     * @return consequence which was created
     */
    public Consequence addNewConsequence(String description) {
        Consequence c = new Consequence();
        c.setDescription(description);
        consequence.add(c);
        return c;
    }
    static public void saveTreeToXML(String path,EventTree eT) throws Exception
    {
        XMLService.saveObjectToFile(eT, path);
    }
    static public String saveTreeToXMLString(EventTree eT) throws Exception
    {
        return XMLService.saveObjectToXMLString(eT);
    }
    static public EventTree loadFromXML(String path) throws Exception
    {
         EventTree eT =  XMLService.readFileToObject(EventTree.class, path);
         if(eT.getInitialBranch() != null) eT.rebuildTreeFromInitialBranch();
         return eT;
    }
    static public EventTree loadFromXMLString(String xmlString) throws Exception
    {
         EventTree eT =  XMLService.readXMLStringToObject(EventTree.class, xmlString);
         if(eT.getInitialBranch() != null) eT.rebuildTreeFromInitialBranch();
         return eT;
    }

    public boolean removeConesquence(Consequence c) {
        return consequence.remove(c);
    }

    /**
     * Assign leaf to consequence
     *
     * @param e edge assigned to consequence
     * @param c used consequence
     */
    public void assignLeafToConsequence(Edge e, Consequence c) throws Exception {
        if (consequence.contains(c) && e.isLeaf()) {
            c.addLeaf(e);
        } else {
            throw new Exception("Edge isn't leaf or consequence doesn't exist");
        }
    }

    public List<Edge> getLeafList() {
        List<Edge> l = new ArrayList<Edge>();
        for (Edge edge : edges.values()) {
            if (edge.isLeaf()) {
                l.add(edge);
            }
        }
        return l;
    }

    public void checkTheCohesionOfTheTree() throws Exception {
        List<Edge> l = this.getLeafList();
        Edge temporaryEdge;
        for (Edge edge : l) {
            temporaryEdge = edge;
            while (temporaryEdge.getParent() != null) {
                temporaryEdge = temporaryEdge.getParent();
            }
            // 0 index is a root of a tree
            if (temporaryEdge != this.InitialBranch) {
                throw new Exception("Incorrect tree's structure!" +
                                    "\nEdge id: " + temporaryEdge.getId() + 
                                    "\nEvent id: " + temporaryEdge.getEvent().getId() + 
                                    "\nIs the event occurred? " + temporaryEdge.getType());
            }
        }
    }
    public void removeUnnecessaryEvents()
    {
        for(Entry<Integer,Event> entry: events.entrySet())
        {
            Event temp = entry.getValue();
            MembershipFunction eventProbability = temp.getProbability();
            /*check here if event is passable*/
            if(true)
            {
                if(temp.getParent()!=null)
                {
                    temp.getParent().setChild(temp.getChild());
                    
                    if(temp.getChild()!=null)
                    {
                    temp.getChild().setParent(temp.getParent());
                    }
                }        
            }
            else if(false)
            {
                /*or if not*/
            
            }
        }
           
    }
    public void rebuildTreeFromInitialBranch()
    {
        if(InitialBranch!=null)
        {
            edges.put(InitialBranch.getId(), InitialBranch);
            events.put(InitialBranch.getEvent().getId(), InitialBranch.getEvent());
            Event tempEvent = InitialBranch.getEvent().getChild();
            
            Event parent = InitialBranch.getEvent();
            while(tempEvent!=null)
            {
               events.put(tempEvent.getId(), tempEvent);
               tempEvent.setParent(parent);
               parent = tempEvent;
               tempEvent = tempEvent.getChild();
            }
            
            resetEdge(InitialBranch);         
        }
        else return;
    }
    private void resetEdge(Edge parent)
    {
        ArrayList<Edge> children = parent.getChild();
        for(Edge ed : children)
        {
            if(ed != null)
            {
                edges.put(ed.getId(), ed);
                ed.setParent(parent);
                resetEdge(ed); 
            }
           
        }
        return;
    }

    /**
     * J.Ch, P. K. - funkcja znjdujaca zdarzenie w drzewie
     * @param name
     * @return
     */
    public Event findEvent (String name) {

        Iterator<Integer> it = this.events.keySet().iterator();
        while (it.hasNext()) {
            Event event = this.events.get(it.next());
            if(event.getName().equals(name)){
                return event;
            }
        }
        return null;
    }

    /**
     * J.Ch
     * @param event
     * @return
     */
    public Edge findEdgeWithTrueStatement (Event event){
        for (int i = 0; i < getEdges().size() ; i++ ){
           if ( edges.get(i).getEvent()== event)
              return edges.get(i).getChild().get(0);
        }
        return null;
    }

    /**
     * J.Ch funkcja wyswielajaca podstawoe informacje o drzewie
     * @return
     */
    public String showInfo(){
        String toReturn = "";
        toReturn += "Opis : " + getDescription();
        toReturn += "\nId : " + getId();
        /*System.out.println( "Opis : " + getDescription());       // wyswietl opis drzewa
        System.out.println( "Id : " + getId());   */               // wyswietl id drzewa
        for (int j = 0; j < getEvents().size() ; j++) {
            toReturn += "\n" + getEvents().get(j).getName();
            //System.out.println(getEvents().get(j).getName());    // wyswietl liste eventow
        }
        toReturn += "\n";

        return toReturn;

    }


}