package pl.wroc.pwr.sa.et;
import javax.xml.bind.annotation.XmlElementWrapper;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import pl.wroc.pwr.sa.fr.*;

@XmlRootElement(namespace = "pl.wroc.pwr.sa.et.EventTree")
public class Event {

    public static int count = 0;
    private int id;
    private String name;  
    @XmlTransient
    private Event parent;
    private Event child;
    @XmlElements({
            @XmlElement(name="trapezoid",type=TrapezoidFunction.class),
            @XmlElement(name="triangle",type=TriangleFunction.class)
        })
    private MembershipFunction probability;

    public Event() {
        this.id = getCount();
        setCount(getCount() + 1);
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
        if(id >= getCount())
            setCount(id + 1);
    }

    public synchronized static void setCount(int count) {
        Event.count = count;
    }

    public synchronized static int getCount() {
        return Event.count;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    @XmlTransient
    public Event getParent() {
        return parent;
    }
    public void setParent(Event parent) {
        this.parent = parent;
    }
    public Event getChild() {
        return child;
    }
    
    public void setChild(Event child) {
        this.child = child;
    }

    public MembershipFunction getProbability() {
        return this.probability;
    }
    
    public void setProbabilty(MembershipFunction probability) {
        this.probability = probability;
    }
    
    public void update(Event event) {
        this.name = event.getName();
        this.parent = event.getParent();
        this.child = event.getChild();
        this.probability = event.getProbability();
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Event))
            return false;
        
        Event event = (Event)obj;
        if(event.getId() != this.id)
            return false;
        if(event.getName() != null && event.getName().equals(this.name))
            return false;
        if(event.getParent() != this.parent)
            return false;
        if(event.getProbability() != this.probability)
            return false;
        
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = (47 * hash) + (int) (Integer.reverseBytes(this.id) ^ (Integer.reverseBytes(this.id)  >>> 8));
        hash = (29 * hash) + (int) (Integer.reverseBytes(this.id) ^ (Integer.reverseBytes(this.id)  >>> 8));
        hash = (27 * hash) + (int) (Integer.reverseBytes(this.name.hashCode()) ^ (Integer.reverseBytes((this.name.hashCode())) >>> 4));
        hash = (97 * hash) + (int) (Integer.reverseBytes(this.parent.hashCode()) ^ (Integer.reverseBytes(this.parent.hashCode()) >>> 4));
        hash = (107 * hash) + (int) (Integer.reverseBytes(this.probability.hashCode()) ^ (Integer.reverseBytes(this.probability.hashCode()) >>> 8));
        return hash;
    }
    
    /**
     * Return event structure:
     * [Event.id,Event.parent,Event.child,Event.name i.e [1,2,3,Test event]
     *
     * @return String information about event structure
     */
    @Override
    public String toString() {
        return "["
                + this.id + ","
                + (this.parent != null ? this.parent.getId() : "null") + ","
                + (this.child != null ? this.child.getId() : "null") + ","
                + this.name + "]";
    }

}
