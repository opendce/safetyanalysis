package pl.wroc.pwr.sa.et;

import java.util.List;
import pl.wroc.pwr.sa.fr.MembershipFunction;


/**
 * Service for analyzing Events Trees with fuzzy conditions.
 */
public interface EventsTreeService {

    /**
     * To analyze the tree from parameter.
     */
    String analyse(EventTree treeToAnalyze);
    MembershipFunction computeProbabilityForLeaf(EventTree tree, List<Edge> leaf) throws Exception;
    MembershipFunction computeProbabilityForConsequence(EventTree tree, List<Consequence> consequence) throws Exception;
    
}

