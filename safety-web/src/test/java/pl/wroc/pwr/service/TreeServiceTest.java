package pl.wroc.pwr.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import pl.wroc.pwr.sa.model.TreeEnum;
import pl.wroc.pwr.sa.model.TreeModel;
import pl.wroc.pwr.sa.repository.ITreeRepository;

import javax.xml.bind.JAXBException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class TreeServiceTest {
    private String EVENT_TREE;
    private String FAULT_TREE;
    @Mock
    private ITreeRepository treeRepository;
    @InjectMocks
    private TreeService treeService;
    private TreeModel eventTreeModel;
    private TreeModel faultTreeModel;

    public TreeServiceTest() throws IOException {
        EVENT_TREE = convertFileToString("event_tree.xml");
        FAULT_TREE = convertFileToString("fault_tree.xml");
        eventTreeModel = new TreeModel(TreeEnum.EVENT_TREE, EVENT_TREE);
        eventTreeModel.setId(1L);
        faultTreeModel = new TreeModel(TreeEnum.FAULT_TREE,FAULT_TREE);
        faultTreeModel.setId(1L);
    }

    @Test
    public void shouldReturnEntityIdWhenSaveSuccessfulTest() throws Exception {
        //given
        doReturn(eventTreeModel).when(treeRepository).save(Mockito.any(TreeModel.class));
        //when
        treeService.saveEventTree(EVENT_TREE, "default-name");
        //then
        assertTrue(1L == eventTreeModel.getId());
    }

    @Test
    public void shouldReturnEntityIdWhenSaveSuccessfulTest2() throws Exception {
        //given
        doReturn(faultTreeModel).when(treeRepository).save(Mockito.any(TreeModel.class));
        //when
        TreeModel eventTreeModel = treeService.saveFaultTree(FAULT_TREE, "default-name");
        //then
        assertTrue(1L ==eventTreeModel.getId());
    }

    @Test(expected = JAXBException.class)
    public void shouldThrowJAXBExceptionWhenXmlIncorrectTest() throws Exception {
        treeService.saveEventTree("will fail", "default-name");
        fail();
    }

    @Test(expected = JAXBException.class)
    public void shouldThrowJAXBExceptionWhenXmlIncorrectTest2() throws Exception {
        treeService.saveFaultTree("will fail", "default-name");
        fail();
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionOnDbErrorTest() throws Exception {
        //given
        doThrow(new RuntimeException("Could be anything")).when(treeRepository).save(new TreeModel());
        //when
        treeService.saveEventTree(null, "default-name");
        //then
        fail();
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionOnDbErrorTest2() throws Exception {
        doThrow(new RuntimeException("Could be anything")).when(treeRepository).save(new TreeModel());
        treeService.saveFaultTree(null, "default-name");
        fail();
    }

    private String convertFileToString(String path) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(path));
        StringBuilder builder = new StringBuilder();
        for (String line : lines)
            builder.append(line);
        return builder.toString();
    }
}
