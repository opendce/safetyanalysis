package pl.wroc.pwr.controller.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.wroc.pwr.service.TreeService;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by sztukers on 2017-04-19.
 */

@RunWith(SpringRunner.class)
@WebMvcTest(TreeRestController.class)
public class TreeRestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private TreeService treeService;

    @Test
    public void shouldReturn500AfterUploadingEmptyEventTree() throws Exception {
        mockMvc.perform(post("/rest/uploadEventTree").sessionAttr("xml", ""))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void shouldReturn500AfterUploadingEmptyFaultTree() throws Exception {
        mockMvc.perform(post("/rest/uploadFaultTree").sessionAttr("xml", ""))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void shouldReturn200AfterListingTrees() throws Exception {
        mockMvc.perform(get("/rest/list"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

}
