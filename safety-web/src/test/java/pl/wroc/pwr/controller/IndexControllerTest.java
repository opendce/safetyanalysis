package pl.wroc.pwr.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import pl.wroc.pwr.controller.view.IndexController;
import pl.wroc.pwr.service.TreeService;

import static org.hamcrest.Matchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import static org.junit.Assert.assertEquals;

/**
 * Created by sztukers on 2017-04-19.
 */

@RunWith(SpringRunner.class)
@WebMvcTest(IndexController.class)
public class IndexControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TreeService service;

    @Test
    public void shouldReturnViewIndex() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(view().name("index"));
    }

    @Test
    public void shouldReturnViewListTreesAndHaveAttributeTrees() throws Exception {
        mockMvc.perform(get("/listTrees"))
                .andExpect(view().name("listTrees"))
                .andExpect(model().attributeExists("trees"));
    }

    @Test
    public void shouldReturnViewUploadEventTree() throws Exception {
        mockMvc.perform(get("/uploadEventTree"))
                .andExpect(view().name("uploadEventTree"));
    }

    @Test
    public void shouldReturnViewUploadFaultTree() throws Exception {
        mockMvc.perform(get("/uploadFaultTree"))
                .andExpect(view().name("uploadFaultTree"));
    }
}
