package pl.wroc.pwr.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.wroc.pwr.controller.rest.dto.TreeDTO;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.graph.EventTreeGraph;
import pl.wroc.pwr.sa.graph.FaultTreeGraph;
import pl.wroc.pwr.sa.model.TreeEnum;
import pl.wroc.pwr.sa.model.TreeModel;
import pl.wroc.pwr.service.TreeService;

import javax.imageio.ImageIO;
import javax.xml.bind.JAXBException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


@RestController
@RequestMapping("/rest")
public class TreeRestController {

	private static final Logger logger = Logger.getLogger(TreeRestController.class.getName());
	private TreeService treeService;

	@Autowired
	public TreeRestController(TreeService treeService) {
		this.treeService = treeService;
	}

    @GetMapping(value = "/tree/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    public String getTree(@PathVariable("id") Long id) throws Exception {
        return treeService.getTree(id).getXml();
    }

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TreeDTO> list() throws Exception {
	    List<TreeModel> listOfTreeModels = treeService.listTrees();
	    List<TreeDTO> listOfTreeDTOs = new ArrayList<>();
        for (TreeModel tree : listOfTreeModels) {
            listOfTreeDTOs.add(new TreeDTO(tree.getId(), tree.getName(), tree.getTreeEnum()));
        }
        return listOfTreeDTOs;
    }

	@PostMapping("/uploadEventTree")
	public ResponseEntity saveEventTree(String xml, String name) {
        try {
            TreeModel eventTreeModel = treeService.saveEventTree(xml, name);
            return ResponseEntity.status(200).body("" + eventTreeModel.getId());
        } catch (JAXBException e) {
            return getResponseEntityWhenJAXBExceptionThrown("There is a problem with the XML you sent.", "" + e);
        } catch (Exception e) {
            return getResponseEntityWhenExceptionIsThrown(e, "You sent empty xml.", "" + e);
        }
    }

    @PostMapping("/uploadFaultTree")
	public ResponseEntity saveFaultTree(String xml, String name){
		try{
			TreeModel faultTreeModel = treeService.saveFaultTree(xml, name);
            return ResponseEntity.status(200).body("" + faultTreeModel.getId());
		}
		catch(JAXBException e){
            return getResponseEntityWhenJAXBExceptionThrown(e  + "", "" + e);
		}
		catch(Exception e){
            return getResponseEntityWhenExceptionIsThrown(e, e  + "", "" + e);
		}
	}

    @GetMapping(value = "/tree/{id}/graph", headers = "Accept=image/jpeg, image/jpg, image/png, image/gif")
    public @ResponseBody byte[] getEventTreeImage(@PathVariable Long id) throws Exception {
	    TreeModel treeModel = treeService.getTree(id);
        BufferedImage image;
        if(treeModel.getTreeEnum() == TreeEnum.EVENT_TREE) {
            EventTree eventTree = EventTree.loadFromXMLString(treeModel.getXml());
            image = new EventTreeGraph().createEventTreeGraphImage(eventTree);
        } else {
            FaultTree faultTree = FaultTree.fromXml(treeModel.getXml());
            image = new FaultTreeGraph().createFaultTreeGraphImage(faultTree);
        }
        return getImageAsBytes(image);
    }

    private byte[] getImageAsBytes(BufferedImage image) throws IOException {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", bao);
        return bao.toByteArray();
    }


    private ResponseEntity getResponseEntityWhenJAXBExceptionThrown(String msg, String t) {
        logger.severe(msg);
        return ResponseEntity.status(400).body(t);
    }

    private ResponseEntity getResponseEntityWhenExceptionIsThrown(Exception e, String msg, String t) {
        logger.severe(msg);
        e.printStackTrace();
        return ResponseEntity.status(500).body(t);
    }
}
