package pl.wroc.pwr.controller.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.wroc.pwr.sa.model.TreeEnum;

/**
 * Created by sztukers on 10:04.
 */
@Data
@AllArgsConstructor
public class TreeDTO {
    private Long id;
    private String name;
    private TreeEnum type;
}
