package pl.wroc.pwr.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.wroc.pwr.sa.ft.TopProbability;
import pl.wroc.pwr.service.TreeService;

import java.util.Map;

/**
 * Created by sztukers on 09:20.
 */
@RestController
@RequestMapping("/rest")
public class AnalysisRestController {

    private TreeService treeService;

    @Autowired
    public AnalysisRestController(TreeService treeService) {
        this.treeService = treeService;
    }

    @GetMapping(value = "/analyseFaultTree/general/{id}")
    public Map<String, TopProbability> analyseFaultTree(@PathVariable Long id) throws Exception {
        return treeService.analyseFaultTree(id);
    }

    @GetMapping(value = "/analyseFaultTree/fuzzy-importance-index/{id}")
    public String getFuzzyImportanceIndex(@PathVariable Long id) throws Exception {
        return treeService.getFuzzyImportanceIndex(id);
    }

    @GetMapping(value = "/analyseEventTree/check-the-cohesion/{id}")
    public String checkTheCohesionOfTheTree(@PathVariable Long id) throws Exception {
        return treeService.checkTheCohesionOfTheTree(id);
    }

    @GetMapping(value = "/analyseEventTree/scenario-counter/{id}")
    public String countTheScenarios(@PathVariable Long id, @RequestParam String options) throws Exception {
        return treeService.countTheScenarios(options, id).toString();
    }
}
