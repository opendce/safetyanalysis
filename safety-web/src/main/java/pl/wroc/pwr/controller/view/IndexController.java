package pl.wroc.pwr.controller.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.graph.EventTreeGraph;
import pl.wroc.pwr.sa.graph.FaultTreeGraph;
import pl.wroc.pwr.service.TreeService;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

@Controller
public class IndexController {
	
	private static final Logger logger = Logger.getLogger(IndexController.class.getName());
	private final TreeService treeService;

	@Autowired
	public IndexController(TreeService treeService) {
		this.treeService = treeService;
	}

	@GetMapping("/")
	public String index(){
		return "index";
	}
	
	@GetMapping("/listTrees")
	public String listTrees(Model model) throws Exception {
		model.addAttribute("trees", treeService.listTrees());
		return "listTrees";
	}
	
	@GetMapping("/uploadEventTree")
	public String uploadEventTree(){
		return "uploadEventTree";
	}
	
	@GetMapping("/uploadFaultTree")
	public String uploadFaultTree(){
		return "uploadFaultTree";
	}


}
