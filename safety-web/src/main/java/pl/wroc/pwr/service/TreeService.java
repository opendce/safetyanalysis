package pl.wroc.pwr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.ft.TopProbability;
import pl.wroc.pwr.sa.model.TreeEnum;
import pl.wroc.pwr.sa.model.TreeModel;
import pl.wroc.pwr.sa.repository.ITreeRepository;

import javax.transaction.Transactional;
import javax.xml.bind.JAXBException;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class TreeService {

	private final ITreeRepository treeDAO;
	private TreeAnalyser analyser;
	
	@Autowired
	public TreeService(ITreeRepository eventTreeDAO, TreeAnalyser analyser){
		this.treeDAO = eventTreeDAO;
		this.analyser = analyser;
	}

	public TreeModel saveEventTree(String xml, String name) throws Exception{
        EventTree eventTree = EventTree.loadFromXMLString(xml);
		if (eventTree != null) {
			TreeModel eventTreeModel = new TreeModel(TreeEnum.EVENT_TREE, xml, name);
			return treeDAO.save(eventTreeModel);
		} else {
			throw new JAXBException("Parser returned null object.");
		}
	}

    public TreeModel saveFaultTree(String xml, String name) throws Exception {
		FaultTree faultTree = FaultTree.fromXml(xml);
		if (faultTree != null) {
			TreeModel faultTreeModel = new TreeModel(TreeEnum.FAULT_TREE, xml, name);
			return treeDAO.save(faultTreeModel);
		} else {
			throw new JAXBException("Parser returned null object.");
		}
	}

	public List<TreeModel> listTrees() throws Exception {
		return treeDAO.findAll();
	}

	public TreeModel getTree(long id) throws Exception {
		return treeDAO.findOne(id);
	}

	public Map<String, TopProbability> analyseFaultTree(Long id) throws Exception {
		return analyser.analyseFaultTree(FaultTree.fromXml(getTree(id).getXml()));
	}

	public String getFuzzyImportanceIndex(@PathVariable Long id) throws Exception {
		return analyser.getFuzzyImportanceIndex(FaultTree.fromXml(getTree(id).getXml())).toString();
	}

	public String checkTheCohesionOfTheTree(@PathVariable Long id) throws Exception {
		return analyser.checkTheCohesionOfTheTree(EventTree.loadFromXMLString(getTree(id).getXml()));
	}

	public MembershipFunction countTheScenarios(String options, Long id) throws Exception {
		return analyser.countTheScenarios(options, EventTree.loadFromXMLString(getTree(id).getXml()));
	}

}
