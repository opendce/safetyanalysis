package pl.wroc.pwr.service;

import org.springframework.stereotype.Component;
import pl.wroc.pwr.sa.api.Api.MainAPI;
import pl.wroc.pwr.sa.et.EventTree;
import pl.wroc.pwr.sa.fr.MembershipFunction;
import pl.wroc.pwr.sa.ft.Fault;
import pl.wroc.pwr.sa.ft.FaultTree;
import pl.wroc.pwr.sa.ft.TopProbability;
import java.io.IOException;
import java.util.Map;

@Component
public class TreeAnalyser {

    private MainAPI api = new MainAPI();

    public String checkTheCohesionOfTheTree(EventTree eventTree) throws IOException {
        return api.checkTheCohesionOfTheTree(eventTree) ? "Event tree is ok" :
                "There is no tree that I could analyze.";
    }

    public MembershipFunction countTheScenarios(String options, EventTree eventTree) {
        return api.eventTreeScenarioCounter(options, eventTree);
    }

    public Map<String, TopProbability> analyseFaultTree(FaultTree faultTree) {
        return api.analyseFaultTree(faultTree);
    }

    public Map<Fault, MembershipFunction> getFuzzyImportanceIndex(FaultTree faultTree) {
        return api.getFuzzyImportanceIndex(faultTree);
    }
}
