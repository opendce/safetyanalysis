package pl.wroc.pwr.sa.fr;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maciej
 */
public class ParamTriangleTest {
	
	public ParamTriangleTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of getA method, of class ParamTriangle.
	 */
	@Test
	public void testGetA() throws Exception {
		System.out.println("getA");
		ParamTriangle instance = new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR);
		double expResult = 0.1;
		double result = instance.getA();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getB method, of class ParamTriangle.
	 */
	@Test
	public void testGetB() throws Exception {
		System.out.println("getB");
		ParamTriangle instance = new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR);
		double expResult = 0.2;
		double result = instance.getB();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getC method, of class ParamTriangle.
	 */
	@Test
	public void testGetC() throws Exception {
		System.out.println("getC");
		ParamTriangle instance = new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR);
		double expResult = 0.3;
		double result = instance.getC();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getScale method, of class ParamTriangle.
	 */
	@Test
	public void testGetScale() throws Exception {
		System.out.println("getScale");
		ParamTriangle instance = new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR);
		Scale expResult = Scale.LINEAR;
		Scale result = instance.getScale();
		assertEquals(expResult, result);
	}

	/**
	 * Test of add method, of class ParamTriangle.
	 */
	@Test
	public void testAdd() throws Exception {
		System.out.println("add");
		Param y = new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR);
		ParamTriangle instance = new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR);
		ParamTriangle expResult = new ParamTriangle (0.2, 0.4, 0.6, Scale.LINEAR);
		ParamTriangle result = instance.add(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		
		y = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
		instance = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
		expResult = new ParamTriangle (1, 1, 1, Scale.LINEAR);
		result = instance.add(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		
		y = new ParamTriangle (-0.3, -0.22, -0.15, Scale.LOGARITHMIC);
		instance = new ParamTriangle (-1, -0.7, -0.52, Scale.LOGARITHMIC);
		expResult = new ParamTriangle (-0.22, -0.1, 0, Scale.LOGARITHMIC);
		result = instance.add(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		
		y = new ParamTriangle (-0.3, -0.22, -0.15, Scale.LOGARITHMIC);
		instance = new ParamTriangle (-0.3, -0.22, -0.15, Scale.LOGARITHMIC);
		expResult = new ParamTriangle (0, 0, 0, Scale.LOGARITHMIC);
		result = instance.add(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		
		try {
			y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			result = instance.add(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		try {
			y = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			instance = new ParamTriangle (-11, -10, -9, Scale.LOGARITHMIC);
			result = instance.add(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
	}

	/**
	 * Test of subtract method, of class ParamTriangle.
	 */
	@Test
	public void testSubtract() throws Exception {
		System.out.println("subtract");
		Param y = new ParamTriangle (0.2, 0.3, 0.4, Scale.LINEAR);
		ParamTriangle instance = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
		ParamTriangle expResult = new ParamTriangle (0.1, 0.3, 0.5, Scale.LINEAR);
		ParamTriangle result = instance.subtract(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		
		y = new ParamTriangle (-0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		instance = new ParamTriangle (-0.3, -0.22, -0.15, Scale.LOGARITHMIC);
		expResult = new ParamTriangle (-1, -0.52, -0.3, Scale.LOGARITHMIC);
		result = instance.subtract(y);
		assertEquals(expResult.getA(), result.getA(), 0.02);
		assertEquals(expResult.getB(), result.getB(), 0.02);
		assertEquals(expResult.getC(), result.getC(), 0.02);
		
		try {
			y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			result = instance.subtract(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		try {
			y = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			instance = new ParamTriangle (-11, -10, -9, Scale.LOGARITHMIC);
			result = instance.subtract(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
	}

	/**
	 * Test of multiply method, of class ParamTriangle.
	 */
	@Test
	public void testMultiply() throws Exception {
		System.out.println("multiply");
		Param y = new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR);
		ParamTriangle instance = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
		ParamTriangle expResult = new ParamTriangle (0.05, 0.12, 0.21, Scale.LINEAR);
		ParamTriangle result = instance.multiply(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		
		y = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
		instance = new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR);
		expResult = new ParamTriangle (0.05, 0.12, 0.21, Scale.LINEAR);
		result = instance.multiply(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		
		y = new ParamTriangle (-0.3, -0.22, -0.15, Scale.LOGARITHMIC);
		instance = new ParamTriangle (-1, -0.7, -0.52, Scale.LOGARITHMIC);
		expResult = new ParamTriangle (-1.3, -0.92, -0.68, Scale.LOGARITHMIC);
		result = instance.multiply(y);
		assertEquals(expResult.getA(), result.getA(), 0.02);
		assertEquals(expResult.getB(), result.getB(), 0.02);
		assertEquals(expResult.getC(), result.getC(), 0.02);
		
		y = new ParamTriangle (-1, -0.7, -0.52, Scale.LOGARITHMIC);
		instance = new ParamTriangle (-0.3, -0.22, -0.15, Scale.LOGARITHMIC);
		expResult = new ParamTriangle (-1.3, -0.92, -0.68, Scale.LOGARITHMIC);
		result = instance.multiply(y);
		assertEquals(expResult.getA(), result.getA(), 0.02);
		assertEquals(expResult.getB(), result.getB(), 0.02);
		assertEquals(expResult.getC(), result.getC(), 0.02);
		
		try {
			y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			result = instance.multiply(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		try {
			y = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			instance = new ParamTriangle (-11, -10, -9, Scale.LOGARITHMIC);
			result = instance.multiply(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
	}

	/**
	 * Test of divide method, of class ParamTriangle.
	 */
	@Test
	public void testDivide() throws Exception {
		System.out.println("divide");
		Param y = new ParamTriangle (0.6, 0.7, 0.8, Scale.LINEAR);
		ParamTriangle instance = new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR);
		ParamTriangle expResult = new ParamTriangle (0.13, 0.29, 0.5, Scale.LINEAR);
		ParamTriangle result = instance.divide(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		
		y = new ParamTriangle (-0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		instance = new ParamTriangle (-1, -0.7, -0.52, Scale.LOGARITHMIC);
		expResult = new ParamTriangle (-0.89, -0.54, -0.3, Scale.LOGARITHMIC);
		result = instance.divide(y);
		assertEquals(expResult.getA(), result.getA(), 0.02);
		assertEquals(expResult.getB(), result.getB(), 0.02);
		assertEquals(expResult.getC(), result.getC(), 0.02);
		
		try {
			y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			result = instance.multiply(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		try {
			y = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			instance = new ParamTriangle (-11, -10, -9, Scale.LOGARITHMIC);
			result = instance.multiply(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
	}

	/**
	 * Test of getValue method, of class ParamTriangle.
	 */
	@Test
	public void testGetValue() throws Exception {
		System.out.println("getValue");
		double argument = 0;
		ParamTriangle instance = new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR);
		double expResult = 0;
		double result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.15;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.25;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTriangle (0.2, 0.2, 0.3, Scale.LINEAR);
		argument = 0.15;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.25;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTriangle (0.2, 0.3, 0.3, Scale.LINEAR);
		argument = 0.15;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.25;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTriangle (0.2, 0.2, 0.2, Scale.LINEAR);
		argument = 0.15;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.2;
		expResult = 1;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.25;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTriangle (-7, -6, -5, Scale.LOGARITHMIC);
		argument = -8;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -6.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -5.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTriangle (-6, -6, -5, Scale.LOGARITHMIC);
		argument = -7;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -5.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTriangle (-6, -5, -5, Scale.LOGARITHMIC);
		argument = -7;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
                
                argument = -12;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -5.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTriangle (-6, -6, -6, Scale.LOGARITHMIC);
		argument = -7;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -6;
		expResult = 1;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -5;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
	}

        
        /**
	 * Test of getJacardSimilarity method, of class ParamTriangle.
	 */
	@Test
	public void testGetJacardSimilarity() throws Exception {
		System.out.println("getJacardSimilarity");
		Param y = new ParamTriangle(0.4, 0.5, 0.6, Scale.LINEAR);
		ParamTriangle instance = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
		double expResult = 0.0;
		double result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR);
		instance = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
		expResult = 0.14;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR);
		instance = new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR);
		expResult = 1.0;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
		instance = new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR);
		expResult = 0.14;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
		instance = new ParamTriangle(0.4, 0.5, 0.6, Scale.LINEAR);
		expResult = 0.0;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		try {
			y = new ParamTriangle(-0.4, -0.3, -0.22, Scale.LOGARITHMIC);
			instance = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
			instance.getJacardSimilarity(y);
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
		
		try {
			y = new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
			instance.getJacardSimilarity(y);
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		y = new ParamTriangle(-0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		instance = new ParamTriangle(-1, -0.7, -0.52, Scale.LOGARITHMIC);
		expResult = 0.0;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTriangle(-0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		instance = new ParamTriangle(-1, -0.7, -0.52, Scale.LOGARITHMIC);
		expResult = 0.13;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTriangle(-0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		instance = new ParamTriangle(-0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		expResult = 1.0;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTriangle(-1.0, -0.7, -0.52, Scale.LOGARITHMIC);
		instance = new ParamTriangle(-0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		expResult = 0.13;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTriangle(-1.0, -0.7, -0.52, Scale.LOGARITHMIC);
		instance = new ParamTriangle(-0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		expResult = 0.0;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		try {
			y = new ParamTriangle(0.4, 0.5, 0.6, Scale.LINEAR);
			instance = new ParamTriangle(-1.0, -0.7, -0.52, Scale.LOGARITHMIC);
			instance.getJacardSimilarity(y);
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
		
		try {
			y = new ParamTrapezoid(-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
			instance = new ParamTriangle(-1.0, -0.7, -0.52, Scale.LOGARITHMIC);
			instance.getJacardSimilarity(y);
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
	}
        
	
        /**
	 * Test of getInclusionDegreeMeasure method, of class ParamTriangle.
	 */
	@Test
	public void testGetInclusionDegreeMeasure() throws Exception {
		System.out.println("getInclusionDegreeMeasure");
		Param y = new ParamTriangle(0.4, 0.5, 0.6, Scale.LINEAR);
		ParamTriangle instance = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
		double expResult = 0.0;
		double result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR);
		instance = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
		expResult = 0.25;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.001);
		
		y = new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR);
		instance = new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR);
		expResult = 1.0;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
		instance = new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR);
		expResult = 0.25;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
		instance = new ParamTriangle(0.4, 0.5, 0.6, Scale.LINEAR);
		expResult = 0.0;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		try {
			y = new ParamTriangle(-0.4, -0.3, -0.22, Scale.LOGARITHMIC);
			instance = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
			instance.getInclusionDegreeMeasure(y);
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
		
		try {
			y = new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
			instance.getInclusionDegreeMeasure(y);
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
                
		y = new ParamTriangle(-0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		instance = new ParamTriangle(-1, -0.7, -0.5, Scale.LOGARITHMIC);
		expResult = 0.0;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTriangle(-0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		instance = new ParamTriangle(-1, -0.7, -0.52, Scale.LOGARITHMIC);
		expResult = 0.19;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTriangle(-0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		instance = new ParamTriangle(-0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		expResult = 1.0;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTriangle(-1.0, -0.7, -0.52, Scale.LOGARITHMIC);
		instance = new ParamTriangle(-0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		expResult = 0.3;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTriangle(-1.0, -0.7, -0.52, Scale.LOGARITHMIC);
		instance = new ParamTriangle(-0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		expResult = 0.0;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		try {
			y = new ParamTriangle(0.4, 0.5, 0.6, Scale.LINEAR);
			instance = new ParamTriangle(-1.0, -0.7, -0.52, Scale.LOGARITHMIC);
			instance.getInclusionDegreeMeasure(y);
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
		
		try {
			y = new ParamTrapezoid(-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
			instance = new ParamTriangle(-1.0, -0.7, -0.52, Scale.LOGARITHMIC);
			instance.getInclusionDegreeMeasure(y);
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
	}
}