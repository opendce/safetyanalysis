package pl.wroc.pwr.sa.fr;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maciej
 */
public class FuzzyOperationsTest {
	
	public FuzzyOperationsTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of add method, of class FuzzyOperations.
	 */
	@Test
	public void testAdd() throws Exception {
		System.out.println("add");
		MembershipFunction a = new TriangleFunction (new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction (new ParamTriangle (0.2, 0.3, 0.4, Scale.LINEAR), "");
		MembershipFunction expResult = new TriangleFunction (new ParamTriangle (0.3, 0.5, 0.7, Scale.LINEAR), "");
                FuzzyOperations operations = new FuzzyOperations();
		MembershipFunction result =  operations.add(a,b);
		assertEquals(((TriangleFunction) expResult).getParameters().getA(), ((TriangleFunction) result).getParameters().getA(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getB(), ((TriangleFunction) result).getParameters().getB(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getC(), ((TriangleFunction) result).getParameters().getC(), 0.01);
	}

	/**
	 * Test of subtract method, of class FuzzyOperations.
	 */
	@Test
	public void testSubtract() throws Exception {
		System.out.println("subtract");
		MembershipFunction a = new TriangleFunction (new ParamTriangle (0.4, 0.5, 0.6, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction (new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR), "");
		MembershipFunction expResult = new TriangleFunction (new ParamTriangle (0.1, 0.3, 0.5, Scale.LINEAR), "");
                FuzzyOperations operations = new FuzzyOperations();
		MembershipFunction result = operations.subtract(a, b);
		assertEquals(((TriangleFunction) expResult).getParameters().getA(), ((TriangleFunction) result).getParameters().getA(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getB(), ((TriangleFunction) result).getParameters().getB(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getC(), ((TriangleFunction) result).getParameters().getC(), 0.01);
        }
        
	/**
	 * Test of multiply method, of class FuzzyOperations.
	 */
	@Test
	public void testMultiply() throws Exception {
		System.out.println("multiply");
		MembershipFunction a = new TriangleFunction (new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction (new ParamTriangle (0.2, 0.3, 0.4, Scale.LINEAR), "");
		MembershipFunction expResult = new TriangleFunction (new ParamTriangle (0.02, 0.06, 0.12, Scale.LINEAR), "");
                FuzzyOperations operations = new FuzzyOperations();
		MembershipFunction result = operations.multiply(a, b);
		assertEquals(((TriangleFunction) expResult).getParameters().getA(), ((TriangleFunction) result).getParameters().getA(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getB(), ((TriangleFunction) result).getParameters().getB(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getC(), ((TriangleFunction) result).getParameters().getC(), 0.01);
	}

	/**
	 * Test of divide method, of class FuzzyOperations.
	 */
	@Test
	public void testDivide() throws Exception {
		System.out.println("divide");
		MembershipFunction a = new TriangleFunction (new ParamTriangle (0.2, 0.3, 0.4, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction (new ParamTriangle (0.4, 0.5, 0.6, Scale.LINEAR), "");
		MembershipFunction expResult = new TriangleFunction (new ParamTriangle (0.33, 0.6, 1, Scale.LINEAR), "");
        FuzzyOperations operations = new FuzzyOperations();
		MembershipFunction result = operations.divide(a, b);
		assertEquals(((TriangleFunction) expResult).getParameters().getA(), ((TriangleFunction) result).getParameters().getA(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getB(), ((TriangleFunction) result).getParameters().getB(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getC(), ((TriangleFunction) result).getParameters().getC(), 0.01);
	}

	/**
	 * Test of getValue method, of class FuzzyOperations.
	 */
	@Test
	public void testGetValue() throws Exception {
		System.out.println("getValue");
		MembershipFunction function = new TriangleFunction (new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR), "");
		double argument = 0.2;
		double expResult = 1.0;
                FuzzyOperations operations = new FuzzyOperations();
		double result = operations.getValue(function, argument);
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getJacardSimilarity method, of class FuzzyOperations.
	 */
	@Test
	public void testGetJacardSimilarity() throws Exception {
		System.out.println("getJacardSimilarity");
		MembershipFunction a = new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction(new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR), "");
		double expResult = 0.14;
                FuzzyOperations operations = new FuzzyOperations();
		double result = operations.getJacardSimilarity(a, b);
		assertEquals(expResult, result, 0.01);
	}
        
        /**
	 * Test of getInclusionDegreeMeasure method, of class FuzzyOperations.
	 */
	@Test
	public void getInclusionDegreeMeasure() throws Exception {
		System.out.println("getInclusionDegreeMeasure");
		MembershipFunction a = new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction(new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR), "");
		double expResult = 0.25;
                FuzzyOperations operations = new FuzzyOperations();
		double result = operations.getInclusionDegreeMeasure(a, b);
		assertEquals(expResult, result, 0.01);
	}
}