package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maciej
 */
public class FuzzyLogicFacadeTest {
	
	public FuzzyLogicFacadeTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of createTriangle method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTriangle() throws Exception {
		System.out.println("createTriangle");
		ParamTriangle parameters = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
		String name = "";
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TriangleFunction expResult = new TriangleFunction(parameters, name);
		TriangleFunction result = instance.createTriangle(parameters, name);
		assertEquals(expResult.getParameters().getScale(), result.getParameters().getScale());
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters(), result.getParameters());
		
		try {
			parameters = new ParamTriangle(0.1, 0.2, 2, Scale.LINEAR);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTriangle(-1, 0.2, 0.3, Scale.LINEAR);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTriangle(0.1, 0.3, 0.2, Scale.LINEAR);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("values in bad order", e.getMessage());
		}
		
		parameters = new ParamTriangle(-12, -11, -10, Scale.LOGARITHMIC);
		expResult = new TriangleFunction(parameters, name);
		result = instance.createTriangle(parameters, name);
		assertEquals(expResult.getParameters().getScale(), result.getParameters().getScale());
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters(), result.getParameters());
		
		try {
			parameters = new ParamTriangle(-12, -11, 1, Scale.LOGARITHMIC);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTriangle(-13, -11, -10, Scale.LOGARITHMIC);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTriangle(-12, -10, -11, Scale.LOGARITHMIC);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("values in bad order", e.getMessage());
		}
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

        /**
	 * Test of createTriangle method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTriangle2() throws Exception {
		System.out.println("createTriangle2");
		ParamTriangle parameters = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR);
		String name = "";
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TriangleFunction expResult = new TriangleFunction(parameters, name);
		TriangleFunction result = instance.createTriangle(0.1, 0.2, 0.3, Scale.LINEAR, name);
		assertEquals(expResult.getParameters().getScale(), result.getParameters().getScale());
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters(), result.getParameters());
	}

        
	/**
	 * Test of createTrapezoid method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTrapezoid() throws Exception {
		System.out.println("createTrapezoid");
		ParamTrapezoid parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		String name = "";
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TrapezoidFunction expResult = new TrapezoidFunction(parameters, name);
		TrapezoidFunction result = instance.createTrapezoid(parameters, name);
		assertEquals(expResult.getParameters().getScale(), result.getParameters().getScale());
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters(), result.getParameters());
		
		try {
			parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 2, Scale.LINEAR);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTrapezoid(-1, 0.2, 0.3, 0.4, Scale.LINEAR);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTrapezoid(0.1, 0.2, 0.4, 0.3, Scale.LINEAR);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("values in bad order", e.getMessage());
		}
		
		parameters = new ParamTrapezoid(-12, -11, -10, -9, Scale.LOGARITHMIC);
		expResult = new TrapezoidFunction(parameters, name);
		result = instance.createTrapezoid(parameters, name);
		assertEquals(expResult.getParameters().getScale(), result.getParameters().getScale());
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters(), result.getParameters());
		
		try {
			parameters = new ParamTrapezoid(-12, -11, -10, 1, Scale.LOGARITHMIC);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTrapezoid(-13, -11, -10, -9, Scale.LOGARITHMIC);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTrapezoid(-12, -11, -9, -10, Scale.LOGARITHMIC);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("values in bad order", e.getMessage());
		}
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}
        
        	/**
	 * Test of createTrapezoid method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTrapezoid2() throws Exception {
		System.out.println("createTrapezoid2");
		ParamTrapezoid parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		String name = "";
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TrapezoidFunction expResult = new TrapezoidFunction(parameters, name);
		TrapezoidFunction result = instance.createTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, name);
		assertEquals(expResult.getParameters().getScale(), result.getParameters().getScale());
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters(), result.getParameters());
	}

	/**
	 * Test of createTriangleLingSymmetric method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTriangleLingSymmetric() throws Exception {
		System.out.println("createTriangleLingSymmetric");
		String name = "";
		Scale scale = Scale.LINEAR;
		List<String> names = new ArrayList<String> (Arrays.asList ("small", "medium", "big"));
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TriangleLingSymmetric result = instance.createTriangleLingSymmetric(name, scale, names);
		assertNotNull(result);
		assertEquals(result.getFunction("small").getParameters().getA(), 0, 0.01);
		assertEquals(result.getFunction("small").getParameters().getB(), 0, 0.01);
		assertEquals(result.getFunction("small").getParameters().getC(), 0.5, 0.01);
		assertEquals(result.getFunction("medium").getParameters().getA(), 0, 0.01);
		assertEquals(result.getFunction("medium").getParameters().getB(), 0.5, 0.01);
		assertEquals(result.getFunction("medium").getParameters().getC(), 1, 0.01);
		assertEquals(result.getFunction("big").getParameters().getA(), 0.5, 0.01);
		assertEquals(result.getFunction("big").getParameters().getB(), 1, 0.01);
		assertEquals(result.getFunction("big").getParameters().getC(), 1, 0.01);
		
		scale = Scale.LOGARITHMIC;
		result = instance.createTriangleLingSymmetric(name, scale, names);
		assertNotNull(result);
		assertEquals(result.getFunction("small").getParameters().getA(), -12, 0.01);
		assertEquals(result.getFunction("small").getParameters().getB(), -12, 0.01);
		assertEquals(result.getFunction("small").getParameters().getC(), -6, 0.01);
		assertEquals(result.getFunction("medium").getParameters().getA(), -12, 0.01);
		assertEquals(result.getFunction("medium").getParameters().getB(), -6, 0.01);
		assertEquals(result.getFunction("medium").getParameters().getC(), 0, 0.01);
		assertEquals(result.getFunction("big").getParameters().getA(), -6, 0.01);
		assertEquals(result.getFunction("big").getParameters().getB(), 0, 0.01);
		assertEquals(result.getFunction("big").getParameters().getC(), 0, 0.01);
		
		try {
			names = new ArrayList<String> (Arrays.asList ("small", "medium"));
			result = instance.createTriangleLingSymmetric(name, scale, names);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong number of names", e.getMessage());
		}
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of createTrapezoidLingSymmetric method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTrapezoidLingSymmetric() throws Exception {
		System.out.println("createTrapezoidLingSymmetric");
		String name = "";
		double ratio = 0.5;
		Scale scale = Scale.LINEAR;
		List<String> names = new ArrayList<String> (Arrays.asList ("small", "medium", "big"));
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TrapezoidLingSymmetric result = instance.createTrapezoidLingSymmetric(name, ratio, scale, names);
		assertNotNull(result);
		assertEquals(0, result.getFunction("small").getParameters().getA(), 0.01);
		assertEquals(0, result.getFunction("small").getParameters().getB(), 0.01);
		assertEquals(0.17, result.getFunction("small").getParameters().getC(), 0.01);
		assertEquals(0.33, result.getFunction("small").getParameters().getD(), 0.01);
		assertEquals(0.17, result.getFunction("medium").getParameters().getA(), 0.01);
		assertEquals(0.33, result.getFunction("medium").getParameters().getB(), 0.01);
		assertEquals(0.66, result.getFunction("medium").getParameters().getC(), 0.01);
		assertEquals(0.83, result.getFunction("medium").getParameters().getD(), 0.01);
		assertEquals(result.getFunction("big").getParameters().getA(), 0.66, 0.01);
		assertEquals(result.getFunction("big").getParameters().getB(), 0.83, 0.01);
		assertEquals(result.getFunction("big").getParameters().getC(), 1.0, 0.01);
		assertEquals(result.getFunction("big").getParameters().getD(), 1.0, 0.01);
		
		scale = Scale.LOGARITHMIC;
		result = instance.createTrapezoidLingSymmetric(name, ratio, scale, names);
		assertNotNull(result);
		assertEquals(result.getFunction("small").getParameters().getA(), -12, 0.01);
		assertEquals(result.getFunction("small").getParameters().getB(), -12, 0.01);
		assertEquals(result.getFunction("small").getParameters().getC(), -10, 0.01);
		assertEquals(result.getFunction("small").getParameters().getD(), -8, 0.01);
		assertEquals(result.getFunction("medium").getParameters().getA(), -10, 0.01);
		assertEquals(result.getFunction("medium").getParameters().getB(), -8, 0.01);
		assertEquals(result.getFunction("medium").getParameters().getC(), -4, 0.01);
		assertEquals(result.getFunction("medium").getParameters().getD(), -2, 0.01);
		assertEquals(result.getFunction("big").getParameters().getA(), -4, 0.01);
		assertEquals(result.getFunction("big").getParameters().getB(), -2, 0.01);
		assertEquals(result.getFunction("big").getParameters().getC(), 0, 0.01);
		assertEquals(result.getFunction("big").getParameters().getD(), 0, 0.01);
		
		try {
			ratio = -1;
			result = instance.createTrapezoidLingSymmetric(name, ratio, scale, names);
			fail();
		}
		catch (Exception e) {
			assertEquals("ratio is not positive", e.getMessage());
		}
		
		try {
			ratio = 0.5;
			names = new ArrayList<String> (Arrays.asList ("small", "medium"));
			result = instance.createTrapezoidLingSymmetric(name, ratio, scale, names);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong number of names", e.getMessage());
		}
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of createTriangleLingNonSymmetric method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTriangleLingNonSymmetric_1() throws Exception {
		System.out.println("createTriangleLingNonSymmetric");
		Scale scale = Scale.LINEAR;
                Type type = Type.FIRST;
		List<String> names = new ArrayList<String> (Arrays.asList ("small", "medium", "big"));
		List<ParamTriangle> parameters = new ArrayList<ParamTriangle> (Arrays.asList (new ParamTriangle(0.1, 0.2, 0.3, scale), new ParamTriangle(0.2, 0.3, 0.4, scale), new ParamTriangle(0.3, 0.4, 0.5, scale)));
		String name = "";
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TriangleLingNonSymmetric result = instance.createTriangleLingNonSymmetric(name, names, parameters);
		assertNotNull(result);
		
		try {
			names = new ArrayList<String> (Arrays.asList ("small", "big"));
			result = instance.createTriangleLingNonSymmetric(name, names, parameters);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong number of parameters", e.getMessage());
		}
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of createTriangleLingNonSymmetric method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTriangleLingNonSymmetric_2() throws Exception {
		System.out.println("createTriangleLingNonSymmetric");
		Scale scale = Scale.LINEAR;
		List<String> names = new ArrayList<String> (Arrays.asList ("small", "medium", "big"));
		List<Double> fractions = new ArrayList<Double> (Arrays.asList (0.2, 0.8));
		String name = "";
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TriangleLingNonSymmetric result = instance.createTriangleLingNonSymmetric(scale, names, fractions, name);
		assertEquals(0, result.getFunction("small").getParameters().getA(), 0.01);
		assertEquals(0, result.getFunction("small").getParameters().getB(), 0.01);
		assertEquals(0.2, result.getFunction("small").getParameters().getC(), 0.01);
		assertEquals(0, result.getFunction("medium").getParameters().getA(), 0.01);
		assertEquals(0.2, result.getFunction("medium").getParameters().getB(), 0.01);
		assertEquals(1, result.getFunction("medium").getParameters().getC(), 0.01);
		assertEquals(0.2, result.getFunction("big").getParameters().getA(), 0.01);
		assertEquals(1, result.getFunction("big").getParameters().getB(), 0.01);
		assertEquals(1, result.getFunction("big").getParameters().getC(), 0.01);
		
		scale = Scale.LOGARITHMIC;
		fractions = new ArrayList<Double> (Arrays.asList (2.0, 10.0));
		result = instance.createTriangleLingNonSymmetric(scale, names, fractions, name);
		assertEquals(-12, result.getFunction("small").getParameters().getA(), 0.01);
		assertEquals(-12, result.getFunction("small").getParameters().getB(), 0.01);
		assertEquals(-10, result.getFunction("small").getParameters().getC(), 0.01);
		assertEquals(-12, result.getFunction("medium").getParameters().getA(), 0.01);
		assertEquals(-10, result.getFunction("medium").getParameters().getB(), 0.01);
		assertEquals(0, result.getFunction("medium").getParameters().getC(), 0.01);
		assertEquals(-10, result.getFunction("big").getParameters().getA(), 0.01);
		assertEquals(0, result.getFunction("big").getParameters().getB(), 0.01);
		assertEquals(0, result.getFunction("big").getParameters().getC(), 0.01);
		
		try {
			scale = Scale.LOGARITHMIC;
			fractions = new ArrayList<Double> (Arrays.asList (1.0, 1.0, 10.0));
			result = instance.createTriangleLingNonSymmetric(scale, names, fractions, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong number of fractions", e.getMessage());
		}
		
		try {
			scale = Scale.LINEAR;
			fractions = new ArrayList<Double> (Arrays.asList (0.2, 0.6, 0.2));
			result = instance.createTriangleLingNonSymmetric(scale, names, fractions, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong number of fractions", e.getMessage());
		}
		
		try {
			scale = Scale.LOGARITHMIC;
			fractions = new ArrayList<Double> (Arrays.asList (3.0, 10.0));
			result = instance.createTriangleLingNonSymmetric(scale, names, fractions, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong fractions", e.getMessage());
		}
		
		try {
			scale = Scale.LINEAR;
			fractions = new ArrayList<Double> (Arrays.asList (0.3, 0.8));
			result = instance.createTriangleLingNonSymmetric(scale, names, fractions, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong fractions", e.getMessage());
		}
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}
	
	/**
	 * Test of createTrapezoidLingNonSymmetric method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTrapezoidLingNonSymmetric_1() throws Exception {
		System.out.println("createTrapezoidLingNonSymmetric");
		String name = "";
		Scale scale = Scale.LINEAR;
                Type type = Type.FIRST;
		List<String> names = new ArrayList<String> (Arrays.asList ("small", "medium", "big"));
		List<ParamTrapezoid> parameters = new ArrayList<ParamTrapezoid> (Arrays.asList (new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, scale), new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, scale), new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, scale)));
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TrapezoidLingNonSymmetric result = instance.createTrapezoidLingNonSymmetric(name, names, parameters);
		assertNotNull(result);
		
		try {
			names = new ArrayList<String> (Arrays.asList ("small", "big"));
			result = instance.createTrapezoidLingNonSymmetric(name, names, parameters);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong number of parameters", e.getMessage());
		}
		// TODO review the generated test code and remove the default call to fail.
		//fail("The test case is a prototype.");
	}
	
	/**
	 * Test of createTrapezoidLingNonSymmetric method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTrapezoidLingNonSymmetric_2() throws Exception {
		System.out.println("createTrapezoidLingNonSymmetric");
		Scale scale = Scale.LINEAR;
		List<String> names = new ArrayList<String> (Arrays.asList ("small", "medium", "big"));
		List<Double> fractions = new ArrayList<Double> (Arrays.asList (0.2, 0.8));
		String name = "";
		double ratio = 0.5;
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TrapezoidLingNonSymmetric result = instance.createTrapezoidLingNonSymmetric(name, ratio, scale, names, fractions);
		assertEquals(0, result.getFunction("small").getParameters().getA(), 0.01);
		assertEquals(0, result.getFunction("small").getParameters().getB(), 0.01);
		assertEquals(0.07, result.getFunction("small").getParameters().getC(), 0.01);
		assertEquals(0.13, result.getFunction("small").getParameters().getD(), 0.01);
		assertEquals(0.07, result.getFunction("medium").getParameters().getA(), 0.01);
		assertEquals(0.13, result.getFunction("medium").getParameters().getB(), 0.01);
		assertEquals(0.47, result.getFunction("medium").getParameters().getC(), 0.01);
		assertEquals(0.73, result.getFunction("medium").getParameters().getD(), 0.01);
		assertEquals(0.47, result.getFunction("big").getParameters().getA(), 0.01);
		assertEquals(0.73, result.getFunction("big").getParameters().getB(), 0.01);
		assertEquals(1, result.getFunction("big").getParameters().getC(), 0.01);
		assertEquals(1, result.getFunction("big").getParameters().getD(), 0.01);
		
		scale = Scale.LOGARITHMIC;
		fractions = new ArrayList<Double> (Arrays.asList (2.0, 10.0));
		result = instance.createTrapezoidLingNonSymmetric(name, ratio, scale, names, fractions);
		assertEquals(-12, result.getFunction("small").getParameters().getA(), 0.01);
		assertEquals(-12, result.getFunction("small").getParameters().getB(), 0.01);
		assertEquals(-11.33, result.getFunction("small").getParameters().getC(), 0.01);
		assertEquals(-10.67, result.getFunction("small").getParameters().getD(), 0.01);
		assertEquals(-11.33, result.getFunction("medium").getParameters().getA(), 0.01);
		assertEquals(-10.67, result.getFunction("medium").getParameters().getB(), 0.01);
		assertEquals(-6.67, result.getFunction("medium").getParameters().getC(), 0.01);
		assertEquals(-3.33, result.getFunction("medium").getParameters().getD(), 0.01);
		assertEquals(-6.67, result.getFunction("big").getParameters().getA(), 0.01);
		assertEquals(-3.33, result.getFunction("big").getParameters().getB(), 0.01);
		assertEquals(0, result.getFunction("big").getParameters().getC(), 0.01);
		assertEquals(0, result.getFunction("big").getParameters().getD(), 0.01);
		
		try {
			scale = Scale.LOGARITHMIC;
			fractions = new ArrayList<Double> (Arrays.asList (1.0, 1.0, 10.0));
			result = instance.createTrapezoidLingNonSymmetric(name, ratio, scale, names, fractions);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong number of fractions", e.getMessage());
		}
		
		try {
			scale = Scale.LINEAR;
			fractions = new ArrayList<Double> (Arrays.asList (0.2, 0.6, 0.2));
			result = instance.createTrapezoidLingNonSymmetric(name, ratio, scale, names, fractions);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong number of fractions", e.getMessage());
		}
		
		try {
			scale = Scale.LOGARITHMIC;
			fractions = new ArrayList<Double> (Arrays.asList (3.0, 10.0));
			result = instance.createTrapezoidLingNonSymmetric(name, ratio, scale, names, fractions);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong fractions", e.getMessage());
		}
		
		try {
			scale = Scale.LINEAR;
			fractions = new ArrayList<Double> (Arrays.asList (0.3, 0.8));
			result = instance.createTrapezoidLingNonSymmetric(name, ratio, scale, names, fractions);
			fail();
		}
		catch (Exception e) {
			assertEquals("wrong fractions", e.getMessage());
		}
		
		try {
			ratio = -1;
			fractions = new ArrayList<Double> (Arrays.asList (0.2, 0.8));
			result = instance.createTrapezoidLingNonSymmetric(name, ratio, scale, names, fractions);
			fail();
		}
		catch (Exception e) {
			assertEquals("ratio is not positive", e.getMessage());
		}
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
                
	}
        
        	/**
	 * Test of add method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testAdd() throws Exception {
		System.out.println("add");
		MembershipFunction a = new TriangleFunction (new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction (new ParamTriangle (0.2, 0.3, 0.4, Scale.LINEAR), "");
		MembershipFunction expResult = new TriangleFunction (new ParamTriangle (0.3, 0.5, 0.7, Scale.LINEAR), "");
                FuzzyLogicFacade facade = new FuzzyLogicFacade();
		MembershipFunction result =  facade.add(a,b);
		assertEquals(((TriangleFunction) expResult).getParameters().getA(), ((TriangleFunction) result).getParameters().getA(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getB(), ((TriangleFunction) result).getParameters().getB(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getC(), ((TriangleFunction) result).getParameters().getC(), 0.01);
	}

	/**
	 * Test of subtract method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testSubtract() throws Exception {
		System.out.println("subtract");
		MembershipFunction a = new TriangleFunction (new ParamTriangle (0.4, 0.5, 0.6, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction (new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR), "");
		MembershipFunction expResult = new TriangleFunction (new ParamTriangle (0.1, 0.3, 0.5, Scale.LINEAR), "");
                FuzzyLogicFacade facade = new FuzzyLogicFacade();
		MembershipFunction result = facade.subtract(a, b);
		assertEquals(((TriangleFunction) expResult).getParameters().getA(), ((TriangleFunction) result).getParameters().getA(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getB(), ((TriangleFunction) result).getParameters().getB(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getC(), ((TriangleFunction) result).getParameters().getC(), 0.01);
        }
        
	/**
	 * Test of multiply method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testMultiply() throws Exception {
		System.out.println("multiply");
		MembershipFunction a = new TriangleFunction (new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction (new ParamTriangle (0.2, 0.3, 0.4, Scale.LINEAR), "");
		MembershipFunction expResult = new TriangleFunction (new ParamTriangle (0.02, 0.06, 0.12, Scale.LINEAR), "");
                FuzzyLogicFacade facade = new FuzzyLogicFacade();
		MembershipFunction result = facade.multiply(a, b);
		assertEquals(((TriangleFunction) expResult).getParameters().getA(), ((TriangleFunction) result).getParameters().getA(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getB(), ((TriangleFunction) result).getParameters().getB(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getC(), ((TriangleFunction) result).getParameters().getC(), 0.01);
	}

	/**
	 * Test of divide method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testDivide() throws Exception {
		System.out.println("divide");
		MembershipFunction a = new TriangleFunction (new ParamTriangle (0.2, 0.3, 0.4, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction (new ParamTriangle (0.4, 0.5, 0.6, Scale.LINEAR), "");
		MembershipFunction expResult = new TriangleFunction (new ParamTriangle (0.33, 0.6, 1, Scale.LINEAR), "");
                FuzzyLogicFacade facade = new FuzzyLogicFacade();
		MembershipFunction result = facade.divide(a, b);
		assertEquals(((TriangleFunction) expResult).getParameters().getA(), ((TriangleFunction) result).getParameters().getA(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getB(), ((TriangleFunction) result).getParameters().getB(), 0.01);
		assertEquals(((TriangleFunction) expResult).getParameters().getC(), ((TriangleFunction) result).getParameters().getC(), 0.01);
	}

	/**
	 * Test of getValue method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testGetValue() throws Exception {
		System.out.println("getValue");
		MembershipFunction function = new TriangleFunction (new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR), "");
		double argument = 0.2;
		double expResult = 1.0;
                FuzzyLogicFacade facade = new FuzzyLogicFacade();
		double result = facade.getValue(function, argument);
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getJacardSimilarity method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testGetJacardSimilarity() throws Exception {
		System.out.println("getJacardSimilarity");
		MembershipFunction a = new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction(new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR), "");
		double expResult = 0.14;
                FuzzyLogicFacade facade = new FuzzyLogicFacade();
		double result = facade.getJacardSimilarity(a, b);
		assertEquals(expResult, result, 0.01);
	}
        
        /**
	 * Test of getInclusionDegreeMeasure method, of class FuzzyLogicFacade.
	 */
	@Test
	public void getInclusionDegreeMeasure() throws Exception {
		System.out.println("getInclusionDegreeMeasure");
		MembershipFunction a = new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR), "");
		MembershipFunction b = new TriangleFunction(new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR), "");
		double expResult = 0.25;
                FuzzyLogicFacade facade = new FuzzyLogicFacade();
		double result = facade.getInclusionDegreeMeasure(a, b);
		assertEquals(expResult, result, 0.01);
	}
}