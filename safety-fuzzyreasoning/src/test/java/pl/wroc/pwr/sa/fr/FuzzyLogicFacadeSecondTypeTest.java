package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MJ
 */
public class FuzzyLogicFacadeSecondTypeTest {
	
	public FuzzyLogicFacadeSecondTypeTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}
        
        /**
	 * Test of createTriangle method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTriangle() throws Exception {
		System.out.println("createTriangle");
		ParamTriangle parameters = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.SECOND, 0.04, 0.5);
		String name = "";
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TriangleFunction expResult = new TriangleFunction(parameters, name);
		TriangleFunction result = instance.createTriangle(parameters, name);
		assertEquals(expResult.getParameters().getScale(), result.getParameters().getScale());
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters(), result.getParameters());
		
		try {
			parameters = new ParamTriangle(0.1, 0.2, 2, Scale.LINEAR, Type.SECOND, 0.04, 0.5);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTriangle(-1, 0.2, 0.3, Scale.LINEAR, Type.SECOND, 0.04, 0.5);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTriangle(0.1, 0.3, 0.2, Scale.LINEAR, Type.SECOND, 0.04, 0.5);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("values in bad order", e.getMessage());
		}
                
                
                try {
			parameters = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.FIRST, 0.5, 0);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Fuzzy set of first type cannot have length value != 0 ", e.getMessage());
		}
                
                try {
			parameters = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.SECOND, 0.5, 0.5);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Value length exceedes the range", e.getMessage());
		}
                
                try {
			parameters = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.FIRST, 0, 0.5);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Fuzzy set of first type cannot have prob value != 0 ", e.getMessage());
		}
                
                try {
			parameters = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.SECOND, 0.04, 1.0);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Value prob exceedes the range", e.getMessage());
		}
		
		parameters = new ParamTriangle(-12, -11, -10, Scale.LOGARITHMIC, Type.SECOND, 0.04, 0.5);
		expResult = new TriangleFunction(parameters, name);
		result = instance.createTriangle(parameters, name);
		assertEquals(expResult.getParameters().getScale(), result.getParameters().getScale());
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters(), result.getParameters());
		
		try {
			parameters = new ParamTriangle(-12, -11, 1, Scale.LOGARITHMIC, Type.SECOND, 0.04, 0.5);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTriangle(-13, -11, -10, Scale.LOGARITHMIC, Type.SECOND, 0.04, 0.5);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTriangle(-12, -10, -11, Scale.LOGARITHMIC, Type.SECOND, 0.04, 0.5);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("values in bad order", e.getMessage());
		}
                
                try {
			parameters = new ParamTriangle(-12, -10, -9, Scale.LOGARITHMIC, Type.FIRST, 0.5, 0);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Fuzzy set of first type cannot have length value != 0 ", e.getMessage());
		}
                
                try {
			parameters = new ParamTriangle(-12, -11, -10, Scale.LOGARITHMIC, Type.SECOND, 1.0, 0.5);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Value length exceedes the range", e.getMessage());
		}
                
                try {
			parameters = new ParamTriangle(-12, -10, -9, Scale.LOGARITHMIC, Type.FIRST, 0, 0.5);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Fuzzy set of first type cannot have prob value != 0 ", e.getMessage());
		}
                
                try {
			parameters = new ParamTriangle(-12, -11, -10, Scale.LOGARITHMIC, Type.SECOND, 0.04, 1.1);
			result = instance.createTriangle(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Value prob exceedes the range", e.getMessage());
		}
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}
        
        /**
	 * Test of createTrapezoid method, of class FuzzyLogicFacade.
	 */
	@Test
	public void testCreateTrapezoid() throws Exception {
		System.out.println("createTrapezoid");
		ParamTrapezoid parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 0.5);
		String name = "";
		FuzzyLogicFacade instance = new FuzzyLogicFacade();
		TrapezoidFunction expResult = new TrapezoidFunction(parameters, name);
		TrapezoidFunction result = instance.createTrapezoid(parameters, name);
		assertEquals(expResult.getParameters().getScale(), result.getParameters().getScale());
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters(), result.getParameters());
		
		try {
			parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 2, Scale.LINEAR, Type.SECOND, 0.04, 0.5);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTrapezoid(-1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 0.5);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTrapezoid(0.1, 0.2, 0.4, 0.3, Scale.LINEAR, Type.SECOND, 0.04, 0.5);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("values in bad order", e.getMessage());
		}
                
                try {
			parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.FIRST, 0.5, 0);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Fuzzy set of first type cannot have length value != 0 ", e.getMessage());
		}
                
                try {
			parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.5, 0.5);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Value length exceedes the range", e.getMessage());
		}
                
                try {
			parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.FIRST, 0, 0.5);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Fuzzy set of first type cannot have prob value != 0 ", e.getMessage());
		}
                
                try {
			parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 1.0);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Value prob exceedes the range", e.getMessage());
		}
		
		parameters = new ParamTrapezoid(-12, -11, -10, -9, Scale.LOGARITHMIC, Type.SECOND, 0.04, 0.5);
		expResult = new TrapezoidFunction(parameters, name);
		result = instance.createTrapezoid(parameters, name);
		assertEquals(expResult.getParameters().getScale(), result.getParameters().getScale());
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters(), result.getParameters());
		
		try {
			parameters = new ParamTrapezoid(-12, -11, -10, 1, Scale.LOGARITHMIC, Type.SECOND, 0.04, 0.5);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTrapezoid(-13, -11, -10, -9, Scale.LOGARITHMIC, Type.SECOND, 0.04, 0.5);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("value exceedes the range", e.getMessage());
		}
		
		try {
			parameters = new ParamTrapezoid(-12, -11, -9, -10, Scale.LOGARITHMIC, Type.SECOND, 0.04, 0.5);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("values in bad order", e.getMessage());
		}
                
                try {
			parameters = new ParamTrapezoid(-12, -11, -10, -8, Scale.LOGARITHMIC, Type.FIRST, 0.5, 0);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Fuzzy set of first type cannot have length value != 0 ", e.getMessage());
		}
                
                try {
			parameters = new ParamTrapezoid(-12, -11, -10, -8, Scale.LOGARITHMIC, Type.SECOND, 0.8, 0.5);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Value length exceedes the range", e.getMessage());
		}
                
                try {
			parameters = new ParamTrapezoid(-12, -11, -10, -8, Scale.LOGARITHMIC, Type.FIRST, 0, 0.5);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Fuzzy set of first type cannot have prob value != 0 ", e.getMessage());
		}
                
                try {
			parameters = new ParamTrapezoid(-12, -11, -10, -8, Scale.LOGARITHMIC, Type.SECOND, 0.4, 2.0);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Value prob exceedes the range", e.getMessage());
		}
                
                try {
			parameters = new ParamTrapezoid(-12, -11, -10, -8, Scale.LOGARITHMIC, Type.SECOND, 0.4, -1.0);
			result = instance.createTrapezoid(parameters, name);
			fail();
		}
		catch (Exception e) {
			assertEquals("Value prob exceedes the range", e.getMessage());
		}
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}
}
