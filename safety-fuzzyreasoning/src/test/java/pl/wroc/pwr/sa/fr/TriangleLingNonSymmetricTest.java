package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maciej
 */
public class TriangleLingNonSymmetricTest {
	
	public TriangleLingNonSymmetricTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of getName method, of class TriangleLingNonSymmetric.
	 */
	@Test
	public void testGetName() throws Exception {
		System.out.println("getName");
		TriangleLingNonSymmetric instance = new TriangleLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTriangle> (Arrays.asList(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR), new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR))));
		String expResult = "Zmienna";
		String result = instance.getName();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of getFunctionsNames method, of class TriangleLingNonSymmetric.
	 */
	@Test
	public void testGetFunctionsNames() throws Exception {
		System.out.println("getFunctionsNames");
		TriangleLingNonSymmetric instance = new TriangleLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTriangle> (Arrays.asList(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR), new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR))));
		List expResult = new ArrayList<String> (Arrays.asList ("Pierwsza", "Druga"));
		List result = instance.getFunctionsNames();
		assertTrue(result.equals(expResult));
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of getFunction method, of class TriangleLingNonSymmetric.
	 */
	@Test
	public void testGetFunction() throws Exception {
		System.out.println("getFunction");
		String name = "Pierwsza";
		TriangleLingNonSymmetric instance = new TriangleLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTriangle> (Arrays.asList(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR), new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR))));
		TriangleFunction expResult = new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR), "Pierwsza");
		TriangleFunction result = instance.getFunction(name);
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters().getA(), result.getParameters().getA(), 0.01);
		assertEquals(expResult.getParameters().getB(), result.getParameters().getB(), 0.01);
		assertEquals(expResult.getParameters().getC(), result.getParameters().getC(), 0.01);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of equals method, of class TriangleLingNonSymmetric.
	 */
	@Test
	public void testEquals() throws Exception {
		System.out.println("equals");
		Object obj = new TriangleLingNonSymmetric(Scale.LINEAR, new ArrayList<String> (Arrays.asList("Pierwsza", "Druga", "Trzecia")), new ArrayList<Double> (Arrays.asList(0.2, 0.8)), "");
		TriangleLingNonSymmetric instance = new TriangleLingNonSymmetric(Scale.LINEAR, new ArrayList<String> (Arrays.asList("Pierwsza", "Druga", "Trzecia")), new ArrayList<Double> (Arrays.asList(0.2, 0.8)), "");
		boolean expResult = true;
		boolean result = instance.equals(obj);
		assertEquals(expResult, result);
	}
}