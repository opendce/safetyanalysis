package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MJ
 */
public class FuzzyLogicObjectsSecondTypeTest {
	
	public FuzzyLogicObjectsSecondTypeTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of addTriangleFunction method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testAddTriangleFunction() throws Exception {
		System.out.println("addTriangleFunction");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TriangleFunction fun = new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.SECOND, 0.04, 0.5), "");
		instance.addTriangleFunction(new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.SECOND, 0.04, 0.5), ""));
		List<TriangleFunction> list = instance.getTriangleFunctions();
		
		int expResult_1 = 1;
		int result_1 = list.size();
		assertEquals(expResult_1, result_1);
		
		boolean expResult_2 = true;
		boolean result_2 = list.contains(fun);
		assertEquals(expResult_2, result_2);
		
		instance.addTriangleFunction(new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.SECOND, 0.04, 0.5), ""));
		list = instance.getTriangleFunctions();
		
		expResult_1 = 1;
		result_1 = list.size();
		assertEquals(expResult_1, result_1);
	}

	/**
	 * Test of addTrapezoidFunction method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testAddTrapezoidFunction() throws Exception {
		System.out.println("addTrapezoidFunction");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TrapezoidFunction fun = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 0.5), "");
		instance.addTrapezoidFunction(new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 0.5), ""));
		List<TrapezoidFunction> list = instance.getTrapezoidFunctions();
		
		int expResult_1 = 1;
		int result_1 = list.size();
		assertEquals(expResult_1, result_1);
		
		boolean expResult_2 = true;
		boolean result_2 = list.contains(fun);
		assertEquals(expResult_2, result_2);
		
		instance.addTrapezoidFunction(new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 0.5), ""));
		list = instance.getTrapezoidFunctions();
		
		expResult_1 = 1;
		result_1 = list.size();
		assertEquals(expResult_1, result_1);
	}

	/**
	 * Test of getTriangleFunctions method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testGetTriangleFunctions() throws Exception {
		System.out.println("getTriangleFunctions");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TriangleFunction fun1 = new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.SECOND, 0.04, 0.5), "");
		TriangleFunction fun2 = new TriangleFunction(new ParamTriangle(0.4, 0.5, 0.6, Scale.LINEAR, Type.SECOND, 0.04, 0.5), "");
		instance.addTriangleFunction(fun1);
		instance.addTriangleFunction(fun2);
		List<TriangleFunction> result = instance.getTriangleFunctions();
		List<TriangleFunction> expResult = new ArrayList<TriangleFunction> (Arrays.asList (fun1, fun2));
		assertEquals(expResult, result);
	}

	/**
	 * Test of getTrapezoidFunctions method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testGetTrapezoidFunctions() throws Exception {
		System.out.println("getTrapezoidFunctions");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TrapezoidFunction fun1 = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 0.5), "");
		TrapezoidFunction fun2 = new TrapezoidFunction(new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR, Type.SECOND, 0.04, 0.5), "");
		instance.addTrapezoidFunction(fun1);
		instance.addTrapezoidFunction(fun2);
		List<TrapezoidFunction> result = instance.getTrapezoidFunctions();
		List<TrapezoidFunction> expResult = new ArrayList<TrapezoidFunction> (Arrays.asList (fun1, fun2));
		assertEquals(expResult, result);
	}

}