package pl.wroc.pwr.sa.fr;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maciej
 */
public class ParamTrapezoidTest {
	
	public ParamTrapezoidTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of getA method, of class ParamTrapezoid.
	 */
	@Test
	public void testGetA() throws Exception {
		System.out.println("getA");
		ParamTrapezoid instance = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		double expResult = 0.1;
		double result = instance.getA();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getB method, of class ParamTrapezoid.
	 */
	@Test
	public void testGetB() throws Exception {
		System.out.println("getB");
		ParamTrapezoid instance = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		double expResult = 0.2;
		double result = instance.getB();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getC method, of class ParamTrapezoid.
	 */
	@Test
	public void testGetC() throws Exception {
		System.out.println("getC");
		ParamTrapezoid instance = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		double expResult = 0.3;
		double result = instance.getC();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getD method, of class ParamTrapezoid.
	 */
	@Test
	public void testGetD() throws Exception {
		System.out.println("getD");
		ParamTrapezoid instance = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		double expResult = 0.4;
		double result = instance.getD();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getScale method, of class ParamTrapezoid.
	 */
	@Test
	public void testGetScale() throws Exception {
		System.out.println("getScale");
		ParamTrapezoid instance = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		Scale expResult = Scale.LINEAR;
		Scale result = instance.getScale();
		assertEquals(expResult, result);
	}

	/**
	 * Test of add method, of class ParamTrapezoid.
	 */
	@Test
	public void testAdd() throws Exception {
		System.out.println("add");
		Param y = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		ParamTrapezoid instance = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		ParamTrapezoid expResult = new ParamTrapezoid (0.2, 0.4, 0.6, 0.8, Scale.LINEAR);
		ParamTrapezoid result = instance.add(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		assertEquals(expResult.getD(), result.getD(), 0.01);
		
		y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
		instance = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
		expResult = new ParamTrapezoid (1, 1, 1, 1, Scale.LINEAR);
		result = instance.add(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		assertEquals(expResult.getD(), result.getD(), 0.01);
		
		y = new ParamTrapezoid (-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid (-1, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		expResult = new ParamTrapezoid (-0.22, -0.1, 0, 0, Scale.LOGARITHMIC);
		result = instance.add(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		assertEquals(expResult.getD(), result.getD(), 0.01);
		
		y = new ParamTrapezoid (-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid (-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		expResult = new ParamTrapezoid (0, 0, 0, 0, Scale.LOGARITHMIC);
		result = instance.add(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		assertEquals(expResult.getD(), result.getD(), 0.01);
		
		try {
			y = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			instance = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			result = instance.add(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		try {
			y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTrapezoid (-12, -11, -10, -9, Scale.LOGARITHMIC);
			result = instance.add(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
	}

	/**
	 * Test of subtract method, of class ParamTrapezoid.
	 */
	@Test
	public void testSubtract() throws Exception {
		System.out.println("subtract");
		Param y = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		ParamTrapezoid instance = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
		ParamTrapezoid expResult = new ParamTrapezoid (0.1, 0.3, 0.5, 0.7, Scale.LINEAR);
		ParamTrapezoid result = instance.subtract(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		assertEquals(expResult.getD(), result.getD(), 0.01);
		
		y = new ParamTrapezoid (-1, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid (-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		expResult = new ParamTrapezoid (-1, -0.52, -0.3, -0.15, Scale.LOGARITHMIC);
		result = instance.subtract(y);
		assertEquals(expResult.getA(), result.getA(), 0.02);
		assertEquals(expResult.getB(), result.getB(), 0.02);
		assertEquals(expResult.getC(), result.getC(), 0.02);
		assertEquals(expResult.getD(), result.getD(), 0.02);
		
		
		try {
			y = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			instance = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			result = instance.subtract(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		try {
			y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTrapezoid (-12, -11, -10, -9, Scale.LOGARITHMIC);
			result = instance.subtract(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
	}

	/**
	 * Test of multiply method, of class ParamTrapezoid.
	 */
	@Test
	public void testMultiply() throws Exception {
		System.out.println("multiply");
		Param y = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		ParamTrapezoid instance = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
		ParamTrapezoid expResult = new ParamTrapezoid (0.05, 0.12, 0.21, 0.32, Scale.LINEAR);
		ParamTrapezoid result = instance.multiply(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		assertEquals(expResult.getD(), result.getD(), 0.01);
		
		y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
		instance = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		expResult = new ParamTrapezoid (0.05, 0.12, 0.21, 0.32, Scale.LINEAR);
		result = instance.multiply(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		assertEquals(expResult.getD(), result.getD(), 0.01);
		
		y = new ParamTrapezoid (-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid (-1, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		expResult = new ParamTrapezoid (-1.3, -0.92, -0.68, -0.51, Scale.LOGARITHMIC);
		result = instance.multiply(y);
		assertEquals(expResult.getA(), result.getA(), 0.02);
		assertEquals(expResult.getB(), result.getB(), 0.02);
		assertEquals(expResult.getC(), result.getC(), 0.02);
		assertEquals(expResult.getD(), result.getD(), 0.02);
		
		y = new ParamTrapezoid (-1, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid (-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		expResult = new ParamTrapezoid (-1.3, -0.92, -0.68, -0.51, Scale.LOGARITHMIC);
		result = instance.multiply(y);
		assertEquals(expResult.getA(), result.getA(), 0.02);
		assertEquals(expResult.getB(), result.getB(), 0.02);
		assertEquals(expResult.getC(), result.getC(), 0.02);
		assertEquals(expResult.getD(), result.getD(), 0.02);
		
		try {
			y = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			instance = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			result = instance.multiply(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		try {
			y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTrapezoid (-12, -11, -10, -9, Scale.LOGARITHMIC);
			result = instance.multiply(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
	}

	/**
	 * Test of divide method, of class ParamTrapezoid.
	 */
	@Test
	public void testDivide() throws Exception {
		System.out.println("divide");
		Param y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
		ParamTrapezoid instance = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		ParamTrapezoid expResult = new ParamTrapezoid (0.13, 0.29, 0.5, 0.8, Scale.LINEAR);
		ParamTrapezoid result = instance.divide(y);
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		assertEquals(expResult.getD(), result.getD(), 0.01);
		
		y = new ParamTrapezoid (-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid (-1, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		expResult = new ParamTrapezoid (-0.89, -0.54, -0.3, -0.1, Scale.LOGARITHMIC);
		result = instance.divide(y);
		assertEquals(expResult.getA(), result.getA(), 0.02);
		assertEquals(expResult.getB(), result.getB(), 0.02);
		assertEquals(expResult.getC(), result.getC(), 0.02);
		assertEquals(expResult.getD(), result.getD(), 0.02);
		
		
		try {
			y = new ParamTriangle (0.5, 0.6, 0.7, Scale.LINEAR);
			instance = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			result = instance.divide(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		try {
			y = new ParamTrapezoid (0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTrapezoid (-12, -11, -10, -9, Scale.LOGARITHMIC);
			result = instance.divide(y);
			fail();
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
	}

	/**
	 * Test of getValue method, of class ParamTrapezoid.
	 */
	@Test
	public void testGetValue() throws Exception {
		System.out.println("getValue");
		double argument = 0;
		ParamTrapezoid instance = new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		double expResult = 0;
		double result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.15;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.25;
		expResult = 1;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.35;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.5;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (0.2, 0.2, 0.3, 0.4, Scale.LINEAR);
		argument = 0.15;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.25;
		expResult = 1;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.35;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.5;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (0.2, 0.3, 0.3, 0.4, Scale.LINEAR);
		argument = 0.15;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.25;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.35;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.5;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (0.2, 0.3, 0.4, 0.4, Scale.LINEAR);
		argument = 0.15;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.25;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.35;
		expResult = 1;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.5;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (0.2, 0.2, 0.2, 0.3, Scale.LINEAR);
		argument = 0.15;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.25;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.35;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (0.2, 0.3, 0.3, 0.3, Scale.LINEAR);
		argument = 0.15;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.25;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.35;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (0.3, 0.3, 0.3, 0.3, Scale.LINEAR);
		argument = 0.2;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.3;
		expResult = 1;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = 0.4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (-8, -7, -6, -5, Scale.LOGARITHMIC);
		argument = -9;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -7.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -6.5;
		expResult = 1;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -5.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (-7, -7, -6, -5, Scale.LOGARITHMIC);
		argument = -8;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -6.5;
		expResult = 1;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -5.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (-7, -6, -6, -5, Scale.LOGARITHMIC);
		argument = -8;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -6.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -5.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (-7, -6, -5, -5, Scale.LOGARITHMIC);
		argument = -8;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -6.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -5.5;
		expResult = 1;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -4;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (-7, -7, -7, -6, Scale.LOGARITHMIC);
		argument = -8;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -6.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -5;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (-7, -6, -6, -6, Scale.LOGARITHMIC);
		argument = -8;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -6.5;
		expResult = 0.5;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument =-5;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		instance = new ParamTrapezoid (-6, -6, -6, -6, Scale.LOGARITHMIC);
		argument = -7;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -6;
		expResult = 1;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
		argument = -5;
		expResult = 0;
		result = instance.getValue(argument);
		assertEquals(expResult, result, 0.01);
		
	}

	/**
	 * Test of getJacardSimilarity method, of class ParamTrapezoid.
	 */
	@Test
	public void testGetJacardSimilarity() throws Exception {
		System.out.println("getJacardSimilarity");
		Param y = new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
		ParamTrapezoid instance = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		double expResult = 0.0;
		double result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, Scale.LINEAR);
		instance = new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR);
		expResult = 0.33;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, Scale.LINEAR);
		instance = new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, Scale.LINEAR);
		expResult = 1.0;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR);
		instance = new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, Scale.LINEAR);
		expResult = 0.33;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.01);
				
		y = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		instance = new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
		expResult = 0.0;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		try {
			y = new ParamTrapezoid(-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
			instance = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
			instance.getJacardSimilarity(y);
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
		
		try {
			y = new ParamTriangle(0.4, 0.5, 0.6, Scale.LINEAR);
			instance = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
			instance.getJacardSimilarity(y);
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		//
		
		y = new ParamTrapezoid(-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid(-1.0, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		expResult = 0.0;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTrapezoid(-0.52, -0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid(-0.7, -0.52, -0.4, -0.3, Scale.LOGARITHMIC);
		expResult = 0.31;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTrapezoid(-0.52, -0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid(-0.52, -0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		expResult = 1.0;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTrapezoid(-0.7, -0.52, -0.4, -0.3, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid(-0.52, -0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		expResult = 0.31;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.01);
				
		y = new ParamTrapezoid(-1.0, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid(-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		expResult = 0.0;
		result = instance.getJacardSimilarity(y);
		assertEquals(expResult, result, 0.0);
		
		try {
			y = new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTrapezoid(-1.0, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
			instance.getJacardSimilarity(y);
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
		
		try {
			y = new ParamTriangle(-0.4, -0.3, -0.22, Scale.LOGARITHMIC);
			instance = new ParamTrapezoid(-1.0, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
			instance.getJacardSimilarity(y);
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
                
                try {
			y = new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTrapezoid(-1.0, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
			instance.getJacardSimilarity(y);
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
		
		try {
			y = new ParamTriangle(-0.4, -0.3, -0.22, Scale.LOGARITHMIC);
			instance = new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance.getJacardSimilarity(y);
		}
		catch (Exception e) {
			assertEquals("types are different and scales are different", e.getMessage());
		}
                
                
	}
        
        /**
	 * Test of getInclusionDegreeMeasure method, of class ParamTrapezoid.
	 */
	@Test
	public void testGetInclusionDegreeMeasure() throws Exception {
		System.out.println("getInclusionDegreeMeasure");
		Param y = new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
		ParamTrapezoid instance = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		double expResult = 0.0;
		double result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, Scale.LINEAR);
		instance = new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR);
		expResult = 0.5;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, Scale.LINEAR);
		instance = new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, Scale.LINEAR);
		expResult = 1.0;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR);
		instance = new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, Scale.LINEAR);
		expResult = 0.5;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.01);
				
		y = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		instance = new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
		expResult = 0.0;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		try {
			y = new ParamTrapezoid(-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
			instance = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
			instance.getInclusionDegreeMeasure(y);
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
		
		try {
			y = new ParamTriangle(0.4, 0.5, 0.6, Scale.LINEAR);
			instance = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
			instance.getInclusionDegreeMeasure(y);
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
		
		
		y = new ParamTrapezoid(-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid(-1.0, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		expResult = 0.0;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTrapezoid(-0.52, -0.4, -0.34, -0.22, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid(-0.7, -0.52, -0.4, -0.3, Scale.LOGARITHMIC);
		expResult = 0.42;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.01);
		
		y = new ParamTrapezoid(-0.52, -0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid(-0.52, -0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		expResult = 1.0;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		y = new ParamTrapezoid(-0.7, -0.52, -0.4, -0.3, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid(-0.52, -0.4, -0.3, -0.22, Scale.LOGARITHMIC);
		expResult = 0.55;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.01);
				
		y = new ParamTrapezoid(-1.0, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
		instance = new ParamTrapezoid(-0.3, -0.22, -0.15, -0.1, Scale.LOGARITHMIC);
		expResult = 0.0;
		result = instance.getInclusionDegreeMeasure(y);
		assertEquals(expResult, result, 0.0);
		
		try {
			y = new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR);
			instance = new ParamTrapezoid(-1.0, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
			instance.getInclusionDegreeMeasure(y);
		}
		catch (Exception e) {
			assertEquals("scales are different", e.getMessage());
		}
		
		try {
			y = new ParamTriangle(-0.4, -0.3, -0.22, Scale.LOGARITHMIC);
			instance = new ParamTrapezoid(-1.0, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
			instance.getInclusionDegreeMeasure(y);
		}
		catch (Exception e) {
			assertEquals("types are different", e.getMessage());
		}
                
                try {
			y = new ParamTriangle(0.5, 0.6, 0.7, Scale.LINEAR);
			instance = new ParamTrapezoid(-1.0, -0.7, -0.52, -0.4, Scale.LOGARITHMIC);
			instance.getInclusionDegreeMeasure(y);
		}
		catch (Exception e) {
			assertEquals("types are different and scales are different", e.getMessage());
		}
	}
}