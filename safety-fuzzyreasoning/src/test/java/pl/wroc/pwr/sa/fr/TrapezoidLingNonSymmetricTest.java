package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maciej
 */
public class TrapezoidLingNonSymmetricTest {
	
	public TrapezoidLingNonSymmetricTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of getName method, of class TrapezoidLingNonSymmetric.
	 */
	@Test
	public void testGetName() throws Exception {
		System.out.println("getName");
		TrapezoidLingNonSymmetric instance = new TrapezoidLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTrapezoid> (Arrays.asList(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR))));
		String expResult = "Zmienna";
		String result = instance.getName();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of getFunctionsNames method, of class TrapezoidLingNonSymmetric.
	 */
	@Test
	public void testGetFunctionsNames() throws Exception {
		System.out.println("getFunctionsNames");
		TrapezoidLingNonSymmetric instance = new TrapezoidLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTrapezoid> (Arrays.asList(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR))));
		List expResult = new ArrayList<String> (Arrays.asList ("Pierwsza", "Druga"));
		List result = instance.getFunctionsNames();
		assertTrue(result.equals(expResult));
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of getFunction method, of class TrapezoidLingNonSymmetric.
	 */
	@Test
	public void testGetFunction() throws Exception {
		System.out.println("getFunction");
		String name = "Pierwsza";
		TrapezoidLingNonSymmetric instance = new TrapezoidLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTrapezoid> (Arrays.asList(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR))));
		TrapezoidFunction expResult = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), "Pierwsza");
		TrapezoidFunction result = instance.getFunction(name);
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters().getA(), result.getParameters().getA(), 0.01);
		assertEquals(expResult.getParameters().getB(), result.getParameters().getB(), 0.01);
		assertEquals(expResult.getParameters().getC(), result.getParameters().getC(), 0.01);
		assertEquals(expResult.getParameters().getD(), result.getParameters().getD(), 0.01);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of equals method, of class TrapezoidLingNonSymmetric.
	 */
	@Test
	public void testEquals() throws Exception {
		System.out.println("equals");
		Object obj = new TrapezoidLingNonSymmetric("", 0.5, Scale.LINEAR, new ArrayList<String> (Arrays.asList("Pierwsza", "Druga", "Trzecia")), new ArrayList<Double> (Arrays.asList(0.2, 0.8)));
		TrapezoidLingNonSymmetric instance = new TrapezoidLingNonSymmetric("", 0.5, Scale.LINEAR, new ArrayList<String> (Arrays.asList("Pierwsza", "Druga", "Trzecia")), new ArrayList<Double> (Arrays.asList(0.2, 0.8)));
		boolean expResult = true;
		boolean result = instance.equals(obj);
		assertEquals(expResult, result);
	}
}