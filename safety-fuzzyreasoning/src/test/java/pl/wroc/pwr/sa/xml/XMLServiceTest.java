/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.sa.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.wroc.pwr.sa.fr.ParamTrapezoid;
import pl.wroc.pwr.sa.fr.ParamTriangle;
import pl.wroc.pwr.sa.fr.Scale;
import pl.wroc.pwr.sa.fr.Type;
import pl.wroc.pwr.sa.fr.TrapezoidFunction;
import pl.wroc.pwr.sa.fr.TrapezoidLingNonSymmetric;
import pl.wroc.pwr.sa.fr.TrapezoidLingSymmetric;
import pl.wroc.pwr.sa.fr.TriangleFunction;
import pl.wroc.pwr.sa.fr.TriangleLingNonSymmetric;
import pl.wroc.pwr.sa.fr.TriangleLingSymmetric;

/**
 *
 * @author Michal
 */
public class XMLServiceTest {
	
	public XMLServiceTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of readObjectFromXMLString method, of class XMLService for ParamTrapezoid
	 */
	@Test
	public void testReadXMLStringToObject() throws Exception {
                System.out.println("readXMLStringToObject");
                String xmlString =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<paramTrapezoid>"
                        + "\n    <scale>LOGARITHMIC</scale>"
                        + "\n    <type>FIRST</type>"
                        + "\n    <a>-8.0</a>"
                        + "\n    <b>-7.0</b>"
                        + "\n    <c>-6.0</c>"
                        + "\n    <d>-5.0</d>"
                        + "\n    <length>0.0</length>"
                        + "\n    <prob>0.0</prob>"
                        + "\n</paramTrapezoid>"
                        + "\n";
                ParamTrapezoid expResult = new ParamTrapezoid(-8, -7, -6, -5, Scale.LOGARITHMIC, Type.FIRST, 0.0, 0.0);
                ParamTrapezoid result = XMLService.readXMLStringToObject(ParamTrapezoid.class, xmlString);
                assertEquals(expResult, result);
	}

	/**
	 * Test of readObjectFromXMLString method, of class XMLService for TriangleFunction
	 */
	@Test
	public void testReadXMLStringToObject2() throws Exception {
                System.out.println("readXMLStringToObject");
                String xmlString =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<triangleFunction>"
                        + "\n    <name>somename</name>"
                        + "\n    <parameters>"
                        + "\n        <scale>LINEAR</scale>"
                        + "\n        <type>FIRST</type>"
                        + "\n        <a>0.1</a>"
                        + "\n        <b>0.2</b>"
                        + "\n        <c>0.3</c>"
                        + "\n        <length>0.0</length>"
                        + "\n        <prob>0.0</prob>"
                        + "\n    </parameters>"
                        + "\n</triangleFunction>"
                        + "\n";
                ParamTriangle parameters = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.FIRST, 0.0, 0.0);
		        TriangleFunction expResult = new TriangleFunction(parameters, "somename");
                TriangleFunction result = XMLService.readXMLStringToObject(TriangleFunction.class, xmlString);
                assertEquals(expResult, result);
	}

	/**
	 * Test of readObjectFromXMLString method, of class XMLService for TrapezoidLingNonSymmetric
	 */
	@Test
	public void testReadXMLStringToObject3() throws Exception {
                System.out.println("readXMLStringToObject");
                String xmlString =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<trapezoidLingNonSymmetric>"
                        + "\n    <functions>"
                        + "\n        <name>first</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>SECOND</type>"
                        + "\n            <a>0.1</a>"
                        + "\n            <b>0.2</b>"
                        + "\n            <c>0.3</c>"
                        + "\n            <d>0.5</d>"
                        + "\n            <length>0.04</length>"
                        + "\n            <prob>0.05</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <functions>"
                        + "\n        <name>second</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>FIRST</type>"
                        + "\n            <a>0.3</a>"
                        + "\n            <b>0.4</b>"
                        + "\n            <c>0.5</c>"
                        + "\n            <d>0.7</d>"
                        + "\n            <length>0.0</length>"
                        + "\n            <prob>0.0</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <name>somename</name>"
                        + "\n</trapezoidLingNonSymmetric>"
                        + "\n";
                ParamTrapezoid parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 0.5, Scale.LINEAR, Type.SECOND, 0.04, 0.05);
                ParamTrapezoid parameters2 = new ParamTrapezoid(0.3, 0.4, 0.5, 0.7, Scale.LINEAR, Type.FIRST, 0.0, 0.0);
                List<String> list1 = new ArrayList<String>();
                list1.add("first");
                list1.add("second");
                List<ParamTrapezoid> list2 = new ArrayList<ParamTrapezoid>();
                list2.add(parameters);
                list2.add(parameters2);
		        TrapezoidLingNonSymmetric expResult = new TrapezoidLingNonSymmetric("somename", list1, list2);
                TrapezoidLingNonSymmetric result = XMLService.readXMLStringToObject(TrapezoidLingNonSymmetric.class, xmlString);
                assertEquals(expResult, result);
	}

	/**
	 * Test of saveObjectToXMLString method, of class XMLService on ParamTriangle
	 */
	@Test
	public void testSaveObjectToXMLString() throws Exception {
                System.out.println("saveObjectToXMLString");
                ParamTriangle paramTriangle = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.FIRST, 0.0, 0.0);
                String expResult =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<paramTriangle>"
                        + "\n    <scale>LINEAR</scale>"
                        + "\n    <type>FIRST</type>"
                        + "\n    <a>0.1</a>"
                        + "\n    <b>0.2</b>"
                        + "\n    <c>0.3</c>"
                        + "\n    <length>0.0</length>"
                        + "\n    <prob>0.0</prob>"
                        + "\n</paramTriangle>"
                        + "\n";
                String result = XMLService.saveObjectToXMLString(paramTriangle);
                System.out.println(result);
                assertEquals(expResult, result);
	}

	/**
	 * Test of saveObjectToXMLString method, of class XMLService on ParamTrapezoid
	 */
	@Test
	public void testSaveObjectToXMLString2() throws Exception {
                System.out.println("saveObjectToXMLString");
                ParamTrapezoid paramTrapezoid = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 0.05);
                String expResult =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<paramTrapezoid>"
                        + "\n    <scale>LINEAR</scale>"
                        + "\n    <type>SECOND</type>"
                        + "\n    <a>0.1</a>"
                        + "\n    <b>0.2</b>"
                        + "\n    <c>0.3</c>"
                        + "\n    <d>0.4</d>"
                        + "\n    <length>0.04</length>"
                        + "\n    <prob>0.05</prob>"
                        + "\n</paramTrapezoid>"
                        + "\n";
                String result = XMLService.saveObjectToXMLString(paramTrapezoid);
                System.out.println(result);
                assertEquals(expResult, result);
	}

	/**
	 * Test of saveObjectToXMLString method, of class XMLService on TriangleFunction
	 */
	@Test
	public void testSaveObjectToXMLString3() throws Exception {
                System.out.println("saveObjectToXMLString");
                ParamTriangle parameters = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.FIRST, 0.0, 0.0);
		        TriangleFunction triangleFunction = new TriangleFunction(parameters, "somename");
                String expResult =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<triangleFunction>"
                        + "\n    <name>somename</name>"
                        + "\n    <parameters>"
                        + "\n        <scale>LINEAR</scale>"
                        + "\n        <type>FIRST</type>"
                        + "\n        <a>0.1</a>"
                        + "\n        <b>0.2</b>"
                        + "\n        <c>0.3</c>"
                        + "\n        <length>0.0</length>"
                        + "\n        <prob>0.0</prob>"
                        + "\n    </parameters>"
                        + "\n</triangleFunction>"
                        + "\n";
                String result = XMLService.saveObjectToXMLString(triangleFunction);
                System.out.println(result);
                assertEquals(expResult, result);
	}

	/**
	 * Test of saveObjectToXMLString method, of class XMLService on TrapezoidFunction
	 */
	@Test
	public void testSaveObjectToXMLString4() throws Exception {
                System.out.println("saveObjectToXMLString");
                ParamTrapezoid parameters = new ParamTrapezoid(-8, -7, -6, -5, Scale.LOGARITHMIC, Type.FIRST, 0.0, 0.0);
                TrapezoidFunction trapezoidFunction = new TrapezoidFunction(parameters, "somename");
                String expResult =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<trapezoidFunction>"
                        + "\n    <name>somename</name>"
                        + "\n    <parameters>"
                        + "\n        <scale>LOGARITHMIC</scale>"
                        + "\n        <type>FIRST</type>"
                        + "\n        <a>-8.0</a>"
                        + "\n        <b>-7.0</b>"
                        + "\n        <c>-6.0</c>"
                        + "\n        <d>-5.0</d>"
                        + "\n        <length>0.0</length>"
                        + "\n        <prob>0.0</prob>"
                        + "\n    </parameters>"
                        + "\n</trapezoidFunction>"
                        + "\n";
                String result = XMLService.saveObjectToXMLString(trapezoidFunction);
                System.out.println(result);
                assertEquals(expResult, result);
	}

	/**
	 * Test of saveObjectToXMLString method, of class XMLService on TrapezoidLingNonSymmetric
	 */
	@Test
	public void testSaveObjectToXMLString5() throws Exception {
                System.out.println("saveObjectToXMLString");
                ParamTrapezoid parameters = new ParamTrapezoid(0.1, 0.2, 0.3, 0.5, Scale.LINEAR, Type.FIRST, 0.0, 0.0);
                ParamTrapezoid parameters2 = new ParamTrapezoid(0.3, 0.4, 0.5, 0.7, Scale.LINEAR, Type.SECOND, 0.04, 0.05);
                List<String> list1 = new ArrayList<String>();
                list1.add("first");
                list1.add("second");
                List<ParamTrapezoid> list2 = new ArrayList<ParamTrapezoid>();
                list2.add(parameters);
                list2.add(parameters2);
		        TrapezoidLingNonSymmetric trapezoidLingNonSymmetric = new TrapezoidLingNonSymmetric("somename", list1, list2);
                String expResult =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<trapezoidLingNonSymmetric>"
                        + "\n    <functions>"
                        + "\n        <name>first</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>FIRST</type>"
                        + "\n            <a>0.1</a>"
                        + "\n            <b>0.2</b>"
                        + "\n            <c>0.3</c>"
                        + "\n            <d>0.5</d>"
                        + "\n            <length>0.0</length>"
                        + "\n            <prob>0.0</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <functions>"
                        + "\n        <name>second</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>SECOND</type>"
                        + "\n            <a>0.3</a>"
                        + "\n            <b>0.4</b>"
                        + "\n            <c>0.5</c>"
                        + "\n            <d>0.7</d>"
                        + "\n            <length>0.04</length>"
                        + "\n            <prob>0.05</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <name>somename</name>"
                        + "\n</trapezoidLingNonSymmetric>"
                        + "\n";
                String result = XMLService.saveObjectToXMLString(trapezoidLingNonSymmetric);
                System.out.println(result);
                assertEquals(expResult, result);
	}

	/**
	 * Test of saveObjectToXMLString method, of class XMLService on TrapezoidLingSymmetric
	 */
	@Test
	public void testSaveObjectToXMLString6() throws Exception {
                System.out.println("saveObjectToXMLString");
                List<String> list1 = new ArrayList<String>();
                list1.add("first");
                list1.add("second");
                list1.add("third");
		        TrapezoidLingSymmetric trapezoidLingSymmetric = new TrapezoidLingSymmetric("somename", 1, Scale.LINEAR, list1);
                String expResult =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<trapezoidLingSymmetric>"
                        + "\n    <functions>"
                        + "\n        <name>first</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>FIRST</type>" 
                        + "\n            <a>0.0</a>"
                        + "\n            <b>0.0</b>"
                        + "\n            <c>0.25</c>"
                        + "\n            <d>0.25</d>"
                        + "\n            <length>0.0</length>"
                        + "\n            <prob>0.0</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <functions>"
                        + "\n        <name>second</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>FIRST</type>"
                        + "\n            <a>0.25</a>"
                        + "\n            <b>0.25</b>"
                        + "\n            <c>0.75</c>"
                        + "\n            <d>0.75</d>"
                        + "\n            <length>0.0</length>"
                        + "\n            <prob>0.0</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <functions>"
                        + "\n        <name>third</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>FIRST</type>"
                        + "\n            <a>0.75</a>"
                        + "\n            <b>0.75</b>"
                        + "\n            <c>1.0</c>"
                        + "\n            <d>1.0</d>"
                        + "\n            <length>0.0</length>"
                        + "\n            <prob>0.0</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <name>somename</name>"
                        + "\n    <partition>2</partition>"
                        + "\n</trapezoidLingSymmetric>"
                        + "\n";
                String result = XMLService.saveObjectToXMLString(trapezoidLingSymmetric);
                System.out.println(result);
                assertEquals(expResult, result);
	}

	/**
	 * Test of saveObjectToXMLString method, of class XMLService on TriangleLingNonSymmetric
	 */
	@Test
	public void testSaveObjectToXMLString7() throws Exception {
                System.out.println("saveObjectToXMLString");
                ParamTriangle parameters = new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR, Type.FIRST, 0.0, 0.0);
                ParamTriangle parameters2 = new ParamTriangle(0.2, 0.4, 0.5, Scale.LINEAR, Type.FIRST, 0.0, 0.0);
                List<String> list1 = new ArrayList<String>();
                list1.add("first");
                list1.add("second");
                List<ParamTriangle> list2 = new ArrayList<ParamTriangle>();
                list2.add(parameters);
                list2.add(parameters2);
		        TriangleLingNonSymmetric triangleLingNonSymmetric = new TriangleLingNonSymmetric("somename", list1, list2);
                String expResult =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<triangleLingNonSymmetric>"
                        + "\n    <functions>"
                        + "\n        <name>first</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>FIRST</type>"
                        + "\n            <a>0.1</a>"
                        + "\n            <b>0.2</b>"
                        + "\n            <c>0.3</c>"
                        + "\n            <length>0.0</length>"
                        + "\n            <prob>0.0</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <functions>"
                        + "\n        <name>second</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>FIRST</type>"
                        + "\n            <a>0.2</a>"
                        + "\n            <b>0.4</b>"
                        + "\n            <c>0.5</c>"
                        + "\n            <length>0.0</length>"
                        + "\n            <prob>0.0</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <name>somename</name>"
                        + "\n</triangleLingNonSymmetric>"
                        + "\n";
                String result = XMLService.saveObjectToXMLString(triangleLingNonSymmetric);
                System.out.println(result);
                assertEquals(expResult, result);
	}

	/**
	 * Test of saveObjectToXMLString method, of class XMLService on TriangleLingSymmetric
	 */
	@Test
	public void testSaveObjectToXMLString8() throws Exception {
                System.out.println("saveObjectToXMLString");
                List<String> list1 = new ArrayList<String>();
                list1.add("first");
                list1.add("second");
                list1.add("third");
		        TriangleLingSymmetric triangleLingSymmetric = new TriangleLingSymmetric("somename", Scale.LINEAR, list1);
                String expResult =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<triangleLingSymmetric>"
                        + "\n    <functions>"
                        + "\n        <name>first</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>FIRST</type>"
                        + "\n            <a>0.0</a>"
                        + "\n            <b>0.0</b>"
                        + "\n            <c>0.5</c>"
                        + "\n            <length>0.0</length>"
                        + "\n            <prob>0.0</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <functions>"
                        + "\n        <name>second</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>FIRST</type>"
                        + "\n            <a>0.0</a>"
                        + "\n            <b>0.5</b>"
                        + "\n            <c>1.0</c>"
                        + "\n            <length>0.0</length>"
                        + "\n            <prob>0.0</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <functions>"
                        + "\n        <name>third</name>"
                        + "\n        <parameters>"
                        + "\n            <scale>LINEAR</scale>"
                        + "\n            <type>FIRST</type>"
                        + "\n            <a>0.5</a>"
                        + "\n            <b>1.0</b>"
                        + "\n            <c>1.0</c>"
                        + "\n            <length>0.0</length>"
                        + "\n            <prob>0.0</prob>"
                        + "\n        </parameters>"
                        + "\n    </functions>"
                        + "\n    <name>somename</name>"
                        + "\n    <partition>2</partition>"
                        + "\n</triangleLingSymmetric>"
                        + "\n";
                String result = XMLService.saveObjectToXMLString(triangleLingSymmetric);
                System.out.println(result);
                assertEquals(expResult, result);
	}
        
        /**
	 * Test of saveObjectToXMLString method, of class XMLService
	 */
	@Test
        public void testSaveObjectToFile() throws Exception {
            System.out.println("saveObjectToFile");
            ParamTrapezoid paramTrapezoid = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.FIRST, 0.0, 0.0);
            XMLService.saveObjectToFile(paramTrapezoid, "testSaveObjectToFile.xml");
            String expResult =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<paramTrapezoid>"
                        + "\n    <scale>LINEAR</scale>"
                        + "\n    <type>FIRST</type>"
                        + "\n    <a>0.1</a>"
                        + "\n    <b>0.2</b>"
                        + "\n    <c>0.3</c>"
                        + "\n    <d>0.4</d>"
                        + "\n    <length>0.0</length>"
                        + "\n    <prob>0.0</prob>"
                        + "\n</paramTrapezoid>"
                        + "\n";
            String result = null;
            File file = new File("testSaveObjectToFile.xml"); //for ex foo.txt
            try {
                FileReader reader = new FileReader(file);
                char[] chars = new char[(int) file.length()];
                reader.read(chars);
                result = new String(chars);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            file.delete();
            assertEquals(expResult, result);
        }
        
         /**
	 * Test of readFileToString method, of class XMLService
	 */
	@Test
        public void testReadFileToString() throws Exception {
            System.out.println("readFileToString");
            ParamTrapezoid paramTrapezoid = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 0.05);
            XMLService.saveObjectToFile(paramTrapezoid, "testSaveObjectToFile.xml");
            String expResult =
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<paramTrapezoid>"
                        + "\n    <scale>LINEAR</scale>"
                        + "\n    <type>SECOND</type>"
                        + "\n    <a>0.1</a>"
                        + "\n    <b>0.2</b>"
                        + "\n    <c>0.3</c>"
                        + "\n    <d>0.4</d>"
                        + "\n    <length>0.04</length>"
                        + "\n    <prob>0.05</prob>"
                        + "\n</paramTrapezoid>"
                        + "\n";
            String result = XMLService.readFileToString("testSaveObjectToFile.xml");
            File file = new File("testSaveObjectToFile.xml");
            file.delete();
            assertEquals(expResult, result);
        }
        
         /**
	 * Test of readObjectFromFile method, of class XMLService
	 */
	@Test
        public void testReadFileToObject() throws Exception {
            System.out.println("readFileToObject");
            ParamTrapezoid paramTrapezoid = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 0.05);
            XMLService.saveObjectToFile(paramTrapezoid, "testReadFileToObject.xml");
            ParamTrapezoid result = XMLService.readFileToObject(ParamTrapezoid.class, "testReadFileToObject.xml");
            File file = new File("testReadFileToObject.xml");
            file.delete();
            assertEquals(paramTrapezoid, result);
        }
        
                 /**
* Test of readObjectFromFile method, of class XMLService
	 */
	@Test
        public void testReadFileToObjectIOException() throws Exception {
            System.out.println("readFileToObject");
            ParamTrapezoid paramTrapezoid = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR, Type.SECOND, 0.04, 0.05);
            XMLService.saveObjectToFile(paramTrapezoid, "testReadFileToObject.xml");
            try{
                ParamTrapezoid result = XMLService.readFileToObject(ParamTrapezoid.class, "Exception.xml");
                fail("readFileToObject didn't throw when I expected it to");
            } catch(IOException exception) {
                assertTrue(true);
            }
            
            File file = new File("testReadFileToObject.xml");
            file.delete();
        }
}
