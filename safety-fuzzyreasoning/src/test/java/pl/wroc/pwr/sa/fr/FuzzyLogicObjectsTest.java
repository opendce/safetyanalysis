package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maciej
 */
public class FuzzyLogicObjectsTest {
	
	public FuzzyLogicObjectsTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of addTriangleFunction method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testAddTriangleFunction() throws Exception {
		System.out.println("addTriangleFunction");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TriangleFunction fun = new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR ), "");
		instance.addTriangleFunction(new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR ), ""));
		List<TriangleFunction> list = instance.getTriangleFunctions();
		
		int expResult_1 = 1;
		int result_1 = list.size();
		assertEquals(expResult_1, result_1);
		
		boolean expResult_2 = true;
		boolean result_2 = list.contains(fun);
		assertEquals(expResult_2, result_2);
		
		instance.addTriangleFunction(new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR ), ""));
		list = instance.getTriangleFunctions();
		
		expResult_1 = 1;
		result_1 = list.size();
		assertEquals(expResult_1, result_1);
	}

	/**
	 * Test of addTrapezoidFunction method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testAddTrapezoidFunction() throws Exception {
		System.out.println("addTrapezoidFunction");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TrapezoidFunction fun = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR ), "");
		instance.addTrapezoidFunction(new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR ), ""));
		List<TrapezoidFunction> list = instance.getTrapezoidFunctions();
		
		int expResult_1 = 1;
		int result_1 = list.size();
		assertEquals(expResult_1, result_1);
		
		boolean expResult_2 = true;
		boolean result_2 = list.contains(fun);
		assertEquals(expResult_2, result_2);
		
		instance.addTrapezoidFunction(new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR ), ""));
		list = instance.getTrapezoidFunctions();
		
		expResult_1 = 1;
		result_1 = list.size();
		assertEquals(expResult_1, result_1);
	}

	/**
	 * Test of addTriangleLingNonSymmetric method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testAddTriangleLingNonSymmetric() throws Exception {
		System.out.println("addTriangleLingNonSymmetric");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TriangleLingNonSymmetric ling = new TriangleLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTriangle> (Arrays.asList(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR ), new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR ))));
		instance.addTriangleLingNonSymmetric(new TriangleLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTriangle> (Arrays.asList(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR ), new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR )))));
		List<TriangleLingNonSymmetric> list = instance.getTriangleLingsNonSymmetric();
		
		int expResult_1 = 1;
		int result_1 = list.size();
		assertEquals(expResult_1, result_1);
		
		boolean expResult_2 = true;
		boolean result_2 = list.contains(ling);
		assertEquals(expResult_2, result_2);
		
		instance.addTriangleLingNonSymmetric(new TriangleLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTriangle> (Arrays.asList(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR ), new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR )))));
		list = instance.getTriangleLingsNonSymmetric();
		
		expResult_1 = 1;
		result_1 = list.size();
		assertEquals(expResult_1, result_1);
	}

	/**
	 * Test of addTrapezoidLingNonSymmetric method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testAddTrapezoidLingNonSymmetric() throws Exception {
		System.out.println("addTrapezoidLingNonSymmetric");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TrapezoidLingNonSymmetric ling = new TrapezoidLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTrapezoid> (Arrays.asList(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR ), new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR ))));
		instance.addTrapezoidLingNonSymmetric(new TrapezoidLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTrapezoid> (Arrays.asList(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR ), new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR )))));
		List<TrapezoidLingNonSymmetric> list = instance.getTrapezoidLingsNonSymmetric();
		
		int expResult_1 = 1;
		int result_1 = list.size();
		assertEquals(expResult_1, result_1);
		
		boolean expResult_2 = true;
		boolean result_2 = list.contains(ling);
		assertEquals(expResult_2, result_2);
		
		instance.addTrapezoidLingNonSymmetric(new TrapezoidLingNonSymmetric("Zmienna", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTrapezoid> (Arrays.asList(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR ), new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR )))));
		list = instance.getTrapezoidLingsNonSymmetric();
		
		expResult_1 = 1;
		result_1 = list.size();
		assertEquals(expResult_1, result_1);
	}

	/**
	 * Test of addTriangleLingSymmetric method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testAddTriangleLingSymmetric() throws Exception {
		System.out.println("addTriangleLingSymmetric");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TriangleLingSymmetric ling = new TriangleLingSymmetric("", Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large")));
		instance.addTriangleLingSymmetric(new TriangleLingSymmetric("", Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large"))));
		List<TriangleLingSymmetric> list = instance.getTriangleLingsSymmetric();
		
		int expResult_1 = 1;
		int result_1 = list.size();
		assertEquals(expResult_1, result_1);
		
		boolean expResult_2 = true;
		boolean result_2 = list.contains(ling);
		assertEquals(expResult_2, result_2);
		
		instance.addTriangleLingSymmetric(new TriangleLingSymmetric("", Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large"))));
		list = instance.getTriangleLingsSymmetric();
		
		expResult_1 = 1;
		result_1 = list.size();
		assertEquals(expResult_1, result_1);
	}

	/**
	 * Test of addTrapezoidLingSymmetric method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testAddTrapezoidLingSymmetric() throws Exception {
		System.out.println("addTrapezoidLingSymmetric");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TrapezoidLingSymmetric ling = new TrapezoidLingSymmetric("", 0.5, Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large")));
		instance.addTrapezoidLingSymmetric(new TrapezoidLingSymmetric("", 0.5, Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large"))));
		List<TrapezoidLingSymmetric> list = instance.getTrapezoidLingsSymmetric();
		
		int expResult_1 = 1;
		int result_1 = list.size();
		assertEquals(expResult_1, result_1);
		
		boolean expResult_2 = true;
		boolean result_2 = list.contains(ling);
		assertEquals(expResult_2, result_2);
		
		instance.addTrapezoidLingSymmetric(new TrapezoidLingSymmetric("", 0.5, Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large"))));
		list = instance.getTrapezoidLingsSymmetric();
		
		expResult_1 = 1;
		result_1 = list.size();
		assertEquals(expResult_1, result_1);
	}

	/**
	 * Test of getTriangleFunctions method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testGetTriangleFunctions() throws Exception {
		System.out.println("getTriangleFunctions");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TriangleFunction fun1 = new TriangleFunction(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR ), "");
		TriangleFunction fun2 = new TriangleFunction(new ParamTriangle(0.4, 0.5, 0.6, Scale.LINEAR ), "");
		instance.addTriangleFunction(fun1);
		instance.addTriangleFunction(fun2);
		List<TriangleFunction> result = instance.getTriangleFunctions();
		List<TriangleFunction> expResult = new ArrayList<TriangleFunction> (Arrays.asList (fun1, fun2));
		assertEquals(expResult, result);
	}

	/**
	 * Test of getTrapezoidFunctions method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testGetTrapezoidFunctions() throws Exception {
		System.out.println("getTrapezoidFunctions");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TrapezoidFunction fun1 = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR ), "");
		TrapezoidFunction fun2 = new TrapezoidFunction(new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR ), "");
		instance.addTrapezoidFunction(fun1);
		instance.addTrapezoidFunction(fun2);
		List<TrapezoidFunction> result = instance.getTrapezoidFunctions();
		List<TrapezoidFunction> expResult = new ArrayList<TrapezoidFunction> (Arrays.asList (fun1, fun2));
		assertEquals(expResult, result);
	}

	/**
	 * Test of getTriangleLingsNonSymmetric method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testGetTriangleLingsNonSymmetric() throws Exception {
		System.out.println("getTriangleLingsNonSymmetric");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TriangleLingNonSymmetric ling1 = new TriangleLingNonSymmetric("Zmienna1", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTriangle> (Arrays.asList(new ParamTriangle(0.1, 0.2, 0.3, Scale.LINEAR ), new ParamTriangle(0.2, 0.3, 0.4, Scale.LINEAR ))));
		TriangleLingNonSymmetric ling2 = new TriangleLingNonSymmetric("Zmienna2", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTriangle> (Arrays.asList(new ParamTriangle(0.3, 0.4, 0.5, Scale.LINEAR ), new ParamTriangle(0.5, 0.6, 0.7, Scale.LINEAR ))));
		instance.addTriangleLingNonSymmetric(ling1);
		instance.addTriangleLingNonSymmetric(ling2);
		List<TriangleLingNonSymmetric> result = instance.getTriangleLingsNonSymmetric();
		List<TriangleLingNonSymmetric> expResult = new ArrayList<TriangleLingNonSymmetric> (Arrays.asList (ling1, ling2));
		assertEquals(expResult, result);
	}

	/**
	 * Test of getTrapezoidLingsNonSymmetric method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testGetTrapezoidLingsNonSymmetric() throws Exception {
		System.out.println("getTrapezoidLingsNonSymmetric");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TrapezoidLingNonSymmetric ling1 = new TrapezoidLingNonSymmetric("Zmienna1", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTrapezoid> (Arrays.asList(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR ), new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR ))));
		TrapezoidLingNonSymmetric ling2 = new TrapezoidLingNonSymmetric("Zmienna2", new ArrayList<String> (Arrays.asList("Pierwsza", "Druga")), new ArrayList<ParamTrapezoid> (Arrays.asList(new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR ), new ParamTrapezoid(0.8, 0.9, 1.0, 1.0, Scale.LINEAR ))));
		instance.addTrapezoidLingNonSymmetric(ling1);
		instance.addTrapezoidLingNonSymmetric(ling2);
		List<TrapezoidLingNonSymmetric> result = instance.getTrapezoidLingsNonSymmetric();
		List<TrapezoidLingNonSymmetric> expResult = new ArrayList<TrapezoidLingNonSymmetric> (Arrays.asList (ling1, ling2));
		assertEquals(expResult, result);
	}

	/**
	 * Test of getTriangleLingsSymmetric method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testGetTriangleLingsSymmetric() throws Exception {
		System.out.println("getTriangleLingsSymmetric");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TriangleLingSymmetric ling1 = new TriangleLingSymmetric("", Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large")));
		TriangleLingSymmetric ling2 = new TriangleLingSymmetric("", Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large", "Huge")));
		instance.addTriangleLingSymmetric(ling1);
		instance.addTriangleLingSymmetric(ling2);
		List<TriangleLingSymmetric> result = instance.getTriangleLingsSymmetric();
		List<TriangleLingSymmetric> expResult = new ArrayList<TriangleLingSymmetric> (Arrays.asList (ling1, ling2));
		assertEquals(expResult, result);
	}

	/**
	 * Test of getTrapezoidLingsSymmetric method, of class FuzzyLogicObjects.
	 */
	@Test
	public void testGetTrapezoidLingsSymmetric() throws Exception {
		System.out.println("getTrapezoidLingsSymmetric");
		FuzzyLogicObjects instance = new FuzzyLogicObjects();
		TrapezoidLingSymmetric ling1 = new TrapezoidLingSymmetric("", 0.5, Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large")));
		TrapezoidLingSymmetric ling2 = new TrapezoidLingSymmetric("", 0.5, Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large", "Huge")));
		instance.addTrapezoidLingSymmetric(ling1);
		instance.addTrapezoidLingSymmetric(ling2);
		List<TrapezoidLingSymmetric> result = instance.getTrapezoidLingsSymmetric();
		List<TrapezoidLingSymmetric> expResult = new ArrayList<TrapezoidLingSymmetric> (Arrays.asList (ling1, ling2));
		assertEquals(expResult, result);
	}
}