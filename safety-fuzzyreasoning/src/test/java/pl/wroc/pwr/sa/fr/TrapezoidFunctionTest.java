package pl.wroc.pwr.sa.fr;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maciej
 */
public class TrapezoidFunctionTest {
	
	public TrapezoidFunctionTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of getParameters method, of class TrapezoidFunction.
	 */
	@Test
	public void testGetParameters() throws Exception {
		System.out.println("getParameters");
		TrapezoidFunction instance = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), null);
		ParamTrapezoid expResult = new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR);
		ParamTrapezoid result = instance.getParameters();
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		assertEquals(expResult.getD(), result.getD(), 0.01);
		
		instance = new TrapezoidFunction(new ParamTrapezoid(-12, -11, -10, -9, Scale.LOGARITHMIC), null);
		expResult = new ParamTrapezoid(-12, -11, -10, -9, Scale.LOGARITHMIC);
		result = instance.getParameters();
		assertEquals(expResult.getA(), result.getA(), 0.01);
		assertEquals(expResult.getB(), result.getB(), 0.01);
		assertEquals(expResult.getC(), result.getC(), 0.01);
		assertEquals(expResult.getD(), result.getD(), 0.01);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of getName method, of class TrapezoidFunction.
	 */
	@Test
	public void testGetName() throws Exception {
		System.out.println("getName");
		TrapezoidFunction instance = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), "Funkcja");
		String expResult = "Funkcja";
		String result = instance.getName();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of equals method, of class TrapezoidFunction.
	 */
	@Test
	public void testEquals() throws Exception {
		System.out.println("equals");
		Object obj = new TrapezoidFunction (new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), "");
		TrapezoidFunction instance = new TrapezoidFunction (new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), "");
		boolean expResult = true;
		boolean result = instance.equals(obj);
		assertEquals(expResult, result);
	}

	/**
	 * Test of add method, of class TrapezoidFunction.
	 */
	@Test
	public void testAdd() throws Exception {
		System.out.println("add");
		MembershipFunction x = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), "");
		TrapezoidFunction instance = new TrapezoidFunction(new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR), "");
		TrapezoidFunction expResult = new TrapezoidFunction(new ParamTrapezoid(0.3, 0.5, 0.7, 0.9, Scale.LINEAR), "");
		TrapezoidFunction result = instance.add(x);
		assertEquals(expResult.getParameters().getA(), result.getParameters().getA(), 0.01);
		assertEquals(expResult.getParameters().getB(), result.getParameters().getB(), 0.01);
		assertEquals(expResult.getParameters().getC(), result.getParameters().getC(), 0.01);
		assertEquals(expResult.getParameters().getD(), result.getParameters().getD(), 0.01);
	}

	/**
	 * Test of subtract method, of class TrapezoidFunction.
	 */
	@Test
	public void testSubtract() throws Exception {
		System.out.println("subtract");
		MembershipFunction x = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), "");
		TrapezoidFunction instance = new TrapezoidFunction(new ParamTrapezoid(0.5, 0.6, 0.7, 0.8, Scale.LINEAR), "");
		TrapezoidFunction expResult = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.3, 0.5, 0.7, Scale.LINEAR), "");
		TrapezoidFunction result = instance.subtract(x);
		assertEquals(expResult.getParameters().getA(), result.getParameters().getA(), 0.01);
		assertEquals(expResult.getParameters().getB(), result.getParameters().getB(), 0.01);
		assertEquals(expResult.getParameters().getC(), result.getParameters().getC(), 0.01);
		assertEquals(expResult.getParameters().getD(), result.getParameters().getD(), 0.01);
	}

	/**
	 * Test of divide method, of class TrapezoidFunction.
	 */
	@Test
	public void testDivide() throws Exception {
		System.out.println("divide");
		MembershipFunction x = new TrapezoidFunction(new ParamTrapezoid(0.5, 0.6, 0.8, 1, Scale.LINEAR), "");
		TrapezoidFunction instance = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), "");
		TrapezoidFunction expResult = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.25, 0.5, 0.8, Scale.LINEAR), "");
		TrapezoidFunction result = instance.divide(x);
		assertEquals(expResult.getParameters().getA(), result.getParameters().getA(), 0.01);
		assertEquals(expResult.getParameters().getB(), result.getParameters().getB(), 0.01);
		assertEquals(expResult.getParameters().getC(), result.getParameters().getC(), 0.01);
		assertEquals(expResult.getParameters().getD(), result.getParameters().getD(), 0.01);
	}

	/**
	 * Test of multiply method, of class TrapezoidFunction.
	 */
	@Test
	public void testMultiply() throws Exception {
		System.out.println("multiply");
		MembershipFunction x = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), "");
		TrapezoidFunction instance = new TrapezoidFunction(new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR), "");
		TrapezoidFunction expResult = new TrapezoidFunction(new ParamTrapezoid(0.02, 0.06, 0.12, 0.2, Scale.LINEAR), "");
		TrapezoidFunction result = instance.multiply(x);
		assertEquals(expResult.getParameters().getA(), result.getParameters().getA(), 0.01);
		assertEquals(expResult.getParameters().getB(), result.getParameters().getB(), 0.01);
		assertEquals(expResult.getParameters().getC(), result.getParameters().getC(), 0.01);
		assertEquals(expResult.getParameters().getD(), result.getParameters().getD(), 0.01);
	}

	/**
	 * Test of getValue method, of class TrapezoidFunction.
	 */
	@Test
	public void testGetValue() throws Exception {
		System.out.println("getValue");
		double argument = 0.2;
		TrapezoidFunction instance = new TrapezoidFunction(new ParamTrapezoid(0.1, 0.2, 0.3, 0.4, Scale.LINEAR), "");
		double expResult = 1;
		double result = instance.getValue(argument);
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getJacardSimilarity method, of class TrapezoidFunction.
	 */
	@Test
	public void testGetJacardSimilarity() throws Exception {
		System.out.println("getJacardSimilarity");
		MembershipFunction x = new TrapezoidFunction(new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, Scale.LINEAR), "");;
		TrapezoidFunction instance = new TrapezoidFunction(new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR), "");
		double expResult = 0.33;
		double result = instance.getJacardSimilarity(x);
		assertEquals(expResult, result, 0.01);
	}
        
        
	/**
	 * Test of getInclusionDegreeMeasure method, of class TrapezoidFunction.
	 */
	@Test
	public void testGetInclusionDegreeMeasure() throws Exception {
		System.out.println("getInclusionDegreeMeasure");
		MembershipFunction x = new TrapezoidFunction(new ParamTrapezoid(0.3, 0.4, 0.5, 0.6, Scale.LINEAR), "");;
		TrapezoidFunction instance = new TrapezoidFunction(new ParamTrapezoid(0.2, 0.3, 0.4, 0.5, Scale.LINEAR), "");
		double expResult = 0.5;
		double result = instance.getInclusionDegreeMeasure(x);
		assertEquals(expResult, result, 0.01);
	}
}