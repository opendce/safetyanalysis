package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maciej
 */
public class TriangleLingSymmetricTest {
	
	public TriangleLingSymmetricTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of getPartition method, of class TriangleLingSymmetric.
	 */
	@Test
	public void testGetPartition() throws Exception {
		System.out.println("getPartition");
		TriangleLingSymmetric instance = new TriangleLingSymmetric(null, Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large")));
		int expResult = 2;
		int result = instance.getPartition();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of getName method, of class TriangleLingSymmetric.
	 */
	@Test
	public void testGetName() throws Exception {
		System.out.println("getName");
		TriangleLingSymmetric instance = new TriangleLingSymmetric("Example", Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large")));
		String expResult = "Example";
		String result = instance.getName();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of getFunctionsNames method, of class TriangleLingSymmetric.
	 */
	@Test
	public void testGetFunctionsNames() throws Exception {
		System.out.println("getFunctionsNames");
		TriangleLingSymmetric instance = new TriangleLingSymmetric(null, Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large")));
		List expResult = new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large"));
		List result = instance.getFunctionsNames();
		assertTrue(result.equals(expResult));
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of getFunction method, of class TriangleLingSymmetric.
	 */
	@Test
	public void testGetFunction() throws Exception {
		System.out.println("getFunction");
		String name = "Small";
		TriangleLingSymmetric instance = new TriangleLingSymmetric(null, Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Small", "Medium", "Large")));
		TriangleFunction expResult = new TriangleFunction(new ParamTriangle(0, 0, 0.5, Scale.LINEAR), "Small");
		TriangleFunction result = instance.getFunction(name);
		assertEquals(expResult.getName(), result.getName());
		assertEquals(expResult.getParameters().getA(), result.getParameters().getA(), 0.01);
		assertEquals(expResult.getParameters().getB(), result.getParameters().getB(), 0.01);
		assertEquals(expResult.getParameters().getC(), result.getParameters().getC(), 0.01);
		// TODO review the generated test code and remove the default call to fail.
		// fail("The test case is a prototype.");
	}

	/**
	 * Test of equals method, of class TriangleLingSymmetric.
	 */
	@Test
	public void testEquals() throws Exception {
		System.out.println("equals");
		Object obj = new TriangleLingSymmetric("", Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Pierwsza", "Druga", "Trzecia")));
		TriangleLingSymmetric instance = new TriangleLingSymmetric("", Scale.LINEAR, new ArrayList<String> (Arrays.asList ("Pierwsza", "Druga", "Trzecia")));;
		boolean expResult = true;
		boolean result = instance.equals(obj);
		assertEquals(expResult, result);
	}
}