package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author N3
 */
@XmlRootElement(name = "trapezoidLingNonSymmetric")
public class TrapezoidLingNonSymmetric implements LingNonSymmetric {
	private String name;
	private List<TrapezoidFunction> functions;
        
        public TrapezoidLingNonSymmetric() {
        }

	public TrapezoidLingNonSymmetric(String name, List<String> names, List<ParamTrapezoid> parameters) throws Exception {
		int i;
		this.name = name;
		this.functions = new ArrayList<TrapezoidFunction> ();
		if (names.size() != parameters.size())
			throw new Exception ("wrong number of parameters");
		i = 0;
		for(String n : names) {
			this.functions.add(new TrapezoidFunction(parameters.get(i), n));
			i++;
		}
	}
	
	TrapezoidLingNonSymmetric(String name, double ratio, Scale scale, List<String> names, List<Double> fractions) throws Exception {
		this.name = name;
		this.functions = new ArrayList<TrapezoidFunction> ();
                int i;
		double sum = 0, a, b;
		for (double f : fractions) {
			sum += f;
		}
		if (ratio < 0)
			throw new Exception ("ratio is not positive");
		if (names.size() != (fractions.size() + 1))
			throw new Exception("wrong number of fractions");
		if(scale == Scale.LINEAR && sum != 1 || scale == Scale.LOGARITHMIC && sum != 12) {
			throw new Exception("wrong fractions");		
		}
		double param1, param2, param3, param4;
		String n;
		if(scale == Scale.LINEAR) {
			a = fractions.get(0)*ratio/(1+ratio);
			param1 = 0;
			param2 = 0;
			param3 = a;
			param4 = fractions.get(0)-a;
			n = names.get(0);
			ParamTrapezoid param = new ParamTrapezoid(param1, param2, param3, param4, scale);
			this.functions.add(new TrapezoidFunction(param, n));
			for(i = 1; i < fractions.size(); i++) {
				n = names.get(i);
				param1 = param3;
				param2 = param4;
				b = (ratio*(fractions.get(i-1)+fractions.get(i)-a)-a)/(1+ratio);
				param3 = param2 + (a+b);
				param4 = param3 + fractions.get(i)-2*b;
				param = new ParamTrapezoid(param1, param2, param3, param4, scale);
				this.functions.add(new TrapezoidFunction(param, n));
				a = b;
			}
			param1 = param3;
			param2 = param4;
			param3 = 1;
			param4 = 1;
			n = names.get(i);
			param = new ParamTrapezoid(param1, param2, param3, param4, scale);
			this.functions.add(new TrapezoidFunction(param, n));
		}
		else {
			a = fractions.get(0)*ratio/(1+ratio);
			param1 = -12;
			param2 = -12;
			param3 = param1 + a;
			param4 = param1 + fractions.get(0)-a;
			n = names.get(0);
			ParamTrapezoid param = new ParamTrapezoid(param1, param2, param3, param4, scale);
			this.functions.add(new TrapezoidFunction(param, n));
			for(i = 1; i < fractions.size(); i++) {
				n = names.get(i);
				param1 = param3;
				param2 = param4;
				b = (ratio*(fractions.get(i-1)+fractions.get(i)-a)-a)/(1+ratio);
				param3 = param2 + (a+b);
				param4 = param3 + fractions.get(i)-2*b;
				param = new ParamTrapezoid(param1, param2, param3, param4, scale);
				this.functions.add(new TrapezoidFunction(param, n));
				a = b;
			}
			param1 = param3;
			param2 = param4;
			param3 = 0;
			param4 = 0;
			n = names.get(i);
			param = new ParamTrapezoid(param1, param2, param3, param4, scale);
			this.functions.add(new TrapezoidFunction(param, n));
		}
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<String> getFunctionsNames() {
		List<String> names = new ArrayList<String> ();
		for(TrapezoidFunction function : functions) {
			names.add(function.getName());
		}
		return names;
	}
	
	public List<TrapezoidFunction> getFunctions() {
		return functions;
	}
	
	public void setFunctions(List<TrapezoidFunction> functions) {
		this.functions = functions;
	}
	
	public TrapezoidFunction getFunction(String name) {
		for(TrapezoidFunction function : functions) {
			if(function.getName().equals(name)) {
				return function;
			}
		}
		return null;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		final TrapezoidLingNonSymmetric l = (TrapezoidLingNonSymmetric) obj;
		if (!l.getName().equals(name) || !functions.equals(l.functions))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 97 * hash + (this.name != null ? this.name.hashCode() : 0);
		return hash;
	}
}
