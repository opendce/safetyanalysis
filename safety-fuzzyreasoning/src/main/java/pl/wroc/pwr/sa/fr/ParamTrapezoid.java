package pl.wroc.pwr.sa.fr;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author N3
 */
@XmlRootElement(name = "paramTrapezoid")
public class ParamTrapezoid extends Param {

	private double a, b, c, d, length, prob;
        
        public ParamTrapezoid() {
            super();
        }
        
	public ParamTrapezoid(double a, double b, double c, double d, Scale scale, Type type, double length, double prob) throws Exception {
                super(scale, type);
		if (a > b || b > c || c > d) {
			throw new Exception("values in bad order");
		}
		if (scale == Scale.LINEAR) {
			if ((a < 0) || (b < 0) || (c < 0) || (d < 0)
					|| (a > 1) || (b > 1) || (c > 1) || (d > 1)) {
				throw new Exception("value exceedes the range");
			}
		} else {
			if ((a < -12) || (b < -12) || (c < -12) || (d < -12)
					|| (a > 0) || (b > 0) || (c > 0) || (d > 0)) {
				throw new Exception("value exceedes the range");
			}
		}
                if (type == Type.FIRST && length != 0){
                    throw new Exception("Fuzzy set of first type cannot have length value != 0 ");
                }
                if (type == Type.SECOND && (length >= Math.min((d-c),(b-a)) || length >= (c-b)/2)) {
                    throw new Exception("Value length exceedes the range");
                }
                if (type == Type.FIRST && prob != 0){
                    throw new Exception("Fuzzy set of first type cannot have prob value != 0 ");
                }
                if (type == Type.SECOND && (prob >= 1 || prob <= 0)){
                    throw new Exception("Value prob exceedes the range");
                }
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
                this.length = length;
                this.prob = prob;
	}

        public ParamTrapezoid(double a, double b, double c, double d, Scale scale) throws Exception {
            this(a,b,c,d,scale,Type.FIRST,0,0);
        }
        
	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}

	public double getD() {
		return d;
	}

	public void setD(double d) {
		this.d = d;
	}
        
        public double getLength(){
            return length;
        }
        
        public void setLength(double length){
            this.length = length;
        }
        
        public double getProb(){
            return prob;
        }
        
        public void setProb(double prob){
            this.prob = prob;
        }

	ParamTrapezoid add(Param y) throws Exception {
		if (y.getClass() != getClass())
			throw new Exception ("types are different");
		ParamTrapezoid x = (ParamTrapezoid) y;
		if (type != x.getType())
			throw new Exception ("fuzzy sets types are different");
                if (scale != x.getScale())
			throw new Exception ("scales are different");
		double a1, b1, c1, d1, a2, b2, c2, d2, a3, b3, c3, d3;
		if (scale == Scale.LOGARITHMIC) {
			a1 = Math.pow(10, a);
			b1 = Math.pow(10, b);
			c1 = Math.pow(10, c);
			d1 = Math.pow(10, d);
			a2 = Math.pow(10, x.getA());
			b2 = Math.pow(10, x.getB());
			c2 = Math.pow(10, x.getC());
			d2 = Math.pow(10, x.getD());
		} else {
			a1 = a;
			b1 = b;
			c1 = c;
			d1 = d;
			a2 = x.getA();
			b2 = x.getB();
			c2 = x.getC();
			d2 = x.getD();
		}
		a3 = Math.min(1.0, a1 + a2);
		b3 = Math.min(1.0, b1 + b2);
		c3 = Math.min(1.0, c1 + c2);
		d3 = Math.min(1.0, d1 + d2);
		if (scale == Scale.LOGARITHMIC) {
			a3 = Math.log(a3) / Math.log(10);
			b3 = Math.log(b3) / Math.log(10);
			c3 = Math.log(c3) / Math.log(10);
			d3 = Math.log(d3) / Math.log(10);
		}
		return new ParamTrapezoid(a3, b3, c3, d3, scale, type, length, prob);
	}

	ParamTrapezoid subtract(Param y) throws Exception {
		if (y.getClass() != getClass())
			throw new Exception ("types are different");
		ParamTrapezoid x = (ParamTrapezoid) y;
		if (type != x.getType())
			throw new Exception ("fuzzy sets types are different");
                if (scale != x.getScale())
			throw new Exception ("scales are different");
		double a1, b1, c1, d1, a2, b2, c2, d2, a3, b3, c3, d3;
		if (scale == Scale.LOGARITHMIC) {
			a1 = Math.pow(10, a);
			b1 = Math.pow(10, b);
			c1 = Math.pow(10, c);
			d1 = Math.pow(10, d);
			a2 = Math.pow(10, x.getA());
			b2 = Math.pow(10, x.getB());
			c2 = Math.pow(10, x.getC());
			d2 = Math.pow(10, x.getD());
		} else {
			a1 = a;
			b1 = b;
			c1 = c;
			d1 = d;
			a2 = x.getA();
			b2 = x.getB();
			c2 = x.getC();
			d2 = x.getD();
		}
		a3 = Math.max(0.0, a1 - d2);
		b3 = Math.max(0.0, b1 - c2);
		c3 = Math.max(0.0, c1 - b2);
		d3 = Math.max(0.0, d1 - a2);
		if (scale == Scale.LOGARITHMIC) {
			a3 = Math.max(-12.0, Math.log(a3) / Math.log(10));
			b3 = Math.max(-12.0, Math.log(b3) / Math.log(10));
			c3 = Math.max(-12.0, Math.log(c3) / Math.log(10));
			d3 = Math.max(-12.0, Math.log(d3) / Math.log(10));
		}
		return new ParamTrapezoid(a3, b3, c3, d3, scale, type, length, prob);
	}

	ParamTrapezoid multiply(Param y) throws Exception {
		if (y.getClass() != getClass())
			throw new Exception ("types are different");
		ParamTrapezoid x = (ParamTrapezoid) y;
		if (type != x.getType())
			throw new Exception ("fuzzy sets types are different");
                if (scale != x.getScale())
			throw new Exception ("scales are different");
		double a1, b1, c1, d1, a2, b2, c2, d2, a3, b3, c3, d3;
		a1 = a;
		b1 = b;
		c1 = c;
		d1 = d;
		a2 = x.getA();
		b2 = x.getB();
		c2 = x.getC();
		d2 = x.getD();
		if (scale == Scale.LOGARITHMIC) {
			a3 = Math.max(-12.0, a1 + a2);
			b3 = Math.max(-12.0, b1 + b2);
			c3 = Math.max(-12.0, c1 + c2);
			d3 = Math.max(-12.0, d1 + d2);
		} else {
			a3 = a1 * a2;
			b3 = b1 * b2;
			c3 = c1 * c2;
			d3 = d1 * d2;
		}
		return new ParamTrapezoid(a3, b3, c3, d3, scale, type, length, prob);
	}

	ParamTrapezoid divide(Param y) throws Exception {
		if (y.getClass() != getClass())
			throw new Exception ("types are different");
		ParamTrapezoid x = (ParamTrapezoid) y;
		if (type != x.getType())
			throw new Exception ("fuzzy sets types are different");
                if (scale != x.getScale())
			throw new Exception ("scales are different");
		double a1, b1, c1, d1, a2, b2, c2, d2, a3, b3, c3, d3;
		a1 = a;
		b1 = b;
		c1 = c;
		d1 = d;
		a2 = x.getA();
		b2 = x.getB();
		c2 = x.getC();
		d2 = x.getD();

		if (scale == Scale.LOGARITHMIC) {
			a3 = Math.max(-12.0, a1 - d2);
			b3 = Math.max(-12.0, b1 - c2);
			c3 = Math.max(-12.0, c1 - b2);
			d3 = Math.max(-12.0, d1 - a2);
		} else {
			a3 = Math.min(1.0, a1 / d2);
			b3 = Math.min(1.0, b1 / c2);
			c3 = Math.min(1.0, c1 / b2);
			d3 = Math.min(1.0, d1 / a2);
		}
		return new ParamTrapezoid(a3, b3, c3, d3, scale, type, length, prob);
	}
        // tutaj przydaloby sie rozrozniac czy to zbior pierwszego czy drugiego rodzaju
        // bo dla drugiego rodzaju mamy przedzial wartosci, a nie wartosc
        // przynajmniej na razie w teorii, nie wiem jak bedziemy musieli to zaimplementowac...
	double getValue(double argument) throws Exception{
		if (type == Type.SECOND)
			throw new Exception ("Wrong fuzzy set type");
                double result = 0;
		if (a != b && b != c && c != d) {
			if (argument >= a && argument < b) {
				result = (argument - a) / (b - a);
			} else if (argument >= b && argument <= c) {
				result = 1;
			} else if (argument > c && argument <= d) {
				result = (d - argument) / (d - c);
			}
		} else if (a == b && b != c && c != d) {
			if (argument >= b && argument <= c) {
				result = 1;
			} else if (argument > c && argument <= d) {
				result = (d - argument) / (d - c);
			}
		} else if (a != b && b == c && c != d) {
			if (argument >= a && argument <= c) {
				result = (argument - a) / (c - a);
			} else if (argument > c && argument <= d) {
				result = (d - argument) / (d - c);
			}
		} else if (a != b && b != c && c == d) {
			if (argument >= a && argument < b) {
				result = (argument - a) / (b - a);
			} else if (argument >= b && argument <= d) {
				result = 1;
			}
		} else if (a == b && b == c && c != d && argument >= c && argument <= d) {
			result = (d - argument) / (d - c);
		} else if (a != b && b == c && c == d && argument >= a && argument <= d) {
			result = (argument - a) / (d - a);
		} else if (a == b && b != c && c == d && argument >= a && argument <= d) {
			result = 1;
		} else if (a == b && b == c && c == d && argument == a) {
			result = 1;
		}
		return result;
	}
        
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		final ParamTrapezoid p = (ParamTrapezoid) obj;
		if(Double.compare(p.getA(), a) != 0 ){
			return false;
		}
		if(Double.compare(p.getB(), b) != 0){
			return false;
		}
		if(Double.compare(p.getC(),c) != 0){
			return false;
		}
		if(Double.compare(p.getD(), d) != 0){
			return false;
		}
		if(p.getScale() != scale || p.getType() != type){
			return false;
		}
		if(Double.compare(p.getLength(), length) != 0){
			return false;
		}
		if (Double.compare(p.getProb(), prob) != 0){
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 47 * hash + (int) (Double.doubleToLongBits(this.a) ^ (Double.doubleToLongBits(this.a) >>> 32));
		hash = 47 * hash + (int) (Double.doubleToLongBits(this.b) ^ (Double.doubleToLongBits(this.b) >>> 32));
		hash = 47 * hash + (int) (Double.doubleToLongBits(this.c) ^ (Double.doubleToLongBits(this.c) >>> 32));
		hash = 47 * hash + (int) (Double.doubleToLongBits(this.d) ^ (Double.doubleToLongBits(this.d) >>> 32));
                hash = 47 * hash + (int) (Double.doubleToLongBits(this.length) ^ (Double.doubleToLongBits(this.length) >>> 32));
                hash = 47 * hash + (int) (Double.doubleToLongBits(this.prob) ^ (Double.doubleToLongBits(this.prob) >>> 32));
		hash = 47 * hash + (this.scale != null ? this.scale.hashCode() : 0);
                hash = 47 * hash + (this.type != null ? this.type.hashCode() : 0);
		return hash;
	}
}
