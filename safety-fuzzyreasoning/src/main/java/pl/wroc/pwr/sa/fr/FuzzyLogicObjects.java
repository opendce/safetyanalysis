package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Maciej
 */
@XmlRootElement(name = "fuzzyLogicObjects")
public class FuzzyLogicObjects {
	private List<TriangleFunction> triangleFunctions;
	private List<TrapezoidFunction> trapezoidFunctions;
	private List<TriangleLingNonSymmetric> triangleLingsNonSymmetric;
	private List<TrapezoidLingNonSymmetric> trapezoidLingsNonSymmetric;
	private List<TriangleLingSymmetric> triangleLingsSymmetric;
	private List<TrapezoidLingSymmetric> trapezoidLingsSymmetric;
	
	FuzzyLogicObjects() {
		triangleFunctions = new ArrayList<TriangleFunction> ();
		trapezoidFunctions = new ArrayList<TrapezoidFunction> ();
		triangleLingsNonSymmetric = new ArrayList<TriangleLingNonSymmetric> ();
		trapezoidLingsNonSymmetric = new ArrayList<TrapezoidLingNonSymmetric> ();
		triangleLingsSymmetric = new ArrayList<TriangleLingSymmetric> ();
		trapezoidLingsSymmetric = new ArrayList<TrapezoidLingSymmetric> ();
	}
	
	TriangleFunction addTriangleFunction (TriangleFunction f) {
		int n = 0;
		if ((n = triangleFunctions.indexOf(f)) >= 0) {
			return triangleFunctions.get(n);
		}
		triangleFunctions.add(f);
		return f;
	}
	
	TrapezoidFunction addTrapezoidFunction (TrapezoidFunction f) {
		int n = 0;
		if ((n = trapezoidFunctions.indexOf(f)) >= 0) {
			return trapezoidFunctions.get(n);
		}
		trapezoidFunctions.add(f);
		return f;
	}
	
	TriangleLingNonSymmetric addTriangleLingNonSymmetric (TriangleLingNonSymmetric l) {
		int n = 0;
		if ((n = triangleLingsNonSymmetric.indexOf(l)) >= 0) {
			return triangleLingsNonSymmetric.get(n);
		}
		triangleLingsNonSymmetric.add(l);
		return l;
	}
	
	TrapezoidLingNonSymmetric addTrapezoidLingNonSymmetric (TrapezoidLingNonSymmetric l) {
		int n = 0;
		if ((n = trapezoidLingsNonSymmetric.indexOf(l)) >= 0) {
			return trapezoidLingsNonSymmetric.get(n);
		}
		trapezoidLingsNonSymmetric.add(l);
		return l;
	}
	
	TriangleLingSymmetric addTriangleLingSymmetric (TriangleLingSymmetric l) {
		int n = 0;
		if ((n = triangleLingsSymmetric.indexOf(l)) >= 0) {
			return triangleLingsSymmetric.get(n);
		}
		triangleLingsSymmetric.add(l);
		return l;
	}
	
	TrapezoidLingSymmetric addTrapezoidLingSymmetric (TrapezoidLingSymmetric l) {
		int n = 0;
		if ((n = trapezoidLingsSymmetric.indexOf(l)) >= 0) {
			return trapezoidLingsSymmetric.get(n);
		}
		trapezoidLingsSymmetric.add(l);
		return l;
	}
	
	List<TriangleFunction> getTriangleFunctions () {
		return triangleFunctions;
	}
	
	void setTriangleFunctions (List<TriangleFunction> triangleFunctions) {
		this.triangleFunctions = triangleFunctions;
	}
	
	List<TrapezoidFunction> getTrapezoidFunctions () {
		return trapezoidFunctions;
	}
	
	void setTrapezoidFunctions (List<TrapezoidFunction> trapezoidFunctions) {
		this.trapezoidFunctions = trapezoidFunctions;
	}
	
	List<TriangleLingNonSymmetric> getTriangleLingsNonSymmetric () {
		return triangleLingsNonSymmetric;
	}
	
	void setTriangleLingsNonSymmetric (List<TriangleLingNonSymmetric> triangleLingsNonSymmetric) {
		this.triangleLingsNonSymmetric = triangleLingsNonSymmetric;
	}
	
	List<TrapezoidLingNonSymmetric> getTrapezoidLingsNonSymmetric () {
		return trapezoidLingsNonSymmetric;
	}
	
	void setTrapezoidLingsNonSymmetric (List<TrapezoidLingNonSymmetric> trapezoidLingsNonSymmetric) {
		this.trapezoidLingsNonSymmetric = trapezoidLingsNonSymmetric;
	}
	
	List<TriangleLingSymmetric> getTriangleLingsSymmetric () {
		return triangleLingsSymmetric;
	}
	
	void setTriangleLingsSymmetric (List<TriangleLingSymmetric> triangleLingsSymmetric) {
		this.triangleLingsSymmetric = triangleLingsSymmetric;
	}
	
	List<TrapezoidLingSymmetric> getTrapezoidLingsSymmetric () {
		return trapezoidLingsSymmetric;
	}
	
	void setTrapezoidLingsSymmetric (List<TrapezoidLingSymmetric> trapezoidLingsSymmetric) {
		this.trapezoidLingsSymmetric = trapezoidLingsSymmetric;
	}
}
