package pl.wroc.pwr.sa.fr;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author N3
 */
@XmlRootElement(name = "trapezoidFunction")
public class TrapezoidFunction extends MembershipFunction {
	private ParamTrapezoid parameters;
        
        public TrapezoidFunction() {
            super();
        }
	
	public TrapezoidFunction(ParamTrapezoid parameters, String name) throws Exception {
                super(name);
		this.parameters = parameters;
	}
	
	public ParamTrapezoid getParameters() {
		return parameters;
	}
	
	public void setParameters(ParamTrapezoid parameters) {
		this.parameters = parameters;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		final TrapezoidFunction f = (TrapezoidFunction) obj;
		if (!f.getName().equals(name) || !f.getParameters().equals(parameters))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 79 * hash + (this.name != null ? this.name.hashCode() : 0);
		return hash;
	}

	TrapezoidFunction add(MembershipFunction x) throws Exception {
		return new TrapezoidFunction (parameters.add(x.getParameters()), "");
	}

	TrapezoidFunction subtract(MembershipFunction x) throws Exception {
		return new TrapezoidFunction (parameters.subtract(x.getParameters()), "");
	}

	TrapezoidFunction divide(MembershipFunction x) throws Exception {
		return new TrapezoidFunction (parameters.divide(x.getParameters()), "");
	}

	TrapezoidFunction multiply(MembershipFunction x) throws Exception {
		return new TrapezoidFunction (parameters.multiply(x.getParameters()), "");
	}

	double getValue(double argument) throws Exception{
		return parameters.getValue(argument);
	}

	@Override
	double getJacardSimilarity(MembershipFunction x) throws Exception {
		return parameters.getJacardSimilarity(x.getParameters());
	}

	@Override
	double getInclusionDegreeMeasure(MembershipFunction x) throws Exception {
		return parameters.getInclusionDegreeMeasure(x.getParameters());
	}
}
