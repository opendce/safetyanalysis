package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author N3
 */
@XmlRootElement(name = "trapezoidLingSymmetric")
public class TrapezoidLingSymmetric implements LingSymmetric {

	private String name;
	private int partition;
	private List<TrapezoidFunction> functions;
        
        public TrapezoidLingSymmetric() {
        }

	public TrapezoidLingSymmetric(String name, double ratio, Scale scale, List<String> names) throws Exception {
		int i;
		String n;
		double param1, param2, param3, param4, r, a;
		this.name = name;
		this.partition = names.size() - 1;
		this.functions = new ArrayList<TrapezoidFunction>();
		if (ratio < 0)
			throw new Exception ("ratio is not positive");
		if (scale == Scale.LINEAR) {
			if (partition > 1) {
				r = 1.0 / partition;
				a = r * ratio / (1 + ratio);
				param1 = 0;
				param2 = 0;
				param3 = a;
				param4 = r - a;
				n = names.get(0);
				ParamTrapezoid param = new ParamTrapezoid(param1, param2, param3, param4, scale);
				this.functions.add(new TrapezoidFunction(param, n));
				for (i = 1; i < partition; i++) {
					n = names.get(i);
					param1 = param3;
					param2 = param4;
					param3 = param2 + 2 * a;
					param4 = param3 + r - 2 * a;
					param = new ParamTrapezoid(param1, param2, param3, param4, scale);
					this.functions.add(new TrapezoidFunction(param, n));
				}
				param1 = param3;
				param2 = param4;
				param3 = 1;
				param4 = 1;
				n = names.get(i);
				param = new ParamTrapezoid(param1, param2, param3, param4, scale);
				this.functions.add(new TrapezoidFunction(param, n));

			} else {
				throw new Exception("wrong number of names");
			}
		} else {
			if (partition > 1) {
				r = 12.0 / partition;
				a = r * ratio / (1 + ratio);
				param1 = -12;
				param2 = -12;
				param3 = -12 + a;
				param4 = -12 + (r - a);
				n = names.get(0);
				ParamTrapezoid param = new ParamTrapezoid(param1, param2, param3, param4, scale);
				this.functions.add(new TrapezoidFunction(param, n));
				for (i = 1; i < partition; i++) {
					n = names.get(i);
					param1 = param3;
					param2 = param4;
					param3 = param2 + 2 * a;
					param4 = param3 + (r - 2 * a);
					param = new ParamTrapezoid(param1, param2, param3, param4, scale);
					this.functions.add(new TrapezoidFunction(param, n));
				}
				param1 = param3;
				param2 = param4;
				param3 = 0;
				param4 = 0;
				n = names.get(i);
				param = new ParamTrapezoid(param1, param2, param3, param4, scale);
				this.functions.add(new TrapezoidFunction(param, n));
			} else {
				throw new Exception("wrong number of names");
			}
		}
	}

	public int getPartition() {
		return partition;
	}

	public void setPartition(int partition) {
		this.partition = partition;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<String> getFunctionsNames() {
		List<String> names = new ArrayList<String>();
		for (TrapezoidFunction function : functions) {
			names.add(function.getName());
		}
		return names;
	}
	
	public List<TrapezoidFunction> getFunctions() {
		return functions;
	}
	
	public void setFunctions(List<TrapezoidFunction> functions) {
		this.functions = functions;
	}

	public TrapezoidFunction getFunction(String name) {
		for (TrapezoidFunction function : functions) {
			if (function.getName().equals(name)) {
				return function;
			}
		}
		return null;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		final TrapezoidLingSymmetric l = (TrapezoidLingSymmetric) obj;
		if (!l.getName().equals(name) || partition != l.getPartition() || !functions.equals(l.functions))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 41 * hash + (this.name != null ? this.name.hashCode() : 0);
		hash = 41 * hash + this.partition;
		return hash;
	}
}
