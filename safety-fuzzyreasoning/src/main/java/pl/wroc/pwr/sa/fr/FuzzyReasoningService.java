package pl.wroc.pwr.sa.fr;

/**
 * Service that uses the base of fuzzy rules (safety-rules) in a fuzzy reasoning process in order to response to a user query.
 */
public interface FuzzyReasoningService {
    
    /** To evaluate a query using a base of fuzzy rules*/
    public String evaluate(String query);
    
}
