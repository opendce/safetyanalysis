package pl.wroc.pwr.sa.fr;

import java.util.List;

/**
 *
 * @author N3
 */
public class FuzzyLogicFacade {
	private final FuzzyLogicObjects flo;
	private final FuzzyOperations fo;
	
	public FuzzyLogicFacade() {
		flo = new FuzzyLogicObjects ();
                fo = new FuzzyOperations ();
	}
        
        /**
	 * Metoda tworząca obiekt zawietający parametry parametry a, b, c
         * oraz skalę i typ funkcji przynależności typu trojkatnego, a takze
         * odleglosc miedzy punktami aa' i c'c - length - oraz ograniczenie 
         * dolne - wartosc na osi prawdopodobienstwa w punkcie b' - prob
         * @param a
         * @param b
         * @param c
         * @param scale skala liniowa lub logarytmiczna
         * @param type domyslnie ustawiony jako FIRST
         * @param length domyslnie ustawiony na 0
         * @param prob domyslnie ustawiony na 0
         * @return
         * @throws Exception 
         */
        public ParamTriangle createParamTriangle(double a, double b, double c, Scale scale) throws Exception {
                Type type = Type.FIRST;
                double length = 0;
                double prob = 0;
                return createParamTriangle(a, b, c, scale, type, length, prob);
        }
        
         /**
	 * Metoda tworząca obiekt zawietający parametry parametry a, b, c
         * oraz skalę i typ funkcji przynależności typu trojkatnego, a takze
         * odleglosc miedzy punktami aa' i c'c - length - oraz ograniczenie 
         * dolne - wartosc na osi prawdopodobienstwa w punkcie b' - prob
         * @param a
         * @param b
         * @param c
         * @param scale skala liniowa lub logarytmiczna
         * @param type zbior pierwszego lub drugiego typu
         * @param length domyslnie ustawiony na 0, jesli zbior pierwszego typu
         * albo domyslnie ustawiony na min((b-a)/2,(c-b)/2) jesli zbior drugiego typu
         * @param prob ustawiomy domyslnie na 0, jesli zbior pierwszego typu
         * albo domyslnie ustawiony na 0.5, jesli zbior drugiego typu
         * @return
         * @throws Exception 
         */
        public ParamTriangle createParamTriangle(double a, double b, double c, Scale scale, Type type) throws Exception {
                double length;
                double prob;
                if (type == Type.FIRST){
                    length = 0;
                    prob = 0;
                }
                else {
                    length = Math.min((b-a)/2,(c-b)/2);
                    prob = 0.5;
                }
                return createParamTriangle(a, b, c, scale, type, length, prob);
        }
        
        /**
	 * Metoda tworząca obiekt zawietający parametry parametry a, b, c
         * oraz skalę i typ funkcji przynależności typu trojkatnego, a takze
         * odleglosc miedzy punktami aa' i c'c - length - oraz ograniczenie 
         * dolne - wartosc na osi prawdopodobienstwa w punkcie b' - prob
         * @param a
         * @param b
         * @param c
         * @param scale skala liniowa lub logarytmiczna
         * @param type zbior pierwszego typu lub drugiego
         * @param length
         * @param prob
         * @return
         * @throws Exception 
         */  
        public ParamTriangle createParamTriangle(double a, double b, double c, Scale scale, Type type, double length, double prob) throws Exception {
                return new ParamTriangle(a, b, c, scale, type, length, prob);
        }
        
        
        /**
	 * Metoda tworząca obiekt zawietający parametry parametry a, b, c, d
         * oraz skalę i typ funkcji przynależności typu trapezoidalnego, a takze
         * odleglosc miedzy punktami aa', bb', c'c, d'd - length - i ograniczenie 
         * dolne - wartosc na osi prawdopodobienstwa w punktach b' i c' - prob
         * @param a
         * @param b
         * @param c
         * @param d
         * @param scale skala liniowa lub logarytmiczna
         * @param type domyslnie ustawiony jako FIRST
         * @param length domyslnie ustawiony na 0
         * @param prob domyslnie ustawiony na 0
         * @return
         * @throws Exception 
         */
        public ParamTrapezoid createParamTrapezoid(double a, double b, double c, double d, Scale scale) throws Exception {
            Type type = Type.FIRST;
            double length = 0;
            double prob = 0;
            return createParamTrapezoid(a, b, c, d, scale, type, length, prob);
        }
        
        /**
	 * Metoda tworząca obiekt zawietający parametry parametry a, b, c, d
         * oraz skalę i typ funkcji przynależności typu trapezoidalnego, a takze
         * odleglosc miedzy punktami aa', bb', c'c, d'd - length - i ograniczenie 
         * dolne - wartosc na osi prawdopodobienstwa w punktach b' i c' - prob
         * @param a
         * @param b
         * @param c
         * @param d
         * @param scale skala liniowa lub logarytmiczna
         * @param type zbior pierwszego lub drugiego typu
         * @param length domyslnie ustawiony na 0, jesli zbior pierwszego typu
         * oraz domyslnie ustawiony na min((b-a)/2,(d-c)/2, ((c-b)/2)-0.01) jesli zbior drugiego typu
         * @param prob ustawiomy domyslnie na 0, jesli zbior pierwszego typu
         * albo domyslnie ustawiony na 0.5, jesli zbior drugiego typu
         * @return
         * @throws Exception 
         */   
        public ParamTrapezoid createParamTrapezoid(double a, double b, double c, double d, Scale scale, Type type) throws Exception { 
            double length;
            double prob;
                if (type == Type.FIRST){
                    length = 0;
                    prob = 0;
                }
                else {
                    length = Math.min((b-a)/2,(d-c)/2);
                    length = Math.min(length,((c-b)/2)-0.01);
                    prob = 0.5;
                }
            return new ParamTrapezoid(a, b, c, d, scale, type, length, prob);
        }
        
        /**
	 * Metoda tworząca obiekt zawietający parametry parametry a, b, c, d
         * oraz skalę i typ funkcji przynależności typu trapezoidalnego, a takze
         * odleglosc miedzy punktami aa', bb', c'c, d'd - length - i ograniczenie 
         * dolne - wartosc na osi prawdopodobienstwa w punktach b' i c' - prob
         * @param a
         * @param b
         * @param c
         * @param d
         * @param scale skala liniowa lub logarytmiczna
         * @param type zbior pierwszego lub drugiego typu
         * @param length
         * @param prob
         * @return
         * @throws Exception 
         */
        public ParamTrapezoid createParamTrapezoid(double a, double b, double c, double d, Scale scale, Type type, double length, double prob) throws Exception { 
            return new ParamTrapezoid(a, b, c, d, scale, type, length, prob);
        }
        
        /**
	 * Metoda tworząca pojedynczą funkcję przynależności typu trójkątnego.
         * @param a
         * @param b
         * @param c
         * @param scale skala liniowa lub logarytmiczna
         * @param type domyslnie ustawiony jako FIRST
         * @param length domyslnie ustawiony na 0
         * @param prob domyslnie ustawiony na 0
         * @param name nazwa funkcji przynależności
         * @return
         * @throws Exception 
         */
	public TriangleFunction createTriangle(double a, double b, double c, Scale scale, String name) throws Exception {
                ParamTriangle parameters = createParamTriangle(a, b, c, scale);
		return flo.addTriangleFunction(new TriangleFunction(parameters, name));			
	}
        
        /**
	 * Metoda tworząca pojedynczą funkcję przynależności typu trójkątnego.
         * @param a
         * @param b
         * @param c
         * @param scale skala liniowa lub logarytmiczna
         * @param type zbior pierwszego lub drugiego typu
         * @param length domyslnie ustawiony na 0, jesli zbior pierwszego typu
         * albo domyslnie ustawiony na min((b-a)/2,(c-b)/2) jesli zbior drugiego typu
         * @param prob ustawiomy domyslnie na 0, jesli zbior pierwszego typu
         * albo domyslnie ustawiony na 0.5, jesli zbior drugiego typu
         * @param name nazwa funkcji przynależności
         * @return
         * @throws Exception 
         */
	public TriangleFunction createTriangle(double a, double b, double c, Scale scale, Type type, String name) throws Exception {
                ParamTriangle parameters = createParamTriangle(a, b, c, scale, type);
		return flo.addTriangleFunction(new TriangleFunction(parameters, name));			
	}
        
         /**
	 * Metoda tworząca pojedynczą funkcję przynależności typu trójkątnego.
         * @param a
         * @param b
         * @param c
         * @param scale skala liniowa lub logarytmiczna
         * @param type zbior pierwszego lub drugiego typu
         * @param length
         * @param prob
         * @param name nazwa funkcji przynależności
         * @return
         * @throws Exception 
         */
	public TriangleFunction createTriangle(double a, double b, double c, Scale scale, Type type, double length, double prob, String name) throws Exception {
                ParamTriangle parameters = createParamTriangle(a, b, c, scale, type, length, prob);
		return flo.addTriangleFunction(new TriangleFunction(parameters, name));			
	}
        
	/**
	 * Metoda tworząca pojedynczą funkcję przynależności typu trójkątnego.
	 * @param parameters parametr w postaci obiektu typu ParamTriangle - tworzy się
	 * go podając parametry a, b, c oraz skalę - liniową lub logarytmiczną np. wywołaniem
	 * new ParamTriangle (0.1, 0.2, 0.3, Scale.LINEAR)
	 * @param name nazwa funkcji przynależności
	 * @throws Exception 
	 */
	public TriangleFunction createTriangle(ParamTriangle parameters, String name) throws Exception {
		return flo.addTriangleFunction(new TriangleFunction(parameters, name));			
	}
	
        /**
	 * Metoda tworząca pojedynczą funkcję przynależności typu trapezoidalnego.
         * @param a
         * @param b
         * @param c
         * @param d
         * @param scale skala liniowa lub logarytmiczna
         * @param type domyslnie ustawiony jako FIRST
         * @param length domyslnie ustawiony na 0
         * @param prob domyslnie ustawiony na 0
         * @param name nazwa funkcji przynaleznosci
         * @return
         * @throws Exception 
         */
        public TrapezoidFunction createTrapezoid(double a, double b, double c, double d, Scale scale, String name) throws Exception {
                ParamTrapezoid parameters = createParamTrapezoid(a, b, c, d, scale);
		return flo.addTrapezoidFunction(new TrapezoidFunction(parameters, name));
	}
        
         /**
	 * Metoda tworząca pojedynczą funkcję przynależności typu trapezoidalnego.
         * @param a
         * @param b
         * @param c
         * @param d
         * @param scale skala liniowa lub logarytmiczna
         * @param type zbior pierwszego lub drugiego typu
         * @param length domyslnie ustawiony na 0, jesli zbior pierwszego typu
         * oraz domyslnie ustawiony na min((b-a)/2,(d-c)/2, ((c-b)/2)-0.01) jesli zbior drugiego typu
         * @param prob ustawiomy domyslnie na 0, jesli zbior pierwszego typu
         * albo domyslnie ustawiony na 0.5, jesli zbior drugiego typu
         * @param name nazwa funkcji przynależności
         * @return
         * @throws Exception 
         */
        public TrapezoidFunction createTrapezoid(double a, double b, double c, double d, Scale scale, Type type, String name) throws Exception {
                ParamTrapezoid parameters = createParamTrapezoid(a, b, c, d, scale, type);
		return flo.addTrapezoidFunction(new TrapezoidFunction(parameters, name));
	}
        
          /**
	 * Metoda tworząca pojedynczą funkcję przynależności typu trapezoidalnego.
         * @param a
         * @param b
         * @param c
         * @param d
         * @param scale skala liniowa lub logarytmiczna
         * @param type zbior pierwszego lub drugiego typu
         * @param length 
         * @param prob
         * @param name nazwa funkcji przynależności
         * @return
         * @throws Exception 
         */
        public TrapezoidFunction createTrapezoid(double a, double b, double c, double d, Scale scale, Type type, double length, double prob, String name) throws Exception {
                ParamTrapezoid parameters = createParamTrapezoid(a, b, c, d, scale, type, length, prob);
		return flo.addTrapezoidFunction(new TrapezoidFunction(parameters, name));
	}
        
	/**
	 * Metoda tworząca pojedynczą funkcję przynależności typu trapezoidalnego.
	 * @param parameters parametr w postaci obiektu typu ParamTrapezoid - tworzy się
	 * go podając parametry a, b, c, d oraz skalę - liniową lub logarytmiczną np. wywołaniem
	 * new ParamTrapezoid (0.1, 0.2, 0.3, 0.4, Scale.LINEAR)
	 * @param name nazwa funkcji przynależności
         * @return
	 * @throws Exception 
	 */
	public TrapezoidFunction createTrapezoid(ParamTrapezoid parameters, String name) throws Exception {
		return flo.addTrapezoidFunction(new TrapezoidFunction(parameters, name));
	}
	
	/**
	 * Metoda tworząca zmienną lingwistyczną typu trójkątnego z symetrycznym podziałem
	 * zbioru argumentów. Przykładowo, dla skali liniowej i nazwach: mało, średnio, dużo, zostanie
	 * utworzona zmienna lingwistyczna zawierająca trzy funkcje przynależności o parametrach:
	 * (0,0,0.5), (0,0.5,1), (0.5,1,1).
	 * @param name nazwa zmiennej lingwistycznej
	 * @param scale skala - liniowa lub logarytmiczna
	 * @param names lista nazw wartości zmiennej lingwistycznej
         * @return
	 * @throws Exception
	 */
        // Funkcje lingwistyczne nie moga byc drugiego typu (nasze zalozenie)
	public TriangleLingSymmetric createTriangleLingSymmetric(String name, Scale scale, List<String> names) throws Exception {
		return flo.addTriangleLingSymmetric(new TriangleLingSymmetric(name, scale, names));
	}
	
	/**
	 * Metoda tworząca zmienną lingwistyczną typu trapezoidalnego z symetrycznym podziałem
	 * zbioru argumentów. Przykładowo, dla skali liniowej nazwach: mało, średnio, dużo oraz
	 * stosunku podstawy górnej do dolnej trapeza równej 0.5, zostanie utworzona zmienna
	 * lingwistyczna zawierająca trzy funkcje przynależności o parametrach:
	 * (0,0,0.17,0.33), (0.17,0.33,0.66,0.83), (0.66,0.83,1,1).
	 * @param name nazwa zmiennej lingwistycznej
	 * @param ratio stosunek podstawy górnej do podstawy dolnej trapeza
	 * @param scale skala - liniowa lub logarytmiczna
	 * @param names lista nazw wartości zmiennej lingwistycznej
         * @return
	 * @throws Exception 
	 */
	public TrapezoidLingSymmetric createTrapezoidLingSymmetric(String name, double ratio, Scale scale, List<String> names) throws Exception {
		return flo.addTrapezoidLingSymmetric(new TrapezoidLingSymmetric(name, ratio, scale, names));
	}
	
	/**
	 * Metoda tworząca zmienną lingwistyczną typu trójkątnego z niesymetrycznym podziałem
	 * zbioru argumentów. Funkcje są tworzone podobnie jak w przypadku pojedynczej funkcji
	 * przynależności - w tym przypadku należy podać listę nazw wraz z listą parametrów.
	 * @param name nazwa zmiennej lingwistycznej
	 * @param names lista nazw wartości zmiennej lingwistycznej
	 * @param parameters lista parametrów wartości zmiennej lingwistycznej
         * @return
	 * @throws Exception
	 */
	public TriangleLingNonSymmetric createTriangleLingNonSymmetric(String name, List<String> names, List<ParamTriangle> parameters) throws Exception {
		return flo.addTriangleLingNonSymmetric(new TriangleLingNonSymmetric(name, names, parameters));
	}
	
	/**
	 * Metoda tworząca zmienną lingwistyczną typu trójkątnego z niesymetrycznym podziałem
	 * zbioru argumentów. Funkcje są tworzone poprzez podanie skali, listy nazw wartości,
	 * nazwy zmiennej lingwistycznej oraz szerokości przedziałów. Liczba podanych szerokości
	 * powinna być o 1 mniejsza od liczby nazw wartości oraz sumować się do 1 (w przypadku skali liniowej)
	 * lub do 12 (w przypadku skali logarytmicznej). Przykładowo, dla skali liniowej oraz 3 wartości, poprawną
	 * listą podziałów jest lista (0.2, 0.8).
	 * @param scale skala - liniowa lub logarytmiczna
	 * @param names lista nazw wartości zmiennej lingwistycznej
	 * @param fractions lista podziałów zbioru argumentów
	 * @param name nazwa zmiennej lingwistycznej
         * @return
	 * @throws Exception 
	 */
	public TriangleLingNonSymmetric createTriangleLingNonSymmetric(Scale scale, List<String> names, List<Double> fractions, String name) throws Exception {
		return flo.addTriangleLingNonSymmetric(new TriangleLingNonSymmetric(scale, names, fractions, name));
	}
	
	/**
	 * Metoda tworząca zmienną lingwistyczną typu trapezoidalnego z niesymetrycznym podziałem
	 * zbioru argumentów. Funkcje są tworzone podobnie jak w przypadku pojedynczej funkcji
	 * przynależności - w tym przypadku należy podać listę nazw wraz z listą parametrów.
	 * @param name nazwa zmiennej lingwistycznej
	 * @param names lista nazw wartości zmiennej lingwistycznej
	 * @param parameters lista parametrów wartości zmiennej lingwistycznej
         * @return
	 * @throws Exception 
	 */
	public TrapezoidLingNonSymmetric createTrapezoidLingNonSymmetric(String name, List<String> names, List<ParamTrapezoid> parameters) throws Exception {
		return flo.addTrapezoidLingNonSymmetric(new TrapezoidLingNonSymmetric(name, names, parameters));
	}
	
	/**
	 * Metoda tworząca zmienną lingwistyczną typu trapezoidalnego z niesymetrycznym podziałem
	 * zbioru argumentów. Funkcje są tworzone poprzez podanie skali, listy nazw wartości,
	 * nazwy zmiennej lingwistycznej, stosunku podstawy górnej do podstawy dolnej trapeza
	 * oraz szerokości przedziałów. Liczba podanych szerokości powinna być o 1 mniejsza od liczby
	 * nazw wartości oraz sumować się do 1 (w przypadku skali liniowej) lub do 12 (w przypadku
	 * skali logarytmicznej). Przykładowo, dla skali liniowej oraz 3 wartości, poprawną
	 * listą podziałów jest lista (0.2, 0.8).
	 * @param name nazwa zmiennej lingwistycznej
	 * @param ratio stosunek podstawy górnej do podstawy dolnej trapeza
	 * @param scale skala - liniowa lub logarytmiczna
	 * @param names lista nazw wartości zmiennej lingwistycznej
	 * @param fractions lista podziałów zbioru argumentów
         * @return
	 * @throws Exception 
	 */
	public TrapezoidLingNonSymmetric createTrapezoidLingNonSymmetric(String name, double ratio, Scale scale, List<String> names, List<Double> fractions) throws Exception {
		return flo.addTrapezoidLingNonSymmetric(new TrapezoidLingNonSymmetric(name, ratio, scale, names, fractions));
	}
	
	public List<TriangleFunction> getTriangleFunctions() {
		return flo.getTriangleFunctions();
	}
	
	public List<TrapezoidFunction> getTrapezoidFunctions() {
		return flo.getTrapezoidFunctions();
	}
	
	public List<TriangleLingNonSymmetric> getTriangleLingsNonSymmetric() {
		return flo.getTriangleLingsNonSymmetric();
	}
	
	public List<TrapezoidLingNonSymmetric> getTrapezoidLingsNonSymmetric() {
		return flo.getTrapezoidLingsNonSymmetric();
	}
	
	public List<TriangleLingSymmetric> getTriangleLingsSymmetric() {
		return flo.getTriangleLingsSymmetric();
	}
	
	public List<TrapezoidLingSymmetric> getTrapezoidLingsSymmetric() {
		return flo.getTrapezoidLingsSymmetric();
	}
	
	/**
	 * Metoda sumująca dwie funkcje przynależności.
	 * @param a pierwsza funkcja przynależności
	 * @param b druga funkcja przynależności
         * @return
	 * @throws Exception 
	 */
	public MembershipFunction add (MembershipFunction a, MembershipFunction b) throws Exception {
		return fo.add(a, b);
	}
	
	/**
	 * Metoda odejmująca dwie funkcje przynależności.
	 * @param a pierwsza funkcja przynależności
	 * @param b druga funkcja przynależności
         * @return
	 * @throws Exception 
	 */
	public MembershipFunction subtract (MembershipFunction a, MembershipFunction b) throws Exception {
		return fo.subtract(a, b);
	}
	
	/**
	 * Metoda mnożąca dwie funkcje przynależności.
	 * @param a pierwsza funkcja przynależności
	 * @param b druga funkcja przynależności
         * @return
	 * @throws Exception 
	 */
	public MembershipFunction multiply (MembershipFunction a, MembershipFunction b) throws Exception {
		return fo.multiply(a, b);
	}
	
	/**
	 * Metoda dzieląca dwie funkcje przynależności.
	 * @param a pierwsza funkcja przynależności
	 * @param b druga funkcja przynależności
         * @return
	 * @throws Exception 
	 */
	public MembershipFunction divide (MembershipFunction a, MembershipFunction b) throws Exception {
		return fo.divide(a, b);
	}
	
	/**
	 * Metoda pobierająca wartość podanej funkcji przynależności.
	 * @param function funkcja przynależności
	 * @param argument argument
         * @return
	 * @throws Exception 
	 */
	public double getValue (MembershipFunction function, double argument) throws Exception {
		return fo.getValue(function, argument);
	}
	
	/**
	 * Metoda obliczająca prawdopodobieństwo Jaccard'a dla podanych funkcji przynależności.
	 * @param a pierwsza funkcja przynależności
	 * @param b druga funkcja przynależności
         * @return
	 * @throws Exception 
	 */
	public double getJacardSimilarity (MembershipFunction a, MembershipFunction b) throws Exception{
		return fo.getJacardSimilarity(a, b);
	}
	
	/**
	 * Metoda obliczająca inclusion degree measure dla podanych funkcji przynależności.
	 * @param a pierwsza funkcja przynależności
	 * @param b druga funkcja przynależności
         * @return
	 * @throws Exception 
	 */
	public double getInclusionDegreeMeasure (MembershipFunction a, MembershipFunction b) throws Exception{
		return fo.getInclusionDegreeMeasure(a, b);
	}
}
