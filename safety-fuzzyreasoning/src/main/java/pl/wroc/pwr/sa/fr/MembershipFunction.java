package pl.wroc.pwr.sa.fr;

/**
 *
 * @author N3
 */
public abstract class MembershipFunction {
    
        protected String name;
    
	abstract MembershipFunction add (MembershipFunction x) throws Exception;
	abstract MembershipFunction subtract (MembershipFunction x) throws Exception;
	abstract MembershipFunction divide (MembershipFunction x) throws Exception;
	abstract MembershipFunction multiply (MembershipFunction x) throws Exception;
	public abstract Param getParameters();
	abstract double getValue (double argument) throws Exception;
	abstract double getJacardSimilarity (MembershipFunction x) throws Exception;
	abstract double getInclusionDegreeMeasure (MembershipFunction x) throws Exception;
        
        public MembershipFunction() {
        }
        
        public MembershipFunction(String name)
        {
            this.name = name;
        }
        
        public String getName() {
		return name;
	}
        
        public void setName(String name) {
		this.name = name;
	}
}
