package pl.wroc.pwr.sa.fr;

/**
 *
 * @author Maciej
 */
abstract class Param {

    final static double dx = 0.001;
    protected Scale scale;
    protected Type type;
    
    public Param() {
    }

    public Param(Scale scale) {
        this.scale = scale;
    }
    
    public Param (Type type){
        this.type = type;
    }
    
    public Param (Scale scale, Type type){
        this.scale = scale;
        this.type = type;
    }

    abstract Param add(Param x) throws Exception;

    abstract Param subtract(Param x) throws Exception;

    abstract Param multiply(Param x) throws Exception;

    abstract Param divide(Param x) throws Exception;

    abstract double getValue(double argument) throws Exception;

    public Scale getScale() {
        return scale;
    }
    
    public Type getType() {
        return type;
    }

    public void setScale(Scale scale) {
        this.scale = scale;
    }
    
    public void setType(Type type) {
        this.type = type;
    }

    public double getJacardSimilarity(Param y) throws Exception {
        if(y.getClass() != getClass() && scale != y.getScale()) {
            throw new Exception("types are different and scales are different");
        }
        if (y.getClass() != getClass()) {
            throw new Exception("types are different");
        }
        
        if (y.getType() != getType()) {
            throw new Exception("fuzzy sets types are different");
        }
        
        if (scale != y.getScale()) {
            throw new Exception("scales are different");
        }
        double p, result = 0;
        double numerator = 0, denominator = 0;
        if (scale == Scale.LINEAR) {
            p = 0;
            while (p < 1) {
                p += dx;
                numerator += getNumerator(y, p);
                denominator += getJackardDenominator(y, p);
            }
        } else {
            p = -12;
            while (p < 0) {
                p += dx;
                numerator += getNumerator(y, p);
                denominator += getJackardDenominator(y, p);
            }
        }
        result = numerator / denominator;
        return Math.min(result, 1);
    }

    public double getInclusionDegreeMeasure(Param y) throws Exception {
        if(y.getClass() != getClass() && scale != y.getScale()) {
            throw new Exception("types are different and scales are different");
        }
        if (y.getClass() != getClass()) {
            throw new Exception("types are different");
        }
        
        if (y.getType() != getType()) {
            throw new Exception("fuzzy sets types are different");
        }
        
        if (scale != y.getScale()) {
            throw new Exception("scales are different");
        }
        double p, result = 0;
        double numerator = 0, denominator = 0;
        if (scale == Scale.LINEAR) {
            p = 0;
            while (p < 0.9) {
                p += dx;
                numerator += getNumerator(y, p);
                denominator += getInclusionDegreeDenominator(y, p);
            }
        } else {
            p = -12;
            while (p < 0.1) {
                p += dx;
                numerator += getNumerator(y, p);
                denominator += getInclusionDegreeDenominator(y, p);
            }
        }
        result = numerator / denominator;
        return Math.min(result, 1);
    }

    private double getNumerator(Param x, double p) throws Exception {
        return dx * (Math.min(getValue(p), x.getValue(p)) + Math.min(getValue(p - dx), x.getValue(p - dx))) / 2;
    }

    private double getJackardDenominator(Param x, double p) throws Exception {
        return dx * (Math.max(getValue(p), x.getValue(p)) + Math.max(getValue(p), x.getValue(p))) / 2;
    }

    private double getInclusionDegreeDenominator (Param x, double p) throws Exception {
        return dx * (getValue(p) + getValue(p - dx)) / 2;
    }

}
