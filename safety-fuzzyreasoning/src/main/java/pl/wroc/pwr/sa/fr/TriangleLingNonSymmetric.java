package pl.wroc.pwr.sa.fr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author N3
 */
@XmlRootElement(name = "triangleLingNonSymmetric")
public class TriangleLingNonSymmetric implements LingNonSymmetric {
	private String name;
	private List<TriangleFunction> functions;
        
        public TriangleLingNonSymmetric() {
        }

	public TriangleLingNonSymmetric(String name, List<String> names, List<ParamTriangle> parameters) throws Exception {
		int i;
		this.name = name;
		this.functions = new ArrayList<TriangleFunction> ();
		if (names.size() != parameters.size())
			throw new Exception ("wrong number of parameters");
                i = 0;
		for(String n : names) {
			this.functions.add(new TriangleFunction(parameters.get(i), n));
			i++;
		}
	}
	
	TriangleLingNonSymmetric(Scale scale, List<String> names, List<Double> fractions, String name) throws Exception {
		this.name = name;
		this.functions = new ArrayList<TriangleFunction> ();
		int i;
		double sum = 0;
		for (double f : fractions) {
			sum += f;
		}
		if (names.size() != (fractions.size() + 1))
			throw new Exception("wrong number of fractions");
		if(scale == Scale.LINEAR && sum != 1 || scale == Scale.LOGARITHMIC && sum != 12) {
			throw new Exception("wrong fractions");		
		}
		double param1, param2, param3;
		String n;
		if(scale == Scale.LINEAR) {
			param1 = 0;
			param2 = 0;
			param3 = fractions.get(0);
			n = names.get(0);
			ParamTriangle param = new ParamTriangle(param1, param2, param3, scale);
			this.functions.add(new TriangleFunction(param, n));
			for(i = 1; i < fractions.size(); i++) {
				n = names.get(i);
				param1 = param2;
				param2 = param3;
				param3 = param2 + fractions.get(i);
				param = new ParamTriangle(param1, param2, param3, scale);
				this.functions.add(new TriangleFunction(param, n));
			}
			param1 = param2;
			param2 = 1;
			param3 = 1;
			n = names.get(i);
			param = new ParamTriangle(param1, param2, param3, scale);
			this.functions.add(new TriangleFunction(param, n));
		}
		else {
			param1 = -12;
			param2 = -12;
			param3 = -12 + fractions.get(0);
			n = names.get(0);
			ParamTriangle param = new ParamTriangle(param1, param2, param3, scale);
			this.functions.add(new TriangleFunction(param, n));
			for(i = 1; i < fractions.size(); i++) {
				n = names.get(i);
				param1 = param2;
				param2 = param3;
				param3 = param2 + fractions.get(i);
				param = new ParamTriangle(param1, param2, param3, scale);
				this.functions.add(new TriangleFunction(param, n));
			}
			param1 = param2;
			param2 = 0;
			param3 = 0;
			n = names.get(i);
			param = new ParamTriangle(param1, param2, param3, scale);
			this.functions.add(new TriangleFunction(param, n));
		}
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<String> getFunctionsNames() {
		List<String> names = new ArrayList<String> ();
		for(TriangleFunction function : functions) {
			names.add(function.getName());
		}
		return names;
	}
	
	public List<TriangleFunction> getFunctions() {
		return functions;
	}
	
	public void setFunctions(List<TriangleFunction> functions) {
		this.functions = functions;
	}
	
	public TriangleFunction getFunction(String name) {
		for(TriangleFunction function : functions) {
			if(function.getName().equals(name)) {
				return function;
			}
		}
		return null;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		final TriangleLingNonSymmetric l = (TriangleLingNonSymmetric) obj;
		if (!l.getName().equals(name) || !functions.equals(l.functions))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 79 * hash + (this.name != null ? this.name.hashCode() : 0);
		return hash;
	}
}
