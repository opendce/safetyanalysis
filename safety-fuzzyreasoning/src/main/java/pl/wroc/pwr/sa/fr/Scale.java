package pl.wroc.pwr.sa.fr;

/**
 *
 * @author N3
 */
public enum Scale {
	LINEAR, LOGARITHMIC;
}
