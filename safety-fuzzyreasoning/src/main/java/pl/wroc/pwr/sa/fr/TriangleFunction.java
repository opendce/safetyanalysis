package pl.wroc.pwr.sa.fr;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author N3
 */
@XmlRootElement(name = "triangleFunction")
public class TriangleFunction extends MembershipFunction {
	private ParamTriangle parameters;
        
        public TriangleFunction() {
            super();
        }
	
	public TriangleFunction(ParamTriangle parameters, String name) throws Exception {
            super(name);
            this.parameters = parameters;
	}
	
	public ParamTriangle getParameters() {
		return parameters;
	}
	
	public void setParameters(ParamTriangle parameters) {
		this.parameters = parameters;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		final TriangleFunction f = (TriangleFunction) obj;
		if (!f.getName().equals(name) || !f.getParameters().equals(parameters))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 73 * hash + (this.name != null ? this.name.hashCode() : 0);
		return hash;
	}

	TriangleFunction add(MembershipFunction x) throws Exception {
		return new TriangleFunction (parameters.add(x.getParameters()), "");
	}

	TriangleFunction subtract(MembershipFunction x) throws Exception {
		return new TriangleFunction (parameters.subtract(x.getParameters()), "");
	}

	TriangleFunction divide(MembershipFunction x) throws Exception {
		return new TriangleFunction (parameters.divide(x.getParameters()), "");
	}

	TriangleFunction multiply(MembershipFunction x) throws Exception {
		return new TriangleFunction (parameters.multiply(x.getParameters()), "");
	}

	double getValue(double argument) throws Exception{
		return parameters.getValue(argument);
	}

	@Override
	double getJacardSimilarity(MembershipFunction x) throws Exception {
		return parameters.getJacardSimilarity(x.getParameters());
	}

	@Override
	double getInclusionDegreeMeasure(MembershipFunction x) throws Exception {
		return parameters.getInclusionDegreeMeasure(x.getParameters());
	}
}
