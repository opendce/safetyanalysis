package pl.wroc.pwr.sa.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Boilerplate code bo pwr
 * 
 * @author Praeterii
 *
 */
@Entity
@Table(name = "trees")
@XmlRootElement(name = "TreeModel")
@XmlAccessorType(XmlAccessType.FIELD)
public class TreeModel implements Serializable{

	private static final long serialVersionUID = -7654981733630432176L;
	
	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String name;

	@Column(name="class")
	@Enumerated(EnumType.STRING)
	private TreeEnum treeEnum;
	
	@Column(columnDefinition="text")
	private String xml;

	public TreeModel(){
	}
	
	public TreeModel(TreeEnum treeEnum, String xml){
		this.setTreeEnum(treeEnum);
		this.xml = xml;
	}

    public TreeModel(TreeEnum treeEnum, String xml, String name){
        this.setTreeEnum(treeEnum);
        this.xml = xml;
        this.name = name;
    }
	
	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return id + " " + getTreeEnum() + " " + xml.toString();
	}

	public TreeEnum getTreeEnum() {
		return treeEnum;
	}

	public void setTreeEnum(TreeEnum treeEnum) {
		this.treeEnum = treeEnum;
	}

    public String getName() {
        return name;
    }
}
