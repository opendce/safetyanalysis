package pl.wroc.pwr.sa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.wroc.pwr.sa.model.TreeModel;


/**
 * Note: you still need to configure magic yourself lol
 * @author Praeterii
 *
 */
@Repository
public interface ITreeRepository extends JpaRepository<TreeModel, Long>{

}