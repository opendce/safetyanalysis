package pl.wroc.pwr.sa.model;

public enum TreeEnum {

	EVENT_TREE,
	FAULT_TREE;
	
	
	@Override
	public String toString() {
		return this.name();
	}
}
